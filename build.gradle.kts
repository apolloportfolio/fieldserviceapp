// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {
    //    ext.kotlin_version = "1.4.10"
    //    ext.kotlin_version = "1.8.10"
    //    ext.kotlin_version = "1.8.21"
    //    ext.kotlin_version = "1.6.21"
//    ext["kotlin_version"] = "1.9.0"
    val kotlin_version = "1.9.0"
    //    ext.compose_version = "1.0.2"
    //    ext.compose_version = "1.2.0-alpha03"
    //    ext.compose_version = "1.3.1"
    //    ext.compose_version = "1.4.7"
//    ext["compose_version"] = "1.5.1"
    val compose_version = "1.5.1"

    repositories {
        google()
        //jcenter() //Migration: https://jeroenmols.com/blog/2021/02/04/migratingjcenter/
        mavenCentral()
        maven { url = uri("https://jitpack.io") }
    }
    dependencies {
        //        def hilt_version = "2.28-alpha"
        val hilt_version = "2.40.1"
        //        def hilt_version = "2.48"
        // last version without @Parcelize bug "non-static type variable T cannot be referenced from a static context"
        //        def hilt_version = "2.35.1" // https://mvnrepository.com/artifact/com.google.dagger/hilt-android-gradle-plugin
        classpath("com.android.tools.build:gradle:8.1.1")
        //classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.4.30")
        //        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.9.10")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.9.0")
        classpath("com.google.dagger:hilt-android-gradle-plugin:$hilt_version")
        classpath("com.google.gms:google-services:4.4.0")

        // https://firebase.google.com/docs/crashlytics/get-started?platform=android
        classpath("com.google.firebase:firebase-crashlytics-gradle:2.9.9")
    }
}

plugins {
    //    id("com.google.devtools.ksp") version "1.8.10-1.0.9" apply false
    id("com.google.devtools.ksp") version "1.9.0-1.0.13" apply false
    id("com.google.dagger.hilt.android") version "2.48" apply false
}

allprojects {
    repositories {
        google()
        //jcenter()
        mavenCentral()
        maven { url = uri("https://jitpack.io") }
    }
}

tasks.register("clean") {
    delete(rootProject.buildDir)
}