<div class="center-title">![Readme Screenshot](./screenshots/Readme.png)</div>

<div class="center-title">

# Field Service App

</div>

<div class="container">

<div class="topleft-column">![Project Screenshot 1](./screenshots/image1.png)</div>

<div class="topcenter-column">

# Project Description

Presenting an Android field service app that streamlines the job assignment process for service technicians, allowing them to receive real-time updates on their work orders and manage their schedules efficiently. With the ability to track work progress, this companion app ensures that technicians can manage their time effectively and provide accurate updates to their managers and customers. Additionally, presented app allows technicians to capture customer feedback on completed work orders, helping to maintain high levels of customer satisfaction. With its intuitive user interface and robust features, this Android field service app is a must-have for any service-based organization looking to improve their operations and customer experience.


</div>

<div class="topright-column">

# My Tech Stack:

*   Kotlin, Java
*   AndroidX
*   Material Design
*   Jetpack Compose
*   Coroutines & Flow
*   Android Architecture Components
*   Dagger Hilt
*   Gradle
*   GitLab
*   JUnit
*   Jupiter
*   Mockito
*   MockK
*   Espresso
*   Web Service Integration (REST, JSON, XML, GSON)
*   Firebase
*   Firestore
*   Room DB
*   REST
*   RESTful APIs
*   Retrofit2
*   GraphQL (Apollo)
*   Glide
*   Glide for Jetpack Compose
*   Coil for Jetpack Compose
*   Google Pay
*   Google Location
*   MVI
*   MVVM
*   MVC
*   Service Locator
*   Clean Architecture
*   Dependency Injection
*   CI/CD
*   Clean Code Principles
*   Agile Software Development Practices
*   Test Driven Development

</div>

</div>

<div class="container">

<div class="left-column">

# Target Customer Persona

The average user of this Android field service app is a service technician or field worker who is responsible for completing tasks on-site, such as repairing equipment or providing installation services. They may work in a variety of industries, such as HVAC, plumbing, electrical, or telecommunications, and are typically mobile, moving between different job sites throughout the day. The user is likely tech-savvy, comfortable using mobile devices and applications to manage their work, and values efficiency and accuracy in completing their tasks. They may also place a high value on providing excellent customer service and leveraging technology to enhance the overall customer experience.


</div>

<div class="right-column">![Project Screenshot 2](./screenshots/image2.png)</div>

</div>

<div class="container">

<div class="left-column">![Project Screenshot 3](./screenshots/image3.png)</div>

<div class="right-column">

# Problem Statement

This field service app has been written to:
	1. Streamline job assignment process: The app allows service technicians to receive job assignments in real-time, eliminating the need for manual paperwork and reducing the likelihood of missed or delayed assignments.
	2. Efficiently manage work schedules: The app enables technicians to manage their schedules more effectively, providing them with a clear view of their upcoming tasks and allowing them to prioritize their workload accordingly.
	3. Enable progress tracking: The app provides a centralized platform for tracking work progress, allowing technicians to update their managers and customers on the status of their work in real-time.
	4. Facilitate accurate reporting: The app ensures that technicians can accurately report their work hours and expenses, reducing the likelihood of errors or discrepancies in invoicing.
	5. Capture customer feedback: The app enables technicians to capture customer feedback on completed work orders, providing valuable insights into customer satisfaction and identifying areas for improvement.
	6. Improve customer service: By providing technicians with the tools they need to manage their work more efficiently and effectively, the app helps to improve overall customer service levels, enhancing customer satisfaction and loyalty.


</div>

</div>

<div class="container">

<div class="right-column">

# Functionalities

	1. Job assignment management: The app enables service technicians to receive and manage job assignments in real-time, providing them with all the information they need to complete their tasks efficiently.
	2. Work progress tracking: The app allows technicians to update their progress on assigned tasks, providing managers and customers with real-time visibility into work status.
	3. Work schedule management: The app provides technicians with a clear view of their upcoming work schedule, allowing them to manage their time effectively and prioritize their workload.
	4. Invoicing and expense reporting: The app enables technicians to accurately report their work hours and expenses, simplifying the invoicing process and reducing the likelihood of errors or discrepancies.
	5. Customer feedback collection: The app enables technicians to collect customer feedback on completed work orders, providing valuable insights into customer satisfaction levels and identifying areas for improvement.
	6. GPS tracking and mapping: The app utilizes GPS tracking to provide technicians with accurate directions to job sites and help managers monitor technician locations in real-time.
	7. Photo and document capture: The app enables technicians to capture and attach photos and documents related to their work orders, providing a comprehensive record of their work.
	8. Real-time communication: The app facilitates real-time communication between technicians, managers, and customers, allowing them to stay connected throughout the job completion process.
	9. Offline mode: The app allows technicians to access important job information even when they are offline, ensuring that they can continue to work in areas with poor network connectivity.


</div>

<div class="left-column">![Project Screenshot 4](./screenshots/image4.png)</div>

</div>

<div class="container">

<div class="right-column">

# Application's Screens

	1. Login Screen: This screen allows users to log in to the app using their credentials, such as their username and password.
	2. Job List Screen: This screen displays a list of job assignments for the logged-in user, including details such as the job description, location, and priority.
	3. Job Details Screen: This screen provides more detailed information about a specific job assignment, including customer information, task details, and any required materials or tools.
	4. Work Order Screen: This screen allows the technician to record and track their work progress, including time spent on the task, materials used, and any notes or comments.
	5. Customer Feedback Screen: This screen enables the technician to capture customer feedback on the completed work order, including a rating system and open-ended comments.
	6. Schedule Screen: This screen displays the technician's upcoming work schedule, including details such as the time, location, and job description.
	7. Settings Screen: This screen allows users to configure app settings, such as language preferences and notification settings.
	8. Notifications Screen: This screen displays notifications related to new job assignments, schedule changes, or other important updates.
	9. Profile Screen: This screen displays the user's profile information, including their name, contact information, and job title.
	10. Map Screen: This screen displays a map view of the technician's current location and job sites, providing real-time tracking and navigation capabilities.


</div>

</div>

<div class="container">

<div class="right-column">

# Disclaimer

In order to provide their clients with protection of trade secrets, protection of user data, legal protection and competitive advantage all commissions of projects of this Developer are by default accompanied by an appropriate Non Disclosure Agreement (NDA). For the same reason all presented: project description, problem statement, functionalities and screen descriptions are for presentation purposes only and may or may not represent an entirety of project.

Due to NDA and obvious copyright and security issues, this GitHub project does not contain entire code of described application.

All included code snippets are for presentation purposes only and should not be used for commercial purposes as they are intellectual property of this application's Developer. In compliance with an appropriate NDA, all presented screenshots of working application have been made after removing any and all graphics that may be considered third party's intellectual property. As such they do not represent a final end product.

The developer does not claim any ownership or affiliation with any of the third-party libraries, technologies, or services used in the project.

While every effort has been made to ensure the accuracy, completeness, and reliability of the presented code, the developer cannot be held responsible for any errors, omissions, or damages that may arise from it's use.

In compliance with an appropriate NDA and in order to provide their clients with all presented: description, target customer persona, problem statement, functionalities, screen descriptions and other items are for presentation purposes only and do not consist an entirety of commisioned project.

Protection of trade secrets, protection of user data, legal protection and protection of competitive advantage of a Client is of utmost importance to the Developer.

</div>

</div>

<div class="container">

<div class="right-column">

# License

Any use of this code is prohibited except of for recruitment purposes only of the Developer who wrote it.

</div>

</div>