package com.aps.catemplateapp.feature02.framework.presentation.activity01.end_to_end

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.MediumTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import com.aps.catemplateapp.BaseInstrumentedTest
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.feature02.business.domain.model.entities.Entity2
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.core.di.NetworkDataSourcesModule
import com.aps.catemplateapp.core.di.ProductionModule
import com.aps.catemplateapp.core.di.RoomModule
import com.aps.catemplateapp.core.util.*
import com.aps.catemplateapp.core.util.extensions.*
import com.aps.catemplateapp.feature02.framework.presentation.activity01.adapters.AdapterEntity2ToListItemType5
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

private const val TAG = "ViewListOfEntities2"
private const val LOG_ME = true
/*
    Test case scenarios:
    1. User views a list of entities2. The list is empty. User has incomplete profile.
    2. User views a list of entities2 The list is empty. User has complete profile.
    3. User views a list of entities2 User clicks on second entity2 to view it.
 */

@MediumTest
@ExperimentalCoroutinesApi
@FlowPreview
@RunWith(AndroidJUnit4ClassRunner::class)
@HiltAndroidTest
@UninstallModules(
    RoomModule::class,
    NetworkDataSourcesModule::class,
    ProductionModule::class,
)
class ViewListOfEntities2: BaseInstrumentedTest() {

    init {

    }

    @Before
    override fun runBeforeEveryTest() {
        hiltRule.inject()
        injectTest()
        context = InstrumentationRegistry.getInstrumentation().targetContext
    }

    @After
    override fun runAfterEveryTest() {
        enableInternetConnection()
    }

    override fun injectTest() {
//        (application.appComponent as TestAppComponent).inject(this)
    }

    private fun navigateToFragmentInTest(methodName : String) {
        // Wait for ActivityHomeScreenFragment1 to appear
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Wait for ActivityHomeScreenFragment1 to appear")
        waitViewShown(withId(R.id.search_query_edittext))

        // Confirm ActivityHomeScreenFragment1 is in view
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Confirm ActivityHomeScreenFragment1 is in view")
        val searchQueryEditText = Espresso.onView(withId(R.id.search_query_edittext))
        searchQueryEditText.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        // Navigate to ActivityHomeScreenFragment2 by clicking bottom nav menu
        clickOnBottomNavigationMenu(
            "Navigate to ActivityHomeScreenFragment2 by clicking bottom nav menu",
            TAG,
            LOG_ME,
            methodName,
            R.id.ActivityHomeScreenFragment2
        )

        // Wait for ActivityHomeScreenFragment2 to appear
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Wait for ActivityHomeScreenFragment2 to appear")
        waitViewShown(withId(R.id.fragment_home_screen_fragment2))

        // Confirm that ActivityHomeScreenFragment2 is in view
        confirmThatViewWithIdIsVisible(
            "Confirm that ActivityHomeScreenFragment2 is in view",
            TAG,
            LOG_ME,
            methodName,
            R.id.fragment_home_screen_fragment2
        )
    }

    @Test
    fun test1() = runBlocking {
        val methodName = "test1"
        val testDescription =
            "1. User views a list of entities2. The list is empty. User has incomplete profile."
        ALog.d(TAG, ".$methodName(): Method start: $methodName \n $testDescription")

        // Prepare data set
        prepareDataSetWithUserWhoHasIncompleteProfile()

        // Launch scenario
        startTestActivityWithUserWhoHasIncompleteProfile()
        navigateToFragmentInTest(methodName)

        // Confirm that the recyclerView is gone
        confirmThatViewWithIdIsNotVisible(
            "Confirm that the recyclerView is gone",
            TAG,
            LOG_ME,
            methodName,
            R.id.recycler_view
        )

        // Confirm that the list is empty
        confirmRecyclerViewSize(
            "Confirm that the list is empty",
            TAG,
            LOG_ME,
            methodName,
            R.id.recycler_view,
            0
        )

        // Confirm that the tip for empty list is visible
        confirmThatViewWithIdIsVisible(
            "Confirm that the tip for empty list is visible",
            TAG,
            LOG_ME,
            methodName,
            R.id.tip_when_list_is_empty,
        )

        // Confirm that the warning about incomplete profile is visible
        confirmThatViewWithIdIsVisible(
            "Confirm that the warning about incomplete profile is visible",
            TAG,
            LOG_ME,
            methodName,
            R.id.profile_completion_state_lbl,
        )

        // Confirm that the explanation of incomplete profile is visible
        confirmThatViewWithIdIsVisible(
            "Confirm that the explanation of incomplete profile is visible",
            TAG,
            LOG_ME,
            methodName,
            R.id.disabled_functionality_explanation_lbl,
        )

        ALog.d(TAG, ".$methodName(): Method end: $methodName")
    }

    @Test
    fun test2() = runBlocking {
        val methodName = "test2"
        val testDescription =
            "2. User views a list of entities2 The list is empty. User has complete profile."
        ALog.d(TAG, ".$methodName(): Method start: $methodName \n $testDescription")

        // Prepare data set
        prepareDataSetWithUserWhoHasCompleteProfile()
        entity2Dao.deleteAllEntities()
        entity2NetworkDataSource.deleteAllEntities()

        // Launch scenario
        startTestActivityWithUserWhoHasCompleteProfile()
        navigateToFragmentInTest(methodName)

        // Confirm that the recyclerView is visible
        confirmThatViewWithIdIsVisible(
            "Confirm that the recyclerView is visible",
            TAG,
            LOG_ME,
            methodName,
            R.id.recycler_view
        )

        // Confirm that the list is empty
        confirmRecyclerViewSize(
            "Confirm that the list is empty",
            TAG,
            LOG_ME,
            methodName,
            R.id.recycler_view,
            0
        )

        // Confirm that the tip for empty list is visible
        confirmThatViewWithIdIsVisible(
            "Confirm that the tip for empty list is visible",
            TAG,
            LOG_ME,
            methodName,
            R.id.tip_when_list_is_empty,
        )

        // Confirm that the warning about incomplete profile is gone
        confirmThatViewWithIdIsNotVisible(
            "Confirm that the warning about incomplete profile is gone",
            TAG,
            LOG_ME,
            methodName,
            R.id.profile_completion_state_lbl,
        )

        // Confirm that the explanation of incomplete profile is gone
        confirmThatViewWithIdIsNotVisible(
            "Confirm that the explanation of incomplete profile is gone",
            TAG,
            LOG_ME,
            methodName,
            R.id.disabled_functionality_explanation_lbl,
        )

        ALog.d(TAG, ".$methodName(): Method end: $methodName")
    }

    @Test
    fun test3() = runBlocking {
        val methodName = "test3"
        val testDescription =
            "3. User views a list of entities2. User clicks on second entity2 to view it."
        ALog.d(TAG, ".$methodName(): Method start: $methodName \n $testDescription")

        // Prepare data set
        prepareDataSetWithUserWhoHasCompleteProfile()
        val testEntities2 = generateListOfEntities2(user!!)
        var expectedNumberOfItemsInRecyclerView = 0
        for ((index, entity) in testEntities2.withIndex()) {
            if (LOG_ME) ALog.d(
                TAG,
                ".$methodName(): Inserting test entities $index:\n$entity"
            )
            entity2NetworkDataSource.insertOrUpdateEntity(entity)

            expectedNumberOfItemsInRecyclerView++
        }

        // Launch scenario
        startTestActivityWithUserWhoHasCompleteProfile()
        navigateToFragmentInTest(methodName)

        val chosenItemPosition = 2


//        if(LOG_ME)ALog.d(TAG, ".test3(): GotHere")
//        Thread.sleep(90000000000000)


        // Wait until data has loaded into recycler view
        Espresso::class.waitForDataToLoadIntoRecyclerView(
            "Wait until data has loaded into recycler view",
            TAG,
            LOG_ME,
            methodName,
            R.id.recycler_view
        )

        // Confirm recyclerView has expected number of items.
        Espresso::class.confirmRecyclerViewHasExpectedNumberOfItems(
            "Confirm recyclerView has expected number of items.",
            TAG,
            LOG_ME,
            methodName,
            R.id.recycler_view,
            expectedNumberOfItemsInRecyclerView,
        )

        // Click on entity number three in the list to view it.
        if (LOG_ME) ALog.d(
            TAG,
            ".$methodName(): Click on entity number three in the list to view it."
        )
        Espresso.onView(withId(R.id.recycler_view))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<AdapterEntity2ToListItemType5.ListItemType5ViewHolder>(
                    chosenItemPosition,
                    ViewActions.click()
                )
            )

        // Wait for entity's activity to come into view.
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Wait for entity's activity to come into view.")
        waitViewShown(withId(R.id.title_lbl))

        // Check that entity's activity is in view.
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Check that entity's activity is in view.")
        val viewFromEntitysActivity = Espresso.onView(withId(R.id.title_lbl))
        viewFromEntitysActivity.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        ALog.d(TAG, ".$methodName(): Method end: $methodName")
    }

    private fun generateListOfEntities2(
        owner: ProjectUser,
    ): ArrayList<Entity2> {
        val methodName: String = "generateIncomingTestRentalOffers"
        ALog.d(TAG, "Method start: $methodName")
        val result = ArrayList<Entity2>()
        try {
            val entityA = entity2Factory.generateEmpty()
            entityA.ownerID = owner.id
            entityA.description = "EntityA"

            val entityB = entity2Factory.generateEmpty()
            entityB.ownerID = owner.id
            entityB.description = "EntityB"

            val entityC = entity2Factory.generateEmpty()
            entityC.ownerID = owner.id
            entityC.description = "EntityC"

            val entityD = entity2Factory.generateEmpty()
            entityD.ownerID = owner.id
            entityD.description = "EntityD"

            result.add(entityA)
            result.add(entityB)
            result.add(entityC)
            result.add(entityD)

        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
        return result
    }
}