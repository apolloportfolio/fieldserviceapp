package com.aps.catemplateapp.feature02.framework.presentation.activity01.end_to_end

import android.content.Intent
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.BundleMatchers
import androidx.test.espresso.intent.matcher.IntentMatchers.*
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.MediumTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.aps.catemplateapp.BaseInstrumentedTest
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.core.di.NetworkDataSourcesModule
import com.aps.catemplateapp.core.di.ProductionModule
import com.aps.catemplateapp.core.di.RoomModule
import com.aps.catemplateapp.core.util.*
import com.aps.catemplateapp.core.util.extensions.*
import com.aps.catemplateapp.feature02.framework.presentation.activity01.ActivityHomeScreen
import com.aps.catemplateapp.feature02.framework.presentation.activity01.ActivityHomeScreenNavigation
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.allOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith




private const val TAG = "ManagingUsersProfile"
private const val LOG_ME = true
/*
    Test case scenarios:
    1. User checks if their info is correct and changes their description. After clicking it a save and cancel buttons should appear.
    2. User decides to verify their email.
    6. User decides to fill their basic info.
    7. User decides to check their ratings.
    8. User decides to recommend the app.
    9. User decides to view terms and conditions.
    10. User decides to report a problem.
    11. User decides to logout.
    12. User decides to change their profile picture by clicking it.
    14. User decides to view the Help page and see guide 1.
    15. User decides to use an invitation code.
    16. User decides to view the Help page and see guide 2.
    17. User decides to view the Help page and email customer support.
    18. User decides to view the Help page and call customer support.
 */

@MediumTest
@ExperimentalCoroutinesApi
@FlowPreview
@RunWith(AndroidJUnit4ClassRunner::class)
@HiltAndroidTest
@UninstallModules(
    RoomModule::class,
    NetworkDataSourcesModule::class,
    ProductionModule::class,
)
class ManagingUsersProfile: BaseInstrumentedTest() {

    @get:Rule(order = 10)
    val intentsTestRule = IntentsTestRule(ActivityHomeScreen::class.java)

    init {

    }

    override fun injectTest() {
//        (application.appComponent as TestAppComponent).inject(this)
    }

    override fun runBeforeEveryTest() {
        super.runBeforeEveryTest()
    }


    private fun navigateToFragmentInTest(methodName : String) {
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Navigating to fragment in test.")
        // Wait for ActivityHomeScreenFragment1 to appear
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Wait for ActivityHomeScreenFragment1 to appear")
        waitViewShown(withId(R.id.search_query_edittext))

        // Confirm ActivityHomeScreenFragment1 is in view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Confirm ActivityHomeScreenFragment1 is in view")
        val searchQueryEditText = Espresso.onView(withId(R.id.search_query_edittext))
        searchQueryEditText.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))


        // Navigate to ActivityHomeScreenFragment5 by clicking bottom nav menu
        clickOnBottomNavigationMenu(
            "Navigate to ActivityHomeScreenFragment5 by clicking bottom nav menu",
            TAG,
            LOG_ME,
            methodName,
            R.id.ActivityHomeScreenFragment5
        )

        // Wait for ActivityHomeScreenFragment5 to appear
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Wait for ActivityHomeScreenFragment5 to appear")
        waitViewShown(withId(R.id.users_name_lbl))

        // Confirm that ActivityHomeScreenFragment5 is in view
        confirmThatViewWithIdIsVisible(
            "Confirm that ActivityHomeScreenFragment5 is in view",
            TAG,
            LOG_ME,
            methodName,
            R.id.users_name_lbl
        )
    }

    @Test
    fun test1() = runBlocking {
        val methodName = "test1"
        val testDescription = "1. User checks if their info is correct and changes their description. After clicking it a save and cancel buttons should appear. "
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName \n $testDescription")

        // Prepare data set
        prepareDataSetWithUserWhoHasIncompleteProfile()

        // Launch scenario
        startTestActivityWithUserWhoHasIncompleteProfile()
        navigateToFragmentInTest(methodName)

        // Check if user's name is correct
        checkIfViewWithStringIsInView(
            "Check if user's name is correct",
            TAG,
            LOG_ME,
            methodName,
            user!!.name!!,
            R.id.users_name_lbl
        )

        // Check if user's city is correct
        checkIfViewWithStringIsInView(
            "Check if user's city is correct",
            TAG,
            LOG_ME,
            methodName,
            user!!.city!!,
            R.id.users_city_lbl
        )

        // Check if user's description is correct
        checkIfViewWithStringIsInView(
            "Check if user's city is correct",
            TAG,
            LOG_ME,
            methodName,
            user!!.description!!,
            R.id.users_description_edittext
        )
//        Thread.sleep(100000)

        // Click on user's description
        clickOnView(
            "Click on user's description",
            TAG,
            LOG_ME,
            methodName,
            R.id.users_description_edittext
        )

        // Check if save button has appeared
        confirmThatViewWithIdIsVisible(
            "Check if cancel and save buttons have appeared",
            TAG,
            LOG_ME,
            methodName,
            R.id.save_button
        )

        // Check if cancel button has appeared
        confirmThatViewWithIdIsVisible(
            "Check if cancel and save buttons have appeared",
            TAG,
            LOG_ME,
            methodName,
            R.id.cancel_button
        )

        // Change user's description
        typeTextInEditText(
            "Change user's description",
            TAG,
            LOG_ME,
            methodName,
            R.id.users_description_edittext,
            TEST_USER_EDITED_DESCRIPTION
        )

        // Click save button
        clickOnView(
            "Click save button",
            TAG,
            LOG_ME,
            methodName,
            R.id.save_button
        )

        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")
    }

    @Test
    fun test10() = runBlocking {
        val methodName = "test10"
        val testDescription = "10. User decides to report a problem."
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName \n $testDescription")

        // Prepare data set
        prepareDataSetWithUserWhoHasIncompleteProfile()

        // Launch scenario
        startTestActivityWithUserWhoHasIncompleteProfile()
        navigateToFragmentInTest(methodName)

        // Click on report a problem button
        Espresso::class.clickOnViewWithText(
            "Click on report a problem button",
            TAG,
            LOG_ME,
            methodName,
            context.getString(R.string.report_problem),
        )


        // Confirm that an email client has appeared with customer support email
        // https://developer.android.com/training/testing/espresso/intents
        // https://stackoverflow.com/questions/63546419/espresso-intents-matchers-and-stubbing
        // https://medium.com/@gangu.balu/espresso-intent-verification-basics-292eb3df792a
        intended(allOf(
            hasAction(Intent.ACTION_SENDTO),
            hasExtras(BundleMatchers.hasValue(
                ActivityHomeScreenNavigation.getEmailSubjectForErrorReporting(context)
            )),
        ));


        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")
    }

    @Test
    fun test11() = runBlocking {
        val methodName = "test11"
        val testDescription = "11. User decides to logout."
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName \n $testDescription")

        // Prepare data set
        prepareDataSetWithUserWhoHasIncompleteProfile()

        // Launch scenario
        startTestActivityWithUserWhoHasIncompleteProfile()
        navigateToFragmentInTest(methodName)

        // Click on logout button
        Espresso::class.clickOnViewWithText(
            "Click on logout button",
            TAG,
            LOG_ME,
            methodName,
            context.getString(R.string.logout),
        )

        // Click on Yes button in logout dialog
        Espresso::class.clickOnViewWithText(
            "Click on Yes button in logout dialog",
            TAG,
            LOG_ME,
            methodName,
            context.getString(R.string.text_yes),
        )

        // ActivityStartMainFragment
        // Wait for ActivityStartMainFragment to come into view
        ALog.d("LoginUserFeatureTest", "$methodName: Wait for ActivityStartMainFragment to come into view")
        waitViewShown(withId(R.id.logo_slogan))
        val logoSlogan = Espresso.onView(withId(R.id.logo_slogan))

        // confirm ActivityStartMainFragment is in view
        ALog.d("LoginUserFeatureTest", "$methodName: confirm ActivityStartMainFragment is in view")
        logoSlogan.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))


        // ActivityStartOptionsFragment
        // Wait for ActivityStartOptionsFragment to come into view
        ALog.d("LoginUserFeatureTest", "$methodName: Wait for ActivityStartOptionsFragment to come into view")
        waitViewShown(withId(R.id.button_goto_login))

        // confirm ActivityStartOptionsFragment is in view
        ALog.d("LoginUserFeatureTest", "$methodName: confirm ActivityStartOptionsFragment is in view")
        val loginOptionButton = Espresso.onView(withId(R.id.button_goto_login))
        loginOptionButton.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))


        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")
    }
}