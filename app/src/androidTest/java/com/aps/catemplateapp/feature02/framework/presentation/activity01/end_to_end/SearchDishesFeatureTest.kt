package com.aps.catemplateapp.feature02.framework.presentation.activity01.end_to_end

import android.content.Context
import android.content.Intent
import android.os.Parcelable
import android.util.Log
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingPolicies
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.scrollTo
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.MediumTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.aps.catemplateapp.BaseInstrumentedTest
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.framework.presentation.IntentExtras
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.core.di.NetworkDataSourcesModule
import com.aps.catemplateapp.core.di.ProductionModule
import com.aps.catemplateapp.core.di.RoomModule
import com.aps.catemplateapp.core.util.*
import com.aps.catemplateapp.core.util.extensions.*
import com.aps.catemplateapp.feature02.framework.presentation.activity01.ActivityHomeScreen
import com.aps.catemplateapp.feature02.framework.presentation.activity01.adapters.AdapterEntity1ToListItemType3
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.tasks.await
import org.hamcrest.CoreMatchers.not
import org.hamcrest.core.StringContains.containsString
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.Thread.sleep
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.test.assertFalse


private const val TAG = "SearchEntities1FeatureTest"
private const val LOG_ME = true
/*
    Test case scenarios:
    1. User decided to complete their profile by clicking on profileStatusCompleteProfileButton.
    2. User searches for entities without setting any filters or search query (search around user).
    3. User searches for entities without setting any filters but inputs search query (city).
    4. User searches for entities after setting filters without search query (search around user).
    5. User searches for entities after setting filters with search query (city).
    6. User sets filters and then clears them with removeFiltersButton.
    7. User types search query but decides to delete it with searchBackButton.
    8. User has completed their profile, check if profile completion button is invisible.
    9. User decided to view the first entity in the list. User has incomplete profile.
    10. User decided to view the first entity in the list. User has complete profile.
 */

@MediumTest
@ExperimentalCoroutinesApi
@FlowPreview
@RunWith(AndroidJUnit4ClassRunner::class)
@HiltAndroidTest
@UninstallModules(
    RoomModule::class,
    NetworkDataSourcesModule::class,
    ProductionModule::class,
)
class SearchDishesFeatureTest: BaseInstrumentedTest() {


    @get:Rule(order = 0)
    override var hiltRule = HiltAndroidRule(this)

    @get: Rule(order = 1)
    override val espressoIdlingResourceRule = EspressoIdlingResourceRule()

    @get:Rule       // Necessary for launching activity with extras
    override var mActivityRule = ActivityTestRule(
        ActivityHomeScreen::class.java, false, false
    )

    override lateinit var context: Context


    override val firestore = FirebaseFirestore.getInstance()

    init {

    }

    @Before
    override fun runBeforeEveryTest() {
        IdlingPolicies.setMasterPolicyTimeout(2, TimeUnit.MINUTES);
        IdlingPolicies.setIdlingResourceTimeout(2, TimeUnit.MINUTES);
        hiltRule.inject()
        injectTest()
        context = InstrumentationRegistry.getInstrumentation().targetContext
        grantLocationPermission()
    }

    @After
    override fun runAfterEveryTest() {
        enableInternetConnection()
    }

    override fun startTestActivityWithUserWhoHasIncompleteProfile() : ActivityHomeScreen {
//        val scenario = launchActivity<ActivityHomeScreen>()       // Launches activity without extras
        val user : ProjectUser = userFactory.generateTestUserWithIncompleteProfile()
        val intent = Intent()
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra(IntentExtras.APP_USER, user as Parcelable)
        return mActivityRule.launchActivity(intent)
    }

    override fun startTestActivityWithUserWhoHasCompleteProfile() : ActivityHomeScreen {
//        val scenario = launchActivity<ActivityHomeScreen>()       // Launches activity without extras
        val user : ProjectUser = userFactory.generateTestUserWithCompleteProfile()
        val intent = Intent()
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra(IntentExtras.APP_USER, user as Parcelable)
        return mActivityRule.launchActivity(intent)
    }

    override fun injectTest() {
//        (application.appComponent as TestAppComponent).inject(this)
    }

    @Test
    fun test0() = runBlocking{
        val methodName = "test0"
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName")
//        ALog.d(TAG, "Deleting all entities in userDao")
//        userDao.deleteAllEntities()
//        ALog.d(TAG, "Deleting all entities in userNetworkDataSource")
//        userNetworkDataSource.deleteAllEntities()
//        ALog.d(TAG, "Creating new user entity")
//        val user : User = userFactory.generateTestUserWithIncompleteProfile()
//        ALog.d(TAG, "Deleting old user account")
//        userNetworkDataSource.deleteAccount(user)
        ALog.d(TAG, "Inserting new user entity to userNetworkDataSource")
//        userNetworkDataSource.registerNewUser(user)

        ALog.d(TAG, "Logging user in.")
//        userNetworkDataSource.loginUser(user)

        // Checking if Firestore allows creation of new documents
        ALog.d(TAG, "Checking Firestore availability.")
        testFirestoreAvailability()

        ALog.d(TAG, "Firestore is available for usage.")
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")
    }

    private suspend fun testFirestoreAvailability() {
        val methodName = "testFirestoreAvailability"
        Log.d(TAG, "Method start: $methodName")
        try {
            val testEntity = hashMapOf(
                "test" to "Yay!"
            )

            if(FirebaseAuth.getInstance().currentUser == null) {
                Log.d(TAG, "$methodName(): User is not logged in")
            } else {
                Log.d(TAG, "$methodName(): User is logged in")
            }

            firestore.collection("test")
                .add(testEntity)
                .addOnSuccessListener { documentReference ->
                    Log.d(TAG, "$methodName(): DocumentSnapshot written with ID: ${documentReference.id}")
                }
                .addOnFailureListener { e ->
                    Log.w(TAG, "$methodName(): Error adding document: $e")
                }
                .await()
            Log.d(TAG, "Method end: $methodName")
        } catch (e: java.lang.Exception) {
            Log.e(TAG, methodName, e)
            return
        } finally {
            Log.d(TAG, "Method end: $methodName")
        }
    }

    //    1. User decided to complete their profile by clicking on profileStatusCompleteProfileButton.
    @Test
    fun test1() = runBlocking{
        val methodName = "test1"
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName")

        // Prepare data set
        prepareDataSetWithUserWhoHasIncompleteProfile()
//        prepareDataSetWithUserWhoHasCompleteProfile()

        // Launch scenario
        val scenario = launchActivity<ActivityHomeScreen>()

        // Wait for ActivityHomeScreenFragment1 to appear
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Wait for ActivityHomeScreenFragment1 to appear")
        waitViewShown(withId(R.id.search_query_edittext))

        // Confirm ActivityHomeScreenFragment1 is in view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Confirm ActivityHomeScreenFragment1 is in view")
        val searchQueryEditText = Espresso.onView(withId(R.id.search_query_edittext))
        searchQueryEditText.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        // Confirm profile completion button is in view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Confirm profile completion button is in view")
        val profileCompletionButton = Espresso.onView(withId(R.id.profile_status_complete_profile_button))
        profileCompletionButton.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        // Click on profile completion button
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Click on profile completion button")
        profileCompletionButton.perform(ViewActions.click())
        Thread.sleep(4000)

        // Wait for ActivityHomeScreenFragment5 to come into view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Wait for ActivityHomeScreenFragment5 to come into view")
        waitViewShown(withId(R.id.users_name_lbl))

        // Confirm ActivityHomeScreenFragment5 is in view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Confirm ActivityHomeScreenFragment5 is in view")
        val usersNameLbl = Espresso.onView(withId(R.id.users_name_lbl))
        usersNameLbl.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        // Wait for ActivityHomeScreenFragment5 to come into view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Wait for ActivityHomeScreenFragment5 to come into view")
        waitViewShown(withId(R.id.users_name_lbl))
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")
    }


    //    2. User searches for entities without setting any filters or search query (search around user).
    @Test
    fun test2() {
        val methodName = "test2"
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName")

        // Prepare data set
        prepareDataSetWithUserWhoHasIncompleteProfile()

        // Launch scenario
        startTestActivityWithUserWhoHasIncompleteProfile()

        // Wait for ActivityHomeScreenFragment1 to appear
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Wait for ActivityHomeScreenFragment1 to appear")
        waitViewShown(withId(R.id.search_query_edittext))

        // Confirm ActivityHomeScreenFragment1 is in view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Confirm ActivityHomeScreenFragment1 is in view")
        val searchQueryEditText = Espresso.onView(withId(R.id.search_query_edittext))
        searchQueryEditText.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        // Wait until data has loaded into recycler view
        Espresso::class.waitForDataToLoadIntoRecyclerView(
            "Wait until data has loaded into recycler view",
            TAG,
            LOG_ME,
            methodName,
            R.id.recycler_view
        )

        // Click on search button
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Click on search button")
        val searchButton = Espresso.onView(withId(R.id.search_button))
        searchButton.perform(ViewActions.click())

        // Wait until data has loaded into recycler view
        Espresso::class.waitForDataToLoadIntoRecyclerView(
            "Wait until data has loaded into recycler view",
            TAG,
            LOG_ME,
            methodName,
            R.id.recycler_view
        )

        // Click on 4th item in search results
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Click on 4th item in search results")
        val LIST_ITEM_IN_TEST = 3
        AdapterEntity1ToListItemType3.ListItemType3ViewHolder::class.clickOnNthItemInRecyclerView(
            "Click on 4th item in search results",
            TAG,
            LOG_ME,
            methodName,
            LIST_ITEM_IN_TEST,
            R.id.recycler_view,
        )

        // Wait for entity details activity to come into view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Wait for entity details activity to come into view")
        waitViewShown(withId(R.id.entity1_picture_1))

        // Check that entity details activity is in view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Check that entity details activity is in view")
        val modelNameLbl = Espresso.onView(withId(R.id.entity1_picture_1))
        modelNameLbl.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")
    }

    //    3. User searches for entities without setting any filters but inputs search query (city).
    @Test
    fun test3() {
        val methodName = "test3"
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName")

        // Prepare data set
        prepareDataSetWithUserWhoHasIncompleteProfile()

        // Launch scenario
        startTestActivityWithUserWhoHasIncompleteProfile()

        // Wait for ActivityHomeScreenFragment1 to appear
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Wait for ActivityHomeScreenFragment1 to appear")
        waitViewShown(withId(R.id.search_query_edittext))

        // Confirm ActivityHomeScreenFragment1 is in view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Confirm ActivityHomeScreenFragment1 is in view")
        val searchQueryEditText = Espresso.onView(withId(R.id.search_query_edittext))

        // Wait until data has loaded into recycler view
        Espresso::class.waitForDataToLoadIntoRecyclerView(
            "Wait until data has loaded into recycler view",
            TAG,
            LOG_ME,
            methodName,
            R.id.recycler_view
        )

        // Click on search query edittext
        searchQueryEditText.perform(ViewActions.click())

        // Type search query to search query edittext
        val searchQueryString = "warsaw"
        searchQueryEditText.perform(ViewActions.replaceText(searchQueryString))

        // Click on search button
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Click on search button")
        val searchButton = Espresso.onView(withId(R.id.search_button))
        searchButton.perform(ViewActions.click())

        // Wait until data has loaded into recycler view
        Espresso::class.waitForDataToLoadIntoRecyclerView(
            "Wait until data has loaded into recycler view",
            TAG,
            LOG_ME,
            methodName,
            R.id.recycler_view
        )

        // Click on 4th item in search results
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Click on 4th item in search results")
        val LIST_ITEM_IN_TEST = 3
        AdapterEntity1ToListItemType3.ListItemType3ViewHolder::class.clickOnNthItemInRecyclerView(
            "Click on 4th item in search results",
            TAG,
            LOG_ME,
            methodName,
            LIST_ITEM_IN_TEST,
            R.id.recycler_view,
        )

        // Wait for entity details activity to come into view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Wait for entity details activity to come into view")
        waitViewShown(withId(R.id.entity1_picture_1))

        // Check that entity details activity is in view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Check that entity details activity is in view")
        val modelNameLbl = Espresso.onView(withId(R.id.entity1_picture_1))
        modelNameLbl.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))


        // Scroll to entity's city label
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Scroll to entity's city label")
        Espresso.onView(withId(R.id.city_lbl)).perform(scrollTo())

        // Check that searched entity is from the provided city
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Check that searched entity is from the provided city")
        val cityLbl = Espresso.onView(withId(R.id.city_lbl))
        cityLbl.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")
    }

    //    4. User searches for entities after setting filters without search query (search around user).
    @Test
    fun test4() {
        val methodName = "test4"
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName")

        // Prepare data set
        prepareDataSetWithUserWhoHasIncompleteProfile()

        // Launch scenario
        startTestActivityWithUserWhoHasIncompleteProfile()

        // Wait for ActivityHomeScreenFragment1 to appear
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Wait for ActivityHomeScreenFragment1 to appear")
        waitViewShown(withId(R.id.search_query_edittext))

        // Confirm ActivityHomeScreenFragment1 is in view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Confirm ActivityHomeScreenFragment1 is in view")
        val searchQueryEditText = Espresso.onView(withId(R.id.search_query_edittext))
        searchQueryEditText.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        // Wait until data has loaded into recycler view
        Espresso::class.waitForDataToLoadIntoRecyclerView(
            "Wait until data has loaded into recycler view",
            TAG,
            LOG_ME,
            methodName,
            R.id.recycler_view
        )

        // Click on filters button
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Click on filters button")
        val filtersButton = Espresso.onView(withId(R.id.search_filters_button))
        filtersButton.perform(ViewActions.click())

        // Wait for bottom sheet to appear
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Wait for bottom sheet to appear")
        waitViewShown(withId(R.id.item_search_options_bottom_sheet_persistent))

        // Confirm that bottom sheet with search filters appeared
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Confirm that bottom sheet with search filters appeared")
        val bottomSheet = Espresso.onView(withId(R.id.item_search_options_bottom_sheet_persistent))
        bottomSheet.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))


        // Set test filters
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Set test filters")
        setTestFilters(methodName)
        sleep(1000)

        // Click on bottom sheet close button
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Click on bottom sheet close button")
        val bottomSheetCloseButton = Espresso.onView(withId(R.id.search_options_bottom_sheet_close_button))
        bottomSheetCloseButton.perform(ViewActions.click())
        sleep(1000)

        // Confirm that bottom sheet button is collapsed
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Confirm that bottom sheet button is collapsed")
        bottomSheet.check(ViewAssertions.matches(ViewMatchers::class.hasBottomSheetBehaviorState(BottomSheetBehavior.STATE_COLLAPSED)))

        // Click on search button
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Click on search button")
        val searchButton = Espresso.onView(withId(R.id.search_button))
        searchButton.perform(ViewActions.click())

        // Click on 1st item in search results
        val LIST_ITEM_IN_TEST = 0
        AdapterEntity1ToListItemType3.ListItemType3ViewHolder::class.clickOnNthItemInRecyclerView(
            "Click on 1st item in search results",
            TAG,
            LOG_ME,
            methodName,
            LIST_ITEM_IN_TEST,
            R.id.recycler_view,
        )

        // Wait for entity details activity to come into view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Wait for entity details activity to come into view")
        waitViewShown(withId(R.id.entity1_picture_1))

        // Check that entity details activity is in view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Check that entity details activity is in view")
        val modelNameLbl = Espresso.onView(withId(R.id.entity1_picture_1))
        modelNameLbl.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")
    }

    //    5. User searches for entities after setting filters with search query (city).
    @Test
    fun test5() {
        val methodName = "test5"
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName")

        // Prepare data set
        prepareDataSetWithUserWhoHasIncompleteProfile()

        // Launch scenario
        startTestActivityWithUserWhoHasIncompleteProfile()

        // Wait for ActivityHomeScreenFragment1 to appear
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Wait for ActivityHomeScreenFragment1 to appear")
        waitViewShown(withId(R.id.search_query_edittext))

        // Confirm ActivityHomeScreenFragment1 is in view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Confirm ActivityHomeScreenFragment1 is in view")
        val searchQueryEditText = Espresso.onView(withId(R.id.search_query_edittext))
        searchQueryEditText.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        // Wait until data has loaded into recycler view
        Espresso::class.waitForDataToLoadIntoRecyclerView(
            "Wait until data has loaded into recycler view",
            TAG,
            LOG_ME,
            methodName,
            R.id.recycler_view
        )

        // Click on search query edittext
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Click on search query edittext")
        searchQueryEditText.perform(ViewActions.click())

        // Type search query to search query edittext
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Type search query to search query edittext")
        val searchQueryString = "Lublin"
        searchQueryEditText.perform(ViewActions.replaceText(searchQueryString))

        // Click on filters button
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Click on filters button")
        val filtersButton = Espresso.onView(withId(R.id.search_filters_button))
        filtersButton.perform(ViewActions.click())

        // Wait for bottom sheet to appear
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Wait for bottom sheet to appear")
        waitViewShown(withId(R.id.item_search_options_bottom_sheet_persistent))

        // Confirm that bottom sheet with search filters appeared
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Confirm that bottom sheet with search filters appeared")
        val bottomSheet = Espresso.onView(withId(R.id.item_search_options_bottom_sheet_persistent))
        bottomSheet.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        // Set test filters
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Set test filters")
        setTestFilters(methodName)
        sleep(1000)

        // Click on bottom sheet close button
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Click on bottom sheet close button")
        val bottomSheetCloseButton = Espresso.onView(withId(R.id.search_options_bottom_sheet_close_button))
        bottomSheetCloseButton.perform(ViewActions.click())
        sleep(1000)

        // Confirm that bottom sheet button is collapsed
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Confirm that bottom sheet button is collapsed")
        bottomSheet.check(ViewAssertions.matches(ViewMatchers::class.hasBottomSheetBehaviorState(BottomSheetBehavior.STATE_COLLAPSED)))

        // Click on search button
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Click on search button")
        val searchButton = Espresso.onView(withId(R.id.search_button))
        searchButton.perform(ViewActions.click())
        sleep(1000)

        // Click on 1st item in search results
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Click on 1st item in search results")
        val LIST_ITEM_IN_TEST = 0
        AdapterEntity1ToListItemType3.ListItemType3ViewHolder::class.clickOnNthItemInRecyclerView(
            "Click on 4th item in search results",
            TAG,
            LOG_ME,
            methodName,
            LIST_ITEM_IN_TEST,
            R.id.recycler_view,
        )

        // Wait for entity details activity to come into view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Wait for entity details activity to come into view")
        waitViewShown(withId(R.id.entity1_picture_1))

        // Check that entity details activity is in view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Check that entity details activity is in view")
        val modelNameLbl = Espresso.onView(withId(R.id.entity1_picture_1))
        modelNameLbl.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        // Check that searched entity is from the provided city
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Scroll to searched entity's city")
        Espresso.onView(withId(R.id.city_lbl)).perform(scrollTo())

        if(LOG_ME)ALog.d(TAG, ".$methodName(): Check that searched entity is from the provided city")
        val cityLbl = Espresso.onView(withId(R.id.city_lbl))
        cityLbl.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        onView(withText(containsString(searchQueryString))).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))

        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")

    }

    private fun setTestFilters(methodName : String) {
        val newMinimalRange1ValueForTextView = "50"
        val newMaximumRange1ValueForTextView = "100"
        val newMinimalRange2ValueForTextView = "30"
        val newMaximumRange2ValueForTextView = "90"
        val bottomSheetBackground = Espresso.onView(withId(R.id.background))

        // Price per day RangeSlider
        // Type minimal range 1 value
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Type minimal range 1 value")
        val minimalRange1ValueEditText = Espresso.onView(withId(R.id.range_1_min_edittext))
        minimalRange1ValueEditText.perform(nestedScrollTo())
        sleep(1000)
        minimalRange1ValueEditText.perform(ViewActions.click(), ViewActions.closeSoftKeyboard())
        minimalRange1ValueEditText.perform(ViewActions.replaceText(newMinimalRange1ValueForTextView))


        // Check if range 1 value range slider changed it's value[0]
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Check if range 1 value range slider changed it's valueFrom")
        val range1ValueRangeSlider = Espresso.onView(withId(R.id.range_1_range_slider))
        bottomSheetBackground.perform((ViewActions::class.clickTopLeftCorner()))
        range1ValueRangeSlider.check(ViewAssertions.matches(
            withRangeSliderValueLeft(newMinimalRange1ValueForTextView.toFloat()))
        )

        // Type maximum range 1 value
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Type maximum range 1 value")
        val maximumRange1ValueEditText = Espresso.onView(withId(R.id.range_1_max_edittext))
        maximumRange1ValueEditText.perform(nestedScrollTo())
        sleep(1000)
        maximumRange1ValueEditText.perform(ViewActions.click(), ViewActions.closeSoftKeyboard())
        maximumRange1ValueEditText.perform(ViewActions.replaceText(newMaximumRange1ValueForTextView))

        // Check if range 1 value range slider changed it's value[1]
        // Reference: https://stackoverflow.com/questions/65390086/androidx-how-to-test-slider-in-ui-tests-espresso
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Check if range 1 value range slider changed it's value[1]")
        bottomSheetBackground.perform((ViewActions::class.clickTopLeftCorner()))
        range1ValueRangeSlider.check(ViewAssertions.matches(
            withRangeSliderValueRight(newMaximumRange1ValueForTextView.toFloat()))
        )



        // Range 2 RangeSlider
        // Type minimal range 2 value
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Type minimal range 2 value")
        val minimalRange2EditText = Espresso.onView(withId(R.id.range_2_min_edittext))
        minimalRange2EditText.perform(nestedScrollTo())
        sleep(1000)
        minimalRange2EditText.perform(ViewActions.click(), ViewActions.closeSoftKeyboard())
        minimalRange2EditText.perform(ViewActions.replaceText(newMinimalRange2ValueForTextView))

        // Check if range 2 value range slider changed it's value[0]
        val range2RangeSlider = Espresso.onView(withId(R.id.range_2_range_slider))
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Check if range 2 value range slider changed it's value[0]")
        bottomSheetBackground.perform((ViewActions::class.clickTopLeftCorner()))
        range2RangeSlider.check(ViewAssertions.matches(withRangeSliderValueLeft(newMinimalRange2ValueForTextView.toFloat())))

        // Type maximal range 2 value
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Type maximal range 2 value")
        val maximumRange2EditText = Espresso.onView(withId(
            R.id.range_2_max_edittext))
        maximumRange2EditText.perform(nestedScrollTo())
        sleep(1000)
        maximumRange2EditText.perform(ViewActions.click(), ViewActions.closeSoftKeyboard())
        maximumRange2EditText.perform(ViewActions.replaceText(newMaximumRange2ValueForTextView))

        // Check if range 2 value range slider changed it's value[1]
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Check if range 2 value range slider changed it's value[1]")
        bottomSheetBackground.perform((ViewActions::class.clickTopLeftCorner()))
        range2RangeSlider.check(ViewAssertions.matches(
            withRangeSliderValueRight(newMaximumRange2ValueForTextView.toFloat()))
        )



        // Tick all switches
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Flip switch 1")
        Espresso.onView(withId(R.id.switch_1)).perform(nestedScrollTo(), ViewActions.click())
        Espresso.onView(withId(R.id.switch_1)).check(matches(isChecked()))
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Flip switch 2")
        Espresso.onView(withId(R.id.switch_2)).perform(nestedScrollTo(), ViewActions.click())
        Espresso.onView(withId(R.id.switch_2)).check(matches(isChecked()))
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Flip switch 3")
        Espresso.onView(withId(R.id.switch_3)).perform(nestedScrollTo(), ViewActions.click())
        Espresso.onView(withId(R.id.switch_3)).check(matches(isChecked()))
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Flip switch 4")
        Espresso.onView(withId(R.id.switch_4)).perform(nestedScrollTo(), ViewActions.click())
        Espresso.onView(withId(R.id.switch_4)).check(matches(isChecked()))
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Flip switch 5")
        Espresso.onView(withId(R.id.switch_5)).perform(nestedScrollTo(), ViewActions.click())
        Espresso.onView(withId(R.id.switch_5)).check(matches(isChecked()))
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Flip switch 6")
        Espresso.onView(withId(R.id.switch_6)).perform(nestedScrollTo(), ViewActions.click())
        Espresso.onView(withId(R.id.switch_6)).check(matches(isChecked()))
    }

    //    6. User sets filters and then clears them with removeFiltersButton.
    @Test
    fun test6() {
        val methodName = "test6"
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName")

        // Prepare data set
        prepareDataSetWithUserWhoHasIncompleteProfile()

        // Launch scenario
        startTestActivityWithUserWhoHasIncompleteProfile()

        // Wait for ActivityHomeScreenFragment1 to appear
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Wait for ActivityHomeScreenFragment1 to appear")
        waitViewShown(withId(R.id.search_query_edittext))

        // Confirm ActivityHomeScreenFragment1 is in view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Confirm ActivityHomeScreenFragment1 is in view")
        val searchQueryEditText = Espresso.onView(withId(R.id.search_query_edittext))

        // Wait until data has loaded into recycler view
        Espresso::class.waitForDataToLoadIntoRecyclerView(
            "Wait until data has loaded into recycler view",
            TAG,
            LOG_ME,
            methodName,
            R.id.recycler_view
        )

        // Click on search query edittext
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Click on search query edittext")
        searchQueryEditText.perform(ViewActions.click())
        sleep(1000)

        // Type search query to search query edittext
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Type search query to search query edittext")
        val searchQueryString = "Lublin"
        searchQueryEditText.perform(ViewActions.replaceText(searchQueryString))

        // Click on filters button
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Click on filters button")
        val filtersButton = Espresso.onView(withId(R.id.search_filters_button))
        filtersButton.perform(ViewActions.click())
        sleep(1000)

        // Wait for bottom sheet to appear
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Wait for bottom sheet to appear")
        waitViewShown(withId(R.id.item_search_options_bottom_sheet_persistent))

        // Confirm that bottom sheet with search filters appeared
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Confirm that bottom sheet with search filters appeared")
        val bottomSheet = Espresso.onView(withId(R.id.item_search_options_bottom_sheet_persistent))
        bottomSheet.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        // Set test filters
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Set test filters")
        setTestFilters(methodName)
        sleep(1000)

        // Click on remove filters button
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Click on remove filters button")
        val removeFiltersButton = Espresso.onView(withId(R.id.remove_filters_button))
        removeFiltersButton.perform(ViewActions.click())            // Espresso gets stuck for no reason at this line.
        sleep(1000)

        // Confirm that bottom sheet is collapsed
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Confirm that bottom sheet is collapsed")
        bottomSheet.check(ViewAssertions.matches(ViewMatchers::class.hasBottomSheetBehaviorState(BottomSheetBehavior.STATE_COLLAPSED)))

        // Click on filters button
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Click on filters button")
        filtersButton.perform(ViewActions.click())

        // Wait for bottom sheet to appear
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Wait for bottom sheet to appear")
        waitViewShown(withId(R.id.item_search_options_bottom_sheet_persistent))

        // Confirm that bottom sheet with search filters appeared
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Confirm that bottom sheet with search filters appeared")
        bottomSheet.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        // Confirm that filters are cleared
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Confirm that filters are cleared.")
        confirmFiltersAreCleared()

        // Click on search button
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Click on search button")
        val searchButton = Espresso.onView(withId(R.id.search_button))
        searchButton.perform(ViewActions.click())
        sleep(1000)

        // Click on 4th item in search results
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Click on 4th item in search results")
        val LIST_ITEM_IN_TEST = 3
        AdapterEntity1ToListItemType3.ListItemType3ViewHolder::class.clickOnNthItemInRecyclerView(
            "Click on 4th item in search results",
            TAG,
            LOG_ME,
            methodName,
            LIST_ITEM_IN_TEST,
            R.id.recycler_view,
        )

        // Wait for entity details activity to come into view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Wait for entity details activity to come into view")
        waitViewShown(withId(R.id.entity1_picture_1))

        // Check that entity details activity is in view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Check that entity details activity is in view")
        val modelNameLbl = Espresso.onView(withId(R.id.entity1_picture_1))
        modelNameLbl.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")
    }

    private fun confirmFiltersAreCleared(){
        val minimalRange1EditText = Espresso.onView(withId(R.id.range_1_min_edittext))
        minimalRange1EditText.perform(nestedScrollTo())
        minimalRange1EditText.check(matches(withText(containsString(""))))

        val maximumRange1EditText = Espresso.onView(withId(R.id.range_1_max_edittext))
        maximumRange1EditText.perform(nestedScrollTo())
        maximumRange1EditText.check(matches(withText(containsString(""))))

        val minimalRange2EditText = Espresso.onView(withId(R.id.range_2_min_edittext))
        minimalRange2EditText.perform(nestedScrollTo())
        minimalRange2EditText.check(matches(withText(containsString(""))))

        val maximumRange2EditText = Espresso.onView(withId(R.id.range_2_max_edittext))
        maximumRange2EditText.perform(nestedScrollTo())
        maximumRange2EditText.check(matches(withText(containsString(""))))

        val range1RangeSlider = Espresso.onView(withId(R.id.range_1_range_slider))
        val valueFromOfRange1RangeSlider = Espresso::class.getValueFrom(range1RangeSlider)
        val valueToOfRange1RangeSlider = Espresso::class.getValueTo(range1RangeSlider)
        range1RangeSlider.check(ViewAssertions.matches(withRangeSliderValueLeft(valueFromOfRange1RangeSlider!!)))
        range1RangeSlider.check(ViewAssertions.matches(withRangeSliderValueRight(valueToOfRange1RangeSlider!!)))

        val range2RangeSlider = Espresso.onView(withId(R.id.range_2_range_slider))
        val valueFromOfRange2RangeSlider = Espresso::class.getValueFrom(range2RangeSlider)
        val valueToOfRange2RangeSlider = Espresso::class.getValueTo(range2RangeSlider)
        range2RangeSlider.check(ViewAssertions.matches(withRangeSliderValueLeft(valueFromOfRange2RangeSlider!!)))
        range2RangeSlider.check(ViewAssertions.matches(withRangeSliderValueRight(valueToOfRange2RangeSlider!!)))


        Espresso.onView(withId(R.id.switch_1)).perform(nestedScrollTo())
        Espresso.onView(withId(R.id.switch_1)).check(matches(isNotChecked()))

        Espresso.onView(withId(R.id.switch_2)).perform(nestedScrollTo())
        Espresso.onView(withId(R.id.switch_2)).check(matches(isNotChecked()))

        Espresso.onView(withId(R.id.switch_3)).perform(nestedScrollTo())
        Espresso.onView(withId(R.id.switch_3)).check(matches(isNotChecked()))

        Espresso.onView(withId(R.id.switch_4)).perform(nestedScrollTo(),)
        Espresso.onView(withId(R.id.switch_4)).check(matches(isNotChecked()))

        Espresso.onView(withId(R.id.switch_5)).perform(nestedScrollTo())
        Espresso.onView(withId(R.id.switch_5)).check(matches(isNotChecked()))

        Espresso.onView(withId(R.id.switch_6)).perform(nestedScrollTo())
        Espresso.onView(withId(R.id.switch_6)).check(matches(isNotChecked()))

        Espresso.onView(withId(R.id.switch_7)).perform(nestedScrollTo(),)
        Espresso.onView(withId(R.id.switch_7)).check(matches(isNotChecked()))
    }

    //    7. User types search query but decides to delete it with searchBackButton.
    @Test
    fun test7() {
        val methodName = "test7"
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName")

        // Prepare data set
        prepareDataSetWithUserWhoHasIncompleteProfile()

        // Launch scenario
        startTestActivityWithUserWhoHasIncompleteProfile()

        // Wait for ActivityHomeScreenFragment1 to appear
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Wait for ActivityHomeScreenFragment1 to appear")
        waitViewShown(withId(R.id.search_query_edittext))

        // Confirm ActivityHomeScreenFragment1 is in view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Confirm ActivityHomeScreenFragment1 is in view")
        val searchQueryEditText = Espresso.onView(withId(R.id.search_query_edittext))

        // Wait until data has loaded into recycler view
        Espresso::class.waitForDataToLoadIntoRecyclerView(
            "Wait until data has loaded into recycler view",
            TAG,
            LOG_ME,
            methodName,
            R.id.recycler_view
        )

        // Click on search query edittext
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Click on search query edittext")
        searchQueryEditText.perform(ViewActions.click())
        sleep(1000)

        // Type search query to search query edittext
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Type search query to search query edittext")
        val searchQueryString = "Lublin"
        searchQueryEditText.perform(ViewActions.replaceText(searchQueryString))

        // Click on search back button
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Click on search back button")
        val searchBackButton = Espresso.onView(withId(R.id.search_back_button))
        searchBackButton.perform(ViewActions.click())
        sleep(1000)

        // Confirm that search query edittext is empty
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Confirm that search query edittext is empty")
        searchQueryEditText.check(ViewAssertions.matches(withText("")))

        // Click on search button
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Click on search button")
        val searchButton = Espresso.onView(withId(R.id.search_button))
        searchButton.perform(ViewActions.click())
        sleep(1000)

        // Click on 4th item in search results
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Click on 4th item in search results")
        val LIST_ITEM_IN_TEST = 3
        AdapterEntity1ToListItemType3.ListItemType3ViewHolder::class.clickOnNthItemInRecyclerView(
            "Click on 4th item in search results",
            TAG,
            LOG_ME,
            methodName,
            LIST_ITEM_IN_TEST,
            R.id.recycler_view,
        )

        // Wait for entity details activity to come into view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Wait for entity details activity to come into view")
        waitViewShown(withId(R.id.entity1_picture_1))

        // Check that entity details activity is in view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Check that entity details activity is in view")
        val modelNameLbl = Espresso.onView(withId(R.id.entity1_picture_1))
        modelNameLbl.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        // Check that searched entity is not from the provided city
        val anotherCity = "Warszawa"
        val entitiesCity = Espresso::class.getTextViewText(withId(R.id.city_lbl))
        if(LOG_ME)ALog.d(
            TAG, ".$methodName(): anotherCity == $anotherCity\n" +
                "entitiesCity == $entitiesCity")
        assertFalse { entitiesCity == anotherCity }

        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")
    }

    //    8. User has completed their profile, check if profile completion button is invisible.
    @Test
    fun test8() {
        val methodName = "test8"
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName")

        // Prepare data set
        prepareDataSetWithUserWhoHasCompleteProfile()

        // Launch scenario
        startTestActivityWithUserWhoHasCompleteProfile()

        // Wait for ActivityHomeScreenFragment1 to appear
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Wait for ActivityHomeScreenFragment1 to appear")
        waitViewShown(withId(R.id.search_query_edittext))

        // Confirm ActivityHomeScreenFragment1 is in view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Confirm ActivityHomeScreenFragment1 is in view")
        val searchQueryEditText = Espresso.onView(withId(R.id.search_query_edittext))
        searchQueryEditText.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        // Confirm profile completion button is gone
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Confirm profile completion button is in view")
        val profileCompletionButton = Espresso.onView(withId(R.id.profile_status_complete_profile_button))
        profileCompletionButton.check(ViewAssertions.matches(not(ViewMatchers.isDisplayed())))

        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")
    }

    @Test
    fun test9() {
        val methodName = "test9"
        val testDescription = "9. User decided to view the first entity in the list. User has incomplete profile."
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName \n $testDescription")

        // Prepare data set
        prepareDataSetWithUserWhoHasIncompleteProfile()

        // Launch scenario
        startTestActivityWithUserWhoHasIncompleteProfile()

        // Wait for ActivityHomeScreenFragment1 to appear
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Wait for ActivityHomeScreenFragment1 to appear")
        waitViewShown(withId(R.id.search_query_edittext))

        // Confirm ActivityHomeScreenFragment1 is in view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Confirm ActivityHomeScreenFragment1 is in view")
        val searchQueryEditText = Espresso.onView(withId(R.id.search_query_edittext))
        searchQueryEditText.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        // Wait until data has loaded into recycler view
        Espresso::class.waitForDataToLoadIntoRecyclerView(
            "Wait until data has loaded into recycler view",
            TAG,
            LOG_ME,
            methodName,
            R.id.recycler_view
        )

        // Click on first entity in recyclerView
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Click on 1st item in search results")
        val LIST_ITEM_IN_TEST = 0
        AdapterEntity1ToListItemType3.ListItemType3ViewHolder::class.clickOnNthItemInRecyclerView(
            "Click on 4th item in search results",
            TAG,
            LOG_ME,
            methodName,
            LIST_ITEM_IN_TEST,
            R.id.recycler_view,
        )

        // Wait for entity details activity to come into view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Wait for entity details activity to come into view")
        waitViewShown(withId(R.id.entity1_picture_1))

        // Check that entity details activity is in view
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Check that entity details activity is in view")
        val modelNameLbl = Espresso.onView(withId(R.id.entity1_picture_1))
        modelNameLbl.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(withId(R.id.action_button)).perform(ViewActions.click())
        Thread.sleep(2000)

        // Confirm that a dialog appeared saying that profile must be completed to make action on entity
        if(LOG_ME)ALog.d(TAG, ".$methodName(): " +
                "Confirm that a dialog appeared saying that profile must be completed " +
                "to make action on entity")
        onView(withText(
            InstrumentationRegistry.getInstrumentation().targetContext.resources.getString(
                R.string.only_fully_verified_users_can_act_on_entity1
            )
        )).check(matches(isDisplayed()))

        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")
    }
}