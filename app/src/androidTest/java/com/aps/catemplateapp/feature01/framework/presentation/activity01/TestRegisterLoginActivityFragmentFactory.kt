package com.aps.catemplateapp.feature01.framework.presentation.activity01

import androidx.fragment.app.FragmentFactory
import androidx.lifecycle.ViewModelProvider
import com.aps.catemplateapp.common.framework.presentation.UIController
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.feature01.framework.presentation.activity01.fragments.RegisterLoginActivityLoginFragment
import com.aps.catemplateapp.feature01.framework.presentation.activity01.fragments.RegisterLoginActivityMainFragment
import com.aps.catemplateapp.feature01.framework.presentation.activity01.fragments.RegisterLoginActivityOptionsFragment
import com.aps.catemplateapp.feature01.framework.presentation.activity01.fragments.RegisterLoginActivityRegistrationFragment
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject
import javax.inject.Singleton

@ExperimentalCoroutinesApi
@FlowPreview
@Singleton
class TestRegisterLoginActivityFragmentFactory
@Inject
constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    private val dateUtil: DateUtil
): FragmentFactory(){
    lateinit var uiController: UIController
    override fun instantiate(classLoader: ClassLoader, className: String) =
        when(className){
            RegisterLoginActivityMainFragment::class.java.name -> {
                val fragment = RegisterLoginActivityMainFragment(viewModelFactory, dateUtil)
                if(::uiController.isInitialized){
                    fragment.setUIController(uiController)
                }
                fragment
            }
            RegisterLoginActivityOptionsFragment::class.java.name -> {
                val fragment = RegisterLoginActivityOptionsFragment(viewModelFactory, dateUtil)
                if(::uiController.isInitialized){
                    fragment.setUIController(uiController)
                }
                fragment
            }
            RegisterLoginActivityLoginFragment::class.java.name -> {
                val fragment = RegisterLoginActivityLoginFragment(viewModelFactory, dateUtil)
                if(::uiController.isInitialized){
                    fragment.setUIController(uiController)
                }
                fragment
            }
            RegisterLoginActivityRegistrationFragment::class.java.name -> {
                val fragment = RegisterLoginActivityRegistrationFragment(viewModelFactory, dateUtil)
                if(::uiController.isInitialized){
                    fragment.setUIController(uiController)
                }
                fragment
            }

            else -> {
                super.instantiate(classLoader, className)
            }
        }
}