package com.aps.catemplateapp.core.framework.datasources

import android.app.Application
import com.aps.catemplateapp.feature02.business.domain.model.entities.Entity2
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import javax.inject.Inject

class EntityDataFactoryEntity2
@Inject
constructor(
    override val application: Application
): EntityDataFactory<Entity2>(application) {
//    override val testClassLoader: ClassLoader,
//    override val fileNameWithTestData: String): com.aps.catemplateapp.core.framework.datasources.EntityDataFactory<Entity2>(testClassLoader, fileNameWithTestData) {


    override fun produceListOfEntities(): List<Entity2>{
        val entities: List<Entity2> = Gson()
            .fromJson(
                getEntitiesFromFile("entity2_test_list.json"),
                object: TypeToken<List<Entity2>>() {}.type
            )
        return entities
    }
}