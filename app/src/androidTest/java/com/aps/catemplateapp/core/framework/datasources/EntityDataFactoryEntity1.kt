package com.aps.catemplateapp.core.framework.datasources

import android.app.Application
import com.aps.catemplateapp.feature02.business.domain.model.entities.Entity1
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import javax.inject.Inject

class EntityDataFactoryEntity1
@Inject
constructor(
    override val application: Application
): EntityDataFactory<Entity1>(application) {
//    override val testClassLoader: ClassLoader,
//    override val fileNameWithTestData: String): com.aps.catemplateapp.core.framework.datasources.EntityDataFactory<Entity1>(testClassLoader, fileNameWithTestData) {


    override fun produceListOfEntities(): List<Entity1> {
        return Gson()
            .fromJson(
                getEntitiesFromFile("entity1_test_list.json"),
                object : TypeToken<List<Entity1>>() {}.type
            )
    }
}