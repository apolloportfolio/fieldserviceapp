package com.aps.catemplateapp.core.framework.datasources

import android.app.Application
import com.aps.catemplateapp.feature02.business.domain.model.entities.Entity3
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import javax.inject.Inject

class EntityDataFactoryEntity3
@Inject
constructor(
    override val application: Application
): EntityDataFactory<Entity3>(application) {
//    override val testClassLoader: ClassLoader,
//    override val fileNameWithTestData: String): com.aps.catemplateapp.core.framework.datasources.EntityDataFactory<Entity3>(testClassLoader, fileNameWithTestData) {


    override fun produceListOfEntities(): List<Entity3> {
        return Gson()
            .fromJson(
                getEntitiesFromFile("entity3_test_list.json"),
                object : TypeToken<List<Entity3>>() {}.type
            )
    }
}