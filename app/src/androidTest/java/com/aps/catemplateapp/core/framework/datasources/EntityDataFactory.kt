package com.aps.catemplateapp.core.framework.datasources

import android.app.Application
import android.content.res.AssetManager
import java.io.IOException
import java.io.InputStream
import javax.inject.Singleton

private const val TAG = "EntityDataFactory"
@Singleton
abstract class EntityDataFactory<Entity>
constructor(
//    open val testClassLoader: ClassLoader,
    open val application: Application
) {

    abstract fun produceListOfEntities(): List<Entity>

    fun produceHashMapOfEntities(entityList: List<Entity>, getId: (Entity)-> String): HashMap<String, Entity>{
        val map = HashMap<String, Entity>()
        for(entity in entityList){
            map[getId(entity)] = entity
        }
        return map
    }

    fun produceEmptyListOfEntities(): List<Entity>{
        return ArrayList()
    }

    fun getEntitiesFromFile(fileName: String): String? {
        return readJSONFromAsset(fileName)
    }

    private fun readJSONFromAsset(fileName: String): String? {
        var json: String? = null
        json = try {
            val inputStream: InputStream = (application.assets as AssetManager).open(fileName)
            inputStream.bufferedReader().use{ it.readText() }
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }
}