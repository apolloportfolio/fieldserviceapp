package com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.abstraction

import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.aps.catemplateapp.BaseInstrumentedTest
import com.aps.catemplateapp.feature02.business.domain.model.factories.Entity3Factory
import com.aps.catemplateapp.core.framework.datasources.EntityDataFactoryEntity3
import com.aps.catemplateapp.feature02.framework.datasources.cachedatasources.room.daos.Entity3Dao
import com.aps.catemplateapp.feature02.framework.datasources.cachedatasources.room.impl.Entity3DaoServiceImpl
import com.aps.catemplateapp.feature02.framework.datasources.cachedatasources.room.mappers.Entity3CacheMapper
import com.aps.catemplateapp.feature02.framework.datasources.cachedatasources.room.abs.Entity3DaoService
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.runBlocking
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters
import javax.inject.Inject
import kotlin.test.assertEquals
import kotlin.test.assertTrue

// runBlockingTest doesn't work:
// https://github.com/Kotlin/kotlinx.coroutines/issues/1204

/*
    LEGEND:
    1. CBS = "Confirm by searching"

    Test cases:
    1. confirm database entity empty to start (should be test data inserted from CacheTest.kt)
    2. insert a new entity, CBS
    3. insert a list of entities, CBS
    4. insert 1000 new entities, confirm filtered search query works correctly
    5. insert 1000 new entities, confirm db size increased
    6. delete new entity, confirm deleted
    7. delete list of entities, CBS
    8. update a entity, confirm updated

 */


@ExperimentalCoroutinesApi
@FlowPreview
@RunWith(AndroidJUnit4ClassRunner::class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class Entity3DaoServiceTest: BaseInstrumentedTest() {
    // system in test
    private val entityDaoService: Entity3DaoService

    // dependencies
    @Inject
    lateinit var dao: Entity3Dao

    @Inject
    lateinit var entityDataFactory: EntityDataFactoryEntity3

    @Inject
    lateinit var entityFactory: Entity3Factory

    @Inject
    lateinit var cacheMapper: Entity3CacheMapper

    init {
//        injectTest()
        insertTestData()
        entityDaoService = Entity3DaoServiceImpl(
            dao = dao,
            mapper = cacheMapper,
            dateUtil = dateUtil
        )
    }

    override fun injectTest() {
        //(application.appComponent as TestAppComponent).inject(this)
    }

    private fun insertTestData() = runBlocking{
        this.javaClass.classLoader?.let { classLoader ->
            entityDataFactory = EntityDataFactoryEntity3(application)
    }
        val entityList = cacheMapper.mapToEntityList(
            entityDataFactory.produceListOfEntities()
        )
        dao.insertEntities(entityList)
    }

    /**
     * This test runs first. Check to make sure the test data was inserted from
     * CacheTest class.
     */
    @Test
    fun a_searchEntities_confirmDbNotEmpty() = runBlocking {

        val numEntities = entityDaoService.getNumEntities()

        assertTrue { numEntities > 0 }

    }

    @Test
    fun insertEntity_CBS() = runBlocking {

        val newEntity = entityFactory.generateEmpty()
        entityDaoService.insertEntity(newEntity)

        val entities = entityDaoService.searchEntities()
        assert(entities.contains(newEntity))
    }

    @Test
    fun insertEntityList_CBS() = runBlocking {

        val entityList = entityFactory.createEntitiesList(10)
        entityDaoService.insertEntities(entityList)

        val queriedEntities = entityDaoService.searchEntities()

        assertTrue { queriedEntities.containsAll(entityList) }
    }

    @Test
    fun insert1000entities_confirmNumEntitiesInDb() = runBlocking {
        val currentNumEntities = entityDaoService.getNumEntities()

        // insert 1000 entities
        val entityList = entityFactory.createEntitiesList(1000)
        entityDaoService.insertEntities(entityList)

        val numEntities = entityDaoService.getNumEntities()
        assertEquals(currentNumEntities + 1000, numEntities)
    }

//    @Test
//    fun insert1000entities_searchEntitiesByTitle_confirm50ExpectedValues() = runBlocking {
//
//        // insert 1000 entities
//        val entityList = entityFactory.createEntitiesList(1000)
//        entityDaoService.insertEntities(entityList)
//
//        // query 50 entities by specific title
//        repeat(50){
//            val randomIndex = Random.nextInt(0, entityList.size - 1)
//            val result = entityDaoService.searchEntities(
//                query = entityList.get(randomIndex).title,
//                page = 1,
//                pageSize = 1
//            )
//            assertEquals(entityList.get(randomIndex).title, result.get(0).title)
//        }
//    }
}