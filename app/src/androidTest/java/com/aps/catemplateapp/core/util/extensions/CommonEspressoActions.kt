package com.aps.catemplateapp.core.util.extensions

import android.app.Activity
import android.app.Activity.RESULT_OK
import android.app.Instrumentation
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.net.Uri
import android.provider.MediaStore
import android.view.InputDevice
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.scrollTo
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import org.hamcrest.core.IsNot.not
import org.hamcrest.core.StringContains.containsString

import androidx.test.espresso.Espresso.onData
import org.hamcrest.CoreMatchers.*

import androidx.test.espresso.matcher.BoundedMatcher

import kotlin.reflect.KClass

import androidx.test.espresso.action.ViewActions.actionWithAssertions
import androidx.test.espresso.contrib.RecyclerViewActions

import android.view.MotionEvent
import androidx.test.espresso.*

import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import android.view.ViewParent
import android.widget.*

import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap

import androidx.test.espresso.util.HumanReadables

import androidx.test.espresso.PerformException

import androidx.core.widget.NestedScrollView
import androidx.test.core.app.ApplicationProvider

import androidx.test.espresso.matcher.ViewMatchers.withEffectiveVisibility

import androidx.test.espresso.matcher.ViewMatchers.isDescendantOfA

import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.*
import androidx.test.espresso.contrib.PickerActions
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.Intents.intending
import androidx.test.espresso.intent.VerificationModes.times
import androidx.test.platform.app.InstrumentationRegistry
import com.google.android.material.bottomsheet.BottomSheetBehavior
import java.lang.Exception
import androidx.test.espresso.matcher.ViewMatchers.withClassName

import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.espresso.intent.matcher.IntentMatchers.*
import junit.framework.AssertionFailedError
import org.hamcrest.*
import java.io.IOException
import android.widget.RatingBar
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import kotlin.test.assertTrue

import androidx.test.espresso.Espresso

import androidx.test.runner.lifecycle.ActivityLifecycleMonitorRegistry
import androidx.test.runner.lifecycle.Stage
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.core.util.ToastMatcher.Companion.onToast
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import com.aps.catemplateapp.R


private const val THREAD_SLEEP_TIME_AFTER_CLICKING = 2000L
fun <T : Espresso> KClass<T>.THREAD_SLEEP_TIME_AFTER_CLICKING() = THREAD_SLEEP_TIME_AFTER_CLICKING

// Other resources worth checking:
// https://medium.com/mindorks/some-useful-custom-espresso-matchers-in-android-33f6b9ca2240
// https://gist.github.com/chemouna/00b10369eb1d5b00401b

// Click on view
@Deprecated(
    "It doesn't work even though it theoretically should",
    ReplaceWith("Espresso::class.clickOnViewWithText()")
)
fun clickOnView(
        actionDescription : String,
        TAG : String,
        LOG_ME : Boolean,
        methodName : String,
        viewId : Int,
        clickActivatesToastToTest : Boolean = false,
) {
    if(LOG_ME) ALog.d(TAG, ".$methodName(): $actionDescription")
    val view = Espresso.onView(withId(viewId))
    try {
        view.perform(scrollTo())
    } catch(e : Exception) {}
    try {
        if(LOG_ME) ALog.d(TAG, ".$methodName(): $actionDescription View is about to be clicked == $view")
        view.perform(ViewActions.click())
        if(LOG_ME) ALog.d(TAG, ".$methodName(): $actionDescription view clicked: $view")
    } catch(e : Exception) {
        ALog.e(TAG, ".$methodName(): Exception: $e")
    }
    if(!clickActivatesToastToTest)Thread.sleep(THREAD_SLEEP_TIME_AFTER_CLICKING)
}


// https://stackoverflow.com/questions/28390574/checking-toast-message-in-android-espresso#:~:text=The%20way%20Toasts%20are%20implemented,the%20toast%20has%20become%20visible.
fun <T : Espresso> KClass<T>.confirmToastAppeared(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    textId : Int,
    activity : Activity,
) {
    if (LOG_ME) ALog.d(TAG, ".$methodName(): $actionDescription")
    try {
        onToast(textId).check(matches(isDisplayed()))
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Toast confirmed.")
    } catch (e: Exception) {
        ALog.e(TAG, methodName, e)
        e.printStackTrace()
    }
}


fun <T : Espresso> KClass<T>.clickOnViewWithText(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    text : String,
) {
    if(LOG_ME) ALog.d(TAG, ".$methodName(): $actionDescription")
    val view = Espresso.onView(withText(text))
    try {
        view.perform(scrollTo())
    } catch(e : Exception) {}
    try {
        view.perform(ViewActions.click())
    } catch(e : Exception) {}
    Thread.sleep(THREAD_SLEEP_TIME_AFTER_CLICKING)
}

fun <T : Espresso> KClass<T>.clickOnOkButtonInDialog(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
) {
    if(LOG_ME) ALog.d(TAG, ".$methodName(): $actionDescription")
    try {
        val view = Espresso.onView(withText(getContext().getString(R.string.dialog_picker_ok_buttons_text)))
        try {
            view.perform(scrollTo())
        } catch(e : Exception) {}
        view.perform(ViewActions.click())
    } catch(e : Exception) {}
    Thread.sleep(THREAD_SLEEP_TIME_AFTER_CLICKING)
}

// Reference: https://stackoverflow.com/questions/28019657/camera-operation-ui-testing-with-espresso
// Remember to call Intents.init() in @Before and
// Intents.release() in @After
fun <T : Espresso> KClass<T>.testStartActivityForResultToGetPicture(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    idOfViewThatStartsActivityForResult : Int,
    idOfViewToVerifyWeCameBackFromStartedActivity : Int,
) {
    if(LOG_ME) ALog.d(TAG, ".$methodName(): $actionDescription")
    // Build an ActivityResult that will return from the camera app and set your extras (if any).
    val resultData = Intent()

    val res: Resources = getContext().resources
    val resId = R.drawable.ic_launcher
    val testUri = Uri.parse(
        ContentResolver.SCHEME_ANDROID_RESOURCE +
                "://" + res.getResourcePackageName(resId)
                + '/' + res.getResourceTypeName(resId)
                + '/' + res.getResourceEntryName(resId)
    )
    resultData.putExtra(MediaStore.EXTRA_OUTPUT, testUri)
    val result = Instrumentation.ActivityResult(Activity.RESULT_OK, resultData)

    // Mock the camera response. Now whenever an intent is sent to the camera, Espresso will respond with the result we pass here.
    intending(hasAction(MediaStore.ACTION_IMAGE_CAPTURE)).respondWith(result)

    // Now click on the button in your app that launches the camera.
    onView(withId(idOfViewThatStartsActivityForResult)).perform(click());

    // Optionally, we can also verify that the intent to the camera has actually been sent from out.
    intended(hasAction(MediaStore.ACTION_IMAGE_CAPTURE))

    // At this point the onActivityResult() has been called so verify that whatever view is supposed to be displayed is indeed displayed.
    onView(withId(idOfViewToVerifyWeCameBackFromStartedActivity)).check(matches(isDisplayed()))
}

// Click on view in it's top left corner
fun <T : ViewActions> KClass<T>.clickTopLeftCorner(): ViewAction? {
    return actionWithAssertions(
        GeneralClickAction(
            Tap.SINGLE,
            GeneralLocation.TOP_RIGHT,
            Press.FINGER,
            InputDevice.SOURCE_UNKNOWN,
            MotionEvent.BUTTON_PRIMARY
        )
    )
}


// Type text in edittext
fun typeTextInEditText(
        actionDescription : String,
        TAG : String,
        LOG_ME : Boolean,
        methodName : String,
        editTextId : Int,
        textToTypeIn : String = "string"
) {
    if(LOG_ME)ALog.d(TAG, ".$methodName(): $actionDescription")
    val editText = Espresso.onView(withId(editTextId))
    try {
        editText.perform(scrollTo())
    } catch(e : Exception) {}
    editText.perform(ViewActions.replaceText(textToTypeIn))
}

// Click and immediately type in text in edittext
fun clickAndTypeTextInEditText(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    editTextId : Int,
    textToTypeIn : String = "string"
) {
    if(LOG_ME)ALog.d(TAG, ".$methodName(): $actionDescription")
    val editText = Espresso.onView(withId(editTextId))
    try {
        editText.perform(scrollTo())
    } catch(e: Exception) {
        ALog.e(TAG, ".$methodName(): Exception: ${e.message}")
    }
    editText.perform( ViewActions.click())
    Thread.sleep(THREAD_SLEEP_TIME_AFTER_CLICKING)
    try {
        editText.perform(scrollTo())
    } catch(e: Exception) {
        ALog.e(TAG, ".$methodName(): Exception: ${e.message}")
    }
    editText.perform(ViewActions.replaceText(textToTypeIn))
}


// Check that TextView with string is in view
fun checkIfViewWithStringIsInView(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    string : String,
    viewId : Int
) {
    if(LOG_ME)ALog.d(TAG, ".$methodName(): $actionDescription")
    val textView = Espresso.onView(withId(viewId))
    val textViewsContent = Espresso::class.getTextViewText(withId(viewId))
    if(LOG_ME)ALog.d(TAG, ".$methodName(): Looking for '$string' in TextView that contains '$textViewsContent'")
    try {
        textView.perform(scrollTo())
    } catch(e : Exception) {}
    textView.check(matches(withText(containsString(string))))
    onView(withText(containsString(string))).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))
}

// Check that dialog with text is in view
fun <T : Espresso> KClass<T>.confirmDialogWithTextIsInView(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    dialogText : String,
) {
    if(LOG_ME)ALog.d(TAG, ".$methodName(): $actionDescription    text == $dialogText")
    onView(withText(dialogText)).check(matches(isDisplayed()));
}

fun <T : Espresso> KClass<T>.getContext():Context {
    return InstrumentationRegistry.getInstrumentation().context
}

fun <T : Espresso> KClass<T>.getTestImageUri():Uri {
    val resources = getContext().resources
    return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
            resources.getResourcePackageName(R.drawable.ic_launcher_background) + "/" +
            resources.getResourceTypeName(R.drawable.ic_launcher_background) + "/" +
            resources.getResourceEntryName(R.drawable.ic_launcher_background)
    )
}

// Check that dialog with text is in view
fun <T : Espresso> KClass<T>.confirmDialogWithTextIsInView(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    dialogText : Int,
) {
    if(LOG_ME)ALog.d(TAG, ".$methodName(): $actionDescription ")
    val text = (ApplicationProvider.getApplicationContext() as Context).getString(dialogText)
    if(LOG_ME)ALog.d(TAG, ".$methodName(): $actionDescription    text == $text")
    onView(withText(text))
        .check(matches(isDisplayed()));
}


// Confirm that view with id is visible
fun confirmThatViewWithIdIsVisible(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    viewId : Int
) {
    if(LOG_ME)ALog.d(TAG, ".$methodName(): $actionDescription")
    val bottomSheet = Espresso.onView(withId(viewId))
    bottomSheet.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
}


// Confirm that view with id is not in view
fun confirmThatViewWithIdIsNotVisible(
    actionDescription: String,
    TAG: String,
    LOG_ME: Boolean,
    methodName: String,
    viewId: Int
) {
    if(LOG_ME)ALog.d(TAG, ".$methodName(): $actionDescription")
    val view = Espresso.onView(withId(viewId))
    view.check(ViewAssertions.matches(not(ViewMatchers.isDisplayed())))
}


// Click on bottom navigation menu
fun clickOnBottomNavigationMenu(
    actionDescription: String,
    TAG: String,
    LOG_ME: Boolean,
    methodName: String,
    menuItemId: Int
) {
    if(LOG_ME)ALog.d(TAG, ".$methodName(): $actionDescription")
    onView(anyOf(withId(menuItemId))).perform(click());
}


// Click on Nth item in a RecyclerView
fun <T : RecyclerView.ViewHolder> KClass<T>.clickOnNthItemInRecyclerView(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    itemsNumber : Int,
    recyclerViewId : Int,
    ) {
    if(LOG_ME)ALog.d(TAG, ".$methodName(): $actionDescription")
    // Wait until data has loaded into recycler view
    Espresso::class.waitForDataToLoadIntoRecyclerView(
        "Wait until data has loaded into recycler view",
        TAG,
        LOG_ME,
        methodName,
        recyclerViewId,
    )
    // Scroll to Nth item in recycler view
    if(LOG_ME)ALog.d(TAG, ".$methodName(): Scroll to $itemsNumber item in recycler view")
    Espresso.onView(withId(R.id.recycler_view)).perform(
        RecyclerViewActions.scrollToPosition<T>(
            itemsNumber
        )
    )
    Espresso.onView(withId(R.id.recycler_view)).perform(
        RecyclerViewActions.actionOnItemAtPosition<T>(
            itemsNumber,
            click()
        )
    )
}

// Click spinner, set an option and confirm that it was set
fun setAndConfirmSpinnerOption(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    selectionText : String,
    spinnerId : Int) {
    if (LOG_ME) ALog.d(TAG, ".$methodName(): $actionDescription")
    try {
        onView(withId(spinnerId)).perform(scrollTo())
    } catch(e : Exception) {}
    onView(withId(spinnerId)).perform(click())
    onData(allOf(`is`(instanceOf(String::class.java)), `is`(selectionText))).perform(click())
    onView(withId(spinnerId)).check(matches(withSpinnerText(containsString(selectionText))))
}

// Confirm that recyclerview has set number of elements
// https://stackoverflow.com/questions/63562046/how-to-check-if-recyclerview-is-empty-espresso
fun confirmRecyclerViewSize(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    recyclerViewId : Int,
    size : Int) {
    if (LOG_ME) ALog.d(TAG, ".$methodName(): $actionDescription")

    onView(withId(recyclerViewId)).check(matches(recyclerViewSizeMatcher(size)))
}
private fun recyclerViewSizeMatcher(matcherSize: Int): Matcher<View?>? {
    return object : BoundedMatcher<View?, RecyclerView>(RecyclerView::class.java) {
        override fun describeTo(description: Description) {
            description.appendText("with list size: $matcherSize")
        }

        override fun matchesSafely(recyclerView: RecyclerView): Boolean {
            return matcherSize == recyclerView.adapter!!.itemCount
        }
    }
}

// Get number of items in recycler view
fun <T : Activity>getNumberOfItemsInRecyclerView(
    TAG : String,
    LOG_ME : Boolean,
    scenario : ActivityScenario<T>,
    recyclerViewId : Int) : Int {
    var itemCount : Int = -1
    scenario.onActivity { activity ->
        itemCount = activity.findViewById<RecyclerView>(recyclerViewId).adapter?.itemCount!!
    }
    return itemCount
}

// Get number of items in recycler view
fun <T : Activity>getNumberOfItemsInRecyclerView(
    TAG : String,
    LOG_ME : Boolean,
    activity : T,
    recyclerViewId : Int) : Int {
    val methodName: String = "getNumberOfItemsInRecyclerView"
    var itemCount : Int = -1
    if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
    try {
        itemCount = activity.findViewById<RecyclerView>(recyclerViewId).adapter?.itemCount!!
    } catch (e: Exception) {
        ALog.e(TAG, methodName, e)
    } finally {
        if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
    }
    return itemCount
}

// https://stackoverflow.com/questions/23381459/how-to-get-text-from-textview-using-espresso
fun <T : Espresso> KClass<T>.getTextViewText(matcher: Matcher<View?>?): String? {
    val stringHolder = arrayOf<String?>(null)
    onView(matcher).perform(object : ViewAction {
        override fun getConstraints(): Matcher<View> {
            return isAssignableFrom(TextView::class.java)
        }

        override fun getDescription(): String {
            return "getting text from a TextView"
        }

        override fun perform(uiController: UiController?, view: View) {
            val tv = view as TextView //Safe, because of check in getConstraints()
            stringHolder[0] = tv.text.toString()
        }
    })
    return stringHolder[0]
}

// https://stackoverflow.com/questions/39688297/how-to-check-if-a-view-is-visible-on-a-specific-recyclerview-item
fun <T : Espresso> KClass<T>.withViewAtPosition(
    position: Int,
    itemMatcher: Matcher<View?>
): Matcher<View?>? {
    return object : BoundedMatcher<View?, RecyclerView>(RecyclerView::class.java) {
        override fun describeTo(description: Description) {
            itemMatcher.describeTo(description)
        }

        override fun matchesSafely(recyclerView: RecyclerView): Boolean {
            val viewHolder = recyclerView.findViewHolderForAdapterPosition(position)
            return viewHolder != null && itemMatcher.matches(viewHolder.itemView)
        }
    }
}

// Check if view in recycler view is visible
fun <T : Espresso> KClass<T>.checkIfViewInRecyclerViewIsVisible(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    recyclerViewId : Int,
    itemPosition : Int,
    viewId : Int
) {
    if (LOG_ME) ALog.d(TAG, ".$methodName(): $actionDescription")
    Espresso.onView(withId(recyclerViewId))
        .check(matches(Espresso::class.withViewAtPosition(
            itemPosition,
            hasDescendant(
                (allOf(withId(viewId), isDisplayed()))
            )))
        )
}

// Wait for data to load into recycler view
// https://stackoverflow.com/questions/51851653/recyclerview-espresso-testing-fails-due-to-runtimeexception
fun <T : Espresso> KClass<T>.waitForDataToLoadIntoRecyclerView(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    recyclerViewId : Int,
) {
    if (LOG_ME) ALog.d(TAG, ".$methodName(): $actionDescription")
    onView(withId(recyclerViewId)).perform(
        hasItemCount(Matchers.greaterThan(0.toInt()))?.let { waitUntil(it) },
    )
}

fun <T : Espresso> KClass<T>.confirmRecyclerViewHasExpectedNumberOfItems(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    recyclerViewId : Int,
    expectedNumberOfItemsInRecyclerView: Int,
) {
    if (LOG_ME) ALog.d(TAG, ".$methodName(): $actionDescription")
    onView(withId(recyclerViewId)).perform(
        hasItemCount(Matchers.equalTo(expectedNumberOfItemsInRecyclerView))?.let { waitUntil(it) },
    )
}

fun hasItemCount(matcher: Matcher<Int?>): Matcher<View?>? {
    return object : BoundedMatcher<View?, RecyclerView>(RecyclerView::class.java) {
        override fun describeTo(description: Description) {
            description.appendText("has item count: ")
            matcher.describeTo(description)
        }

        override fun matchesSafely(view: RecyclerView): Boolean {
            return matcher.matches(view.adapter!!.itemCount)
        }
    }
}

fun waitUntil(matcher: Matcher<View?>): ViewAction? {
    return actionWithAssertions(object : ViewAction {
        override fun getConstraints(): Matcher<View> {
            return isAssignableFrom(View::class.java)
        }

        override fun getDescription(): String {
            val description = StringDescription()
            matcher.describeTo(description)
            return String.format("wait until: %s", description)
        }

        override fun perform(uiController: UiController, view: View) {
            if (!matcher.matches(view)) {
                val callback = LayoutChangeCallback(matcher)
                try {
                    IdlingRegistry.getInstance().register(callback)
                    view.addOnLayoutChangeListener(callback)
                    uiController.loopMainThreadUntilIdle()
                } finally {
                    view.removeOnLayoutChangeListener(callback)
                    IdlingRegistry.getInstance().unregister(callback)
                }
            }
        }
    })
}

private class LayoutChangeCallback internal constructor(private val matcher: Matcher<View?>) :
    IdlingResource, View.OnLayoutChangeListener {
    private var callback: IdlingResource.ResourceCallback? = null
    private var matched = false
    override fun getName(): String {
        return "Layout change callback"
    }

    override fun isIdleNow(): Boolean {
        return matched
    }

    override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback) {
        this.callback = callback
    }

    override fun onLayoutChange(
        v: View,
        left: Int,
        top: Int,
        right: Int,
        bottom: Int,
        oldLeft: Int,
        oldTop: Int,
        oldRight: Int,
        oldBottom: Int
    ) {
        matched = matcher.matches(v)
        callback!!.onTransitionToIdle()
    }
}

// Scrolling in NestedScrollView
// Reference: https://stackoverflow.com/questions/39642631/espresso-testing-nestedscrollview-error-performing-scroll-to-on-view-with
fun nestedScrollTo(): ViewAction? {
    return object : ViewAction {
        override fun getConstraints(): Matcher<View> {
            return Matchers.allOf(
                isDescendantOfA(isAssignableFrom(NestedScrollView::class.java)),
                withEffectiveVisibility(Visibility.VISIBLE)
            )
        }

        override fun getDescription(): String {
            return "View is not NestedScrollView"
        }

        override fun perform(uiController: UiController, view: View) {
            try {
                val nestedScrollView = findFirstParentLayoutOfClass(
                    view,
                    NestedScrollView::class.java
                ) as NestedScrollView?
                if (nestedScrollView != null) {
                    nestedScrollView.scrollTo(0, view.top)
                } else {
                    throw Exception("Unable to find NestedScrollView parent.")
                }
            } catch (e: Exception) {
                throw PerformException.Builder()
                    .withActionDescription(this.description)
                    .withViewDescription(HumanReadables.describe(view))
                    .withCause(e)
                    .build()
            }
            uiController.loopMainThreadUntilIdle()
        }
    }
}
private fun findFirstParentLayoutOfClass(view: View, parentClass: Class<out View>): View? {
    var parent: ViewParent = FrameLayout(view.context)
    var incrementView: ViewParent? = null
    var i = 0
    while (parent != null && parent.javaClass != parentClass) {
        parent = if (i == 0) {
            findParent(view)
        } else {
            findParent(incrementView)
        }
        incrementView = parent
        i++
    }
    return parent as View
}
private fun findParent(view: View): ViewParent {
    return view.parent
}
private fun findParent(view: ViewParent?): ViewParent {
    return view!!.parent
}

// Checks whether bottom sheet is in specified state
// Reference: https://stackoverflow.com/questions/63233538/is-there-an-espresso-check-for-the-state-of-a-bottomsheetbehavior
fun <T : ViewMatchers> KClass<T>.hasBottomSheetBehaviorState(expectedState: Int): Matcher<in View>? {
    return object : BoundedMatcher<View, View>(View::class.java) {
        override fun describeTo(description: Description) {
            description.appendText("has BottomSheetBehavior state $expectedState")
        }

        override fun matchesSafely(view: View): Boolean {
            val bottomSheetBehavior = BottomSheetBehavior.from(view)
            return expectedState == bottomSheetBehavior.state
        }
    }
}

fun <T : Espresso> KClass<T>.setDateInDatePicker(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    dayOfMonth : Int,
    monthOfYear : Int,
    year : Int,
) {
    if(LOG_ME) ALog.d(TAG, ".$methodName(): $actionDescription")
    try {
        val datePicker = onView(withClassName(Matchers.equalTo(DatePicker::class.java.name)))

        datePicker.perform(
            PickerActions.setDate(
                year,
                monthOfYear,
                dayOfMonth
            )
        )
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Date set")
        Thread.sleep(THREAD_SLEEP_TIME_AFTER_CLICKING)
    } catch(e : Exception) {
        ALog.e(TAG, methodName, e)
        e.printStackTrace()
    }
}

fun <T : Espresso> KClass<T>.setNumberInNumberPicker(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    number : Int,
) {
    if(LOG_ME) ALog.d(TAG, ".$methodName(): $actionDescription")
    try {
        val numPicker = onView(
            withClassName(
                Matchers.equalTo(
                    NumberPicker::class.java.name
                )
            )
        )
        numPicker.perform(setNumber(number))

        if(LOG_ME) ALog.d(TAG, ".$methodName(): Number set")
        Thread.sleep(THREAD_SLEEP_TIME_AFTER_CLICKING)
    } catch(e : Exception) {
        ALog.e(TAG, methodName, e)
//        e.printStackTrace()
    }
}
private fun setNumber(num: Int): ViewAction? {
    return object : ViewAction {
        override fun perform(uiController: UiController?, view: View) {
            val np = view as NumberPicker
            np.value = num
        }

        override fun getDescription(): String {
            return "Set the passed number into the NumberPicker"
        }

        override fun getConstraints(): Matcher<View> {
            return isAssignableFrom(NumberPicker::class.java)
        }
    }
}



// https://www.youtube.com/watch?v=CpQQ6-vo5bw&list=PLgCYzUzKIBE_ZuZzgts135GuLQNX5eEPk&index=11
// https://developer.android.com/training/testing/espresso/intents
fun <T : Espresso> KClass<T>.testChoosingPictureFromGallery(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    pickPictureViewId : Int,
    timesGalleryIntentHasBeenCalledInTest : Int = 0,
) {
    if(LOG_ME) ALog.d(TAG, ".$methodName(): $actionDescription")
    try {
        // -Intent { act=android.intent.action.GET_CONTENT cat=[android.intent.category.OPENABLE] typ=image/* }
        // handling packages:[[com.android.documentsui, com.google.android.apps.docs, com.google.android.apps.photos]])
        val expectedIntent: Matcher<Intent> = allOf(
            hasAction(Intent.ACTION_PICK),
            IntentMatchers.hasData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        )
        val activityResult = createGalleryPickActivityResultStub(TAG, LOG_ME)
        intending(expectedIntent).respondWith(activityResult)

        onView(withId(pickPictureViewId)).perform(click())
        intended(expectedIntent, times(timesGalleryIntentHasBeenCalledInTest+1))
    } catch(e : Exception) {
        ALog.e(TAG, methodName, e)
    }
}
private const val GALLERY_PICK_TEST_DRAWABLE_ID = R.drawable.ic_launcher_background
private const val TEST_IMAGE_URI_STRING = "content://com.android.providers.media.documents/document/image%3A27"
private fun createGalleryPickActivityResultStub(
    TAG : String,
    LOG_ME : Boolean,
): Instrumentation.ActivityResult {
    val methodName: String = "createGalleryPickActivityResultStub"
    if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
    val resultIntent = Intent()
    try {
        val resources: Resources = InstrumentationRegistry.getInstrumentation().context.resources
        val imageUri = Uri.parse(TEST_IMAGE_URI_STRING)

        if(LOG_ME)ALog.d(TAG, ".$methodName(): imageUri == $imageUri")
        resultIntent.data = imageUri
    } catch (e: Exception) {
        ALog.e(TAG, methodName, e)
    } finally {
        if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
    }
    return Instrumentation.ActivityResult(RESULT_OK, resultIntent)
}

// https://medium.com/@miloszlewandowski/espresso-matcher-for-imageview-made-easy-with-android-ktx-977374ca3391
// https://medium.com/@dbottillo/android-ui-test-espresso-matcher-for-imageview-1a28c832626f
// https://www.youtube.com/watch?v=xxO0Z9GzcBU
fun <T : Espresso> KClass<T>.confirmImageViewHasDrawable(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    imageViewId : Int,
    drawableId : Int = GALLERY_PICK_TEST_DRAWABLE_ID,
) {
    if(LOG_ME) ALog.d(TAG, ".$methodName(): $actionDescription")
    try {
        onView(withId(imageViewId))
            .check(matches(isDisplayed()))
            .check(matches(withDrawable(TAG, LOG_ME, drawableId, tint = null)))
    } catch(e : Exception) {
        ALog.e(TAG, methodName, e)
    }
}

private fun withDrawable(
    TAG : String,
    LOG_ME : Boolean,
    @DrawableRes id: Int,
    @ColorRes tint: Int? = null,
    tintMode: PorterDuff.Mode = PorterDuff.Mode.SRC_IN,
) = object : TypeSafeMatcher<View>() {
    override fun describeTo(description: Description) {
        description.appendText("ImageView with drawable same as drawable with id $id")
        tint?.let { description.appendText(", tint color id: $tint, mode: $tintMode") }
    }

    override fun matchesSafely(view: View): Boolean {
        val methodName: String = "matchesSafely"
        if(LOG_ME) ALog.d(TAG, "Method start: $methodName")
        return try {
            val context = view.context
            val tintColor = tint?.toColor(context)
            val expectedBitmap = context.getDrawable(id)!!.tinted(tintColor, tintMode).toBitmap()

            view is ImageView && view.drawable.toBitmap().sameAs(expectedBitmap)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            false
        } finally {
            if(LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }
}
private fun Int.toColor(context: Context) = ContextCompat.getColor(context, this)
private fun Drawable.tinted(
    @ColorInt tintColor: Int? = null,
    tintMode: PorterDuff.Mode = PorterDuff.Mode.SRC_IN
) =
    apply {
        setTintList(tintColor?.toColorStateList())
        setTintMode(tintMode)
    }
private fun Int.toColorStateList() = ColorStateList.valueOf(this)

fun <T : Espresso> KClass<T>.confirmImageViewHasImage(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    imageViewId : Int,
) {
    try {
        if(LOG_ME) ALog.d(TAG, ".$methodName(): $actionDescription")
        onView(withId(imageViewId))
            .check(matches(isDisplayed()))
            .check(matches(withImage(TAG, LOG_ME)))
    } catch(e : Exception) {
        ALog.e(TAG, methodName, e)
    }
}
private fun withImage(
    TAG : String,
    LOG_ME : Boolean,
) = object : TypeSafeMatcher<View>() {
    override fun describeTo(description: Description) {
        description.appendText("ImageView with drawable ")
    }

    override fun matchesSafely(view: View): Boolean {
        val methodName: String = "matchesSafely"
        if(LOG_ME) ALog.d(TAG, "Method start: $methodName")
        return try {

            view is ImageView && view.drawable != null

        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            false
        } finally {
            if(LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }
}

fun <T : Espresso> KClass<T>.confirmImageViewHasImageFromUri(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    imageViewId : Int,
    imageUriString : String = TEST_IMAGE_URI_STRING,
) {
    if(LOG_ME) ALog.d(TAG, ".$methodName(): $actionDescription")
    try {
        onView(withId(imageViewId))
            .check(matches(isDisplayed()))
            .check(matches(withImageFromUri(TAG, LOG_ME, imageUriString, tint = null)))
    } catch(e : Exception) {
        ALog.e(TAG, methodName, e)
    }
}
private fun withImageFromUri(
    TAG : String,
    LOG_ME : Boolean,
    imageUriString: String = TEST_IMAGE_URI_STRING,
    @ColorRes tint: Int? = null,
    tintMode: PorterDuff.Mode = PorterDuff.Mode.SRC_IN,
) = object : TypeSafeMatcher<View>() {
    override fun describeTo(description: Description) {
        description.appendText("ImageView with drawable same as drawable with id $imageUriString")
        tint?.let { description.appendText(", tint color id: $tint, mode: $tintMode") }
    }

    override fun matchesSafely(view: View): Boolean {
        val methodName: String = "matchesSafely"
        if(LOG_ME) ALog.d(TAG, "Method start: $methodName")
        return try {
            val context = InstrumentationRegistry.getInstrumentation().targetContext

            val expectedBitmap = getBitmapFromUri(Uri.parse(imageUriString), context)
            if(LOG_ME)ALog.d(TAG, ".$methodName(): $expectedBitmap")
            view is ImageView && view.drawable.toBitmap().sameAs(expectedBitmap)

        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            false
        } finally {
            if(LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }
    @Throws(IOException::class)
    private fun getBitmapFromUri(uri: Uri, context : Context): Bitmap? {
        val contentResolver = context.contentResolver

        val bitmapInputStream = contentResolver.openInputStream(uri)
        val bitmap = BitmapFactory.decodeStream(contentResolver.openInputStream(uri))
        bitmapInputStream?.close()
        return bitmap
    }
}

// https://stackoverflow.com/questions/63546419/espresso-intents-matchers-and-stubbing
fun <T : Espresso> KClass<T>.testEmailIntent(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    idOfTheViewToClickToStartIntent : Int,
    recipientEmail : String?,
    emailMessage : String?,
    emailSubject : String?,
) {
    if(LOG_ME) ALog.d(TAG, ".$methodName(): $actionDescription")
    try {

        intending(CoreMatchers.not(IntentMatchers.isInternal())).respondWith(
            Instrumentation.ActivityResult(
                Activity.RESULT_OK,
                null
            )
        )

        clickOnView(
            "Clicking on view to start email intent",
            TAG,
            LOG_ME,
            methodName,
            idOfTheViewToClickToStartIntent,
        )

        val matchers = mutableListOf<org.hamcrest.Matcher<android.content.Intent>>()
        matchers.add(hasAction(Intent.ACTION_SENDTO))
//        matchers.add(hasData(Uri.parse("mailto:")))
        if(emailMessage != null) {
            if(LOG_ME)ALog.d(TAG, ".$methodName(): Adding matcher for emailMessage == $emailMessage")
            matchers.add(
                hasExtra(
                    `is`(Intent.EXTRA_TEXT),
                    `is`(emailMessage)
                )
            )
        }
        if(recipientEmail != null) {
            if(LOG_ME)ALog.d(TAG, ".$methodName(): Adding matcher for recipientEmail == $recipientEmail")
            matchers.add(
                hasExtra(
                    `is`(Intent.EXTRA_EMAIL),
                    `is`(arrayOf(recipientEmail))
                )
            )
        }
        if(emailSubject != null) {
            if(LOG_ME)ALog.d(TAG, ".$methodName(): Adding matcher for emailSubject == $emailSubject")
            matchers.add(
                hasExtra(
                    `is`(Intent.EXTRA_TITLE),
                    `is`(arrayOf(emailSubject))
                )
            )
        }
        val expectedIntent = allOf(
            hasAction(Intent.ACTION_CHOOSER),
            hasExtra(
                equalTo(Intent.EXTRA_INTENT),
                allOf(
                    matchers
                )
            )
        )

        intended(expectedIntent)

//        Intents.release()

    } catch(e : Exception) {
        ALog.e(TAG, methodName, e)
    }
}

//
fun <T : Espresso> KClass<T>.testPhoneIntent(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    idOfTheViewToClickToStartIntent : Int,
    expectedPhoneNumber : String,
) {
    if(LOG_ME) ALog.d(TAG, ".$methodName(): $actionDescription")
    try {

        intending(CoreMatchers.not(IntentMatchers.isInternal())).respondWith(
            Instrumentation.ActivityResult(
                Activity.RESULT_OK,
                null
            )
        )

        clickOnView(
            "Clicking on view to start phone intent",
            TAG,
            LOG_ME,
            methodName,
            idOfTheViewToClickToStartIntent,
        )

        val assertionErrors = mutableListOf<AssertionFailedError>()
        try {
            if(LOG_ME)ALog.d(
                TAG,
                ".$methodName(): Checking if ACTION_CALL intent with " +
                        "expectedPhoneNumber == $expectedPhoneNumber, was launched."
            )
            intended(allOf(
                hasAction(Intent.ACTION_CALL),
                hasData(expectedPhoneNumber)))
        } catch (e : AssertionFailedError) {
            assertionErrors.add(e)
        }
        try {
            if(LOG_ME)ALog.d(
                TAG,
                ".$methodName(): Checking if ACTION_DIAL intent with " +
                        "expectedPhoneNumber == $expectedPhoneNumber, was launched."
            )
            intended(allOf(
                hasAction(Intent.ACTION_DIAL),
                hasData(expectedPhoneNumber)))
        } catch (e : AssertionFailedError) {
            assertionErrors.add(e)
        }
        if(assertionErrors.isNotEmpty()) {
            if(assertionErrors.size == 2) {
                ALog.e(TAG, ".$methodName(): Detected ${assertionErrors.size} assertion errors: ")
                for(assertionError in assertionErrors) {
                    ALog.e(TAG, ".$methodName(): ${assertionError.message}")
                }
                throw assertionErrors[0]
            }
        }

//        Intents.release()

    } catch(e : Exception) {
        ALog.e(TAG, methodName, e)
    }
}



fun <T : Espresso> KClass<T>.setRateInRatingBar(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    ratingBarID: Int,
    rating: Float,
) {
    if(LOG_ME) ALog.d(TAG, ".$methodName(): $actionDescription")
    ALog.w(TAG, ".$methodName(): Warning!!! setRateInRatingBar() Does not" +
            "fire up OnRatingBarChangeListener. Take this into consideration during test.")
    try {
        try {
            onView(withId(ratingBarID)).perform(scrollTo())
        } catch(e: Exception) {}
        onView(withId(ratingBarID)).perform(SetRatingInRatingBarProgrammatically(rating))
    } catch(e : Exception) {
        ALog.e(TAG, methodName, e)
    }
}
// Reference: https://stackoverflow.com/questions/25209508/how-to-set-a-specific-rating-on-ratingbar-in-espresso
private fun setRatingByTouch(rating: Float): ViewAction? {
    require(rating % 0.5 == 0.0) { "Rating must be multiple of 0.5f" }
    return object : ViewAction {
        override fun getConstraints(): Matcher<View> {
            return isAssignableFrom(RatingBar::class.java)
        }

        override fun getDescription(): String {
            return "Set rating on RatingBar in 0.5f increments"
        }

        override fun perform(uiController: UiController?, view: View?) {
            val viewAction = GeneralClickAction(
                Tap.SINGLE,
                { view ->
                    val locationOnScreen = IntArray(2)
                    view.getLocationOnScreen(locationOnScreen)
                    val screenX = locationOnScreen[0]
                    val screenY = locationOnScreen[1]
                    val numStars = (view as RatingBar).numStars
                    val widthPerStar = 1f * view.getWidth() / numStars
                    val percent = rating / numStars
                    val x = screenX + view.getWidth() * percent
                    val y = screenY + view.getHeight() * 0.5f
                    floatArrayOf(x - widthPerStar * 0.5f, y)
                },
                Press.FINGER,
                InputDevice.SOURCE_UNKNOWN,
                MotionEvent.BUTTON_PRIMARY
            )
            viewAction.perform(uiController, view)
        }
    }
}

private class SetRatingInRatingBarProgrammatically(val rating: Float) : ViewAction {
    override fun getConstraints(): Matcher<View> {
        return isAssignableFrom(
            RatingBar::class.java
        )
    }

    override fun getDescription(): String {
        return "Custom view action to set rating."
    }

    override fun perform(uiController: UiController?, view: View) {
        val ratingBar = view as RatingBar
        ratingBar.rating = rating
    }
}

fun <T : Espresso> KClass<T>.checkIfActivityHasOpenedDialogs(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    activity: FragmentActivity,
) {
    if(LOG_ME) ALog.d(TAG, ".$methodName(): $actionDescription")
    val fragments: List<Fragment> = activity.supportFragmentManager.fragments
    var result = false
    if (fragments != null) {
        for (fragment in fragments) {
            if(LOG_ME)ALog.d(TAG, ".$methodName(): fragment == $fragment")
            if (fragment is DialogFragment) {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): DialogFragment found!")
                result = true
            }
        }
    }
    assertTrue(result)
}

// https://stackoverflow.com/questions/24517291/get-current-activity-in-espresso-android
suspend fun <T : Espresso> KClass<T>.getCurrentActivity(): Activity? {
    var currentActivity: Activity? = null
    withContext(Dispatchers.Main) {
        val resumedActivities: Collection<*> =
            ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(Stage.RESUMED)
        if (resumedActivities.iterator().hasNext()) {
            currentActivity = resumedActivities.iterator().next() as Activity?
            ALog.d("CommonEspressoActions", ".getCurrentActivity(): currentActivity == $currentActivity")
        }
    }
    return currentActivity
}