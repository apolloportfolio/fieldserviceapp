package com.aps.catemplateapp.core.di

import com.aps.catemplateapp.core.util.SecureKeyStorage
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton
import okhttp3.Request
import com.aps.catemplateapp.feature02.framework.datasources.networkdatasources.retrofit2.daos.Entity1DaoRetrofit2
import javax.inject.Named

@InstallIn(SingletonComponent::class)
@Module
class Retrofit2Module {
    // TODO: Add provide methods for other Retrofit2 DAOs as needed.
    @Provides
    @Singleton
    @Named("Retrofit2ClientForEntity1DaoRetrofit2")
    fun provideRetrofit2ClientForEntity1DaoRetrofit2(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(Entity1DaoRetrofit2.RETROFIT2_BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(secureKeyStorage: SecureKeyStorage): OkHttpClient {
        val API_KEY = secureKeyStorage.getRetrofit2APIKey()
        return OkHttpClient.Builder().addInterceptor { chain ->
            val original: Request = chain.request()
            val requestBuilder: Request.Builder = original.newBuilder()
                .header("Authorization", "Bearer $API_KEY")
            val request: Request = requestBuilder.build()
            chain.proceed(request)
        }.build()
    }
}