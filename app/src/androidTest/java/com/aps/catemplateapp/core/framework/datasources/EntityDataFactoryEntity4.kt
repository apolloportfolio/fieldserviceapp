package com.aps.catemplateapp.core.framework.datasources

import android.app.Application
import com.aps.catemplateapp.feature02.business.domain.model.entities.Entity4
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import javax.inject.Inject

class EntityDataFactoryEntity4
@Inject
constructor(
    override val application: Application
): EntityDataFactory<Entity4>(application) {
//    override val testClassLoader: ClassLoader,
//    override val fileNameWithTestData: String): com.aps.catemplateapp.core.framework.datasources.EntityDataFactory<Entity4>(testClassLoader, fileNameWithTestData) {


    override fun produceListOfEntities(): List<Entity4> {
        return Gson()
            .fromJson(
                getEntitiesFromFile("entity4_test_list.json"),
                object : TypeToken<List<Entity4>>() {}.type
            )
    }
}