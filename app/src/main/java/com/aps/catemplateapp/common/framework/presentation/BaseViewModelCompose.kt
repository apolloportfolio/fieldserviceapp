package com.aps.catemplateapp.common.framework.presentation

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.test.core.app.ApplicationProvider
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.util.GenericErrors
import com.aps.catemplateapp.common.business.domain.state.AreYouSureCallback
import com.aps.catemplateapp.common.business.domain.state.DataChannelManager
import com.aps.catemplateapp.common.business.domain.state.DataState
import com.aps.catemplateapp.common.business.domain.state.MessageType
import com.aps.catemplateapp.common.business.domain.state.OkButtonCallback
import com.aps.catemplateapp.common.business.domain.state.Response
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.common.business.domain.state.StateMessage
import com.aps.catemplateapp.common.business.domain.state.UIComponentType
import com.aps.catemplateapp.common.business.domain.state.ViewState
import com.aps.catemplateapp.common.framework.presentation.views.DialogState
import com.aps.catemplateapp.common.framework.presentation.views.ProgressIndicatorState
import com.aps.catemplateapp.common.framework.presentation.views.SnackBarState
import com.aps.catemplateapp.common.framework.presentation.views.ToastState
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.TodoCallback
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

private const val TAG = "BaseViewModelCompose"
private const val LOG_ME = true

@FlowPreview
@ExperimentalCoroutinesApi
abstract class BaseViewModelCompose<EntityType, EntityViewState : ViewState<EntityType>> : ViewModel()
{
    val _viewState: MutableLiveData<EntityViewState> = MutableLiveData()

    val viewState: LiveData<EntityViewState>
        get() = _viewState

    abstract fun getViewStateCopy(): EntityViewState

    abstract fun postUpdateViewStateWithCopy(viewStateToCopy: EntityViewState): EntityViewState

    fun setViewState(viewState: EntityViewState, setterMethodName: String = "unknownMethod"){
//        if(LOG_ME)ALog.d(TAG, "setViewState(): $setterMethodName(): " +
//                                "Changing _viewState.value from " +
//                "${_viewState.value} to $viewState")
        _viewState.value = viewState
    }

    fun postViewState(viewState: EntityViewState, setterMethodName: String = "unknownMethod"){
        if(LOG_ME)ALog.d(TAG, ".postViewState(): $setterMethodName(): " +
                "Changing _viewState.value from ${System.identityHashCode(_viewState.value)} to " +
                "${System.identityHashCode(viewState)}")
        _viewState.postValue(viewState)
    }

    fun getCurrentViewStateOrNew(): EntityViewState{
        if(_viewState.value == null) {
            if(LOG_ME)ALog.d(TAG, ".getCurrentViewStateOrNew(): " +
                    "_viewState.value == null creating a new instance")
            _viewState.value = createNewViewState()
        }
        return _viewState.value!!
    }

    fun getCurrentViewStateOrNewAsLiveData(): LiveData<EntityViewState>{
        if(_viewState.value == null) {
            if(LOG_ME)ALog.d(TAG, ".getCurrentViewStateOrNewAsLiveData(): " +
                    "_viewState.value == null creating a new instance")
            _viewState.value = createNewViewState()
            if(LOG_ME)ALog.d(TAG, ".getCurrentViewStateOrNewAsLiveData(): " +
                    "_viewState.value == ${_viewState.value}")
        }
        return _viewState
    }

    fun notifyStateEventStarting(stateEvent: StateEvent) {
        if (LOG_ME) ALog.d(TAG,
            "notifyStateEventStarting(): Adding stateEvent: ${stateEvent.eventName()}")
        val updatedViewState = getViewStateCopy()
        updatedViewState.stateEventTracker.notifyStateEventStarting(stateEvent)
        setViewState(updatedViewState, "notifyStateEventStarting")
    }

    fun notifyStateEventEnded(stateEvent: StateEvent) {
        if (LOG_ME) ALog.d(TAG,
            "notifyStateEventEnded(): Removing stateEvent: ${stateEvent.eventName()}")
        val updatedViewState = getViewStateCopy()
        updatedViewState.stateEventTracker.notifyStateEventEnded(stateEvent)
        setViewState(updatedViewState, "notifyStateEventStarting")
    }

    fun setIsDrawerOpen(isDrawerOpenP: Boolean?) {
        if(LOG_ME)ALog.d(TAG, "setIsDrawerOpen" +
                "Setting isDrawerOpen to $isDrawerOpenP")
        val updatedViewState = getViewStateCopy()
        updatedViewState.isDrawerOpen = isDrawerOpenP
        setViewState(updatedViewState, "setIsDrawerOpen")
    }

    fun dismissPermissionDialog(permission: String) {
        if(LOG_ME) ALog.d(
            TAG_PERMISSION_ASKING+TAG, "dismissPermissionDialog(): \n" +
                    "Dismissing permission dialog for: $permission"
        )
        val updatedViewState = getViewStateCopy()
        updatedViewState.permissionHandlingData.permissionsToRequestWithDialogs.remove(permission)
        setViewState(updatedViewState, "dismissPermissionDialog")
    }

    fun addGrantedAndDeniedPermissions(
        grantedPermissions: MutableSet<String>,
        deniedPermissions: MutableSet<String>,
    ) {
        if(LOG_ME) ALog.d(
            TAG_PERMISSION_ASKING+TAG, "addGrantedAndDeniedPermissions(): \n" +
                    "Adding grantedPermissions: ${grantedPermissions.joinToString()}\n" +
                    "Adding deniedPermissions: ${deniedPermissions.joinToString()}"
        )
        val updatedViewState = getViewStateCopy()
        updatedViewState.permissionHandlingData.permissionsToRequestWithDialogs.addAll(
            deniedPermissions
        )
        updatedViewState.permissionHandlingData.grantedPermissions.addAll(
            grantedPermissions
        )
        setViewState(updatedViewState, "addGrantedAndDeniedPermissions")
    }

    fun addRequiredPermissions(
        activity: Activity?,
        permissionsToAdd: MutableSet<String>,
    ) {
        if(activity == null) {
            if(LOG_ME) ALog.d(
                TAG_PERMISSION_ASKING+TAG, "addRequiredPermissions(): " +
                        "Can't add permissions because activity == null\n" +
                        "permissionsToAdd == ${permissionsToAdd.joinToString()}"
            )
            return
        }
        if(LOG_ME) ALog.d(
            TAG_PERMISSION_ASKING+TAG, "addRequiredPermissions(): " +
                    "Adding required permissions: ${permissionsToAdd.joinToString()}"
        )
        val newNotGrantedButRequiredPermissions: MutableSet<String> = mutableSetOf()
        for(permission in permissionsToAdd) {
            if(!permissionIsGranted(activity, permission)) {
                newNotGrantedButRequiredPermissions.add(permission)
            }
        }
        if(newNotGrantedButRequiredPermissions.isNotEmpty()) {
            if(LOG_ME)ALog.d(TAG_PERMISSION_ASKING+TAG, "addRequiredPermissions(): " +
                    "There are permissions that require granting:" +
                    newNotGrantedButRequiredPermissions.joinToString())
            val updatedViewState = getViewStateCopy()

            val updatedPermissionHandlingData = updatedViewState.permissionHandlingData.copy()
            updatedPermissionHandlingData.apply {
                requiredPermissions = mutableSetOf()
                requiredPermissions.addAll(
                    newNotGrantedButRequiredPermissions
                )
            }
            updatedViewState.permissionHandlingData = updatedPermissionHandlingData
            setViewState(updatedViewState, TAG_PERMISSION_ASKING+"addRequiredPermissions")
        } else {
            if(LOG_ME)ALog.d(TAG_PERMISSION_ASKING+TAG, "addRequiredPermissions(): " +
                    "All permissions are granted.")
        }
    }

    fun permissionIsGranted(
        activity: Activity?,
        permission: String,
    ): Boolean {
        if(activity == null) {
            if(LOG_ME)ALog.w(TAG_PERMISSION_ASKING+TAG, "permissionIsGranted(): " +
                    "activity == null")
            return false
        }

        return ContextCompat.checkSelfPermission(
            activity,
            permission,
        ) == PackageManager.PERMISSION_GRANTED
    }

    fun setSnackBarState(snackBarState: SnackBarState?) {
        if(LOG_ME) ALog.d(
            TAG, "setSnackBarState(): " +
                    "Setting snackBarStatus to ${snackBarState?.message}, " +
                    "visibility == ${snackBarState?.show}"
        )
        val updatedViewState = getViewStateCopy()
        updatedViewState.snackBarState = snackBarState?.apply {
            updateSnackBarInViewModel = { state ->
                setSnackBarState(state)
            }
        }
//        this._viewState.value = updatedViewState
        setViewState(updatedViewState, "setSnackBarState")
    }

    fun setToastState(toastState: ToastState?) {
        if(LOG_ME)ALog.d(TAG, "setToastState(): " +
                "Before init: _viewState.value == ${_viewState.value}")

        val updatedViewState = getViewStateCopy()

        if(LOG_ME) {
            ALog.d(TAG, "setToastState(): \n" +
                    "Setting toastState from \n${updatedViewState.toastState} " +
                    "to \n$toastState\n" +
                    "After init: \n" +
                    "_viewState.value == ${_viewState.value}\n" +
                    "updatedViewState == $updatedViewState\n")
        }

        toastState!!.updateToastInViewModel = { state ->
            setToastState(state)
        }
        updatedViewState.toastState = toastState

        if(LOG_ME) {
            ALog.d(TAG, "setToastState(): " +
                    "Before change: \n" +
                    "_viewState.value == ${_viewState.value}\n" +
                    "_viewState.value === updatedViewState is " +
                    "${_viewState.value === updatedViewState} \n" +
                    "_viewState.value.hashCode() == updatedViewState.hashCode() is " +
                    "${_viewState.value.hashCode() == updatedViewState.hashCode()}\n")
        }

        setViewState(updatedViewState, "setToastState")

        if(LOG_ME)ALog.d(TAG, "setToastState(): " +
                "After change: _viewState.value == ${_viewState.value}")
    }

    fun setDialogState(dialogState: DialogState?) {
        if(LOG_ME) ALog.d(
            TAG, "setDialogState(): " +
                    "Setting dialogState to ${dialogState?.title}, " +
                    "visibility == ${dialogState?.show}"
        )
        val updatedViewState = getViewStateCopy()
        updatedViewState.dialogState = dialogState?.apply {
            updateDialogInViewModel = { state ->
                setDialogState(state)
            }
        }
        setViewState(updatedViewState, "setDialogState")
    }

    fun setProgressIndicatorState(progressIndicatorState: ProgressIndicatorState?) {
        if(LOG_ME) ALog.d(
            TAG, "setProgressIndicatorState(): " +
                    "Setting progressIndicatorState to $progressIndicatorState"
        )
        val updatedViewState = getViewStateCopy()
        updatedViewState.progressIndicatorState = progressIndicatorState?.apply {
            updateProgressIndicatorStateInViewModel = { state ->
                setProgressIndicatorState(state)
            }
        }
        setViewState(updatedViewState, "setProgressIndicatorState")
    }



    val dataChannelManager: DataChannelManager<EntityViewState>
            = object: DataChannelManager<EntityViewState>(){

        override fun handleNewData(data: EntityViewState) {
            if(LOG_ME) ALog.d(TAG, "DataChannelManager.handleNewData(): Method start")
            this@BaseViewModelCompose.handleNewData(data)
            if(LOG_ME) ALog.d(TAG, "DataChannelManager.handleNewData(): Method end")
        }
    }

    val shouldDisplayProgressBar: LiveData<Boolean>
            = dataChannelManager.shouldDisplayProgressBar

    val stateMessage: LiveData<StateMessage?>
        get() = dataChannelManager.messageStack.stateMessage

    // FOR DEBUGGING
    fun getMessageStackSize(): Int{
        return dataChannelManager.messageStack.size
    }

    fun setupChannel() = dataChannelManager.setupChannel()

    abstract fun handleNewData(data: EntityViewState)

    abstract fun setStateEvent(stateEvent: StateEvent)

//    abstract fun setSnackBarState(snackBarState: SnackBarState?)
//
//    abstract fun setToastState(toastState: ToastState?)
//
//    abstract fun setDialogState(dialogState: DialogState?)

    fun emitStateMessageEvent(
        stateMessage: StateMessage,
        stateEvent: StateEvent
    ) = flow {
        emit(
            DataState.error<EntityViewState>(
                response = stateMessage.response,
                stateEvent = stateEvent
            )
        )
    }

    fun emitInvalidStateEvent(stateEvent: StateEvent) = flow {
        emit(
            DataState.error<EntityViewState>(
                response = Response(
                    messageId = R.string.error,
                    message = GenericErrors.INVALID_STATE_EVENT,
                    uiComponentType = UIComponentType.None(),
                    messageType = MessageType.Error()
                ),
                stateEvent = stateEvent
            )
        )
    }

    fun launchJob(
        stateEvent: StateEvent,
        jobFunction: Flow<DataState<EntityViewState>?>,
        launchJob: Boolean = true
    ) {
        if(LOG_ME) ALog.d(
            TAG,
            "launchJob(): Trying to launch job in dataChannelManager    launchJob == $launchJob"
        )
        if(launchJob)dataChannelManager.launchJob(stateEvent, jobFunction, false)
    }

    fun clearStateMessage(index: Int = 0){
        if(LOG_ME) ALog.d(
            TAG, ".clearStateMessage(): " +
                    "clearStateMessage"
        )
        dataChannelManager.clearStateMessage(index)
    }

    fun clearActiveStateEvents() = dataChannelManager.clearActiveStateEventCounter()

    fun clearAllStateMessages() = dataChannelManager.clearAllStateMessages()

    fun printStateMessages() = dataChannelManager.printStateMessages()

    fun cancelActiveJobs() = dataChannelManager.cancelJobs()

    abstract fun createNewViewState(): EntityViewState

    fun getIsUpdatePending(): Boolean{
        return getCurrentViewStateOrNew().isUpdatePending ?: false
    }

    fun setIsUpdatePending(isPending: Boolean) {
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.isUpdatePending = isPending
//        setViewState(update)
        setViewState(getViewStateCopy().apply { isUpdatePending = isPending }, "setIsUpdatePending")
    }

    open fun setMainEntity(entity: EntityType?){
//        val update = getCurrentViewStateOrNew()
//        update.mainEntity = entity
//        setViewState(update)
        setViewState(getViewStateCopy().apply { mainEntity = entity }, "setListOfEntities")
    }

    open fun setIsInternetAvailable(available: Boolean?){
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.isInternetAvailable = isInternetAvailable
//        setViewState(update)
        setViewState(getViewStateCopy().apply { isInternetAvailable = available }, "setIsInternetAvailable")
    }

    open fun postIsInternetAvailable(available: Boolean){
        if(LOG_ME)ALog.d(TAG, ".postIsInternetAvailable(): " +
                "Setting isInternetAvailable to $available")
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.isInternetAvailable = available
//        postViewState(update)
        postViewState(getViewStateCopy().apply { isInternetAvailable = available })
    }

    open fun setListOfEntities(list: List<EntityType>?){
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.listOfEntities = listOfEntities
//        setViewState(update)
        setViewState(getViewStateCopy().apply { listOfEntities = list }, "setListOfEntities")
    }

    protected fun emitDoNothingStateMessageEvent(stateEvent: StateEvent): Flow<DataState<EntityViewState>> {
        return emitStateMessageEvent(
            stateMessage = StateMessage(
                response = Response(
                    messageId = R.string.empty_string,
                    message = (ApplicationProvider.getApplicationContext() as Context).getString(R.string.empty_string),
                    uiComponentType = UIComponentType.None(),
                    messageType = MessageType.Info()
                )
            ),
            stateEvent = stateEvent
        )
    }


    @Deprecated(
        message = "State message events are for legacy code with Views.",
        replaceWith = ReplaceWith("setToastState()")
    )
    protected fun emitToastStateMessageEvent(
        stateEvent: StateEvent,
        messageID : Int,
    ): Flow<DataState<EntityViewState>> {
        return emitStateMessageEvent(
            stateMessage = StateMessage(
                response = Response(
                    messageId = messageID,
                    message = (ApplicationProvider.getApplicationContext() as Context).getString(
                        messageID
                    ),
                    uiComponentType = UIComponentType.Toast(),
                    messageType = MessageType.Info()
                )
            ),
            stateEvent = stateEvent
        )
    }


    @Deprecated(
        message = "State message events are for legacy code with Views.",
        replaceWith = ReplaceWith("setDialogState()")
    )
    protected fun emitQuestionDialogStateMessageEvent(
        stateEvent: StateEvent,
        titleID: Int,
        messageID : Int,
        yesCallback: AreYouSureCallback,
    ): Flow<DataState<EntityViewState>> {
        return emitStateMessageEvent(
            stateMessage = StateMessage(
                response = Response(
                    messageId = messageID,
                    message = (ApplicationProvider.getApplicationContext() as Context).getString(
                        messageID
                    ),
                    uiComponentType = UIComponentType.AreYouSureDialog(titleID, yesCallback),
                    messageType = MessageType.Info()
                )
            ),
            stateEvent = stateEvent
        )
    }

    @Deprecated(
        message = "State message events are for legacy code with Views.",
        replaceWith = ReplaceWith("showOkDialogWithStateMessageEvent()")
    )
    protected fun emitOkDialogStateMessageEvent(
        stateEvent: StateEvent,
        titleID: Int,
        messageID : Int,
        okCallback: OkButtonCallback,
    ): Flow<DataState<EntityViewState>> {
        return emitStateMessageEvent(
            stateMessage = StateMessage(
                response = Response(
                    messageId = messageID,
                    message = (ApplicationProvider.getApplicationContext() as Context).getString(
                        messageID
                    ),
                    uiComponentType = UIComponentType.Dialog(okCallback, titleID),
                    messageType = MessageType.Info()
                )
            ),
            stateEvent = stateEvent
        )
    }

    protected fun showOkDialogWithStateMessageEvent(
        stateEvent: StateEvent,
        context: Context,
        titleID: Int,
        messageID : Int,
        okCallback: OkButtonCallback,
        ): Flow<DataState<EntityViewState>> {
        setDialogState(DialogState(
            show = true,
            onDismissRequest = {},
            title = context.getString(titleID),
            text = context.getString(messageID),
            leftButtonText = null,
            leftButtonOnClick = {},
            rightButtonText = context.getString(R.string.text_ok),
            rightButtonOnClick = {
                okCallback.proceed()
            },
            updateDialogInViewModel = {
                setDialogState(it)
            }
        ))
        return emitDoNothingStateMessageEvent(stateEvent)
    }


    @Deprecated(
        message = "State message events are for legacy code with Views.",
        replaceWith = ReplaceWith("setSnackBarState()")
    )
    protected fun emitSnackbarStateMessageEvent(
        stateEvent: StateEvent,
        onDismissCallback: TodoCallback?,
        actionTextId: Int,
        actionTextString: String? = null,
        actionOnClickListener: View.OnClickListener = View.OnClickListener {
            if(LOG_ME) ALog.d(TAG, ".emitSnackbarStateMessageEvent(): Snackbar clicked.")
        }
    ): Flow<DataState<EntityViewState>> {
        val message = (ApplicationProvider.getApplicationContext() as Context).getString(actionTextId)
        return emitStateMessageEvent(
            stateMessage = StateMessage(
                response = Response(
                    messageId = actionTextId,
                    message = message,
                    uiComponentType = UIComponentType.SnackBar(
                        onDismissCallback,
                        actionTextId,
                        actionTextString,
                        actionOnClickListener,
                    ),
                    messageType = MessageType.Info()
                )
            ),
            stateEvent = stateEvent
        )
    }
}