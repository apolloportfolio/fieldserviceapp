package com.aps.catemplateapp.common.util

import android.os.Parcelable
import com.google.firebase.firestore.GeoPoint
import kotlinx.android.parcel.Parcelize

@Parcelize
class ParcelableGeoPoint(
        private val latitudeM: Double,
        private val longitudeM: Double
    ) : GeoPoint(latitudeM, longitudeM), Parcelable, java.io.Serializable {
}