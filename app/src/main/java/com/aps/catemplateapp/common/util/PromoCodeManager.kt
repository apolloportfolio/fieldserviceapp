package com.aps.catemplateapp.common.util


abstract class PromoCodeManager {
    companion object {
        private const val TAG = "PromoCodeManager"
        private const val LOG_ME = true
        private val ALLOWED_CODE_SYMBOLS = "EUYAOIVPQX".toCharArray()

        fun getInviteCode(systemID: String) : String? {
            val methodName: String = "getInviteCode"
            var result: String = ""
            if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
            return try {
                result = systemID
                if (LOG_ME) ALog.d(TAG, "Generated invitation code for $systemID is:\n $result")
                result
            } catch (e: Exception) {
                ALog.e(TAG, methodName, e)
                null
            } finally {
                if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
            }
        }
    }
}