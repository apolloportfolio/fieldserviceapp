package com.aps.catemplateapp.common.business.domain.state

import android.content.Context
import android.view.View
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.TodoCallback


private const val LOG_ME = true

data class StateMessage(val response: Response)

data class Response(
    val messageId: Int?,
    val message: String?,
    val uiComponentType: UIComponentType,
    val messageType: MessageType
)

sealed class UIComponentType{

    abstract fun equalsByValue(otherComponent: UIComponentType): Boolean

    class Toast: UIComponentType() {
        override fun equalsByValue(otherComponent: UIComponentType): Boolean {
            if(otherComponent !is Toast)return false
            if(LOG_ME) ALog.d("Toast", "equalsByValue(): Toasts can't be compared.")
            return false
        }
    }

    class Dialog(
        val okButtonCallback: OkButtonCallback? = null,
        val titleID: Int? = null,
    ): UIComponentType() {
        override fun equalsByValue(otherComponent: UIComponentType): Boolean {
            if(otherComponent !is Dialog)return false
            return titleID == otherComponent.titleID
        }

        override fun toString(): String {
            val title = if(titleID == null) {
                "null"
            } else {
                (getApplicationContext() as Context).getString(titleID)
            }
            return "Dialog with title: $title"
        }
    }

    class AreYouSureDialog(
        val titleID: Int = R.string.are_you_sure,
        val callback: AreYouSureCallback
    ): UIComponentType() {
        override fun equalsByValue(otherComponent: UIComponentType): Boolean {
            if(otherComponent !is AreYouSureDialog)return false
            return titleID == otherComponent.titleID
        }

        override fun toString(): String {
            val title = if(titleID == null) {
                "null"
            } else {
                (getApplicationContext() as Context).getString(titleID)
            }
            return "AreYouSureDialog with title: $title"
        }
    }

    class SnackBar(
        val onDismissCallback: TodoCallback? = null,
        var actionTextId: Int,
        val actionTextString: String? = null,
        val actionOnClickListener: View.OnClickListener
    ): UIComponentType() {
        override fun equalsByValue(otherComponent: UIComponentType): Boolean {
            if(otherComponent !is SnackBar)return false
            return actionTextId == otherComponent.actionTextId &&
                    actionTextString == otherComponent.actionTextString
        }

        override fun toString(): String {
            val actionTextId = (getApplicationContext() as Context).getString(actionTextId)
            val actionTextString = if(actionTextString == null) {
                "null"
            } else {
                actionTextString
            }
            return "SnackBar with actionTextId == $actionTextId and " +
                    "actionTextString == $actionTextString"
        }
    }

    class None: UIComponentType() {
        override fun equalsByValue(otherComponent: UIComponentType): Boolean {
            return otherComponent is None
        }
    }
}

sealed class MessageType{

    class Success: MessageType()

    class Error: MessageType()

    class Info: MessageType()

    class None: MessageType()
}


interface StateMessageCallback{

    fun removeMessageFromStack()
}


interface AreYouSureCallback {

    fun proceed()

    fun cancel()
}


interface OkButtonCallback {
    fun proceed()
}

interface SnackbarUndoCallback {

    fun undo()
}

class SnackbarUndoListener
    constructor(
        private val snackbarUndoCallback: SnackbarUndoCallback?
    ): View.OnClickListener {

    override fun onClick(v: View?) {
        snackbarUndoCallback?.undo()
    }

}


interface DialogInputCaptureCallback {

    fun onTextCaptured(text: String)
}















