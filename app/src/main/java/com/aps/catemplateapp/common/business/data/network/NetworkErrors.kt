package com.aps.catemplateapp.common.business.data.network

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.platform.app.InstrumentationRegistry
import com.aps.catemplateapp.R
import java.lang.Exception

object NetworkErrors {
//    const val UNABLE_TO_RESOLVE_HOST = "Unable to resolve host"
//    const val UNABLE_TODO_OPERATION_WO_INTERNET = "Can't do that operation without an internet connection"
//    const val ERROR_CHECK_NETWORK_CONNECTION = "Check network connection."
//    const val NETWORK_ERROR_UNKNOWN = "Unknown network error"
//    const val NETWORK_ERROR = "Network error"
//    const val NETWORK_ERROR_TIMEOUT = "Network timeout"
//    const val NETWORK_DATA_NULL = "Network data is null"


    fun isNetworkError(msg: String): Boolean{
        return when{
            msg.contains(UNABLE_TO_RESOLVE_HOST) -> true
            else-> false
        }
    }

    var UNABLE_TO_RESOLVE_HOST = try {
        (ApplicationProvider.getApplicationContext() as Context).getString(R.string.UNABLE_TO_RESOLVE_HOST)
        } catch(e: Exception) {
            InstrumentationRegistry.getInstrumentation().targetContext.getString(R.string.UNABLE_TO_RESOLVE_HOST)
        }

    var UNABLE_TODO_OPERATION_WO_INTERNET = try {
        (ApplicationProvider.getApplicationContext() as Context).getString(R.string.UNABLE_TODO_OPERATION_WO_INTERNET)
    } catch(e: Exception) {
        InstrumentationRegistry.getInstrumentation().targetContext.getString(R.string.UNABLE_TODO_OPERATION_WO_INTERNET)
    }

    var ERROR_CHECK_NETWORK_CONNECTION = try {
        (ApplicationProvider.getApplicationContext() as Context).getString(R.string.ERROR_CHECK_NETWORK_CONNECTION)
    } catch(e: Exception) {
        InstrumentationRegistry.getInstrumentation().targetContext.getString(R.string.ERROR_CHECK_NETWORK_CONNECTION)
    }

    var NETWORK_ERROR_UNKNOWN = try {
        (ApplicationProvider.getApplicationContext() as Context).getString(R.string.NETWORK_ERROR_UNKNOWN)
    } catch(e: Exception) {
        InstrumentationRegistry.getInstrumentation().targetContext.getString(R.string.NETWORK_ERROR_UNKNOWN)
    }

    var NETWORK_ERROR = try {
        (ApplicationProvider.getApplicationContext() as Context).getString(R.string.NETWORK_ERROR)
    } catch(e: Exception) {
        InstrumentationRegistry.getInstrumentation().targetContext.getString(R.string.NETWORK_ERROR)
    }

    var NETWORK_ERROR_TIMEOUT = try {
        (ApplicationProvider.getApplicationContext() as Context).getString(R.string.NETWORK_ERROR_TIMEOUT)
    } catch(e: Exception) {
        InstrumentationRegistry.getInstrumentation().targetContext.getString(R.string.NETWORK_ERROR_TIMEOUT)
    }

    var NETWORK_DATA_NULL = try {
        (ApplicationProvider.getApplicationContext() as Context).getString(R.string.NETWORK_DATA_NULL)
    } catch(e: Exception) {
        InstrumentationRegistry.getInstrumentation().targetContext.getString(R.string.NETWORK_DATA_NULL)
    }
}