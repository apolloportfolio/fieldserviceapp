package com.aps.catemplateapp.common.business.interactors.abstraction


import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.business.domain.state.DataState
import kotlinx.coroutines.flow.Flow

interface DoNothingAtAll<
        Entity,
        CacheDataSource: StandardCacheDataSource<Entity>,
        NetworkDataSource: StandardNetworkDataSource<Entity>,
        ViewState: com.aps.catemplateapp.common.business.domain.state.ViewState<Entity>> {
    fun doNothing(): Flow<DataState<ViewState>?>
}