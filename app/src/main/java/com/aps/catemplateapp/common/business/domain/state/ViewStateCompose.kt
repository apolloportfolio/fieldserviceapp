package com.aps.catemplateapp.common.business.domain.state


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.aps.catemplateapp.common.framework.presentation.views.DialogState
import com.aps.catemplateapp.common.framework.presentation.views.SnackBarState
import com.aps.catemplateapp.common.framework.presentation.views.ToastState

abstract class ViewStateCompose<T>(
    private val _mainEntity: MutableLiveData<T?> = MutableLiveData(),
    private val _listOfEntities: MutableLiveData<List<T>?> = MutableLiveData(),
    private val _isUpdatePending: MutableLiveData<Boolean?> = MutableLiveData(),
    private val _isInternetAvailable: MutableLiveData<Boolean?> = MutableLiveData(),
    private val _isNetworkRepositoryAvailable: MutableLiveData<Boolean> = MutableLiveData(true),
    private val _noCachedEntity: MutableLiveData<Boolean?> = MutableLiveData(),
    private val _snackBarState: MutableLiveData<SnackBarState?> = MutableLiveData(),
    private val _toastState: MutableLiveData<ToastState?> = MutableLiveData(),
    private val _dialogState: MutableLiveData<DialogState?> = MutableLiveData(),
//) : Parcelable {
) {

    val mainEntity: LiveData<T?> get() = _mainEntity
    fun setMainEntity(newValue: T?) { _mainEntity.value = newValue }
    fun postMainEntity(newValue: T?) { _mainEntity.postValue(newValue) }

    val listOfEntities: LiveData<List<T>?> get() = _listOfEntities
    fun setListOfEntities(newValue: List<T>?) { _listOfEntities.value = newValue }
    fun postListOfEntities(newValue: List<T>?) { _listOfEntities.postValue(newValue) }

    val isUpdatePending: LiveData<Boolean?> get() = _isUpdatePending
    fun setIsUpdatePending(newValue: Boolean?) { _isUpdatePending.value = newValue }
    fun postIsUpdatePending(newValue: Boolean?) { _isUpdatePending.postValue(newValue) }

    val isInternetAvailable: LiveData<Boolean?> get() = _isInternetAvailable
    fun setIsInternetAvailable(newValue: Boolean?) { _isInternetAvailable.value = newValue }
    fun postIsInternetAvailable(newValue: Boolean?) { _isInternetAvailable.postValue(newValue) }

    val isNetworkRepositoryAvailable: LiveData<Boolean> get() = _isNetworkRepositoryAvailable
    fun setIsNetworkRepositoryAvailable(newValue: Boolean) { _isNetworkRepositoryAvailable.value = newValue }
    fun postIsNetworkRepositoryAvailable(newValue: Boolean) { _isNetworkRepositoryAvailable.postValue(newValue) }

    val noCachedEntity: LiveData<Boolean?> get() = _noCachedEntity
    fun setNoCachedEntity(newValue: Boolean?) { _noCachedEntity.value = newValue }
    fun postNoCachedEntity(newValue: Boolean?) { _noCachedEntity.postValue(newValue) }

    val snackBarState: LiveData<SnackBarState?> get() = _snackBarState
    fun setSnackBarState(newValue: SnackBarState?) { _snackBarState.value = newValue }
    fun postSnackBarState(newValue: SnackBarState?) { _snackBarState.postValue(newValue) }

    val toastState: LiveData<ToastState?> get() = _toastState
    fun setToastState(newValue: ToastState?) { _toastState.value = newValue }
    fun postToastState(newValue: ToastState?) { _toastState.postValue(newValue) }

    val dialogState: LiveData<DialogState?> get() = _dialogState
    fun setDialogState(newValue: DialogState?) { _dialogState.value = newValue }
    fun postDialogState(newValue: DialogState?) { _dialogState.postValue(newValue) }

    override fun toString(): String {
        return "mainEntity == ${mainEntity.value.toString()} ${hashCode()}"
    }
}

