package com.aps.catemplateapp.common.framework.presentation.views

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.aps.catemplateapp.common.framework.presentation.values.CommonTheme


typealias DrawnBackground = @Composable (Modifier, Alignment, @Composable () -> Unit) -> Unit

interface BackgroundsOfLayouts {
    @Composable
    fun backgroundTitleType01(
        colors: List<Color>?,
        brush: Brush?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground

    @Composable
    fun backgroundListItemType01(
        colors: List<Color>?,
        brush: Brush?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground

    @Composable
    fun backgroundScreen01(
        colors: List<Color>?,
        brush: Brush?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground

}
internal object BackgroundsOfLayoutsImpl: BackgroundsOfLayouts {
    @Composable
    override fun backgroundTitleType01(
        colors: List<Color>?,
        brush: Brush?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground {
        return { modifier: Modifier, contentAlignment: Alignment, content: @Composable () -> Unit ->
            val defaultColors = listOf(
                MaterialTheme.colors.primary,
                MaterialTheme.colors.primaryVariant,
            )
            val defaultBrush = Brush.verticalGradient(
                colors = colors ?: defaultColors,
            )
            val defaultShape = RectangleShape
            val defaultAlpha = 1.0f

            BoxWithConstraints(
                modifier = modifier
                    .background(
                        brush = brush ?: defaultBrush,
                        shape = shape ?: defaultShape,
                        alpha = alpha ?: defaultAlpha,
                    ),
                contentAlignment = contentAlignment,
            ) {
                content()
            }
        }
    }
    @Composable
    override fun backgroundListItemType01(
        colors: List<Color>?,
        brush: Brush?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground {
        return { modifier: Modifier, contentAlignment: Alignment, content: @Composable () -> Unit ->
            val defaultColors = listOf(
                MaterialTheme.colors.primary,
                MaterialTheme.colors.primaryVariant,
            )
            val defaultBrush = Brush.verticalGradient(
                colors = colors ?: defaultColors,
            )
            val defaultShape = RectangleShape
            val defaultAlpha = 1.0f

            BoxWithConstraints(
                modifier = modifier
                    .background(
                        brush = brush ?: defaultBrush,
                        shape = shape ?: defaultShape,
                        alpha = alpha ?: defaultAlpha,
                    ),
                contentAlignment = contentAlignment,
            ) {
                content()
            }
        }
    }

    @Composable
    override fun backgroundScreen01(
        colors: List<Color>?,
        brush: Brush?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground {
        return { modifier: Modifier, contentAlignment: Alignment, content: @Composable () -> Unit ->
            val defaultColors = listOf(
                MaterialTheme.colors.primary,
                MaterialTheme.colors.primaryVariant,
            )
            val defaultBrush = Brush.radialGradient(
                colors = colors ?: defaultColors,
                center = Offset.Zero, // Starting from the top left corner
                radius = 1000f,
            )
            val defaultShape = RectangleShape
            val defaultAlpha = 1.0f

            BoxWithConstraints(
                modifier = modifier
                    .background(
                        brush = brush ?: defaultBrush,
                        shape = shape ?: defaultShape,
                        alpha = alpha ?: defaultAlpha,
                    ),
                contentAlignment = contentAlignment,
            ) {
                content()
            }
        }
    }
}
@Preview
@Composable
private fun BackgroundTitle01Preview() {
    CommonTheme {
        val backgrounds: BackgroundsOfLayouts = BackgroundsOfLayoutsImpl
        BoxWithBackground(
            modifier = Modifier
                .fillMaxWidth()
                .height(56.dp),
            composableBackground = backgrounds.backgroundTitleType01(null, null, null, null)
        ) {}
    }
}
@Preview
@Composable
private fun BackgroundListItemType01Preview() {
    CommonTheme {
        val backgrounds: BackgroundsOfLayouts = BackgroundsOfLayoutsImpl
        BoxWithBackground(
            modifier = Modifier
                .fillMaxWidth()
                .height(56.dp),
            composableBackground = backgrounds.backgroundListItemType01(null, null, null, null)
        ) {}
    }
}
@Preview
@Composable
private fun BackgroundScreen01Preview() {
    CommonTheme {
        val backgrounds: BackgroundsOfLayouts = BackgroundsOfLayoutsImpl
        BoxWithBackground(
            modifier = Modifier
                .fillMaxWidth()
                .height(56.dp),
            composableBackground = backgrounds.backgroundScreen01(null, null, null, null)
        ) {}
    }
}