package com.aps.catemplateapp.common.util;

import android.util.Log;

import com.aps.catemplateapp.core.util.Config;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

public class ALog {
	private static final boolean DONT_SPLIT_STRINGS = true;

	public static void d(String string1, String string2){
		if(Config.LOGGING){
			if(string1 == null || string1 == ".") {
				string1 = Config.APP_NAME;
			}
			if(string2.length() < 4000 || ALog.DONT_SPLIT_STRINGS){
//				Log.d(string1, Config.LOGGING_TAG + "    " + string2);
				Log.d(Config.LOGGING_TAG, string1 + "." + string2);
			} else {
				Log.d(string1, "String too long, splitting.");
				List<String> splitted = splitEqually(string2, 4000);
				Log.d(Config.LOGGING_TAG, string1 + "." + splitted.get(0));
				for(int i=1; i< splitted.size(); i++){
					Log.d(string1, Config.LOGGING_TAG + "    " + splitted.get(i));
				}
			}
		}
	}

	public static void w(String string1, String string2){
		if(Config.LOGGING){
			if(string1 == null || string1 == ".") {
				string1 = Config.APP_NAME;
			}
			if(string2.length() < 4000 || ALog.DONT_SPLIT_STRINGS){
//				Log.w(string1, Config.LOGGING_TAG + "    " + string2);
				Log.w(Config.LOGGING_TAG, string1 + "." + string2);
			} else {
				Log.w(string1, "String too long, splitting.");
				List<String> splitted = splitEqually(string2, 4000);
				Log.w(Config.LOGGING_TAG, string1 + "." + splitted.get(0));
				for(int i=1; i< splitted.size(); i++){
					Log.w(string1, Config.LOGGING_TAG + "    " + splitted.get(i));
				}
			}
		}
	}

	public static void i(String string1, String string2){
		if(Config.LOGGING){
			if(string1 == null || string1 == ".") {
				string1 = Config.APP_NAME;
			}
			if(string2.length() < 4000 || ALog.DONT_SPLIT_STRINGS){
//				Log.i(string1, Config.LOGGING_TAG + "    " + string2);
				Log.i(Config.LOGGING_TAG, string1 + "." + string2);
			} else {
				Log.i(string1, "String too long, splitting.");
				List<String> splitted = splitEqually(string2, 4000);
				Log.i(Config.LOGGING_TAG, string1 + "." + splitted.get(0));
				for(int i=1; i< splitted.size(); i++){
					Log.i(string1, Config.LOGGING_TAG + "    " + splitted.get(i));
				}
			}
		}
	}

	public static void e(String string1, String string2){
		if(Config.LOGGING){
			if(string1 == null || string1 == ".") {
				string1 = Config.APP_NAME;
			}
			if(string2.length() < 4000 || ALog.DONT_SPLIT_STRINGS){
//				Log.e(string1, Config.LOGGING_TAG + "    " + string2);
				Log.e(Config.LOGGING_TAG, string1 + "." + string2);
			} else {
				Log.e(string1, "String too long, splitting.");
				List<String> splitted = splitEqually(string2, 4000);
				Log.e(Config.LOGGING_TAG, string1 + "." + splitted.get(0));
				for(int i=1; i< splitted.size(); i++){
					Log.e(string1, Config.LOGGING_TAG + "    " + splitted.get(i));
				}
			}
		}
	}

	public static void e(String string1, String string2, Exception e){
		if(Config.LOGGING){
			if(string1 == null || string1 == ".") {
				string1 = Config.APP_NAME;
			}
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			string2 += "() Exception: " + sw.toString();
			if(string2.length() < 4000 || ALog.DONT_SPLIT_STRINGS){
//				Log.e(string1, Config.LOGGING_TAG + "    " + string2);
				Log.e(Config.LOGGING_TAG, string1 + "." + string2);
			} else {
				Log.e(string1, "String too long, spliting.");
				List<String> splitted = splitEqually(string2, 4000);
				Log.e(Config.LOGGING_TAG, string1 + "." + splitted.get(0));
				for(int i=1; i< splitted.size(); i++){
					Log.e(string1, Config.LOGGING_TAG + "    " + splitted.get(i));
				}
			}
		}
	}

	public static void v(String string1, String string2){
		if(Config.LOGGING){
			if(string1 == null || string1 == ".") {
				string1 = Config.APP_NAME;
			}
			if(string2.length() < 4000 || ALog.DONT_SPLIT_STRINGS){
//				Log.v(string1, Config.LOGGING_TAG + "    " + string2);
				Log.v(Config.LOGGING_TAG, string1 + "." + string2);
			} else {
				Log.v(string1, "String too long, spliting.");
				List<String> splitted = splitEqually(string2, 4000);
				Log.v(Config.LOGGING_TAG, string1 + "." + splitted.get(0));
				for(int i=1; i< splitted.size(); i++){
					Log.v(string1, Config.LOGGING_TAG + "    " + splitted.get(i));
				}
			}
		}
	}

	public static List<String> splitEqually(String text, int size) {
		// Give the list the right capacity to start with. You could use an array
		// instead if you wanted.
		List<String> ret = new ArrayList<String>((text.length() + size - 1) / size);

		for (int start = 0; start < text.length(); start += size) {
			ret.add(text.substring(start, Math.min(text.length(), start + size)));
		}
		return ret;
	}

	public static void logFloatArray(String methodName, String comment, String arrayName, float[] array) {
		String arrayString = ".";
		int n = 1;
		if(array != null) {
			for(int i=0; i<array.length; i++) {
				arrayString += array[i] + ", ";
				if(n == 4){
					arrayString += "   ";
					n=1;
				}
			}
		} else {
			arrayString += "null";
		}
		ALog.d(".", methodName + ": " + comment + "   " + arrayName + " == " + arrayString);
	}

	public static void logMultiple(String string1, ArrayList<String> textArray) {
		for(String string : textArray) {
			d(string1, string);
		}
	}
}
