package com.aps.catemplateapp.common.framework.presentation

import androidx.navigation.NavHostController

data class NavItem(
    val nameStringID: Int,
    val route: NavDestination,
    val iconDrawableID: Int,
    val badgeCount: Int = 0,
    val onItemClickNavigation: (
        Boolean,                 // Is layout dual pane
        NavItem,                 // This NavItem
        NavHostController?,      // Left pane NavHostController
        NavHostController?,      // Right pane NavHostController
    ) -> Unit,
    val contentDescriptionID: Int,
)