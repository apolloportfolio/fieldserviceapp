package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.adapters

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DeviceLocation
import com.aps.catemplateapp.common.util.GeoLocationUtilities
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity4
import com.aps.catemplateapp.core.util.ProjectConstants
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import java.math.RoundingMode
import java.text.DecimalFormat

private const val TAG = "AdapterEntity4ToListItemType3"
private const val LOG_ME = true

class AdapterEntity4ToListItemType3(
    private var objectsList : List<Entity4>?,
    private val onItemClickListener: OnItemClickListener?,
    private val onThumbnailClickListener: OnItemClickListener?,
    )
    : RecyclerView.Adapter<AdapterEntity4ToListItemType3.ListItemType3ViewHolder>() {

    lateinit var viewHolder : ListItemType3ViewHolder
    var deviceLocation : DeviceLocation? = null

    fun getData() = objectsList
    fun setData(data : List<Entity4>?) {
        objectsList = data
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListItemType3ViewHolder {
        val methodName: String = "onCreateViewHolder"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Creating ViewHolder")
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.list_item_type3,
            parent,
            false
        )
        return ListItemType3ViewHolder(itemView)
    }

    @SuppressLint("LongLogTag")
    override fun onBindViewHolder(holder: ListItemType3ViewHolder, position: Int) {
        val methodName: String = "onBindViewHolder"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(LOG_ME) ALog.d(TAG, ".$methodName(): Binding ViewHolder")
            this.viewHolder = holder
            if(objectsList != null) {
                val currentItem = objectsList!![position]

                val entitysDistanceFromUser = getEntitysDistanceFromUser(currentItem)
                val decimalFormatRatings = DecimalFormat("#")
                decimalFormatRatings.roundingMode = RoundingMode.CEILING
                holder.distanceFromUser.text = entitysDistanceFromUser

                holder.shortDescription.text = currentItem.description
                setImage(currentItem, holder)
                if(LOG_ME) ALog.d(TAG, ".$methodName(): All view content set.")
            } else {
                Log.w(TAG, "onBindViewHolder: objectsList == null")
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override fun getItemCount(): Int {
        return objectsList?.size ?: 0
    }

    inner class ListItemType3ViewHolder(itemView : View)
        : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val imgThumbnail : ImageView = itemView.findViewById(R.id.item_thumbnail)
        val title : TextView = itemView.findViewById(R.id.item_title)
        val boldTextToTheRight : TextView = itemView.findViewById(R.id.lblBigAttribute)
        val smallCounter : TextView = itemView.findViewById(R.id.lblSmallShortAttribute1)
        val distanceFromUser : TextView = itemView.findViewById(R.id.lblSmallShortAttribute2)
        val longCounter : TextView = itemView.findViewById(R.id.lblSmallShortAttribute3)
        val shortDescription : TextView = itemView.findViewById(R.id.lblSmallLongAttribute)
        val ratingBar : RatingBar = itemView.findViewById(R.id.user_rating_bar)
        init {
            val methodName: String = "ListItemType3ViewHolder.init"
            if (LOG_ME) ALog.d("ListItemType3ViewHolder", "Method start: $methodName")
            try {
                itemView.setOnClickListener(this)
                if(onThumbnailClickListener != null)imgThumbnail.setOnClickListener(this)
            } catch (e: Exception) {
                ALog.e("ListItemType3ViewHolder", methodName, e)
            } finally {
                if (LOG_ME) ALog.d("ListItemType3ViewHolder", "Method end: $methodName")
            }

        }
        override fun onClick(v: View?) {
            val methodName: String = "onClick"
            if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
            try {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    when(v) {
                        imgThumbnail -> {
                            if (LOG_ME) ALog.d(TAG, "$methodName(): Click on thumbnail detected")
                            onThumbnailClickListener?.onItemClick(position)
                            return
                        }
                        else -> {
                            if (LOG_ME) ALog.d(TAG, "$methodName(): Click on item detected")
                            onItemClickListener?.onItemClick(position)
                        }
                    }
                }
            } catch (e: Exception) {
                ALog.e(TAG, methodName, e)
                return
            } finally {
                if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

    interface OnItemTouchListener {
        fun onItemTouch(position: Int, motionEvent: MotionEvent?)
    }

    private fun setImage(entity4 : Entity4, holder : ListItemType3ViewHolder) {
        val methodName: String = "setImage"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(entity4.picture1URI == null) {
                if(LOG_ME) ALog.w(
                    TAG, ".$methodName(): " +
                            "rentalOffer.picture1URI == null"
                )
                return
            }
            if(entity4.picture1URI == "") {
                if(LOG_ME) ALog.w(
                    TAG, ".$methodName(): " +
                            "rentalOffer.id == \"\""
                )
                return
            }
            if(entity4.id == null) {
                if(LOG_ME) ALog.w(
                    TAG, ".$methodName(): " +
                            "rentalOffer.id == null"
                )
                return
            }
            if(LOG_ME) ALog.d(
                TAG, ".$methodName(): " +
                        "entity4.picture1URI == ${entity4.picture1URI}"
            )
            val storageReference = Firebase.storage
            val imageRef = storageReference.reference
                .child(ProjectConstants.FIRESTORE_IMAGES_COLLECTION)
                .child(ProjectConstants.FIRESTORE_ENTITY_1_IMAGES_SUB_COLLECTION)
                .child(entity4.id!!.firestoreDocumentID)
                .child(entity4.picture1URI!!)

            if (LOG_ME) ALog.d(TAG, ".$methodName(): imageRef == $imageRef")

            val glideOptions: RequestOptions = RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.ic_launcher)
                .error(R.drawable.ic_launcher)
                .diskCacheStrategy(DiskCacheStrategy.ALL)

            Glide.with(holder.imgThumbnail)
                .load(imageRef)
                .apply(glideOptions)
                .into(holder.imgThumbnail)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }


    private fun getEntitysDistanceFromUser(entity4: Entity4) : String {
        val methodName: String = "getEntitysDistanceFromUser"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        var result : String = ""
        try {
            var distance : Double? = null
            if(deviceLocation != null) {
                if(LOG_ME) ALog.d(
                    TAG, ".$methodName(): " +
                            "Calculating entity's distance from location == $deviceLocation"
                )
                if(entity4.latitude != null && entity4.longitude != null) {
                    if(LOG_ME) ALog.d(
                        TAG, ".$methodName(): " +
                                "entity4.lattitude == ${entity4.latitude};    " +
                                "entity4.longitude == ${entity4.longitude}"
                    )
                    distance = GeoLocationUtilities.distance(
                        deviceLocation!!.latitude.toDouble(),
                        deviceLocation!!.longitude.toDouble(),
                        entity4.latitude!!,
                        entity4.longitude!!,
                        GeoLocationUtilities.UNIT_KM
                    )

                    val decimalFormatDistance = DecimalFormat("#")
                    decimalFormatDistance.roundingMode = RoundingMode.CEILING

                    result = decimalFormatDistance.format(distance) + " km"
                } else {
                    ALog.w(
                        TAG, ".$methodName(): " +
                                "entity4.latitude = ${entity4.latitude};    " +
                                "entity4.longitude == ${entity4.longitude}"
                    )
                }
            } else {
                ALog.w(TAG, ".$methodName(): usersLocation == null")
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return result
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
        return result
    }
}