package com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.abs

import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.BasicFirestoreService
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity4
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.mappers.Entity4FirestoreMapper
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.model.Entity4FirestoreEntity

interface Entity4FirestoreService: BasicFirestoreService<
        Entity4,
        Entity4FirestoreEntity,
        Entity4FirestoreMapper
        > {

}