package com.aps.catemplateapp.fieldserviceapp.business.interactors.abs

import android.content.Context
import com.aps.catemplateapp.common.business.domain.state.DataState
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.core.business.domain.model.currencies.Currency
import com.aps.catemplateapp.core.business.domain.model.currencies.ExchangeRates
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenViewState
import com.google.android.gms.wallet.PaymentsClient
import kotlinx.coroutines.flow.Flow

interface InitiateGooglePayPaymentProcess {
    fun initiateGooglePayPaymentProcess(
        context: Context,
        baseCurrency: Currency,
        usedCurrencies: ArrayList<String>?,
        stateEvent : StateEvent,
        returnViewState : HomeScreenViewState<ProjectUser>,
        updateReturnViewState : (
            HomeScreenViewState<ProjectUser>,
            ExchangeRates?,                 // exchangeRatesP
            String?,                        // merchantNameP
            String?,                        // gatewayNameP
            String?,                        // gatewayMerchantIDP
            PaymentsClient?,                // gPaymentsClientP
            Boolean?,                       // gPayIsAvailable
        ) -> (HomeScreenViewState<ProjectUser>),
        errorReturnViewState : (HomeScreenViewState<ProjectUser>,) -> (HomeScreenViewState<ProjectUser>),
    ) : Flow<DataState<HomeScreenViewState<ProjectUser>>?>
}