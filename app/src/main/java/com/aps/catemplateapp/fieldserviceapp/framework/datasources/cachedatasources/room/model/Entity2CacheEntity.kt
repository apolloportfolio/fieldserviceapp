package com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID

@Entity(tableName = "entity2")
data class Entity2CacheEntity(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    @NonNull
    var id : UniqueID,

    @ColumnInfo(name = "updated_at")
    val updated_at: String?,

    @ColumnInfo(name = "created_at")
    val created_at: String?,

    @ColumnInfo(name = "picture1URI")
    var picture1URI: String?,

    @ColumnInfo(name = "name")
    var name: String?,

    @ColumnInfo(name = "description")
    var description : String?,

    @ColumnInfo(name = "ownerID")
    var ownerID: UserUniqueID?,

    ) {
}