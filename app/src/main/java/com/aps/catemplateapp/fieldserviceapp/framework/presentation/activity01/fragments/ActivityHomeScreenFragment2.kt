package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.view.*
import androidx.activity.OnBackPressedCallback
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.common.framework.presentation.BaseMVIFragment
import com.aps.catemplateapp.common.framework.presentation.BaseViewModel
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.extensions.hideKeyboard
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.databinding.FragmentHomeScreenFragment2Binding
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.ActivityHomeScreen
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.ActivityHomeScreenNavigation
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.HomeScreenViewModel
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.adapters.AdapterTaskAssignmentToListItemType3
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenStateEvent
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenViewState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*

const val HomeScreen_Fragment2_STATE_BUNDLE_KEY = "com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.fragments.fragment2"


private const val TAG = "ActivityHomeScreenFragment2"
private const val LOG_ME = true

@FlowPreview
@ExperimentalCoroutinesApi
@AndroidEntryPoint
class ActivityHomeScreenFragment2
constructor(
        private val viewModelFactory: ViewModelProvider.Factory,
        private val dateUtil: DateUtil
): BaseMVIFragment<ProjectUser, HomeScreenViewState<ProjectUser>>(layoutRes = R.layout.fragment_home_screen_fragment2),
    ActivityHomeScreenNavigation {

    private val viewModel : HomeScreenViewModel
        get() = (activity as ActivityHomeScreen).viewModel

    private lateinit var binding: FragmentHomeScreenFragment2Binding

    private lateinit var recyclerView : RecyclerView

    private val fragment = this
    private val onItemClickListener : AdapterTaskAssignmentToListItemType3.OnItemClickListener =
        object : AdapterTaskAssignmentToListItemType3.OnItemClickListener {
            override fun onItemClick(position: Int) {
                val methodName: String = "onItemClick"
                if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
                try {
                    val clickedEntity =
                        getViewModel().getCurrentViewStateOrNew().userTakenJobs?.get(position)
                    if(clickedEntity != null) {
                        if (LOG_ME) ALog.d(TAG, "$methodName(): Navigating to entity nr $position")
                        val entityOwner = getViewModel().getCurrentViewStateOrNew().mainEntity
                        if(entityOwner != null) {
                            navigateToEntity2DetailsScreen(
                                fragment,
                                clickedEntity,
                                entityOwner,
                            )
                        } else {
                            ALog.e(TAG, ".$methodName(): user == null")
                        }
                    } else {
                        if(LOG_ME)ALog.e(TAG, ".onItemClick(): clickedItem == null")
                    }
                } catch (e: Exception) {
                    ALog.e(TAG, methodName, e)
                    return
                } finally {
                    if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
                }
            }
        }

    constructor(parcel: Parcel) : this(
            TODO("viewModelFactory"),
            TODO("dateUtil")) {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.setupChannel()
    }

    @InternalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
		recyclerView = binding.recyclerView

        recyclerView = binding.recyclerView
        recyclerView.adapter = AdapterTaskAssignmentToListItemType3(
            getViewModel().getCurrentViewStateOrNew().userTakenJobs,
            onItemClickListener,
            onItemClickListener,
        )
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.setHasFixedSize(false)

        restoreInstanceState(savedInstanceState)
        setupOnBackPressDispatcher()
    }

    override fun onResume() {
        super.onResume()
        activity?.apply {
            setTitle(R.string.app_bar_title_home_screen_fragment_2)
        }
    }

    override fun onPause() {
        super.onPause()
    }

    override fun restoreInstanceState(savedInstanceState: Bundle?){
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    private fun setupOnBackPressDispatcher() {
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                onBackPressed()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }

    override fun onBackPressed() {
        viewModel.exitEditState()
        super.onBackPressed()
        view?.hideKeyboard()
        findNavController().popBackStack()
    }
    override fun setupUI(){
        view?.hideKeyboard()
    }

    override fun setupBinding(view: View) {
        binding = FragmentHomeScreenFragment2Binding.bind(view)
    }

    companion object CREATOR : Parcelable.Creator<ActivityHomeScreenFragment2> {
        override fun createFromParcel(parcel: Parcel): ActivityHomeScreenFragment2 {
            return ActivityHomeScreenFragment2(parcel)
        }

        override fun newArray(size: Int): Array<ActivityHomeScreenFragment2?> {
            return arrayOfNulls(size)
        }
    }

    override fun subscribeCustomObservers() {
    }

    override fun getViewModel(): BaseViewModel<ProjectUser, HomeScreenViewState<ProjectUser>> {
        return viewModel
    }

    override fun getInitStateEvent(): StateEvent {
        return HomeScreenStateEvent.GetEntities2StateEvent
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun changeUiOrNavigateDependingOnViewState(viewState: HomeScreenViewState<ProjectUser>) {
        val methodName: String = "changeUiOrNavigateDependingOnViewState"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {

            viewState.mainEntity?.let {
                if(viewState.mainEntity!!.fullyVerified) {
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): User is fully verified.")
                    binding.recyclerView.visibility = View.VISIBLE
                    binding.profileCompletionStateLbl.visibility = View.GONE
                    binding.disabledFunctionalityExplanationLbl.visibility = View.GONE
                    binding.floatingActionButton.visibility = View.VISIBLE
                } else {
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): User is not fully verified.")
                    binding.recyclerView.visibility = View.GONE
                    recyclerView.visibility = View.GONE
                    binding.profileCompletionStateLbl.visibility = View.VISIBLE
                    binding.disabledFunctionalityExplanationLbl.visibility = View.VISIBLE
                    binding.floatingActionButton.visibility = View.GONE
                }
            }

            if(viewState.refreshListOfEntities2) {
                viewState.userTakenJobs?.let { entities2List ->
                    if(LOG_ME){
                        ALog.d(TAG, ".$methodName(): Got entities2:")
                        for((index, entity2) in entities2List.withIndex()) {
                            ALog.d(TAG, ".$methodName(): Entity2 #$index == $entity2")
                        }
                    }
                    if(viewState.userTakenJobs!!.isNotEmpty()){
                        binding.recyclerView.visibility = View.VISIBLE
                        binding.tipWhenListIsEmpty.visibility = View.GONE
                    } else binding.tipWhenListIsEmpty.visibility = View.VISIBLE
                    val adapter = (recyclerView.adapter as AdapterTaskAssignmentToListItemType3)
                    adapter.setData(entities2List)

                    recyclerView.adapter?.let {
                        it.notifyDataSetChanged()
                        if(LOG_ME)ALog.d(TAG, ".$methodName(): notifyDataSetChanged")
                    }
                }
            }


            if(viewState.navigateToStartScreen == true) {
                navigateToStartScreen(this)
                viewModel.setNavigateToStartScreen(null)
                return
            }

        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override fun onEntityUpdateSuccess() {

    }

    override fun onEntityDeleteSuccess() {
        
    }

    override fun getStateBundleKey(): String? {
        return HomeScreen_Fragment4_STATE_BUNDLE_KEY
    }

    override fun setAllOnClickAndOnTouchListeners() {
        binding.floatingActionButton.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (LOG_ME) ALog.d(TAG, "onClick(): Method start")
        super.onClick(v)
        when(v) {
            binding.floatingActionButton -> {
                if (LOG_ME) ALog.d(TAG, "onClick(): binding.floatingActionButton")
                getViewModel().getCurrentViewStateOrNew().mainEntity?.let { user ->
                    navigateToEntity1SearchActivity(
                        this,
                        user,
                    )
                }
            }
        }
    }

    override fun updateUIInViewModel() {
        if(viewModel.checkEditState()){
            if (LOG_ME)ALog.d(TAG, "updateUIInViewModel():  Users interface is in EditState")
            view?.hideKeyboard()
            viewModel.exitEditState()
        }
    }

    private fun updateMainEntityInViewModel() {
        if(viewModel.getIsUpdatePending()){
            viewModel.getCurrentViewStateOrNew().mainEntity?.let {
                HomeScreenStateEvent.UpdateMainEntityEvent(
                    it
                )
            }?.let {
                if (LOG_ME) ALog.d(TAG, "updateMainEntityInViewModel(): Sending UpdateMainEntityEvent")
                viewModel.setStateEvent(
                    it
                )
            }
        }
    }
}