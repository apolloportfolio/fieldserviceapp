package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.fragments

import android.annotation.SuppressLint
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.framework.presentation.views.BottomNavigationHeightPublic
import com.aps.catemplateapp.common.framework.presentation.views.PeekingClosableTwoButtonBottomSheetsRow
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.values.ColorTransparent
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.views.HomeScreenCard1SFCol
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenCard1SearchFilters
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

private const val TAG = "HomeScreenCompFragment1BottomSheet"
private const val LOG_ME = true
const val HomeScreenCompFragment1BottomSheetBackgroundTestTag =
    "HomeScreenComposableFragment1BottomSheetBackgroundTestTag"
const val HomeScreenCompFragment1BottomSheetMinimalRange1ValueEditTextTestTag =
    "HomeScreenComposableFragment1BottomSheetMinimalRange1ValueEditTextTestTag"

@SuppressLint("ComposableNaming")
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun HomeScreenCompFragment1BottomSheet(

    scope: CoroutineScope,
    sheetState: BottomSheetState,
    toggleBottomSheetState: () -> Unit =
        remember { standardToggleBottomSheet(scope, sheetState) },
    searchFilters: HomeScreenCard1SearchFilters,
    onSearchFiltersUpdated: (HomeScreenCard1SearchFilters) -> Unit,
    leftButtonOnClick: () -> Unit,
    rightButtonOnClick: () -> Unit,
    bottomPadding: Dp = 0.dp,
): @Composable ColumnScope.() -> Unit {
    return {
        HomeScreenComposableFragment1BottomSheetContent(
            scope,
            sheetState,
            toggleBottomSheetState,
            searchFilters,
            onSearchFiltersUpdated,
            leftButtonOnClick,
            rightButtonOnClick,
        )
        Spacer(modifier = Modifier.height(bottomPadding))
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun HomeScreenComposableFragment1BottomSheetContent(
    scope: CoroutineScope,
    sheetState: BottomSheetState,
    toggleBottomSheetState: () -> Unit =
        remember { standardToggleBottomSheet(scope, sheetState) },
    searchFilters: HomeScreenCard1SearchFilters,
    onSearchFiltersUpdated: (HomeScreenCard1SearchFilters) -> Unit,
    leftButtonOnClick: () -> Unit,
    rightButtonOnClick: () -> Unit,
) {
    Box(modifier = Modifier
        .fillMaxWidth()
        .testTag(HomeScreenCompFragment1BottomSheetBackgroundTestTag)
    ) {
        Image(
            painterResource(id = R.drawable.example_background_1),
            contentDescription = "",
            contentScale = ContentScale.FillBounds,
            modifier = Modifier.matchParentSize()
        )

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .background(ColorTransparent)
        ) {
            var leftButtonClicked by remember { mutableStateOf(false) }
            var rightButtonClicked by remember { mutableStateOf(false) }

            var searchFiltersUpdated by remember { mutableStateOf(false) }
            val toggleSearchFiltersUpdated: (Boolean) -> Unit = { searchFiltersUpdated = it }
            val removeFilters = {
                searchFilters.removeFilters()
                toggleSearchFiltersUpdated(true)
            }

            PeekingClosableTwoButtonBottomSheetsRow(
                scope = scope,
                sheetState = sheetState,
                toggleBottomSheetState = toggleBottomSheetState,
                leftButtonsIconID = R.drawable.ic_filters,
                leftButtonOnClick = {
                    toggleBottomSheetState()
                    leftButtonOnClick()
                },
                rightButtonOnClick = {
                    removeFilters()
                    toggleBottomSheetState()
                    rightButtonOnClick()
                },
                leftButtonTextID = R.string.filters,
                rightButtonTextID = R.string.remove_filters,
                modifier = Modifier
                    .padding(
                        start = dimensionResource(id = R.dimen.rounded_corner_margin),
                        top = dimensionResource(id = R.dimen.rounded_corner_margin),
                        end = dimensionResource(id = R.dimen.rounded_corner_margin),
                        bottom = dimensionResource(id = R.dimen.rounded_corner_margin),
                    )
            )

            HomeScreenCard1SFCol(
                searchFilters,
                rangeSlider1Min = 0.0f,
                rangeSlider1Max = 200.0f,
                rangeSlider1StepSize = 10.0f,
                rangeSlider2Min = 0.0f,
                rangeSlider2Max = 1000.0f,
                rangeSlider2StepSize = 5.0f,
                toggleSearchFiltersUpdated,
                range1NameID = R.string.range_lbl_distance,
                range2NameID = R.string.example_range_2_lbl,
                switch1TextID = R.string.switch_1,
                switch2TextID = R.string.switch_2,
                switch3TextID = R.string.switch_3,
                switch4TextID = R.string.switch_4,
                switch5TextID = R.string.switch_5,
                switch6TextID = R.string.switch_6,
                switch7TextID = R.string.switch_7,
            )

            var firstComposition by rememberSaveable { mutableStateOf(true) }
            LaunchedEffect(searchFiltersUpdated) {
                if(firstComposition) {
                    if(LOG_ME)ALog.d(TAG, "LaunchedEffect(searchFiltersUpdated): " +
                                            "This is the first composition.")
                    firstComposition = false
                } else {
                    if(searchFiltersUpdated) {
                        if(LOG_ME)ALog.d(TAG, "LaunchedEffect(searchFiltersUpdated): " +
                                "Running onSearchFiltersUpdated().")
                        onSearchFiltersUpdated(searchFilters)
                    }
                }
                searchFiltersUpdated = false
            }
        }
    }

}

@OptIn(ExperimentalMaterialApi::class)
internal fun standardToggleBottomSheet(
    scope: CoroutineScope,
    sheetState: BottomSheetState,
): () -> Unit {
    return {
        scope.launch {
            if(sheetState.isCollapsed) {
                sheetState.expand()
            } else {
                sheetState.collapse()
            }
        }
    }
}

data class HomeScreenComposableFragment1BottomSheetActions(
    val leftButtonOnClick: () -> Unit,
    val rightButtonOnClick: () -> Unit,
)

//=================================================================================================
@OptIn(ExperimentalMaterialApi::class)
@Preview
@Composable
fun HomeScreenComposableFragment1BottomSheetPreview() {
    HomeScreenTheme {
        HomeScreenCompFragment1BottomSheet(
            scope = rememberCoroutineScope(),
            sheetState = rememberBottomSheetState(
                initialValue = BottomSheetValue.Collapsed,
//        initialValue = BottomSheetValue.Expanded,
                animationSpec = spring(dampingRatio = Spring.DampingRatioHighBouncy)
            ),
            searchFilters = HomeScreenCard1SearchFilters(
                range1min = 50.0,
                range1max = 100.0,
                range2min = 30.0,
                range2max = 90.0,
                switch1 = false,
                switch2 = false,
                switch3 = false,
                switch4 = false,
                switch5 = false,
                switch6 = false,
                switch7 = false,
            ),
            onSearchFiltersUpdated = {},
            leftButtonOnClick = {},
            rightButtonOnClick = {},
            bottomPadding = BottomNavigationHeightPublic,
        )
    }
}