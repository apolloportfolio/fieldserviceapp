package com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.TaskAssignment
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.model.TaskAssignmentFirestoreEntity
import javax.inject.Inject

class TaskAssignmentFirestoreMapper
@Inject
constructor() : EntityMapper<TaskAssignmentFirestoreEntity, TaskAssignment> {
    override fun mapFromEntity(entity: TaskAssignmentFirestoreEntity): TaskAssignment {
        val taskAssignment = TaskAssignment(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.taskDescription,
            entity.taskAddress,
            entity.clientName,
            entity.phoneNumber,
            entity.scheduledTaskStartTimeStamp,
            entity.expectedTaskDuration,
            entity.actualTaskStartTimeStamp,
            entity.actualTaskEndTimeStamp,
            entity.isTaskCompleted,
            entity.taskNotes,
            entity.customerFeedbackGrade,
            entity.customerFeedbackGradeReview,
            entity.taskResultsPicturesURIs,
            entity.materialsNote,
            entity.jobPriority,

            entity.lattitude,
            entity.longitude,
            null,
            entity.firestoreGeoLocation,

            entity.picture1URI,
            entity.description,

            entity.city,

            entity.ownerID,

            entity.name,

            entity.switch1,
            entity.switch2,
            entity.switch3,
            entity.switch4,
            entity.switch5,
            entity.switch6,
            entity.switch7,
        )
        if(entity.geoLocation != null){
            taskAssignment.geoLocation = ParcelableGeoPoint(
                entity.geoLocation!!.latitude,
                entity.geoLocation!!.longitude
            )
        }
        return taskAssignment
    }

    override fun mapToEntity(domainModel: TaskAssignment): TaskAssignmentFirestoreEntity {
        val entity1NetworkEntity =
        TaskAssignmentFirestoreEntity(
            domainModel.id,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.taskDescription,
            domainModel.taskAddress,
            domainModel.clientName,
            domainModel.phoneNumber,
            domainModel.scheduledTaskStartTimeStamp,
            domainModel.expectedTaskDuration,
            domainModel.actualTaskStartTimeStamp,
            domainModel.actualTaskEndTimeStamp,
            domainModel.isTaskCompleted,
            domainModel.taskNotes,
            domainModel.customerFeedbackGrade,
            domainModel.customerFeedbackGradeReview,
            domainModel.taskResultsPicturesURIs,
            domainModel.materialsNote,
            domainModel.jobPriority,

            domainModel.latitude,
            domainModel.longitude,
            domainModel.geoLocation,
            domainModel.firestoreGeoLocation,

            domainModel.picture1URI,
            domainModel.description,
            
            domainModel.city,

            null,           //keywords
            
            domainModel.ownerID,

            domainModel.name,
            
            domainModel.switch1,
            domainModel.switch2,
            domainModel.switch3,
            domainModel.switch4,
            domainModel.switch5,
            domainModel.switch6,
            domainModel.switch7,
        )
        return entity1NetworkEntity.generateKeywords()
    }

    override fun mapFromEntityList(entities : List<TaskAssignmentFirestoreEntity>) : List<TaskAssignment> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<TaskAssignment>): List<TaskAssignmentFirestoreEntity> {
        return entities.map{mapToEntity(it)}
    }
}