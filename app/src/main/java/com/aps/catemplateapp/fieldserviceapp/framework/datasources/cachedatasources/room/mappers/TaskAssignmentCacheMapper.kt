package com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.TaskAssignment
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.model.TaskAssignmentCacheEntity
import javax.inject.Inject

class TaskAssignmentCacheMapper
@Inject
constructor() : EntityMapper<TaskAssignmentCacheEntity, TaskAssignment> {
    override fun mapFromEntity(entity: TaskAssignmentCacheEntity): TaskAssignment {
        return TaskAssignment(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.taskDescription,
            entity.taskAddress,
            entity.clientName,
            entity.phoneNumber,
            entity.scheduledTaskStartTimeStamp,
            entity.expectedTaskDuration,
            entity.actualTaskStartTimeStamp,
            entity.actualTaskEndTimeStamp,
            entity.isTaskCompleted,
            entity.taskNotes,
            entity.customerFeedbackGrade,
            entity.customerFeedbackGradeReview,
            entity.taskResultsPicturesURIs,
            entity.materialsNote,
            entity.jobPriority,

            entity.latitude ,
            entity.longitude,
            entity.geoLocation,
            entity.firestoreGeoLocation,

            entity.picture1URI,
            entity.description,

            entity.city,

            entity.ownerID,

            entity.name,

            entity.switch1,
            entity.switch2,
            entity.switch3,
            entity.switch4,
            entity.switch5,
            entity.switch6,
            entity.switch7,
        )
    }

    override fun mapToEntity(domainModel: TaskAssignment): TaskAssignmentCacheEntity {
        return TaskAssignmentCacheEntity(
            domainModel.id!!,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.taskDescription,
            domainModel.taskAddress,
            domainModel.clientName,
            domainModel.phoneNumber,
            domainModel.scheduledTaskStartTimeStamp,
            domainModel.expectedTaskDuration,
            domainModel.actualTaskStartTimeStamp,
            domainModel.actualTaskEndTimeStamp,
            domainModel.isTaskCompleted,
            domainModel.taskNotes,
            domainModel.customerFeedbackGrade,
            domainModel.customerFeedbackGradeReview,
            domainModel.taskResultsPicturesURIs,
            domainModel.materialsNote,
            domainModel.jobPriority,

            domainModel.latitude ,
            domainModel.longitude,
            domainModel.geoLocation,
            domainModel.firestoreGeoLocation,

            domainModel.picture1URI,
            domainModel.description,

            domainModel.city,

            domainModel.ownerID,

            domainModel.name,

            domainModel.switch1,
            domainModel.switch2,
            domainModel.switch3,
            domainModel.switch4,
            domainModel.switch5,
            domainModel.switch6,
            domainModel.switch7,
        )
    }

    override fun mapFromEntityList(entities : List<TaskAssignmentCacheEntity>) : List<TaskAssignment> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<TaskAssignment>): List<TaskAssignmentCacheEntity> {
        return entities.map{mapToEntity(it)}
    }
}