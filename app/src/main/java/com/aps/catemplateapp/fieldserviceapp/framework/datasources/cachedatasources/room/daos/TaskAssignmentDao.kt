package com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.model.TaskAssignmentCacheEntity
import com.google.firebase.firestore.GeoPoint
import java.util.*


@Dao
interface TaskAssignmentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity : TaskAssignmentCacheEntity) : Long

    @Query("SELECT * FROM taskassignment")
    suspend fun get() : List<TaskAssignmentCacheEntity>
    
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEntities(entities: List<TaskAssignmentCacheEntity>): LongArray

    @Query("SELECT * FROM taskassignment WHERE id = :id")
    suspend fun getEntityById(id: UniqueID?): TaskAssignmentCacheEntity?

    @Query("DELETE FROM taskassignment WHERE id IN (:ids)")
    suspend fun deleteEntities(ids: List<UniqueID?>): Int

    @Query("DELETE FROM taskassignment")
    suspend fun deleteAllEntities()

    @Query("SELECT * FROM taskassignment")
    suspend fun getAllEntities(): List<TaskAssignmentCacheEntity>

    @Query(
        """
        UPDATE taskassignment 
        SET 
        created_at = :created_at,
        updated_at = :updated_at,
        taskDescription = :taskDescription,
        taskAddress = :taskAddress,
        clientName = :clientName,
        phoneNumber = :phoneNumber,
        scheduledTaskStartTimeStamp = :scheduledTaskStartTimeStamp,
        expectedTaskDuration = :expectedTaskDuration,
        actualTaskStartTimeStamp = :actualTaskStartTimeStamp,
        actualTaskEndTimeStamp = :actualTaskEndTimeStamp,
        isTaskCompleted = :isTaskCompleted,
        taskNotes = :taskNotes,
        customerFeedbackGrade = :customerFeedbackGrade,
        customerFeedbackGradeReview = :customerFeedbackGradeReview,
        taskResultsPicturesURIs = :taskResultsPicturesURIs,
        materialsNote = :materialsNote,
        jobPriority = :jobPriority,
        latitude = :latitude,
        longitude = :longitude,
        geoLocation = :geoLocation,
        firestoreGeoLocation = :firestoreGeoLocation,
        picture1URI = :picture1URI,
        description = :description,
        city = :city,
        ownerID = :ownerID,
        name = :name,
        switch1 = :switch1,
        switch2 = :switch2,
        switch3 = :switch3,
        switch4 = :switch4,
        switch5 = :switch5,
        switch6 = :switch6,
        switch7 = :switch7
        WHERE id = :id
        """
    )
    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        taskDescription: String?,
        taskAddress: String?,
        clientName: String?,
        phoneNumber: String?,
        scheduledTaskStartTimeStamp: String?,
        expectedTaskDuration: String?,
        actualTaskStartTimeStamp: String?,
        actualTaskEndTimeStamp: String?,
        isTaskCompleted: Boolean?,
        taskNotes: String?,
        customerFeedbackGrade: Int?,
        customerFeedbackGradeReview: String?,
        taskResultsPicturesURIs: String?,
        materialsNote: String?,
        jobPriority: Int?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: GeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,

        description : String?,

        city : String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean? = null,
        switch2: Boolean? = null,
        switch3: Boolean? = null,
        switch4: Boolean? = null,
        switch5: Boolean? = null,
        switch6: Boolean? = null,
        switch7: Boolean? = null,
    ): Int

    @Query("DELETE FROM taskassignment WHERE id = :primaryKey")
    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    @Query("SELECT * FROM taskassignment")
    suspend fun searchEntities(): List<TaskAssignmentCacheEntity>
    
    @Query("SELECT COUNT(*) FROM taskassignment")
    suspend fun getNumEntities(): Int
}