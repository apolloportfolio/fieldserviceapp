package com.aps.catemplateapp.fieldserviceapp.business.data.network.abs

import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity4

interface Entity4NetworkDataSource: StandardNetworkDataSource<Entity4> {
    override suspend fun insertOrUpdateEntity(entity: Entity4): Entity4?

    override suspend fun deleteEntity(primaryKey: UniqueID?)

    override suspend fun insertDeletedEntity(entity: Entity4)

    override suspend fun insertDeletedEntities(Entities: List<Entity4>)

    override suspend fun deleteDeletedEntity(entity: Entity4)

    override suspend fun getDeletedEntities(): List<Entity4>

    override suspend fun deleteAllEntities()

    override suspend fun searchEntity(entity: Entity4): Entity4?

    override suspend fun getAllEntities(): List<Entity4>

    override suspend fun insertOrUpdateEntities(Entities: List<Entity4>): List<Entity4>?

    override suspend fun getEntityById(id: UniqueID): Entity4?
}