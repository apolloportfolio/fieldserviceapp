package com.aps.catemplateapp.fieldserviceapp.business.data.network.impl

import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fieldserviceapp.business.data.network.abs.Entity3NetworkDataSource
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity3
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.abs.Entity3FirestoreService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Entity3NetworkDataSourceImpl
@Inject
constructor(
    private val firestoreService: Entity3FirestoreService
): Entity3NetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: Entity3): Entity3? {
        return firestoreService.insertOrUpdateEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        return firestoreService.deleteEntity(primaryKey)
    }

    override suspend fun insertDeletedEntity(entity: Entity3) {
        return firestoreService.insertDeletedEntity(entity)
    }

    override suspend fun insertDeletedEntities(entities: List<Entity3>) {
        return firestoreService.insertDeletedEntities(entities)
    }

    override suspend fun deleteDeletedEntity(entity: Entity3) {
        return firestoreService.deleteDeletedEntity(entity)
    }

    override suspend fun getDeletedEntities(): List<Entity3> {
        return firestoreService.getAllDeletedEntities()
    }

    override suspend fun deleteAllEntities() {
        firestoreService.deleteAllEntities()
    }

    override suspend fun searchEntity(entity: Entity3): Entity3? {
        return firestoreService.searchEntity(entity)
    }

    override suspend fun getAllEntities(): List<Entity3> {
        return firestoreService.getAllEntities()
    }

    override suspend fun insertOrUpdateEntities(entities: List<Entity3>): List<Entity3>? {
        return firestoreService.insertOrUpdateEntities(entities)
    }

    override suspend fun getEntityById(id: UniqueID): Entity3? {
        return firestoreService.getEntityById(id)
    }

    override suspend fun getUsersEntities3(userId: UserUniqueID): List<Entity3>? {
        return firestoreService.getUsersEntities3(userId)
    }
}