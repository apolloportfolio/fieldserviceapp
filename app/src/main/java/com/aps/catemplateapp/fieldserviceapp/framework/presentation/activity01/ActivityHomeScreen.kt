package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupWithNavController
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.callbacks.onDismiss
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.DialogInputCaptureCallback
import com.aps.catemplateapp.common.framework.presentation.IntentExtras
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.TaskAssignment
import com.aps.catemplateapp.core.framework.presentation.ProjectActivity
import com.aps.catemplateapp.fieldserviceapp.di.activity01.ActivityHomeScreenFragmentFactoryEntryPoint
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.EntryPointAccessors
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject


private const val TAG = "ActivityHomeScreen"
private const val LOG_ME = true
@ExperimentalCoroutinesApi
@AndroidEntryPoint
@FlowPreview
class ActivityHomeScreen : ProjectActivity() {
    @Inject lateinit var fragmentFactory: ActivityHomeScreenFragmentFactory
    override val getContentView: Int
        get() = R.layout.home_screen
    override val mainFragmentId: Int
        get() = R.id.nav_host_fragment

    lateinit var viewModel : HomeScreenViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val methodName: String = "onCreate"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            this.viewModel =
                ViewModelProvider(this).get(HomeScreenViewModel::class.java)
            setFragmentFactory()
            setContentView(getContentView)

            val bottomNavigationView =
                findViewById<BottomNavigationView>(R.id.bottom_navigation_bar)
            val navController = findNavController(R.id.nav_host_fragment)
            bottomNavigationView.setupWithNavController(navController)

            setMainEntityInViewModelFromBundle(viewModel, IntentExtras.APP_USER)
            setSearchedEntities1ListInViewModelFromBundle(viewModel, IntentExtras.SEARCHED_ITEMS_LIST)
            navigateToDestination(navController)
        } catch (e: Exception) {
            if (LOG_ME) ALog.d(TAG, "$methodName(): Exception: ")
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    fun navigateToProfileScreen(){
        val methodName: String = "navigateToProfileScreen"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val navController = findNavController(R.id.nav_host_fragment)
            navController.navigate(R.id.ActivityHomeScreenFragment5)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun setSearchedEntities1ListInViewModelFromBundle(
        viewModel : HomeScreenViewModel,
        searchedEntities1ListTag : String,
    ) {
        val methodName: String = "setSearchedEntites1ListInViewModelFromBundle"
        if (LOG_ME) ALog.d(
            TAG, "Method start: $methodName")
        try {
            val bundle : Bundle? = intent.extras
            bundle?.let {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): bundle != null")
                val searchedEntities1List : ArrayList<TaskAssignment>? =
                    it.get(searchedEntities1ListTag) as ArrayList<TaskAssignment>?
                searchedEntities1List?.let {
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): item != null")
                    viewModel.setSearchedEntities1List(it)
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun navigateToDestination(
        navController: NavController,
    ) {
        val methodName: String = "navigateToDestination"
        if (LOG_ME) ALog.d(
            TAG, "Method start: $methodName")
        try {
            val bundle : Bundle? = intent.extras
            bundle?.let {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): bundle != null")
                val destination : Int = it.getInt(IntentExtras.DESTINATION)
                if(destination != 0) {
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): destination == $destination")
                    if (destination != null) {
                        navController.navigate(destination)
                    }
                } else {
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): destination == 0")
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun setFragmentFactory(){
        val entryPoint = EntryPointAccessors.fromActivity(
            this,
            ActivityHomeScreenFragmentFactoryEntryPoint::class.java
        )

        fragmentFactory = entryPoint.getFieldServiceAppHomeScreenFragmentFactory()
        supportFragmentManager.fragmentFactory = fragmentFactory
    }

    override fun onSupportNavigateUp(): Boolean {
        return findNavController(R.id.nav_host_fragment)
                .navigateUp(appBarConfiguration as AppBarConfiguration)
                || super.onSupportNavigateUp()
    }

    override fun displayInputCaptureDialog(
            title: String,
            callback: DialogInputCaptureCallback
    ) {
        dialogInView = MaterialDialog(this).show {
            title(text = title)
            positiveButton(R.string.text_ok)
            onDismiss {
                dialogInView = null
            }
            cancelable(true)
        }
    }

    companion object {
        val DESTINATION_PROFILE_SCREEN = R.id.ActivityHomeScreenFragment5
    }
}