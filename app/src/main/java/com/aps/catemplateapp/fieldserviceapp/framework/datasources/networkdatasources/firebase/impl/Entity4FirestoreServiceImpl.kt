package com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.impl

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.implementation.BasicFirestoreServiceImpl
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity4
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.abs.Entity4FirestoreService
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.mappers.Entity4FirestoreMapper
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.model.Entity4FirestoreEntity
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class Entity4FirestoreServiceImpl
@Inject
constructor(
//    private val firebaseAuth: FirebaseAuth,
//    private val firestore: FirebaseFirestore,
    private val networkMapper: Entity4FirestoreMapper,
    private val dateUtil: DateUtil,
    private val userNetworkDataSource: UserNetworkDataSource,
): Entity4FirestoreService,
    BasicFirestoreServiceImpl<Entity4, Entity4FirestoreEntity, Entity4FirestoreMapper>(
//        firebaseAuth,
//        firestore,
        networkMapper,
        dateUtil
    ) {

    override fun getCollectionName(): String {
        return COLLECTION_NAME
    }

    override fun getDeletesCollectionName(): String {
        return DELETES_COLLECTION_NAME
    }

    override fun setEntityID(entity: Entity4, id: UniqueID) {
        entity.id = id
    }

    override fun getEntityID(entity: Entity4): UniqueID? {
        return entity.id
    }

    override fun setFirestoreEntityID(entity: Entity4FirestoreEntity, id: UniqueID) {
        entity.id = id
    }

    override fun updateEntityTimestamp(entity: Entity4, timestamp: String){
        entity.updated_at = timestamp
    }

    override fun updateFirestoreEntityTimestamp(entity: Entity4FirestoreEntity, timestamp: String){
        entity.updated_at = timestamp
    }

    override fun getFirestoreEntityType(): Class<Entity4FirestoreEntity> {
        return Entity4FirestoreEntity::class.java
    }

    override fun getFirestoreEntityID(entity: Entity4FirestoreEntity): UniqueID? {
        return entity.id
    }

    companion object {
        const val TAG = "Entity4FirestoreServiceImpl"
        const val LOG_ME = true
        const val COLLECTION_NAME = "entity4"
        const val DELETES_COLLECTION_NAME = "entity4_d"
    }
}