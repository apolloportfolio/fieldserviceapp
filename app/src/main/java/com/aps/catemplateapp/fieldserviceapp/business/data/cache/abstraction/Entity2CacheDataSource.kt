package com.aps.catemplateapp.fieldserviceapp.business.data.cache.abstraction

import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity2

interface Entity2CacheDataSource: StandardCacheDataSource<Entity2> {
    override suspend fun insertOrUpdateEntity(entity: Entity2): Entity2?

    override suspend fun insertEntity(entity: Entity2): Long

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int

    override suspend fun deleteEntities(entities: List<Entity2>): Int

    suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        name: String?,
        description: String?,

        ownerID: UserUniqueID?,
    ): Int

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<Entity2>

    override suspend fun getAllEntities(): List<Entity2>

    override suspend fun getEntityById(id: UniqueID?): Entity2?

    override suspend fun getNumEntities(): Int

    override suspend fun insertEntities(entities: List<Entity2>): LongArray
}