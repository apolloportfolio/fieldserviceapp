package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.layouts

import android.app.Activity
import androidx.compose.runtime.Composable
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.aps.catemplateapp.common.business.domain.state.StateEventTracker
import com.aps.catemplateapp.common.framework.presentation.PermissionHandlingData
import com.aps.catemplateapp.common.framework.presentation.views.DialogState
import com.aps.catemplateapp.common.framework.presentation.views.ProgressIndicatorState
import com.aps.catemplateapp.common.framework.presentation.views.SnackBarState
import com.aps.catemplateapp.common.framework.presentation.views.ToastState
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.TaskAssignment
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.HomeScreenDestination
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen1
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen2
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen3
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen4
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen5
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreenPlaceholder
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenStateEvent

@Composable
internal fun RightPaneNavGraphBuilder(
    launchStateEvent: (HomeScreenStateEvent) -> Unit,

    navigateToTaskAssignmentEditScreen: () -> Unit,
    navigateToEditTaskAssignmentFeedbackScreen: () -> Unit,
    navigateToEditTaskAssignmentNotesScreen: () -> Unit,
    dateUtil: DateUtil,

    activity: Activity?,
    permissionHandlingData: PermissionHandlingData,
    stateEventTracker: StateEventTracker,
    snackBarState: SnackBarState?,
    toastState: ToastState?,
    dialogState: DialogState?,
    progressIndicatorState: ProgressIndicatorState?,

    card1CurrentlyShownEntity: TaskAssignment? = null,
    card1ActionOnEntity: (TaskAssignment?, ()-> Unit) -> Unit,
    navigateToProfileScreen: () -> Unit,
): NavGraphBuilder.() -> Unit {
    return {
        composable(HomeScreenDestination.DetailsScreen1.route) {
            HomeScreenCompDetailsScreen1(
                launchStateEvent = launchStateEvent,
                navigateToTaskAssignmentEditScreen = navigateToTaskAssignmentEditScreen,
                navigateToEditTaskAssignmentFeedbackScreen = navigateToEditTaskAssignmentFeedbackScreen,
                navigateToEditTaskAssignmentNotesScreen = navigateToEditTaskAssignmentNotesScreen,
                dateUtil = dateUtil,

                activity = activity,
                permissionHandlingData = permissionHandlingData,
                stateEventTracker = stateEventTracker,
                snackBarState = snackBarState,
                toastState = toastState,
                dialogState = dialogState,
                progressIndicatorState = progressIndicatorState,

                entity = card1CurrentlyShownEntity,
                actionOnEntity = card1ActionOnEntity,
                navigateToProfileScreen = navigateToProfileScreen,
            )
        }
        composable(HomeScreenDestination.DetailsScreen2.route) {
            HomeScreenCompDetailsScreen2()
        }
        composable(HomeScreenDestination.DetailsScreen3.route) {
            HomeScreenCompDetailsScreen3()
        }
        composable(HomeScreenDestination.DetailsScreen4.route) {
            HomeScreenCompDetailsScreen4()
        }
        composable(HomeScreenDestination.DetailsScreen5.route) {
            HomeScreenCompDetailsScreen5()
        }
        composable(HomeScreenDestination.DetailsPlaceholderScreen.route) {
            HomeScreenCompDetailsScreenPlaceholder()
        }
    }
}