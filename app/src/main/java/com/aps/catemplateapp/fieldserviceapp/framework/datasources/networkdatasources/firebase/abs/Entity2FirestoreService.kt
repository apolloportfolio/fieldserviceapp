package com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.abs

import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.BasicFirestoreService
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity2
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.mappers.Entity2FirestoreMapper
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.model.Entity2FirestoreEntity

interface Entity2FirestoreService: BasicFirestoreService<
        Entity2,
        Entity2FirestoreEntity,
        Entity2FirestoreMapper
        > {
    suspend fun getUsersEntities2(userId: UserUniqueID): List<Entity2>?

}