package com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.model.Entity4CacheEntity
import com.google.firebase.firestore.GeoPoint
import java.util.*


@Dao
interface Entity4Dao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity : Entity4CacheEntity) : Long

    @Query("SELECT * FROM entity4")
    suspend fun get() : List<Entity4CacheEntity>
    
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEntities(entities: List<Entity4CacheEntity>): LongArray

    @Query("SELECT * FROM entity4 WHERE id = :id")
    suspend fun getEntityById(id: UniqueID?): Entity4CacheEntity?

    @Query("DELETE FROM entity4 WHERE id IN (:ids)")
    suspend fun deleteEntities(ids: List<UniqueID?>): Int

    @Query("DELETE FROM entity4")
    suspend fun deleteAllEntities()

    @Query("SELECT * FROM entity4")
    suspend fun getAllEntities(): List<Entity4CacheEntity>

    @Query(
        """
        UPDATE entity4 
        SET 
        created_at = :created_at,
        updated_at = :updated_at,
        latitude = :latitude,
        longitude = :longitude,
        geoLocation = :geoLocation,
        firestoreGeoLocation = :firestoreGeoLocation,
        picture1URI = :picture1URI,
        name = :name,
        description = :description
        WHERE id = :id
        """
    )
    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: GeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        name: String?,
        description : String?,
    ): Int

    @Query("DELETE FROM entity4 WHERE id = :primaryKey")
    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    @Query("SELECT * FROM entity4")
    suspend fun searchEntities(): List<Entity4CacheEntity>
    
    @Query("SELECT COUNT(*) FROM entity4")
    suspend fun getNumEntities(): Int
}