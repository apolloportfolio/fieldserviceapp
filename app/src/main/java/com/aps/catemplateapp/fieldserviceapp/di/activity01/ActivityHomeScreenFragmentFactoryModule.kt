package com.aps.catemplateapp.fieldserviceapp.di.activity01

import androidx.lifecycle.ViewModelProvider
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.ActivityHomeScreenFragmentFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Singleton

@FlowPreview
@ExperimentalCoroutinesApi
@Module
@InstallIn(SingletonComponent::class)
object ActivityHomeScreenFragmentFactoryModule {
    @JvmStatic
    @Singleton
    @Provides
    fun provideActivityHomeScreenFragmentFactory(
        viewModelFactory: ViewModelProvider.Factory,
        dateUtil: DateUtil
    ): ActivityHomeScreenFragmentFactory {
        return ActivityHomeScreenFragmentFactory(
            viewModelFactory,
            dateUtil
        )
    }
}