package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.views

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.RangeSlider
import androidx.compose.material.SliderDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.tooling.preview.Preview
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.framework.presentation.views.RangeRow
import com.aps.catemplateapp.common.framework.presentation.views.RowWithTextAndSwitch
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenCard1SearchFilters

private const val TAG = "HomeScreenCard1SFCol"
private const val LOG_ME = false

const val HomeScreenCard1SearchFiltersColumnTestTag = TAG
const val HomeScreenCard1SearchFiltersRange1TestTag = "HomeScreenCard1SearchFiltersRange1TestTag"
const val HomeScreenCard1SearchFiltersRange2TestTag = "HomeScreenCard1SearchFiltersRange2TestTag"
const val HomeScreenCard1SearchFiltersRangeSlider1TestTag = "HomeScreenCard1SearchFiltersRangeSlider1TestTag"
const val HomeScreenCard1SearchFiltersRangeSlider2TestTag = "HomeScreenCard1SearchFiltersRangeSlider2TestTag"

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun HomeScreenCard1SFCol(
    searchFilters: HomeScreenCard1SearchFilters,
    rangeSlider1Min: Float = 0.0f,
    rangeSlider1Max: Float = 200.0f,
    rangeSlider1StepSize: Float = 10.0f,
    rangeSlider2Min: Float = 0.0f,
    rangeSlider2Max: Float = 1000.0f,
    rangeSlider2StepSize: Float = 5.0f,
    toggleSearchFiltersUpdated: (Boolean) -> Unit,
    range1NameID: Int? = null,
    range2NameID: Int? = null,
    switch1TextID: Int? = null,
    switch2TextID: Int? = null,
    switch3TextID: Int? = null,
    switch4TextID: Int? = null,
    switch5TextID: Int? = null,
    switch6TextID: Int? = null,
    switch7TextID: Int? = null,
) {
    LazyColumn(
        modifier = Modifier
            .fillMaxWidth()
            .testTag(HomeScreenCard1SearchFiltersColumnTestTag)
    ) {
        if(LOG_ME)ALog.d(TAG, "(): " +
                                "Recomposition start. searchFilters == $searchFilters")
        if(range1NameID != null) {
            val searchFiltersRange1min =
                searchFilters.range1min?.toFloat() ?: (rangeSlider1Min + rangeSlider1StepSize)
            val searchFiltersRange1max =
                searchFilters.range1max?.toFloat() ?: (rangeSlider1Max - rangeSlider1StepSize)
            val numberOfSteps = (
                (searchFiltersRange1min+searchFiltersRange1max)/rangeSlider1StepSize
            ).toInt()

            item {
                RangeRow(
                    rangeNameID = range1NameID,
                    min = searchFilters.range1min,
                    max = searchFilters.range1max,
                    updateMin = { newValue ->
                        if(LOG_ME)ALog.d(TAG, "(): " +
                                "Min value changed in RangeRow from ${searchFilters.range1min} to " +
                                "$newValue.")
                        searchFilters.range1min = newValue
                        toggleSearchFiltersUpdated(true)
                    },
                    updateMax = { newValue ->
                        if(LOG_ME)ALog.d(TAG, "(): " +
                                "Max value changed in RangeRow from ${searchFilters.range1max} to " +
                                "$newValue.")
                        searchFilters.range1max = newValue
                        toggleSearchFiltersUpdated(true)
                    },
                    testTagPrefix = HomeScreenCard1SearchFiltersRange1TestTag
                )
            }

            item {
                val rangeSlider1ContentDescription = buildString {
                    append(stringResource(R.string.range_slider_content_description_from))
                    append(" ")
                    append(String.format("%.2f", searchFiltersRange1min))
                    append(" ")
                    append(stringResource(R.string.range_slider_content_description_to))
                    append(" ")
                    append(String.format("%.2f", searchFiltersRange1max))
                }
                RangeSlider(
                    value = searchFiltersRange1min..searchFiltersRange1max,
                    onValueChange = { newValues ->
                        if(LOG_ME)ALog.d(TAG, "(): " +
                                                "Value changed in RangeSlider.")
                        searchFilters.range1min = newValues.start.toDouble()
                        searchFilters.range1max = newValues.endInclusive.toDouble()
                        toggleSearchFiltersUpdated(true)
                    },
                    modifier = Modifier
                        .testTag(HomeScreenCard1SearchFiltersRangeSlider1TestTag)
                        .semantics { contentDescription =  rangeSlider1ContentDescription},
                    enabled = true,
                    valueRange = rangeSlider1Min..rangeSlider1Max,
                    steps = numberOfSteps,
                    onValueChangeFinished = null,
                    colors = SliderDefaults.colors(),
                )
            }
        } else {
            if(LOG_ME) ALog.w(TAG, "(): " +
                    "Can't show first RangeRow and RangeSlider because sth is null:\n" +
                    "range1NameID == $range1NameID")
        }

        if(range2NameID != null) {
            val searchFiltersRange2min =
                searchFilters.range2min?.toFloat() ?: (rangeSlider2Min + rangeSlider2StepSize)
            val searchFiltersRange2max =
                searchFilters.range2max?.toFloat() ?: (rangeSlider2Max - rangeSlider2StepSize)
            val numberOfSteps = (
                (searchFiltersRange2min+searchFiltersRange2max)/rangeSlider2StepSize
            ).toInt()

            item {
                RangeRow(
                    rangeNameID = range2NameID,
                    min = searchFilters.range2min,
                    max = searchFilters.range2max,
                    updateMin = { newValue ->
                        searchFilters.range2min = newValue
                        toggleSearchFiltersUpdated(true)
                    },
                    updateMax = { newValue ->
                        searchFilters.range2max = newValue
                        toggleSearchFiltersUpdated(true)
                    },
                    testTagPrefix = HomeScreenCard1SearchFiltersRange2TestTag,
                )
            }
            item {
                val rangeSlider2ContentDescription = buildString {
                    append(stringResource(R.string.range_slider_content_description_from))
                    append(" ")
                    append(String.format("%.2f", searchFiltersRange2min))
                    append(" ")
                    append(stringResource(R.string.range_slider_content_description_to))
                    append(" ")
                    append(String.format("%.2f", searchFiltersRange2max))
                }
                RangeSlider(
                    value = searchFiltersRange2min..searchFiltersRange2max,
                    onValueChange = { newValues ->
                        searchFilters.range2min = newValues.start.toDouble()
                        searchFilters.range2max = newValues.endInclusive.toDouble()
                        toggleSearchFiltersUpdated(true)
                    },
                    modifier = Modifier
                        .testTag(HomeScreenCard1SearchFiltersRangeSlider2TestTag)
                        .semantics { contentDescription =  rangeSlider2ContentDescription},
                    enabled = true,
                    valueRange = rangeSlider2Min..rangeSlider2Max,
                    steps = numberOfSteps,
                    onValueChangeFinished = null,
                    colors = SliderDefaults.colors(),
                )
            }
        } else {
            if(LOG_ME) ALog.w(TAG, "(): " +
                    "Can't show second RangeRow and RangeSlider because sth is null:\n" +
                    "range2NameID == $range2NameID")
        }


        if(switch1TextID != null) {
            item {
                RowWithTextAndSwitch(
                    switch1TextID,
                    searchFilters.switch1 ?: false
                ) { newSwitchValue ->
                    searchFilters.switch1 = newSwitchValue
                    toggleSearchFiltersUpdated(true)
                }
            }
        }

        if(switch2TextID != null) {
            item {
                RowWithTextAndSwitch(
                    switch2TextID,
                    searchFilters.switch2 ?: false
                ) { newSwitchValue ->
                    searchFilters.switch2 = newSwitchValue
                    toggleSearchFiltersUpdated(true)
                }
            }
        }

        if(switch3TextID != null) {
            item {
                RowWithTextAndSwitch(
                    switch3TextID,
                    searchFilters.switch3 ?: false
                ) { newSwitchValue ->
                    searchFilters.switch3 = newSwitchValue
                    toggleSearchFiltersUpdated(true)
                }
            }
        }

        if(switch4TextID != null) {
            item {
                RowWithTextAndSwitch(
                    switch4TextID,
                    searchFilters.switch4 ?: false
                ) { newSwitchValue ->
                    searchFilters.switch4 = newSwitchValue
                    toggleSearchFiltersUpdated(true)
                }
            }
        }

        if(switch5TextID != null) {
            item {
                RowWithTextAndSwitch(
                    switch5TextID,
                    searchFilters.switch5 ?: false
                ) { newSwitchValue ->
                    searchFilters.switch5 = newSwitchValue
                    toggleSearchFiltersUpdated(true)
                }
            }
        }

        if(switch6TextID != null) {
            item {
                RowWithTextAndSwitch(
                    switch6TextID,
                    searchFilters.switch6 ?: false
                ) { newSwitchValue ->
                    searchFilters.switch6 = newSwitchValue
                    toggleSearchFiltersUpdated(true)
                }
            }
        }

        if(switch7TextID != null) {
            item {
                RowWithTextAndSwitch(
                    switch7TextID,
                    searchFilters.switch7 ?: false
                ) { newSwitchValue ->
                    searchFilters.switch7 = newSwitchValue
                    toggleSearchFiltersUpdated(true)
                }
            }
        }


    }
}

//========================================================================================
@Preview
@Composable
fun HomeScreenCard1SearchFiltersColumnPreview() {
    HomeScreenTheme {
        HomeScreenCard1SFCol(
            searchFilters = HomeScreenCard1SearchFilters(
                range1min = 50.0,
                range1max = 100.0,
                range2min = 150.0,
                range2max = 700.0,
                switch1 = false,
                switch2 = false,
                switch3 = false,
                switch4 = false,
                switch5 = false,
                switch6 = false,
                switch7 = false,
            ),
            rangeSlider1Min = 0.0f,
            rangeSlider1Max = 200.0f,
            rangeSlider1StepSize = 10.0f,
            rangeSlider2Min = 0.0f,
            rangeSlider2Max = 1000.0f,
            rangeSlider2StepSize = 5.0f,
            toggleSearchFiltersUpdated = {  },
            range1NameID = R.string.example_range_1_lbl,
            range2NameID = R.string.example_range_2_lbl,
            switch1TextID = R.string.switch_1,
            switch2TextID = R.string.switch_2,
            switch3TextID = R.string.switch_3,
            switch4TextID = R.string.switch_4,
            switch5TextID = R.string.switch_5,
            switch6TextID = R.string.switch_6,
            switch7TextID = R.string.switch_7,
        )
    }
}