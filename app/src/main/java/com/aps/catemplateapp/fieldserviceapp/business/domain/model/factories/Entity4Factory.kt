package com.aps.catemplateapp.fieldserviceapp.business.domain.model.factories

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity4
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Entity4Factory
@Inject
constructor(
    private val dateUtil: DateUtil
) {
    fun createSingleEntity(
        id: UniqueID?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        name: String?,
        description : String?,
    ): Entity4 {
        return Entity4(
            id,
            dateUtil.getCurrentTimestamp(),
            dateUtil.getCurrentTimestamp(),

            latitude,
            longitude,
            geoLocation,
            firestoreGeoLocation,

            picture1URI,
            name,
            description,
        )
    }

    fun createEntitiesList(numEntities: Int): List<Entity4> {
        val list: ArrayList<Entity4> = ArrayList()
        for(i in 0 until numEntities){ // exclusive on upper bound
            list.add(
                createSingleEntity(
                    UniqueID.generateFakeRandomID(),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                )
            )
        }
        return list
    }


    fun generateEmpty(): Entity4 {
        return createSingleEntity(
            UniqueID.generateFakeRandomID(),
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        )
    }

    companion object {
        fun createPreviewEntitiesList(): List<Entity4> {
            return arrayListOf(
                Entity4(
                    id = UniqueID("test01"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    name = "test01",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium."
                ),
                Entity4(
                    id = UniqueID("test02"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    name = "test02",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium."
                ),
                Entity4(
                    id = UniqueID("test03"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    name = "test03",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium."
                ),
                Entity4(
                    id = UniqueID("test04"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23164,
                    longitude = 22.614167,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    name = "test04",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium."
                ),
                Entity4(
                    id = UniqueID("test05"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23254,
                    longitude = 22.615117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    name = "test05",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium."
                ),
                Entity4(
                    id = UniqueID("test06"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23354,
                    longitude = 22.614717,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    name = "test06",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium."
                ),
                Entity4(
                    id = UniqueID("test07"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.624117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    name = "test07",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium."
                ),
                Entity4(
                    id = UniqueID("test08"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.664117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    name = "test08",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium."
                ),
                Entity4(
                    id = UniqueID("test09"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.6147,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    name = "test09",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium."
                ),
                Entity4(
                    id = UniqueID("test10"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.714117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    name = "test10",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium."
                )
            )
        }
    }
}