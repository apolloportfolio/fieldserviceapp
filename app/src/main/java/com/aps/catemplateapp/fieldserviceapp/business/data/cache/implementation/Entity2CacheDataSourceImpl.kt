package com.aps.catemplateapp.fieldserviceapp.business.data.cache.implementation


import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fieldserviceapp.business.data.cache.abstraction.Entity2CacheDataSource
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity2
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.abs.Entity2DaoService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Entity2CacheDataSourceImpl
@Inject
constructor(
    private val entityDaoService: Entity2DaoService
): Entity2CacheDataSource {

    override suspend fun insertOrUpdateEntity(entity: Entity2): Entity2? {
        return entityDaoService.insertOrUpdateEntity(entity)
    }

    override suspend fun insertEntity(entity: Entity2): Long {
        return entityDaoService.insertEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return entityDaoService.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<Entity2>): Int {
        return entityDaoService.deleteEntities(entities)
    }

    override suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        name: String?,
        description: String?,

        ownerID: UserUniqueID?,
    ): Int {
        return entityDaoService.updateEntity(
            id,
            updated_at,
            created_at,

            picture1URI,

            name,
            description,

            ownerID,
        )
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<Entity2> {
        return entityDaoService.searchEntities()
    }

    override suspend fun getAllEntities(): List<Entity2> {
        return entityDaoService.getAllEntities()
    }

    override suspend fun getEntityById(id: UniqueID?): Entity2? {
        return entityDaoService.getEntityById(id)
    }

    override suspend fun getNumEntities(): Int {
        return entityDaoService.getNumEntities()
    }

    override suspend fun insertEntities(entities: List<Entity2>): LongArray{
        return entityDaoService.insertEntities(entities)
    }
}