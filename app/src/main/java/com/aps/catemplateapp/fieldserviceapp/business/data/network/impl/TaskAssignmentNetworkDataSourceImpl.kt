package com.aps.catemplateapp.fieldserviceapp.business.data.network.impl

import android.net.Uri
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fieldserviceapp.business.data.network.abs.TaskAssignmentNetworkDataSource
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.TaskAssignment
import com.aps.catemplateapp.fieldserviceapp.business.interactors.impl.FirestoreEntity1SearchParameters
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.abs.TaskAssignmentFirestoreService
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class TaskAssignmentNetworkDataSourceImpl
@Inject
constructor(
    private val firestoreService: TaskAssignmentFirestoreService
): TaskAssignmentNetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: TaskAssignment): TaskAssignment? {
        return firestoreService.insertOrUpdateEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        return firestoreService.deleteEntity(primaryKey)
    }

    override suspend fun insertDeletedEntity(entity: TaskAssignment) {
        return firestoreService.insertDeletedEntity(entity)
    }

    override suspend fun insertDeletedEntities(entities: List<TaskAssignment>) {
        return firestoreService.insertDeletedEntities(entities)
    }

    override suspend fun deleteDeletedEntity(entity: TaskAssignment) {
        return firestoreService.deleteDeletedEntity(entity)
    }

    override suspend fun getDeletedEntities(): List<TaskAssignment> {
        return firestoreService.getAllDeletedEntities()
    }

    override suspend fun deleteAllEntities() {
        firestoreService.deleteAllEntities()
    }

    override suspend fun searchEntity(entity: TaskAssignment): TaskAssignment? {
        return firestoreService.searchEntity(entity)
    }

    override suspend fun getAllEntities(): List<TaskAssignment> {
        return firestoreService.getAllEntities()
    }

    override suspend fun insertOrUpdateEntities(entities: List<TaskAssignment>): List<TaskAssignment>? {
        return firestoreService.insertOrUpdateEntities(entities)
    }

    override suspend fun getEntityById(id: UniqueID): TaskAssignment? {
        return firestoreService.getEntityById(id)
    }

    override suspend fun searchEntities(
        searchParameters : FirestoreEntity1SearchParameters
    ) : List<TaskAssignment>? {
        if(LOG_ME) ALog.d(TAG, ".searchEntities(): searchParameters:\n$searchParameters")
        return firestoreService.searchEntities(searchParameters)
    }

    override suspend fun getUserTakenJobs(userID: UserUniqueID) : List<TaskAssignment>? {
        return firestoreService.getUsersEntities1(userID)
    }

    override suspend fun getUserDoneJobs(userID: UserUniqueID) : List<TaskAssignment>? {
        return firestoreService.getUserDoneJobs(userID)
    }

    override suspend fun uploadEntity1PhotoToFirestore(
        entity : TaskAssignment,
        entitysPhotoUri : Uri,
        entitysPhotoNumber: Int,
    ) : String? {
        return firestoreService.uploadEntity1PhotoToFirestore(
            entity,
            entitysPhotoUri,
            entitysPhotoNumber
        )
    }

    companion object {
        const val TAG = "Entity1NetworkDataSourceImpl"
        const val LOG_ME = true
        const val COLLECTION_NAME = "entity1"
        const val DELETES_COLLECTION_NAME = "entity1_d"
    }
}
