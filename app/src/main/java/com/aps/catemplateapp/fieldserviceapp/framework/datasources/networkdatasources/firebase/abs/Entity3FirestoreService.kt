package com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.abs

import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.BasicFirestoreService
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity3
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.mappers.Entity3FirestoreMapper
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.model.Entity3FirestoreEntity

interface Entity3FirestoreService: BasicFirestoreService<
        Entity3,
        Entity3FirestoreEntity,
        Entity3FirestoreMapper
        > {
    suspend fun getUsersEntities3(userId: UserUniqueID): List<Entity3>?

}