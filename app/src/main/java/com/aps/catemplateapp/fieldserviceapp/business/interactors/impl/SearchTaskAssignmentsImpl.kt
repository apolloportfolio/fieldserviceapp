package com.aps.catemplateapp.fieldserviceapp.business.interactors.impl

import android.location.Location
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.cache.CacheResponseHandler
import com.aps.catemplateapp.common.business.data.network.ApiResponseHandler
import com.aps.catemplateapp.common.business.data.util.safeApiCall
import com.aps.catemplateapp.common.business.data.util.safeCacheCall
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.fieldserviceapp.business.data.cache.abstraction.TaskAssignmentCacheDataSource
import com.aps.catemplateapp.fieldserviceapp.business.data.network.abs.TaskAssignmentNetworkDataSource
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.TaskAssignment
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.fieldserviceapp.business.interactors.abs.SearchTaskAssignments
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val TAG = "SearchEntities1Impl"
private const val LOG_ME = true

class SearchTaskAssignmentsImpl
@Inject
constructor(
    private val taskAssignmentCacheDataSource: TaskAssignmentCacheDataSource,
    private val taskAssignmentNetworkDataSource: TaskAssignmentNetworkDataSource,
): SearchTaskAssignments {
    override fun searchTaskAssignments(
        location : Location?,
        searchParameters : HomeScreenViewState<ProjectUser>,
        stateEvent: StateEvent,
        returnViewState: HomeScreenViewState<ProjectUser>,
        updateReturnViewState: (HomeScreenViewState<ProjectUser>, List<TaskAssignment>?)-> HomeScreenViewState<ProjectUser>,
        onErrorAction: () -> Unit,
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?> = flow {
        val syncedEntities = safeCacheCall(Dispatchers.IO, onErrorAction = onErrorAction){
            if(LOG_ME) ALog.d(TAG, ".searchEntities1(): searchParameters: \n$searchParameters")
            syncEntities(location, searchParameters)
        }


        val response = object: CacheResponseHandler<HomeScreenViewState<ProjectUser>, List<TaskAssignment>>(
            response = syncedEntities,
            stateEvent = stateEvent
        ){
            override suspend fun handleSuccess(resultObj: List<TaskAssignment>): DataState<HomeScreenViewState<ProjectUser>>? {
                var message: String? = GET_ENTITIES_SUCCESS
                var uiComponentType: UIComponentType? = UIComponentType.None()
                if(LOG_ME)ALog.d(TAG, "searchEntities1().handleSuccess(): ")


                if(resultObj == null){
                    if(LOG_ME)ALog.d(TAG, "searchEntities1(): resultObj == null")
                    message = GET_ENTITY_NO_MATCHING_RESULTS
                    uiComponentType = UIComponentType.Toast()
                } else {
                    if(LOG_ME)ALog.d(TAG, "searchEntities1(): resultObj != null")
                    returnViewState.searchedAvailableJobs = ArrayList(resultObj)
                    updateReturnViewState(returnViewState, resultObj)
                }
                return DataState.data(
                    response = Response(
                        messageId = R.string.error,
                        message = message,
                        uiComponentType = uiComponentType as UIComponentType,
                        messageType = MessageType.Success()
                    ),
                    data = returnViewState,
                    stateEvent = stateEvent
                )
            }
        }.getResult()

        emit(response)
    }

    // Method downloads entities from network and saves them in cache
    private suspend fun syncEntities(
        location : Location?,
        searchParameters : HomeScreenViewState<ProjectUser>,
    ) : List<TaskAssignment> {
        val firestoreSearchParameters = FirestoreEntity1SearchParameters(searchParameters, location)
        val networkEntitiesList = getNetworkEntities(firestoreSearchParameters)
        return networkEntitiesList
    }

    private suspend fun getNetworkEntities(searchParameters : FirestoreEntity1SearchParameters): List<TaskAssignment>{
        val networkResult = safeApiCall(Dispatchers.IO){
            if(LOG_ME)ALog.d(TAG, ".getNetworkEntities(): searchParameters: \n$searchParameters")
            taskAssignmentNetworkDataSource.searchEntities(searchParameters)
        }

        val response = object: ApiResponseHandler<List<TaskAssignment>, List<TaskAssignment>>(
            response = networkResult,
            stateEvent = null
        ){
            override suspend fun handleSuccess(resultObj: List<TaskAssignment>): DataState<List<TaskAssignment>>? {
                return DataState.data(
                    response = null,
                    data = resultObj,
                    stateEvent = null
                )
            }
        }.getResult()

        return response?.data ?: ArrayList()
    }

    companion object {
        const val GET_ENTITIES_SUCCESS = "Successfully searched entities."
        const val GET_ENTITY_NO_MATCHING_RESULTS = "There are no entities that match that query."
        const val SEARCH_ENTITIES_FAILED = "Failed to retrieve the list of entities."
    }
}
