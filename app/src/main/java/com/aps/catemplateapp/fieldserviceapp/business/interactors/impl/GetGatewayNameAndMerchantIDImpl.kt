package com.aps.catemplateapp.fieldserviceapp.business.interactors.impl

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.platform.app.InstrumentationRegistry
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.network.ApiResponseHandler
import com.aps.catemplateapp.common.business.data.util.safeApiCall
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.core.util.SecureKeyStorage
import com.aps.catemplateapp.fieldserviceapp.business.interactors.abs.GetGatewayNameAndMerchantID
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val TAG = "GetGatewayNameAndMerchantIDImpl"
private const val LOG_ME = true

class GetGatewayNameAndMerchantIDImpl
@Inject
constructor(
    private val secureKeyStorage: SecureKeyStorage,
    private val dateUtil: DateUtil,
) : GetGatewayNameAndMerchantID {
    override fun getGatewayNameAndMerchantID(
        continueFlag: Boolean?,
        stateEvent : StateEvent,
        onErrorAction: () -> Unit,
        returnViewState : HomeScreenViewState<ProjectUser>,
        updateReturnViewState : (
            HomeScreenViewState<ProjectUser>,
            String?,
            String?,
            Boolean?,
        ) -> (HomeScreenViewState<ProjectUser>),
    ) : Flow<DataState<HomeScreenViewState<ProjectUser>>?> = flow {
        val methodName: String = "getGatewayNameAndMerchantID"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val safeApiCall = safeApiCall(
                dispatcher = Dispatchers.IO,
                onErrorAction = onErrorAction,
                apiCall = {
                    val gatewayName = secureKeyStorage.getGatewayNameForProject()
                    val merchantID = secureKeyStorage.getGatewayMerchantID()
                    if(gatewayName == null || merchantID == null) {
                        ALog.w(
                            TAG, ".$methodName(): " +
                                "gatewayName == $gatewayName " +
                                "merchantID == $merchantID")
                        null
                    } else {
                        Pair(gatewayName, merchantID)
                    }
                }
            )

            val networkDataNullMessage = try {
                (ApplicationProvider.getApplicationContext() as Context).getString(
                    R.string.failed_to_get_gateway_name_and_merchant_id
                )
            } catch(e: java.lang.Exception) {
                InstrumentationRegistry.getInstrumentation().targetContext.getString(
                    R.string.failed_to_get_gateway_name_and_merchant_id
                )
            }

            val response = object : ApiResponseHandler<HomeScreenViewState<ProjectUser>, Pair<String, String>?>(
                response = safeApiCall,
                stateEvent = stateEvent,
                networkDataNullMessage = networkDataNullMessage,
            ) {
                override suspend fun handleSuccess(resultObj: Pair<String, String>?): DataState<HomeScreenViewState<ProjectUser>>? {
                    val methodName: String = "handleSuccess"
                    if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
                    var messageId : Int
                    var message : String
                    var uiComponentType : UIComponentType
                    var messageType : MessageType

                    if(resultObj != null) {
                        if(LOG_ME)ALog.d(TAG, ".$methodName(): resultObj != null")
                        messageId = R.string.successfully_got_gateway_name_and_merchant_id
                        message = (ApplicationProvider.getApplicationContext() as Context).getString(messageId)
                        uiComponentType = UIComponentType.None()
                        messageType = MessageType.Success()

                        updateReturnViewState(
                            returnViewState,
                            resultObj.first,
                            resultObj.second,
                            continueFlag,
                        )
                    } else {
                        if(LOG_ME)ALog.d(TAG, ".$methodName(): resultObj == null")
                        messageId = R.string.failed_to_get_gateway_name_and_merchant_id
                        message = (ApplicationProvider.getApplicationContext() as Context).getString(messageId)
                        uiComponentType = UIComponentType.Toast()
                        messageType = MessageType.Error()
                    }

                    if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
                    return DataState.data(
                        response = Response(
                            messageId, message, uiComponentType, messageType,
                        ),
                        data = returnViewState,
                        stateEvent,
                    )
                }
            }.getResult()

            emit(response)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }
}