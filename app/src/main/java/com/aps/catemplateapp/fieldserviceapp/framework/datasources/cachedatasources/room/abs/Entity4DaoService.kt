package com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.abs

import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity4

interface Entity4DaoService {
    suspend fun insertOrUpdateEntity(entity: Entity4): Entity4

    suspend fun insertEntity(entity: Entity4): Long

    suspend fun insertEntities(Entities: List<Entity4>): LongArray

    suspend fun getEntityById(id: UniqueID?): Entity4?

    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        name: String?,
        description : String?,
    ): Int

    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteEntities(Entities: List<Entity4>): Int

    suspend fun searchEntities(): List<Entity4>

    suspend fun getAllEntities(): List<Entity4>

    suspend fun getNumEntities(): Int
}