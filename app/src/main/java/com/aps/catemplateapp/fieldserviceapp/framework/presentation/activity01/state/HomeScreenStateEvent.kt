package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.common.business.domain.state.StateMessage
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.TaskAssignment
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import java.io.File

sealed class HomeScreenStateEvent: StateEvent {


    object SearchUserTakenJobsStateEvent : HomeScreenStateEvent() {

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_search_items_error)
        }

        override fun eventName(): String {
            return "SearchUserTakenJobsStateEvent"
        }

        override fun shouldDisplayProgressBar() = true

    }

    object SearchUserDoneJobsStateEvent : HomeScreenStateEvent() {

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_search_items_error)
        }

        override fun eventName(): String {
            return "SearchUserDoneJobsStateEvent"
        }

        override fun shouldDisplayProgressBar() = true

    }

    class ActOnEntity1(
        val taskAssignment: TaskAssignment?,
        val callbackOnProfileIncomplete: () -> Unit,
    ): HomeScreenStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_act_on_entity1_error)
        }

        override fun eventName(): String {
            return "ActOnEntity1"
        }

        override fun shouldDisplayProgressBar() = true
    }

	class UpdateMainEntityEvent(
        val mainEntity: ProjectUser
    ): HomeScreenStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_update_main_entity_error)
        }

        override fun eventName(): String {
            return "UpdateMainEntityEvent"
        }

        override fun shouldDisplayProgressBar() = true
    }

    class CreateStateMessageEvent(
        val stateMessage: StateMessage
    ): HomeScreenStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_create_state_message_error)
        }

        override fun eventName(): String {
            return "CreateStateMessageEvent"
        }

        override fun shouldDisplayProgressBar() = false
    }

    object None: HomeScreenStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_none_error)
        }

        override fun eventName(): String {
            return "None"
        }

        override fun shouldDisplayProgressBar() = false
    }

    class SetDrawerOpen(
        val isDrawerOpen: Boolean?,
    ) : HomeScreenStateEvent() {

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context)
                .getString(R.string.homeScreen_state_event_set_drawer_open_error) + " $isDrawerOpen"
        }

        override fun eventName(): String {
            return "SetDrawerOpen"
        }

        override fun shouldDisplayProgressBar() = true

    }

    class GetAvailableJobsAroundUserStateEvent(
        val internalDirectory: File,
    ) : HomeScreenStateEvent() {

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_get_items_around_user_error)
        }

        override fun eventName(): String {
            return "GetEntities1AroundUserStateEvent"
        }

        override fun shouldDisplayProgressBar() = true

    }


    class SaveInFileAndClearSearchedEntities1ListStateEvent(
        val internalDirectory: File,
    ) : HomeScreenStateEvent() {

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_save_and_clear_searched_items_list_error)
        }

        override fun eventName(): String {
            return "SaveInFileAndClearSearchedEntities1ListStateEvent"
        }

        override fun shouldDisplayProgressBar() = true

    }

    object SearchAvailableJobsStateEvent : HomeScreenStateEvent() {

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_search_items_error)
        }

        override fun eventName(): String {
            return "SearchEntities1StateEvent"
        }

        override fun shouldDisplayProgressBar() = true

    }

    object ClearFiltersStateEvent : HomeScreenStateEvent() {

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_clear_filters_error)
        }

        override fun eventName(): String {
            return "ClearFiltersStateEvent"
        }

        override fun shouldDisplayProgressBar() = false

    }


    object GetEntities2StateEvent : HomeScreenStateEvent() {

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_get_entities1_error)
        }

        override fun eventName(): String {
            return "GetEntities2StateEvent"
        }

        override fun shouldDisplayProgressBar() = true

    }

    object ClearUIChangesInFragment5 : HomeScreenStateEvent() {

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_clear_ui_in_fragment5_error)
        }

        override fun eventName(): String {
            return "ClearUIChangesInFragment5"
        }

        override fun shouldDisplayProgressBar() = true

    }

    object AskUserAboutLogout : HomeScreenStateEvent() {

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_ask_user_about_logout_error)
        }

        override fun eventName(): String {
            return "AskUserAboutLogout"
        }

        override fun shouldDisplayProgressBar() = true

    }

    object LogoutUser : HomeScreenStateEvent() {

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_logout_user_error)
        }

        override fun eventName(): String {
            return "LogoutUser"
        }

        override fun shouldDisplayProgressBar() = true

    }

    class CheckGooglePayAvailability(
        val context: Context,
        val continueFlag: Boolean?,
    ) : HomeScreenStateEvent() {

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_check_google_pay_availability_error)
        }

        override fun eventName(): String {
            return "CheckGooglePayAvailability"
        }

        override fun shouldDisplayProgressBar() = true

    }

    class GetMerchantName(
        val continueFlag: Boolean?,
    ) : HomeScreenStateEvent() {

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_get_merchant_name_error)
        }

        override fun eventName(): String {
            return "GetMerchantName"
        }

        override fun shouldDisplayProgressBar() = true

    }

    class ShowDialogSayingThatGooglePayIsUnavailable(
        val context: Context,
    ): HomeScreenStateEvent() {

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_show_dialog_saying_that_google_pay_is_unavailable_error)
        }

        override fun eventName(): String {
            return "ShowDialogSayingThatGooglePayIsUnavailable"
        }

        override fun shouldDisplayProgressBar() = false

    }

    class DownloadExchangeRates(
        val currencies: ArrayList<String>,
        val continueFlag: Boolean?,
    ): HomeScreenStateEvent() {

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_download_exchange_rates_error)
        }

        override fun eventName(): String {
            return "DownloadExchangeRates"
        }

        override fun shouldDisplayProgressBar() = true

    }

    class InitiateGooglePayPaymentProcess(
        val context: Context,
    ): HomeScreenStateEvent() {

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_initialize_google_pay_process_error)
        }

        override fun eventName(): String {
            return "InitiateGooglePayPaymentProcess"
        }

        override fun shouldDisplayProgressBar() = true

    }

    class GetGatewayNameAndMerchantID(
        val continueFlag: Boolean?,
    ): HomeScreenStateEvent() {

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_get_gateway_name_and_merchant_id_error)
        }

        override fun eventName(): String {
            return "GetGatewayNameAndMerchantID"
        }

        override fun shouldDisplayProgressBar() = true

    }

    object GetEntities3 : HomeScreenStateEvent() {

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_get_entities3_error)
        }

        override fun eventName(): String {
            return "GetEntities3"
        }

        override fun shouldDisplayProgressBar() = true

    }

    object GetEntities4 : HomeScreenStateEvent() {

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_get_entities3_error)
        }

        override fun eventName(): String {
            return "GetEntities4"
        }

        override fun shouldDisplayProgressBar() = true

    }

    class RateApplication(
        val rateAppFunction : () -> (Unit),
    ): HomeScreenStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_navigate_to_rate_app_error)
        }

        override fun eventName(): String {
            return "RateApplication"
        }

        override fun shouldDisplayProgressBar() = false
    }

    class Navigate(
        val navigationFunction : () -> (Unit),
    ): HomeScreenStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_navigate_error)
        }

        override fun eventName(): String {
            return "Navigate"
        }

        override fun shouldDisplayProgressBar() = false
    }

    class RecommendApp(
        val recommendationFunction : () -> (Unit)
    ) : HomeScreenStateEvent() {

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_recommend_app_error)
        }

        override fun eventName(): String {
            return "RecommendApp"
        }

        override fun shouldDisplayProgressBar() = false

    }

    class ReportProblem(
        val reportProblemFunction : () -> (Unit)
    ) : HomeScreenStateEvent() {

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_report_problem_error)
        }

        override fun eventName(): String {
            return "ReportProblem"
        }

        override fun shouldDisplayProgressBar() = false

    }

    object NotifyUserAboutLackOfEmailAppInstalled : HomeScreenStateEvent() {

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_notify_that_some_no_email_app_is_installed_error)
        }

        override fun eventName(): String {
            return "NotifyUserAboutLackOfEmailAppInstalled"
        }

        override fun shouldDisplayProgressBar() = false

    }

    object GetUsersRating : HomeScreenStateEvent() {

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.homeScreen_state_event_get_users_rating_error)
        }

        override fun eventName(): String {
            return "GetUsersRating"
        }

        override fun shouldDisplayProgressBar() = true

    }

}
