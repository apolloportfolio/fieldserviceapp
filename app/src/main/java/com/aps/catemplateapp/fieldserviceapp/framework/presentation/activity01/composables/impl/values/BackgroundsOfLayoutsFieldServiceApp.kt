package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.values

import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import com.aps.catemplateapp.common.framework.presentation.views.BackgroundsOfLayouts
import com.aps.catemplateapp.common.framework.presentation.views.BackgroundsWithGradientsImpl
import com.aps.catemplateapp.common.framework.presentation.views.DrawnBackground

object BackgroundsOfLayoutsFieldServiceApp: BackgroundsOfLayouts {
    @Composable
    override fun backgroundTitleType01(
        colors: List<Color>?,
        brush: Brush?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground {
        val defaultColors = listOf(
            MaterialTheme.colors.surface,
            MaterialTheme.colors.primary,
            MaterialTheme.colors.primaryVariant,
        )
        return BackgroundsWithGradientsImpl.backgroundRadiantGradientFromCenterToOutside(
            colors = colors ?: defaultColors,
            radius = null,
            shape = shape,
            alpha = alpha,
        )
    }
    @Composable
    override fun backgroundListItemType01(
        colors: List<Color>?,
        brush: Brush?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground {
        val defaultColors = listOf(
            MaterialTheme.colors.primary,
            MaterialTheme.colors.surface,
        )
        return BackgroundsWithGradientsImpl.backgroundVerticalGradient(
            colors = colors ?: defaultColors,
            radius = null,
            shape = shape,
            alpha = alpha,
        )
    }

    @Composable
    override fun backgroundScreen01(
        colors: List<Color>?,
        brush: Brush?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground {
        val defaultColors = listOf(
            MaterialTheme.colors.surface,
            MaterialTheme.colors.primary,
        )
        return BackgroundsWithGradientsImpl.backgroundRadiantGradientFromCenterToOutside(
            colors = colors ?: defaultColors,
            radius = null,
            shape = shape,
            alpha = alpha,
        )
    }
}