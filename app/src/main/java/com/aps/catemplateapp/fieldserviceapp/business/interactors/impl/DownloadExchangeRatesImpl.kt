package com.aps.catemplateapp.fieldserviceapp.business.interactors.impl

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.aps.catemplateapp.R
import com.aps.catemplateapp.core.business.domain.model.currencies.ApiProvider
import com.aps.catemplateapp.core.business.domain.model.currencies.ExchangeRates
import com.aps.catemplateapp.core.business.domain.model.currencies.Rate
import com.aps.catemplateapp.fieldserviceapp.business.interactors.abs.DownloadExchangeRates
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenViewState
import com.aps.catemplateapp.common.business.data.network.ApiResponseHandler
import com.aps.catemplateapp.common.business.data.util.safeApiCall
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.core.business.domain.model.currencies.Currency
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import javax.inject.Inject

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.awaitResult
import com.github.kittinunf.fuel.moshi.moshiDeserializerOf
import com.github.kittinunf.result.map
import com.squareup.moshi.*
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import java.io.IOException
import java.util.*
import com.github.kittinunf.result.Result

private const val TAG = "DownloadExchangeRatesImpl"
private const val LOG_ME = true

class DownloadExchangeRatesImpl
@Inject constructor(): DownloadExchangeRates {
    override fun downloadExchangeRates(
        baseCurrency: Currency,
        stateEvent : StateEvent,
        onErrorAction: () -> Unit,
        returnViewState : HomeScreenViewState<ProjectUser>,
        updateReturnViewState : (HomeScreenViewState<ProjectUser>, ExchangeRates?) -> (HomeScreenViewState<ProjectUser>),
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?> = flow {
        val downloadExchangeRatesApiCall = safeApiCall(Dispatchers.IO, onErrorAction = onErrorAction){
            downloadExchangeRates(baseCurrency)
        }


        val response = object: ApiResponseHandler<HomeScreenViewState<ProjectUser>, ExchangeRates?>(
            response = downloadExchangeRatesApiCall,
            stateEvent = stateEvent
        ){
            override suspend fun handleSuccess(resultObj: ExchangeRates?): DataState<HomeScreenViewState<ProjectUser>>? {
                val methodName: String = "handleSuccess"
                if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
                var messageId : Int
                var message : String
                var uiComponentType : UIComponentType
                var messageType : MessageType

                if(resultObj != null) {
                    if(LOG_ME) ALog.d(TAG, ".$methodName(): resultObj != null")
                    messageId = R.string.successfully_downloaded_exchange_rates
                    message = (ApplicationProvider.getApplicationContext() as Context).getString(messageId)
                    uiComponentType = UIComponentType.None()
                    messageType = MessageType.Success()

                    updateReturnViewState(returnViewState, resultObj)
                } else {
                    if(LOG_ME) ALog.d(TAG, ".$methodName(): resultObj == null")
                    messageId = R.string.failed_to_download_exchange_rates
                    message = (ApplicationProvider.getApplicationContext() as Context).getString(messageId)
                    uiComponentType = UIComponentType.Toast()
                    messageType = MessageType.Error()
                }

                if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
                return DataState.data(
                    response = Response(
                        messageId, message, uiComponentType, messageType,
                    ),
                    data = returnViewState,
                    stateEvent,
                )
            }
        }.getResult()

        emit(response)
    }

    // https://github.com/sal0max/currencies
    private suspend fun downloadExchangeRates(baseCurrency: Currency): ExchangeRates? {
        val methodName: String = "downloadExchangeRates"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val exchangeRatesProvider1 = ApiProvider.EXCHANGERATE_HOST
            val exchangeRatesProvider2 = ApiProvider.FRANKFURTER_APP
            val exchangeRatesProvider3 = ApiProvider.FER_EE

            var exchangeRatesProvider = exchangeRatesProvider1
            var downloadAllExchangeRatesResult = getRates(exchangeRatesProvider, null, baseCurrency)
            if(downloadAllExchangeRatesResult.component1() != null &&
                downloadAllExchangeRatesResult.component2() == null) {
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): Downloaded exchange rates from ${exchangeRatesProvider.baseUrl}")
                    return downloadAllExchangeRatesResult.component1()
            } else {
                ALog.w(
                    TAG, ".$methodName(): " +
                        "For ${exchangeRatesProvider.baseUrl} " +
                        "exchangeRates == $ ${downloadAllExchangeRatesResult.component1()} \nand " +
                        "fuelError == ${downloadAllExchangeRatesResult.component2()}")
            }

            exchangeRatesProvider = exchangeRatesProvider2
            downloadAllExchangeRatesResult = getRates(exchangeRatesProvider, null, baseCurrency)
            if(downloadAllExchangeRatesResult.component1() != null &&
                downloadAllExchangeRatesResult.component2() == null) {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): Downloaded exchange rates from ${exchangeRatesProvider.baseUrl}")
                    return downloadAllExchangeRatesResult.component1()
            } else {
                ALog.w(
                    TAG, ".$methodName(): " +
                        "For ${exchangeRatesProvider.baseUrl} " +
                        "exchangeRates == $ ${downloadAllExchangeRatesResult.component1()} \nand " +
                        "fuelError == ${downloadAllExchangeRatesResult.component2()}")
            }

            exchangeRatesProvider = exchangeRatesProvider3
            downloadAllExchangeRatesResult = getRates(exchangeRatesProvider, null, baseCurrency)
            if(downloadAllExchangeRatesResult.component1() != null &&
                downloadAllExchangeRatesResult.component2() == null) {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): Downloaded exchange rates from ${exchangeRatesProvider.baseUrl}")
                    return downloadAllExchangeRatesResult.component1()
            } else {
                ALog.w(
                    TAG, ".$methodName(): " +
                        "For ${exchangeRatesProvider.baseUrl} " +
                        "exchangeRates == $ ${downloadAllExchangeRatesResult.component1()} \nand " +
                        "fuelError == ${downloadAllExchangeRatesResult.component2()}")
            }

        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }

        ALog.w(TAG, ".$methodName(): Failed to download exchange rates from all exchange rates providers.")
        return null
    }



    /**
     * Get all the current exchange rates from the given api provider. Base will be Euro.
     */
    private suspend fun getRates(apiProvider: ApiProvider, date: LocalDate? = null, base: Currency): Result<ExchangeRates, FuelError> {
        // Currency conversions are done relatively to each other - so it basically doesn't matter
        // which base is used here. However, Euro is a strong currency, preventing rounding errors.
        val base = Currency.EUR
        val dateString = if (date != null) date.format(DateTimeFormatter.ISO_LOCAL_DATE) else "latest"

        return Fuel.get(
            when (apiProvider) {
                ApiProvider.EXCHANGERATE_HOST -> apiProvider.baseUrl +
                        "/$dateString" +
                        "?base=$base" +
                        "&v=${UUID.randomUUID()}"
                ApiProvider.FRANKFURTER_APP -> apiProvider.baseUrl +
                        "/$dateString" +
                        "?base=$base"
                ApiProvider.FER_EE -> apiProvider.baseUrl +
                        "/$dateString" +
                        "?base=$base"
            }
        ).awaitResult(
            moshiDeserializerOf(
                Moshi.Builder()
                    .addLast(KotlinJsonAdapterFactory())
                    .add(RatesAdapter(base))
                    .add(LocalDateAdapter())
                    .build()
                    .adapter(ExchangeRates::class.java)
            )
        ).map { timeline ->
            timeline.copy(provider = apiProvider)
        }
    }

    /*
     * Converts currency object to array of currencies.
     * Also removes some unwanted values and adds some wanted ones.
     */
    @Suppress("unused", "UNUSED_PARAMETER")
    internal class RatesAdapter(private val base: Currency) {

        @Synchronized
        @FromJson
        @Suppress("SpellCheckingInspection")
        @Throws(IOException::class)
        fun fromJson(reader: JsonReader): List<Rate> {
            val list = mutableListOf<Rate>()
            reader.beginObject()
            // convert
            while (reader.hasNext()) {
                val name: String = reader.nextName()
                val value: Double = reader.nextDouble()
                // filter these:
                if (name != "BTC" // Bitcoin
                    && name != "CLF" // Unidad de Fomento
                    && name != "XDR" // special drawing rights
                    && name != "XAG" // silver
                    && name != "XAU" // gold
                    && name != "XPD" // palladium
                    && name != "XPT" // platinum
                    && name != "MRO" // Mauritanian ouguiya (pre-2018)
                    && name != "STD" // S??o Tom?? and Pr??ncipe dobra (pre-2018)
                    && name != "VEF" // Venezuelan bol??var fuerte (old)
                    && name != "CNH" // Chinese renminbi (Offshore)
                    && name != "CUP" // Cuban peso (moneda nacional)
                ) {
                    Currency.fromString(name)?.let { list.add(Rate(it, value.toFloat())) }
                }
            }
            reader.endObject()
            // add base - but only if it's missing in the api response!
            if (list.find { rate -> rate.currency == base } == null)
                list.add(Rate(base, 1f))
            // also add Faroese kr??na (same as Danish krone) if it isn't already there - I simply like it!
            if (list.find { it.currency == Currency.FOK } == null)
                list.find { it.currency == Currency.DKK }?.value?.let { dkk ->
                    list.add(Rate(Currency.FOK, dkk))
                }
            return list
        }

        @Synchronized
        @ToJson
        @Throws(IOException::class)
        fun toJson(writer: JsonWriter, value: List<Rate>?) {
            writer.nullValue()
        }

    }

    @Suppress("unused", "UNUSED_PARAMETER")
    internal class LocalDateAdapter {

        @Synchronized
        @FromJson
        @Throws(IOException::class)
        fun fromJson(reader: JsonReader): LocalDate? {
            return LocalDate.parse(reader.nextString())
        }

        @Synchronized
        @ToJson
        @Throws(IOException::class)
        fun toJson(writer: JsonWriter, value: LocalDate?) {
            writer.value(value?.toString())
        }

    }

}