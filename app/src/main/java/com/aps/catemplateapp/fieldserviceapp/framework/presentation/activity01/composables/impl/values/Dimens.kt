package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.values

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.ui.unit.dp

object Dimens {
    const val fragmentsSwipingLeftSideThresholdFraction = 0.15f
    val activityHorizontalMargin = 16.dp
    val activityVerticalMargin = 16.dp
    val listItemHorizontalMargin = 3.dp
    val listItemVerticalMargin = 3.dp
    val fragmentHorizontalMargin = 3.dp
    val fragmentVerticalMargin = 3.dp
    val buttonHorizontalMargin = 6.dp
    val buttonVerticalMargin = 6.dp
    val listTileHorizontalMargin = 6.dp
    val listTileVerticalMargin = 6.dp
    val listItemHorizontalPadding = 6.dp
    val listItemVerticalPadding = 4.dp
    val marginBetweenTextViews = 12.dp
    val paragraphVerticalMargin = 60.dp
    val paragraphHorizontalMargin = 60.dp
    val paragraphVerticalMarginSmall = 16.dp
    val paragraphHorizontalMarginSmall = 16.dp
    val roundedCornerRadius = 40.dp
    val roundedCornerMargin = 8.dp
    val roundedCornerMarginForButtons = 12.dp
    val floatingActionButtonMargin = 16.dp
    val floatingActionButtonImageSize = 44.dp
    val toolbarHorizontalMargin = 16.dp
    val toolbarVerticalMargin = 16.dp
    val profilePicVerticalMargin = 16.dp
    val profilePicHorizontalMargin = 16.dp
    val layoutWithShadowHorizontalPadding = 4.dp
    val layoutWithShadowVerticalPadding = 4.dp
    val buttonRoundedCornerRadius = 4.dp
    val datePickerNegativeHorizontalMargin = (-20).dp
    val datePickerNegativeVerticalMargin = (-40).dp
    val datePickerNegativeVerticalMarginSmall = (-20).dp
    val datePickerNegativeHorizontalPadding = (-10).dp
    val datePickerNegativeVerticalPadding = 0.dp
    val timePickerNegativeHorizontalMargin = (-40).dp
    val timePickerNegativeVerticalMargin = (-40).dp
    val timePickerPositiveVerticalMargin = 20.dp
    val timePickerNegativeHorizontalPadding = 6.dp
    val timePickerNegativeVerticalPadding = 0.dp
    val textViewToEditTextAlignmentPadding = 4.dp
    val searchEntities1BottomSheetPeekHeight = 54.dp
    val activationEmailBigIconNegativeVerticalMargin = (-20).dp
    val verticalMarginForTextViewThatIsLabelForEditTextOnItsLeft = 16.dp
    val verticalMarginForEditTextThatHasTextViewLabelOnItsLeft = 10.dp
    val verticalMarginForSpinnerThatHasTextViewLabelOnItsLeft = 10.dp
    val floatingWindowVerticalMargin = 40.dp
    val floatingWindowHorizontalMargin = 40.dp
    val spacingSmall = 8.dp
    val spacingMedium = 16.dp
    val spacingLarge = 24.dp
    val columnPadding = PaddingValues(2.dp, 4.dp, 2.dp, 4.dp)
    val defaultPadding = PaddingValues(4.dp, 4.dp, 4.dp, 4.dp)
    val paddingMedium = PaddingValues(8.dp, 8.dp, 8.dp, 8.dp)
    val elevationVerySmall = 4.dp
    val elevationSmall = 8.dp
    val elevationMedium = 16.dp
    val elevationLarge = 24.dp
    const val titleFontSize = 32
}
