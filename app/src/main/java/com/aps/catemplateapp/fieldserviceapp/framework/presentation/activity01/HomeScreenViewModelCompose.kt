package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.test.core.app.ApplicationProvider
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.AreYouSureCallback
import com.aps.catemplateapp.common.business.domain.state.DataState
import com.aps.catemplateapp.common.business.domain.state.MessageType
import com.aps.catemplateapp.common.business.domain.state.OkButtonCallback
import com.aps.catemplateapp.common.business.domain.state.Response
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.common.business.domain.state.StateMessage
import com.aps.catemplateapp.common.business.domain.state.UIComponentType
import com.aps.catemplateapp.common.framework.presentation.BaseViewModelCompose
import com.aps.catemplateapp.common.framework.presentation.views.DialogState
import com.aps.catemplateapp.common.framework.presentation.views.SnackBarState
import com.aps.catemplateapp.common.framework.presentation.views.ToastState
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DeviceLocation
import com.aps.catemplateapp.common.util.extensions.convertToString
import com.aps.catemplateapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.currencies.ExchangeRates
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.core.util.GooglePayPaymentsUtil
import com.aps.catemplateapp.core.util.ProjectConstants
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.TaskAssignment
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity2
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity4
import com.aps.catemplateapp.fieldserviceapp.business.interactors.HomeScreenInteractors
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.HomeScreenDestination
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.CollapsingToolbarState
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenCard1SearchFilters
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenInteractionManager
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenInteractionState
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenStateEvent
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenViewState
import com.google.android.gms.tasks.Task
import com.google.android.gms.wallet.PaymentData
import com.google.android.gms.wallet.PaymentDataRequest
import com.google.android.gms.wallet.PaymentsClient
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import java.io.File
import java.util.Date
import javax.inject.Inject

private const val TAG = "HomeScreenViewModelCompose"
private const val LOG_ME = true
private const val SEARCHED_ENTITIES1_LIST_FILE_NAME = "se1l"

@ExperimentalCoroutinesApi
@FlowPreview
@HiltViewModel
class HomeScreenViewModelCompose
@Inject
constructor(
): BaseViewModelCompose<ProjectUser, HomeScreenViewState<ProjectUser>>() {


    // === Legacy code used with views
    @Inject
    lateinit var interactors: HomeScreenInteractors<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            HomeScreenViewState<ProjectUser>
            >

    private val interactionManager: HomeScreenInteractionManager =
        HomeScreenInteractionManager()

    // Fragment 1 interaction states
    val searchQueryEdittextInteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.searchQueryEdittextInteractionState
    val range1ValueMinInteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.searchOptionsBottomSheetRange1MinInteractionState
    val range1ValueMaxInteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.searchOptionsBottomSheetRange1MaxInteractionState
    val range1SeekbarInteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.range1SeekbarInteractionState
    val searchOptionsBottomSheetPricePerKmOverLimitValueMinInteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.searchOptionsBottomSheetRange2MinInteractionState
    val pricePerKmOverLimitValueMaxInteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.searchOptionsBottomSheetRange2MaxInteractionState
    val range2SeekbarInteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.range2SeekbarInteractionState
    val switch1InteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.switch1InteractionState
    val switch2InteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.switch2InteractionState
    val switch3InteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.switch3InteractionState
    val switch4InteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.switch4InteractionState
    val switch5InteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.switch5InteractionState
    val switch6InteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.switch6InteractionState
    val switch7InteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.switch7InteractionState

    // Fragment5 (user's profile) interaction states
    val usersNameEdittextInteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.usersNameEdittextInteractionState
    val usersCityEdittextInteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.usersCityEdittextInteractionState
    val usersDescriptionEdittextInteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.usersDescriptionEdittextInteractionState

    private val _collapsingToolbarState: LiveData<CollapsingToolbarState>
        get() = interactionManager.collapsingToolbarState
    // === Legacy code used with views

    private var _googlePaymentsClient: PaymentsClient? = null
    fun getGooglePaymentsClient(): PaymentsClient? = _googlePaymentsClient


    override fun handleNewData(data: HomeScreenViewState<ProjectUser>) {
        val methodName: String = "handleNewData"
        data.let { viewState ->
            viewState.location?.let { location ->
                if(LOG_ME) ALog.d(TAG, "$methodName(): Handling viewState.location")
                setLocation(location)
            }

            viewState.mainEntity?.let { mainEntity ->
                if(LOG_ME) ALog.d(TAG, "$methodName(): Handling viewState.mainEntity")
                setMainEntity(mainEntity)
            }

            viewState.searchedAvailableJobs?.let { searchedEntitiesList ->
                if(LOG_ME) ALog.d(TAG, "$methodName(): Handling viewState.searchedEntities1List")
                setSearchedEntities1List(searchedEntitiesList)

                if(LOG_ME) {
                    searchedEntitiesList.let {
                        ALog.d(
                            TAG,
                            "handleNewData(): searchedEntitiesList != null    ${searchedEntitiesList!!.size}"
                        )
                        for((index, entity) in searchedEntitiesList!!) {
                            ALog.d(TAG, ".handleNewData(): Entity #$index: ${entity.toString()}")
                        }
                    }
                }
            }

            viewState.userTakenJobs?.let { list ->
                if(LOG_ME) ALog.d(TAG, "$methodName(): Handling viewState.entities2List")
                setUserTakenJobsList(list)
            }

            viewState.userDoneJobs?.let { list ->
                if(LOG_ME) ALog.d(
                    TAG, "$methodName(): " +
                            "Handling viewState.entities3List"
                )
                setUserDoneJobsList(list)
            }

            viewState.entities4List?.let { list ->
                if(LOG_ME) ALog.d(TAG, "$methodName(): Handling viewState.entities4List")
                setEntities4List(list)
            }

            viewState.listOfEntities?.let { listOfEntities ->
                if(LOG_ME) ALog.d(TAG, "$methodName(): Handling viewState.listOfEntities")
                setListOfEntities(listOfEntities)
            }

            viewState.noCachedEntity?.let { noCachedEntity ->
                if(LOG_ME) ALog.d(TAG, "$methodName(): Handling viewState.noCachedEntity")
                setNoCachedEntity(noCachedEntity)
            }

        }
    }

    private fun searchedEntities1ListIsFresh(internalDirectory: File): Boolean {
        if (LOG_ME) ALog.d(TAG, "Method start: searchedEntities1ListIsFresh()")
        try {
            internalDirectory.mkdirs()
            val searchedEntities1ListFile =
                File(internalDirectory, SEARCHED_ENTITIES1_LIST_FILE_NAME)
            if(LOG_ME) ALog.d(
                TAG, ".searchedEntities1ListIsFresh(): " +
                        "searchedEntities1ListFile.path == ${searchedEntities1ListFile.path}"
            )
            if(searchedEntities1ListFile == null) {
                if(LOG_ME) ALog.w(
                    TAG, ".searchedEntities1ListIsFresh(): " +
                            "searchedEntities1ListFile == null"
                )
                return false
            }
            if(!searchedEntities1ListFile.exists()) {
                if(LOG_ME) ALog.d(
                    TAG, ".searchedEntities1ListIsFresh(): " +
                            "searchedEntities1ListFile does not exist."
                )
                return false
            }
            val currentViewState = getCurrentViewStateOrNew()
            if(currentViewState.searchedEntities1ListRefreshDate == null) {
                if(LOG_ME) ALog.d(
                    TAG, ".searchedEntities1ListIsFresh(): " +
                            "searchedEntities1ListRefreshDate is null"
                )
                return false
            }
            val freshDate = Date()
            freshDate.time -= SEARCHED_ENTITIES1_FRESHNESS_INTERVAL
            if(currentViewState.searchedEntities1ListRefreshDate!!.before(freshDate)) {
                if(LOG_ME) ALog.d(
                    TAG, ".searchedEntities1ListIsFresh(): " +
                            "searchedEntities1ListRefreshDate is before freshDate"
                )
                return false
            }
            return true
        } catch (e: Exception) {
            ALog.e(TAG, "searchedEntities1ListIsFresh()", e)
            return false
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: searchedEntities1ListIsFresh()")
        }
    }

    override fun setStateEvent(stateEvent: StateEvent) {
        val methodName: String = "setStateEvent()"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        var launchJob = true
        val job: Flow<DataState<HomeScreenViewState<ProjectUser>>?> = when(stateEvent){

            is HomeScreenStateEvent.SetDrawerOpen -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: SetDrawerOpen(${stateEvent.isDrawerOpen})")
                setIsDrawerOpen(stateEvent.isDrawerOpen)
                emitDoNothingStateMessageEvent(stateEvent)
            }

            is HomeScreenStateEvent.GetAvailableJobsAroundUserStateEvent -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: GetEntities1AroundUserStateEvent")
                if(searchedEntities1ListIsFresh(stateEvent.internalDirectory)) {
                    if(LOG_ME) ALog.d(
                        TAG, ".setStateEvent(): " +
                                "searchedEntities1List is fresh, restoring it from file"
                    )
                    restoreSearchedEntities1ListFromFile(stateEvent.internalDirectory)
                    setRefreshListOfSearchedEntities1(true)
                    emitDoNothingStateMessageEvent(stateEvent)
                } else {
                    notifyStateEventStarting(stateEvent)
                    interactors.taskAssignmentsAroundUser.getTaskAssignmentsAroundUser(
                        location = getCurrentViewStateOrNew().location,
                        stateEvent = stateEvent,
                        onErrorAction = {
                            notifyStateEventEnded(stateEvent)
                        },
                        returnViewState = getCurrentViewStateOrNew(),
                        updateReturnViewState = {
                                viewState, foundEntities1 -> viewState.apply {
                            searchedAvailableJobs = ArrayList(foundEntities1)
                                    searchedEntities1ListRefreshDate = Date()
                                    saveSearchedEntities1ListInFile(stateEvent.internalDirectory)
                                    if(LOG_ME) ALog.d(
                                        TAG, ".setStateEvent(): " +
                                                "Refreshing searched entities1 list after GetEntities1AroundUserStateEvent"
                                    )
                                    stateEventTracker.notifyStateEventStarting(stateEvent)
                                    postRefreshListOfSearchedEntities1(true)

                                    if(LOG_ME) {
                                        if(searchedAvailableJobs != null) {
                                            ALog.d(
                                                TAG,
                                                "setStateEvent(): searchedEntities1List != null    " +
                                                        "${searchedAvailableJobs!!.size}"
                                            )
                                            for((index, entity) in searchedAvailableJobs!!) {
                                                ALog.d(
                                                    TAG,
                                                    ".setStateEvent(): Entity #$index: $entity"
                                                )
                                            }
                                        } else {
                                            if(LOG_ME) ALog.w(
                                                TAG, ".setStateEvent(): " +
                                                        "searchedEntities1List == null"
                                            )
                                        }
                                    }
                                }
                            postUpdateViewStateWithCopy(viewState)
                        }
                    )
                }
            }

            is HomeScreenStateEvent.SaveInFileAndClearSearchedEntities1ListStateEvent -> {
                if (LOG_ME) {
                    ALog.d(TAG, "setStateEvent: SaveInFileAndClearSearchedEntities1ListStateEvent")
                }
                saveSearchedEntities1ListInFile(stateEvent.internalDirectory)
                emitDoNothingStateMessageEvent(stateEvent)
            }

            is HomeScreenStateEvent.SearchAvailableJobsStateEvent -> {
                val currentViewState = getCurrentViewStateOrNew()
                if (LOG_ME) {
                    ALog.d(
                        TAG, ".$methodName(): SearchAvailableJobsStateEvent\n" +
                                "currentViewState:\n${currentViewState}\n"
                    )
                }
                notifyStateEventStarting(stateEvent)
                interactors.searchTaskAssignments.searchTaskAssignments(
                    location = getCurrentViewStateOrNew().location,
                    searchParameters = getCurrentViewStateOrNew(),
                    stateEvent = stateEvent,
                    onErrorAction = {
                        notifyStateEventEnded(stateEvent)
                    },
                    returnViewState = currentViewState,
                    updateReturnViewState = {
                            viewState, foundEntities -> viewState.apply {
                                searchedAvailableJobs = ArrayList(foundEntities)
                                stateEventTracker.notifyStateEventStarting(stateEvent)
                                if(LOG_ME) ALog.d(
                                    TAG, ".setStateEvent(): " +
                                            "Refreshing searched entities1 list after SearchEntities1StateEvent\n" +
                                            "searchedEntities1List == ${searchedAvailableJobs?.convertToString()}"
                                )
                                postRefreshListOfSearchedEntities1(true)
                        }
                        postUpdateViewStateWithCopy(viewState)
                    }
                )
            }

            is HomeScreenStateEvent.SearchUserTakenJobsStateEvent -> {
                val currentViewState = getCurrentViewStateOrNew()
                if (LOG_ME) {
                    ALog.d(
                        TAG, ".$methodName(): SearchUserTakenJobsStateEvent\n" +
                                "currentViewState:\n${currentViewState}\n"
                    )
                }
                notifyStateEventStarting(stateEvent)
                interactors.searchTaskAssignments.searchTaskAssignments(
                    location = getCurrentViewStateOrNew().location,
                    searchParameters = getCurrentViewStateOrNew(),
                    stateEvent = stateEvent,
                    onErrorAction = {
                        notifyStateEventEnded(stateEvent)
                    },
                    returnViewState = currentViewState,
                    updateReturnViewState = {
                            viewState, foundEntities -> viewState.apply {
                        userTakenJobs = ArrayList(foundEntities)
                        stateEventTracker.notifyStateEventStarting(stateEvent)
                        if(LOG_ME) ALog.d(
                            TAG, ".setStateEvent(): " +
                                    "Refreshing searched entities1 list after SearchEntities1StateEvent\n" +
                                    "searchedEntities1List == ${searchedAvailableJobs?.convertToString()}"
                        )
                        postRefreshListOfSearchedEntities1(true)
                    }
                        postUpdateViewStateWithCopy(viewState)
                    }
                )
            }

            is HomeScreenStateEvent.SearchUserDoneJobsStateEvent -> {
                val currentViewState = getCurrentViewStateOrNew()
                if (LOG_ME) {
                    ALog.d(
                        TAG, ".$methodName(): SearchUserDoneJobsStateEvent\n" +
                                "currentViewState:\n${currentViewState}\n"
                    )
                }
                notifyStateEventStarting(stateEvent)
                interactors.searchTaskAssignments.searchTaskAssignments(
                    location = getCurrentViewStateOrNew().location,
                    searchParameters = getCurrentViewStateOrNew(),
                    stateEvent = stateEvent,
                    onErrorAction = {
                        notifyStateEventEnded(stateEvent)
                    },
                    returnViewState = currentViewState,
                    updateReturnViewState = {
                            viewState, foundEntities -> viewState.apply {
                        userDoneJobs = ArrayList(foundEntities)
                        stateEventTracker.notifyStateEventStarting(stateEvent)
                        if(LOG_ME) ALog.d(
                            TAG, ".setStateEvent(): " +
                                    "Refreshing searched entities1 list after SearchEntities1StateEvent\n" +
                                    "searchedEntities1List == ${searchedAvailableJobs?.convertToString()}"
                        )
                        postRefreshListOfSearchedEntities1(true)
                    }
                        postUpdateViewStateWithCopy(viewState)
                    }
                )
            }

            is HomeScreenStateEvent.ClearFiltersStateEvent -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: ClearFiltersStateEvent")
                // Legacy code that used to work with views.
//                val currentViewState = getCurrentViewStateOrNew()
//                currentViewState.clearEntity1SearchFilters()
//                setViewState(currentViewState)

                val copy = getViewStateCopy()
                copy.clearEntity1SearchFilters()
                setViewState(copy, "HomeScreenStateEvent.ClearFiltersStateEvent")

                emitDoNothingStateMessageEvent(stateEvent)
            }

            is HomeScreenStateEvent.GetUsersRating -> {
                if(LOG_ME) ALog.d(
                    TAG, ".setStateEvent(): " +
                            "GetUsersRating"
                )
                val currentViewState = getCurrentViewStateOrNew()
                notifyStateEventStarting(stateEvent)
                if(currentViewState.mainEntity != null) {
                    if(currentViewState.mainEntity!!.id != null) {
                        interactors.getUsersRating.getUsersRating(
                            stateEvent = stateEvent,
                            onErrorAction = {
                                notifyStateEventEnded(stateEvent)
                            },
                            returnViewState = currentViewState,
                            updateReturnViewState = {
                                viewState, obtainedUsersRating -> viewState.apply {
                                        stateEventTracker.notifyStateEventStarting(stateEvent)
                                        usersRating = obtainedUsersRating
                                }
                                postUpdateViewStateWithCopy(viewState)
                            }
                        )
                    } else {
                        setToastState(
                            ToastState(
                                show = true,
                                message = (ApplicationProvider.getApplicationContext() as Context).getString(
                                    R.string.mainEntity_is_null_when_trying_to_get_users_rating
                                ),
                                duration = Toast.LENGTH_LONG,
                                updateToastInViewModel = { toastState -> setToastState(toastState) },
                            )
                        )
                        emitDoNothingStateMessageEvent(stateEvent)
                    }
                } else {
                    setToastState(
                        ToastState(
                            show = true,
                            message = (ApplicationProvider.getApplicationContext() as Context).getString(
                                R.string.mainEntity_is_null_when_trying_to_get_users_rating
                            ),
                            duration = Toast.LENGTH_LONG,
                            updateToastInViewModel = { toastState -> setToastState(toastState) },
                        )
                    )
                    emitDoNothingStateMessageEvent(stateEvent)
                }
            }

            is HomeScreenStateEvent.ClearUIChangesInFragment5 -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: ClearUIChangesInFragment5")
                val copy = getViewStateCopy()
                copy.mainEntity?.let {
                    copy.usersName = it.name
                    copy.usersCity = it.city
                    copy.usersDescription = it.description
                }
                setViewState(copy, "HomeScreenStateEvent.ClearUIChangesInFragment5")
                setToastState(
                    ToastState(
                        show = true,
                        message = (ApplicationProvider.getApplicationContext() as Context).getString(
                            R.string.changes_reverted
                        ),
                        duration = Toast.LENGTH_SHORT,
                        updateToastInViewModel = { toastState -> setToastState(toastState) },
                    )
                )
                emitDoNothingStateMessageEvent(stateEvent)
            }

            is HomeScreenStateEvent.AskUserAboutLogout -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: AskUserAboutLogout")
                emitStateMessageEvent(
                    stateMessage = StateMessage(
                        response = Response(
                            messageId = R.string.are_you_sure_you_want_to_logout,
                            message = (ApplicationProvider.getApplicationContext() as Context).getString(
                                R.string.are_you_sure_you_want_to_logout
                            ),
                            uiComponentType = UIComponentType.AreYouSureDialog(
                                callback = object : AreYouSureCallback {
                                    override fun proceed() {
                                        if (LOG_ME) ALog.d(TAG, ".$methodName(): Logging user out.")
                                        setStateEvent(HomeScreenStateEvent.LogoutUser)
                                    }

                                    override fun cancel() {
                                        if (LOG_ME) ALog.d(
                                            TAG,
                                            ".$methodName(): User cancelled logout."
                                        )
                                    }
                                }
                            ),
                            messageType = MessageType.Info()
                        )
                    ),
                    stateEvent = stateEvent
                )
            }

            is HomeScreenStateEvent.DownloadExchangeRates -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: DownloadExchangeRates")
                val currentViewState = getCurrentViewStateOrNew()
                notifyStateEventStarting(stateEvent)
                interactors.downloadExchangeRates.downloadExchangeRates(
                    baseCurrency = ProjectConstants.COMPANYS_ACCEPTED_CURRENCY,
                    stateEvent = stateEvent,
                    onErrorAction = {
                        notifyStateEventEnded(stateEvent)
                    },
                    returnViewState = currentViewState,
                    updateReturnViewState = {
                        viewState, exchangeRatesP -> viewState.apply {
                            stateEventTracker.notifyStateEventStarting(stateEvent)
                            exchangeRates = exchangeRatesP
                        }
                        postUpdateViewStateWithCopy(viewState)
                    }
                )
            }

            is HomeScreenStateEvent.LogoutUser -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: AskUserAboutLogout")
                val currentViewState = getCurrentViewStateOrNew()
                if(currentViewState.mainEntity != null) {
                    interactors.logout.logout(
                        user = currentViewState.mainEntity!!,
                        stateEvent = stateEvent,
                        returnViewState = getCurrentViewStateOrNew(),
                        updateReturnViewState = {
                            viewState -> viewState.apply {
                                mainEntity = null
                                usersName = null
                                usersSurname = null
                                usersCity = null
                                usersDescription = null
                                navigateToStartScreen = true
                            }
                            postUpdateViewStateWithCopy(viewState)
                        }
                    )
                } else {
                    setToastState(
                        ToastState(
                            show = true,
                            message = (ApplicationProvider.getApplicationContext() as Context).getString(
                                R.string.error_user_is_null_during_logout
                            ),
                            duration = Toast.LENGTH_LONG,
                            updateToastInViewModel = { toastState -> setToastState(toastState) },
                        )
                    )
                    emitDoNothingStateMessageEvent(stateEvent)
                }
            }

            is HomeScreenStateEvent.ReportProblem -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: ReportProblem")
                val currentViewState = getCurrentViewStateOrNew()
                if(currentViewState.mainEntity != null) {
                    stateEvent.reportProblemFunction.invoke()
                    emitDoNothingStateMessageEvent(stateEvent)
                } else {
                    setToastState(
                        ToastState(
                            show = true,
                            message = (ApplicationProvider.getApplicationContext() as Context).getString(
                                R.string.error_user_is_null_during_error_reporting
                            ),
                            duration = Toast.LENGTH_LONG,
                            updateToastInViewModel = { toastState -> setToastState(toastState) },
                        )
                    )
                    emitDoNothingStateMessageEvent(stateEvent)
                }
            }

            is HomeScreenStateEvent.RateApplication -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: RateApplication")
                val currentViewState = getCurrentViewStateOrNew()
                if(currentViewState.mainEntity != null) {
                    stateEvent.rateAppFunction.invoke()
                    emitDoNothingStateMessageEvent(stateEvent)
                } else {
                    setToastState(
                        ToastState(
                            show = true,
                            message = (ApplicationProvider.getApplicationContext() as Context).getString(
                                R.string.error_user_is_null_during_app_rating
                            ),
                            duration = Toast.LENGTH_LONG,
                            updateToastInViewModel = { toastState -> setToastState(toastState) },
                        )
                    )
                    emitDoNothingStateMessageEvent(stateEvent)
                }
            }

            is HomeScreenStateEvent.RecommendApp -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: RecommendApp")
                val currentViewState = getCurrentViewStateOrNew()
                if(currentViewState.mainEntity != null) {
                    stateEvent.recommendationFunction.invoke()
                    emitDoNothingStateMessageEvent(stateEvent)
                } else {
                    setToastState(
                        ToastState(
                            show = true,
                            message = (ApplicationProvider.getApplicationContext() as Context).getString(
                                R.string.error_user_is_null_during_app_recommending
                            ),
                            duration = Toast.LENGTH_LONG,
                            updateToastInViewModel = { toastState -> setToastState(toastState) },
                        )
                    )
                    emitDoNothingStateMessageEvent(stateEvent)
                }
            }

            is HomeScreenStateEvent.GetMerchantName -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: GetMerchantName")
                val currentViewState = getCurrentViewStateOrNew()
                notifyStateEventStarting(stateEvent)
                if(currentViewState.mainEntity != null) {
                    interactors.getMerchantName.getMerchantName(
                        continueFlag = stateEvent.continueFlag,
                        stateEvent = stateEvent,
                        onErrorAction = {
                            notifyStateEventEnded(stateEvent)
                        },
                        returnViewState = getCurrentViewStateOrNew(),
                        updateReturnViewState = {
                            viewState,
                            merchantNameP,
                            continueFlag -> viewState.apply {
                                stateEventTracker.notifyStateEventStarting(stateEvent)
                                merchantName = merchantNameP
                            }
                            postUpdateViewStateWithCopy(viewState)
                        }
                    )
                } else {
                    setToastState(
                        ToastState(
                            show = true,
                            message = (ApplicationProvider.getApplicationContext() as Context).getString(
                                R.string.mainEntity_is_null_when_trying_to_get_merchant_name
                            ),
                            duration = Toast.LENGTH_LONG,
                            updateToastInViewModel = { toastState -> setToastState(toastState) },
                        )
                    )
                    emitDoNothingStateMessageEvent(stateEvent)
                }
            }

            is HomeScreenStateEvent.GetGatewayNameAndMerchantID -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: GetGatewayNameAndMerchantID")
                val currentViewState = getCurrentViewStateOrNew()
                if(currentViewState.mainEntity != null) {
                    notifyStateEventStarting(stateEvent)
                    interactors.getGatewayNameAndMerchantID.getGatewayNameAndMerchantID(
                        continueFlag = stateEvent.continueFlag,
                        stateEvent = stateEvent,
                        onErrorAction = {
                            notifyStateEventEnded(stateEvent)
                        },
                        returnViewState = getCurrentViewStateOrNew(),
                        updateReturnViewState = {
                            viewState,
                            gatewayNameP,
                            gatewayMerchantIDP,
                            continueFlag -> viewState.apply {
                                stateEventTracker.notifyStateEventStarting(stateEvent)
                                gatewayName = gatewayNameP
                                gatewayMerchantID = gatewayMerchantIDP
                            }
                            postUpdateViewStateWithCopy(viewState)
                        }
                    )
                } else {
                    setToastState(
                        ToastState(
                            show = true,
                            message = (ApplicationProvider.getApplicationContext() as Context).getString(
                                R.string.mainEntity_is_null_when_trying_to_get_merchant_name
                            ),
                            duration = Toast.LENGTH_LONG,
                            updateToastInViewModel = { toastState -> setToastState(toastState) },
                        )
                    )
                    emitDoNothingStateMessageEvent(stateEvent)
                }
            }

            is HomeScreenStateEvent.CheckGooglePayAvailability -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: CheckGooglePayAvailability")
                val currentViewState = getCurrentViewStateOrNew()
                if(currentViewState.mainEntity != null) {
                    if(LOG_ME) ALog.d(TAG, ".$methodName(): Checking Google Pay availability.")
                    try {
                        notifyStateEventStarting(stateEvent)
                        interactors.checkGooglePayAvailability.checkGooglePayAvailability(
                            context = stateEvent.context,
                            continueFlag = stateEvent.continueFlag,
                            stateEvent = stateEvent,
                            onErrorAction = {
                                notifyStateEventEnded(stateEvent)
                            },
                            returnViewState = getCurrentViewStateOrNew(),
                            updateReturnViewState = {
                                    viewState,
                                    gPaymentsClient,
                                    gPayIsAvailable,
                                    continueFlag -> viewState.apply {
                                        stateEventTracker.notifyStateEventStarting(stateEvent)
                                        _googlePaymentsClient = gPaymentsClient
                                        googlePayIsAvailable = gPayIsAvailable
                                    }
                                postUpdateViewStateWithCopy(viewState)
                            }
                        )
                    } catch (e: Exception) {
                        ALog.e(TAG, methodName, e)
                        setToastState(
                            ToastState(
                                show = true,
                                message = (ApplicationProvider.getApplicationContext() as Context).getString(
                                    R.string.error_user_is_null_while_checking_gpay_availability
                                ),
                                duration = Toast.LENGTH_LONG,
                                updateToastInViewModel = { toastState -> setToastState(toastState) },
                            )
                        )
                        emitDoNothingStateMessageEvent(stateEvent)
                    }
                } else {
                    setToastState(
                        ToastState(
                            show = true,
                            message = (ApplicationProvider.getApplicationContext() as Context).getString(
                                R.string.error_user_is_null_while_checking_gpay_availability
                            ),
                            duration = Toast.LENGTH_LONG,
                            updateToastInViewModel = { toastState -> setToastState(toastState) },
                        )
                    )
                    emitDoNothingStateMessageEvent(stateEvent)
                }
            }

            is HomeScreenStateEvent.ShowDialogSayingThatGooglePayIsUnavailable -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: ShowDialogSayingThatGooglePayIsUnavailable")
                showOkDialogWithStateMessageEvent(
                    stateEvent = stateEvent,
                    context = getContext(),
                    titleID = R.string.google_pay_is_unavailable,
                    messageID = R.string.please_make_google_pay_available_on_this_device,
                    okCallback = object: OkButtonCallback {
                        override fun proceed() {
                            //navigateToPlayStoreToInstallGooglePay(stateEvent.context)
                        }
                    }
                )
            }

            is HomeScreenStateEvent.NotifyUserAboutLackOfEmailAppInstalled -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: NotifyUserAboutLackOfEmailAppInstalled")
                showOkDialogWithStateMessageEvent(
                    stateEvent = stateEvent,
                    context = getContext(),
                    titleID = R.string.no_email_application_installed,
                    messageID = R.string.please_install_an_email_application,
                    okCallback = object: OkButtonCallback {
                        override fun proceed() {
                            //navigateToPlayStoreToInstallEmailApp(stateEvent.context)
                        }
                    }
                )
            }

            is HomeScreenStateEvent.ActOnEntity1 -> {
                if(LOG_ME) ALog.d(
                    TAG, ".setStateEvent(): " +
                            "ActOnEntity1"
                )
                val currentViewState = getCurrentViewStateOrNew()
                notifyStateEventStarting(stateEvent)
                if(currentViewState.mainEntity != null) {
                    if(currentViewState.mainEntity!!.id != null) {
                        if(currentViewState.mainEntity!!.fullyVerified) {
                            // TODO: Unavailable in demo project.
                            interactors.doNothingAtAll.doNothing()
                        } else {
                            if(LOG_ME)ALog.d(TAG, ".$methodName(): " +
                                                    "User is not fully verified. Showing dialog.")

                            setDialogState(
                                DialogState(
                                    show = true,
                                    onDismissRequest = {},
                                    title = getContext().getString(
                                            R.string.profile_incomplete
                                        ),
                                    text = getContext().getString(
                                            R.string.only_fully_verified_users_can_act_on_entity1
                                        ),
                                    leftButtonText = getContext().getString(R.string.dismiss),
                                    leftButtonOnClick = {},
                                    rightButtonText = getContext().getString(R.string.complete_profile),
                                    rightButtonOnClick = {
                                        stateEvent.callbackOnProfileIncomplete()
                                    },
                                    updateDialogInViewModel = {
                                        setDialogState(it)
                                    }
                                )
                            )
                            emitDoNothingStateMessageEvent(stateEvent)
                        }
                    } else {
                        setToastState(
                            ToastState(
                                show = true,
                                message = (ApplicationProvider.getApplicationContext() as Context).getString(
                                    R.string.mainEntity_is_null_when_trying_to_get_users_rating
                                ),
                                duration = Toast.LENGTH_LONG,
                                updateToastInViewModel = { toastState -> setToastState(toastState) },
                            )
                        )
                        emitDoNothingStateMessageEvent(stateEvent)
                    }
                } else {
                    setToastState(
                        ToastState(
                            show = true,
                            message = (ApplicationProvider.getApplicationContext() as Context).getString(
                                R.string.mainEntity_is_null_when_trying_to_get_users_rating
                            ),
                            duration = Toast.LENGTH_LONG,
                            updateToastInViewModel = { toastState -> setToastState(toastState) },
                        )
                    )
                    emitDoNothingStateMessageEvent(stateEvent)
                }
            }

            is HomeScreenStateEvent.Navigate -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: Navigate")
                val currentViewState = getCurrentViewStateOrNew()
                if(currentViewState.mainEntity != null) {
                    stateEvent.navigationFunction.invoke()
                    emitDoNothingStateMessageEvent(stateEvent)
                } else {
                    setToastState(
                        ToastState(
                            show = true,
                            message = (ApplicationProvider.getApplicationContext() as Context).getString(
                                R.string.error_user_is_null_while_navigating
                            ),
                            duration = Toast.LENGTH_LONG,
                            updateToastInViewModel = { toastState -> setToastState(toastState) },
                        )
                    )
                    emitDoNothingStateMessageEvent(stateEvent)
                }
            }

            is HomeScreenStateEvent.CreateStateMessageEvent -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: CreateStateMessageEvent")
                emitStateMessageEvent(
                    stateMessage = stateEvent.stateMessage,
                    stateEvent = stateEvent
                )
            }

            is HomeScreenStateEvent.None -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: None")
                launchJob = false
                interactors.doNothingAtAll.doNothing()
            }

            else -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: InvalidStateEvent")
                emitInvalidStateEvent(stateEvent)
            }
        }
        if (LOG_ME) ALog.d(TAG, "launching stateEvent in job: ${stateEvent.eventName()}")
        launchJob(stateEvent, job, launchJob)
        if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
    }

    override fun createNewViewState(): HomeScreenViewState<ProjectUser> {
        if(LOG_ME)ALog.d(TAG, ".createNewViewState(): " +
                                "Method start")
        val newInstance: HomeScreenViewState<ProjectUser> = HomeScreenViewState()
        newInstance.snackBarState = SnackBarState(
            updateSnackBarInViewModel = {
                setSnackBarState(it)
            }
        )
        newInstance.toastState = ToastState(
            updateToastInViewModel = {
                setToastState(it)
            }
        )
        newInstance.dialogState = DialogState(
            updateDialogInViewModel = {
                setDialogState(it)
            }
        )
        newInstance.permissionHandlingData.apply {
            this.addRequiredPermissionsInViewModel = { activity, permissionsToAdd ->
                this@HomeScreenViewModelCompose.addRequiredPermissions(activity, permissionsToAdd)
            }
            this.addGrantedAndDeniedPermissionsInViewModel = {
                    grantedPermissions, deniedPermissions ->
                this@HomeScreenViewModelCompose.addGrantedAndDeniedPermissions(
                    grantedPermissions,
                    deniedPermissions,
                )
            }
            this.dismissPermissionDialog = { permission ->
                this@HomeScreenViewModelCompose.dismissPermissionDialog(permission)
            }
        }
        if(LOG_ME) {
            ALog.d(TAG, ".createNewViewState(): " +
                    "newInstance == $newInstance")
            ALog.d(TAG, ".createNewViewState(): " +
            "Method end")
        }

        return newInstance
    }

    override fun setMainEntity(projectUser: ProjectUser?){
        if(LOG_ME)ALog.d(TAG, ".setMainEntity(): " +
                "Setting mainEntity to $projectUser")
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.mainEntity = projectUser
//        projectUser?.let {
//            update.usersName = projectUser.name
//            update.usersCity = projectUser.city
//            update.usersDescription = projectUser.description
//            update.usersProfilePhotoImageURI = projectUser.profilePhotoImageURI
//        }
//        setViewState(update)

        val copy = getViewStateCopy()
        copy.mainEntity = projectUser
        projectUser?.let {
            copy.usersName = projectUser.name
            copy.usersCity = projectUser.city
            copy.usersDescription = projectUser.description
            copy.usersProfilePhotoImageURI = projectUser.profilePhotoImageURI
        }
        setViewState(copy, "setMainEntity")
    }

    fun setCurrentComposableDestination(destination: HomeScreenDestination){
        if(LOG_ME) ALog.d(
            TAG, "setCurrentComposableDestination(): " +
                    "Setting currentComposableDestination to $destination"
        )
        setViewState(getViewStateCopy().apply { currentComposableDestination = destination }, "setCurrentComposableDestination")
    }
    fun updateIsDualPane(isDualPane: Boolean){
        val copy = getViewStateCopy()
        val wasDualPane = copy.isDualPane
        if(isDualPane != wasDualPane) {
            if(LOG_ME) ALog.d(
                TAG, "updateIsDualPane(): " +
                        "Setting isDualPane to $isDualPane and \n" +
                        "setting wasDualPane to $wasDualPane"
            )
            copy.wasDualPane = wasDualPane
            copy.isDualPane = isDualPane

            setViewState(copy, "updateIsDualPane")
        } else {
            if(LOG_ME) ALog.d(
                TAG, ".setCurrentComposableDestination(): " +
                        "No need to update: isDualPane == $isDualPane"
            )
        }
    }

    private fun setLocation(locationP: DeviceLocation?){
        if(LOG_ME)ALog.d(TAG, ".setLocation(): " +
                "Setting location to $locationP")
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.location = locationP
//        setViewState(update)
        setViewState(getViewStateCopy().apply { location = locationP }, "setLocation")
    }

    fun setCard1CurrentlyShownEntity(entity: TaskAssignment) {
        if(LOG_ME)ALog.d(TAG, ".setCard1CurrentlyShownEntity(): " +
                "Setting card1CurrentlyShownEntity to $entity")
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.card1CurrentlyShownEntity = entity
//        setViewState(update)
        setViewState(getViewStateCopy().apply { card1CurrentlyShownEntity = entity }, "setCard1CurrentlyShownEntity")
    }

    fun setCard2CurrentlyShownEntity(entity: TaskAssignment) {
        if(LOG_ME)ALog.d(TAG, ".setCard2CurrentlyShownEntity(): " +
                                "Setting card2CurrentlyShownEntity to $entity")
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.card2CurrentlyShownEntity = entity
//        setViewState(update)
        setViewState(getViewStateCopy().apply { card2CurrentlyShownEntity = entity }, "setCard2CurrentlyShownEntity")
    }

    fun setCard3CurrentlyShownEntity(entity: TaskAssignment) {
        if(LOG_ME)ALog.d(TAG, ".setCard3CurrentlyShownEntity(): " +
                "Setting card3CurrentlyShownEntity to $entity")
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.card3CurrentlyShownEntity = entity
//        setViewState(update)
        setViewState(getViewStateCopy().apply { card3CurrentlyShownEntity = entity }, "setCard3CurrentlyShownEntity")
    }

    fun setCard4CurrentlyShownEntity(entity: Entity4) {
        if(LOG_ME)ALog.d(TAG, ".setCard4CurrentlyShownEntity(): " +
                "Setting card4CurrentlyShownEntity to $entity")
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.card4CurrentlyShownEntity = entity
//        setViewState(update)
        setViewState(getViewStateCopy().apply { card4CurrentlyShownEntity = entity }, "setCard4CurrentlyShownEntity")
    }

    fun setAllAvailableJobsSearchQuery(query: String?){
        if(LOG_ME)ALog.d(TAG, ".setEntity1SearchQuery(): " +
                "Setting entity1SearchQuery to $query")
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.entity1SearchQuery = query
//        setViewState(update)
        setViewState(getViewStateCopy().apply { availableJobsSearchQuery = query }, "setEntity1SearchQuery")
    }

    fun setUserTakenJobsSearchQuery(query: String?){
        if(LOG_ME)ALog.d(TAG, ".setUserTakenJobsSearchQuery(): " +
                "Setting userTakenJobsSearchQuery to $query")
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.entity1SearchQuery = query
//        setViewState(update)
        setViewState(getViewStateCopy().apply { userTakenJobsSearchQuery = query }, "setUserTakenJobsSearchQuery")
    }

    fun setUserDoneJobsSearchQuery(query: String?){
        if(LOG_ME)ALog.d(TAG, ".setUserDoneJobsSearchQuery(): " +
                "Setting userDoneJobsSearchQuery to $query")
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.entity1SearchQuery = query
//        setViewState(update)
        setViewState(getViewStateCopy().apply { userDoneJobsSearchQuery = query }, "setUserDoneJobsSearchQuery")
    }

    fun setEntity1SearchFilters(filters: HomeScreenCard1SearchFilters){
        if(LOG_ME)ALog.d(TAG, ".setEntity1SearchFilters(): " +
                "Setting entity1 search filters to $filters")
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.setEntity1SearchFilters(filters)
//        setViewState(update)
        val copy = getViewStateCopy()
        copy.setEntity1SearchFilters(filters)
        setViewState(copy, "setEntity1SearchFilters")
    }

    fun setNavigateToStartScreen(navigate: Boolean?){
        if(LOG_ME)ALog.d(TAG, ".setNavigateToStartScreen(): " +
                "Setting navigateToStartScreen to $navigate")
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.navigateToStartScreen = navigate
//        setViewState(update)
        setViewState(getViewStateCopy().apply { navigateToStartScreen = navigate }, "setNavigateToStartScreen")
    }

    fun setSearchedEntities1List(list: ArrayList<TaskAssignment>?) {
        if(LOG_ME)ALog.d(TAG, ".setSearchedEntities1List(): " +
                "Setting searchedEntities1List to $list")
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.searchedEntities1List = list
//        setViewState(update)
        setViewState(getViewStateCopy().apply { searchedAvailableJobs = list }, "setSearchedEntities1List")
    }

    fun postSearchedEntities1List(list : ArrayList<TaskAssignment>?) {
        if(LOG_ME)ALog.d(TAG, ".postSearchedEntities1List(): " +
                "Setting searchedEntities1List to $list")
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.searchedEntities1List = list
//        postViewState(update)
        postViewState(getViewStateCopy().apply { searchedAvailableJobs = list })
    }

    private fun postRefreshListOfSearchedEntities1(refresh: Boolean) {
        if(LOG_ME)ALog.d(TAG, ".postRefreshListOfSearchedEntities1(): " +
                "Setting refreshListOfSearchedEntities1 to $refresh")
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.refreshListOfSearchedEntities1 = refresh
//        postViewState(update)
        postViewState(getViewStateCopy().apply { refreshListOfSearchedEntities1 = refresh })
    }


    fun setRefreshListOfSearchedEntities1(refresh: Boolean) {
        if(LOG_ME)ALog.d(TAG, ".setRefreshListOfSearchedEntities1(): " +
                "Setting refreshListOfSearchedEntities1 to $refresh")
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.refreshListOfSearchedEntities1 = refresh
//        setViewState(update)
        setViewState(getViewStateCopy().apply { refreshListOfSearchedEntities1 = refresh }, "setRefreshListOfSearchedEntities1")
    }


    fun setRefreshListOfEntities3(refresh: Boolean) {
        if(LOG_ME)ALog.d(TAG, ".setRefreshListOfEntities3(): " +
                "Setting refreshListOfEntities3 to $refresh")
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.refreshListOfEntities3 = refreshListOfEntities3
//        setViewState(update)
        setViewState(getViewStateCopy().apply { refreshListOfEntities3 = refresh }, "setRefreshListOfEntities3")
    }

    private fun setUserTakenJobsList(list : ArrayList<TaskAssignment>) {
        if(LOG_ME)ALog.d(TAG, ".setUserTakenJobsList(): " +
                "Setting entities2List to $list")
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.entities2List = list
//        setViewState(update)
        setViewState(getViewStateCopy().apply { userTakenJobs = list }, "setUserTakenJobsList")
    }

    private fun setUserDoneJobsList(list : ArrayList<TaskAssignment>) {
        if(LOG_ME)ALog.d(TAG, ".setUserDoneJobsList(): " +
                "Setting entities3List to $list")
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.entities3List = list
//        setViewState(update)
        setViewState(getViewStateCopy().apply { userDoneJobs = list }, "setUserDoneJobsList")
    }

    fun setExchangeRates(exchangeRatesP: ExchangeRates?) {
        if(LOG_ME)ALog.d(TAG, ".setExchangeRates(): " +
                "Setting exchangeRates to $exchangeRatesP")
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.exchangeRates = exchangeRatesP
//        setViewState(update)
        setViewState(getViewStateCopy().apply { exchangeRates = exchangeRatesP }, "setExchangeRates")
    }

    private fun setEntities4List(entities : ArrayList<Entity4>) {
        if(LOG_ME)ALog.d(TAG, ".setEntities4List(): " +
                "Setting entities4List to $entities")
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.entities4List = entities
//        setViewState(update)
        setViewState(getViewStateCopy().apply { entities4List = entities }, "setEntities4List")
    }

    private fun setNoCachedEntity(noCachedEntityP: Boolean?) {
        if(LOG_ME)ALog.d(TAG, ".setNoCachedEntity(): " +
                "Setting noCachedEntity to $noCachedEntityP")
        // Legacy code that used to work with views.
//        val update = getCurrentViewStateOrNew()
//        update.noCachedEntity = noCachedEntityP
//        setViewState(update)
        setViewState(getViewStateCopy().apply { noCachedEntity = noCachedEntityP }, "setNoCachedEntity")
    }

    fun setCollapsingToolbarState(
        state: CollapsingToolbarState
    ) = interactionManager.setCollapsingToolbarState(state)

    fun validateAndUpdateInputField(
        inputFieldContent: String?,
        updateFun : (HomeScreenViewState<ProjectUser>) -> Unit,
    ){
        val methodName = "validateAndUpdateInputField"
        if(stringIsNeitherNullNorEmpty(inputFieldContent)) {
            if(LOG_ME) ALog.d(
                TAG,
                ".$methodName(): Input fields content $inputFieldContent is valid. Updating it in ViewModel"
            )
            val copy = getViewStateCopy()
            updateFun.invoke(copy)

            setViewState(copy, "setNoCachedEntity")
        } else {
            if(LOG_ME) ALog.d(
                TAG,
                ".$methodName(): Input fields content $inputFieldContent is invalid."
            )
            setStateEvent(
                HomeScreenStateEvent.CreateStateMessageEvent(
                    stateMessage = StateMessage(
                        response = Response(
                            messageId = R.string.text_ok,
                            message = (ApplicationProvider.getApplicationContext() as Context).getString(
                                R.string.text_ok
                            ),
                            uiComponentType = UIComponentType.SnackBar(
                                null,
                                R.string.invalid_input,
                                (ApplicationProvider.getApplicationContext() as Context).getString(R.string.invalid_input),
                                View.OnClickListener { }
                            ),
                            messageType = MessageType.Error()
                        )
                    )
                )
            )
        }
    }


    /**
     * Creates a [Task] that starts the payment process with the transaction details included.
     *
     * @param priceCents the price to show on the payment sheet.
     * @return a [Task] with the payment information.
     * @see [](https://developers.google.com/android/reference/com/google/android/gms/wallet/PaymentsClient#loadPaymentData(com.google.android.gms.wallet.PaymentDataRequest)
    ) */
    fun getLoadPaymentDataTask(
        priceCents: Long,
    ): Task<PaymentData> {
        val currentViewState = getCurrentViewStateOrNew()
        val gatewayName = currentViewState.gatewayName
        val gatewayMerchantID = currentViewState.gatewayMerchantID
        if(gatewayName == null || gatewayMerchantID == null) {
            ALog.e(
                TAG, ".getLoadPaymentDataTask(): " +
                        "gatewayName == $gatewayName and " +
                        "gatewayMerchantID == $gatewayMerchantID"
            )
            throw NullPointerException("gateway name and merchant id should be not null at this point.")
        }

        val paymentDataRequestJson = GooglePayPaymentsUtil.getPaymentDataRequest(
            priceCents,
            GooglePayPaymentsUtil.generateMerchantInfo(getCurrentViewStateOrNew().merchantName!!),
            gatewayName!!,
            gatewayMerchantID!!,
        )
        val request = PaymentDataRequest.fromJson(paymentDataRequestJson.toString())

        return _googlePaymentsClient!!.loadPaymentData(request)
    }

    fun getLoadPaymentDataTask(
        priceCents: Long,
        feesCurrency: String,
    ): Task<PaymentData> {
        val currentViewState = getCurrentViewStateOrNew()
        val gatewayName = currentViewState.gatewayName
        val gatewayMerchantID = currentViewState.gatewayMerchantID
        if(gatewayName == null || gatewayMerchantID == null) {
            ALog.e(
                TAG, ".getLoadPaymentDataTask(): " +
                        "gatewayName == $gatewayName and " +
                        "gatewayMerchantID == $gatewayMerchantID"
            )
            throw NullPointerException("gateway name and merchant id should be not null at this point.")
        }

        val paymentDataRequestJson = GooglePayPaymentsUtil.getPaymentDataRequest(
            priceCents,
            GooglePayPaymentsUtil.generateMerchantInfo(getCurrentViewStateOrNew().merchantName!!),
            gatewayName!!,
            gatewayMerchantID!!,
        )
        val request = PaymentDataRequest.fromJson(paymentDataRequestJson.toString())

        return _googlePaymentsClient!!.loadPaymentData(request)
    }

    private fun inputField1IsValid(inputFieldContent : String?): Boolean {
        return stringIsNeitherNullNorEmpty(inputFieldContent)
    }

    private fun inputField2IsValid(inputFieldContent : String?): Boolean {
        return stringIsNeitherNullNorEmpty(inputFieldContent)
    }

    private fun inputField3IsValid(inputFieldContent : String?): Boolean {
        return stringIsNeitherNullNorEmpty(inputFieldContent)
    }

    private fun stringIsNeitherNullNorEmpty(string : String?): Boolean {
        return string?.isNotEmpty() ?: false
    }

    fun setSearchQueryInteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField1InteractionState(state)
    }

    fun setRange1MinInteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField2InteractionState(state)
    }

    fun setRange1MaxInteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField3InteractionState(state)
    }

    fun setRange1RangeSliderInteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField4InteractionState(state)
    }

    fun setRange2MinInteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField5InteractionState(state)
    }

    fun setRange2MaxInteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField6InteractionState(state)
    }

    fun setRange2RangeSliderInteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField7InteractionState(state)
    }

    fun setSwitch1InteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField8InteractionState(state)
    }

    fun setSwitch2InteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField9InteractionState(state)
    }

    fun setSwitch3InteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField10InteractionState(state)
    }

    fun setSwitch4InteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField11InteractionState(state)
    }

    fun setSwitch5InteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField12InteractionState(state)
    }

    fun setSwitch6InteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField13InteractionState(state)
    }

    fun setSwitch7InteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField14InteractionState(state)
    }



    fun setEditingUsersNameInteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField15InteractionState(state)
    }
    fun setEditingUsersDescriptionInteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField16InteractionState(state)
    }
    fun setEditingUsersCityInteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField17InteractionState(state)
    }


    fun isToolbarCollapsed() = _collapsingToolbarState.toString()
        .equals(CollapsingToolbarState.Collapsed().toString())

    fun isToolbarExpanded() = _collapsingToolbarState.toString()
        .equals(CollapsingToolbarState.Expanded().toString())

    // return true if in EditState
    fun checkEditState() = interactionManager.checkEditState()

    fun exitEditState() = interactionManager.exitEditState()

    fun setInitialState() = interactionManager.setInitialState()

    fun isEditingSearchQuery() = interactionManager.isEditingInputField1()

    fun isEditingRange1Min() = interactionManager.isEditingInputField2()

    fun isEditingRange1Max() = interactionManager.isEditingInputField3()

    fun isEditingRange1RangeSlider() = interactionManager.isEditingInputField4()

    fun isEditingRange2Min() = interactionManager.isEditingInputField5()

    fun isEditingRange2Max() = interactionManager.isEditingInputField6()

    fun isEditingRange2RangeSlider() = interactionManager.isEditingInputField7()

    fun isEditingSwitch1() = interactionManager.isEditingInputField8()

    fun isEditingSwitch2() = interactionManager.isEditingInputField9()

    fun isEditingSwitch3() = interactionManager.isEditingInputField10()

    fun isEditingSwitch4() = interactionManager.isEditingInputField11()

    fun isEditingSwitch5() = interactionManager.isEditingInputField12()

    fun isEditingSwitch6() = interactionManager.isEditingInputField13()

    fun isEditingSwitch7() = interactionManager.isEditingInputField14()


    fun isEditingUsersName() = interactionManager.isEditingInputField15()

    fun isEditingUsersDescription() = interactionManager.isEditingInputField16()

    fun isEditingUsersCity() = interactionManager.isEditingInputField17()



    // force observers to refresh
    fun triggerEntityObservers(){
        getCurrentViewStateOrNew().mainEntity?.let { mainEntity ->
            setMainEntity(mainEntity)
        }
        getCurrentViewStateOrNew().listOfEntities?.let { listOfEntities ->
            setListOfEntities(listOfEntities)
        }
    }

    fun logInteractionState() {
        ALog.d(TAG, ".logInteractionState(): interactionManager == \n $interactionManager")
    }

    // Method saves searchedEntities1List from ViewState and clears it so that savedInstanceState was small enough.
    // Reference: https://stackoverflow.com/questions/45193941/how-to-read-and-write-txt-files-in-android-in-kotlin
    fun saveSearchedEntities1ListInFile(internalDirectory: File) {
        if (LOG_ME) ALog.d(TAG, "Method start: saveSearchedEntities1ListInFile()")
        try {
            val currentViewState = getCurrentViewStateOrNew()
            if(currentViewState.searchedAvailableJobs == null) {
                if(LOG_ME) ALog.d(
                    TAG, ".saveSearchedEntities1ListInFile(): " +
                            "There is nothing to save: searchedEntities1List == null"
                )
                return
            }
            if(currentViewState.searchedAvailableJobs!!.isEmpty()) {
                if(LOG_ME) ALog.d(
                    TAG, ".saveSearchedEntities1ListInFile(): " +
                            "There is nothing to save: searchedEntities1List is empty"
                )
                return
            }

            internalDirectory.mkdirs()
            val searchedEntities1ListFile =
                File(internalDirectory, SEARCHED_ENTITIES1_LIST_FILE_NAME)

            val gson = Gson()
            val searchedEntities1ListJson: String = gson.toJson(currentViewState.searchedAvailableJobs!!)
            if(LOG_ME) ALog.d(
                TAG, ".saveSearchedEntities1ListInFile(): " +
                        "searchedEntities1ListFile.path == ${searchedEntities1ListFile.path}"
            )
            searchedEntities1ListFile.writeText(searchedEntities1ListJson)
            if(LOG_ME) ALog.d(
                TAG, ".saveSearchedEntities1ListInFile(): " +
                        "Saved searchedEntities1List as string in file $SEARCHED_ENTITIES1_LIST_FILE_NAME\n" +
                        "Content:\n" + searchedEntities1ListJson
            )

        } catch (e: Exception) {
            ALog.e(TAG, "saveSearchedEntities1ListInFile()", e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: saveSearchedEntities1ListInFile()")
        }
    }

    // Reference: https://stackoverflow.com/questions/12384064/gson-convert-from-json-to-a-typed-arraylistt
    private fun restoreSearchedEntities1ListFromFile(internalDirectory: File) {
        if (LOG_ME) ALog.d(TAG, "Method start: restoreSearchedEntities1ListFromFile()")
        try {

            val searchedEntities1ListFile =
                File(internalDirectory, SEARCHED_ENTITIES1_LIST_FILE_NAME)
            if(!searchedEntities1ListFile.exists()) {
                if(LOG_ME) ALog.d(
                    TAG, ".restoreSearchedEntities1ListFromFile(): " +
                            "searchedEntities1ListFile does not exist."
                )
                return
            }

            val gson = Gson()
            val fileContent = searchedEntities1ListFile.readText()
            val searchedEntities1List = gson.fromJson<ArrayList<TaskAssignment>>(fileContent, searchedEntities1ListType)
            if(searchedEntities1List != null) {
                setSearchedEntities1List(searchedEntities1List)
            } else {
                if(LOG_ME) ALog.d(
                    TAG, ".restoreSearchedEntities1ListFromFile(): " +
                            "searchedEntities1List == null\n" +
                            "fileContent == $fileContent"
                )
            }
        } catch (e: Exception) {
            ALog.e(TAG, "restoreSearchedEntities1ListFromFile()", e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: restoreSearchedEntities1ListFromFile()")
        }
    }

    override fun getViewStateCopy(): HomeScreenViewState<ProjectUser> {
        return if(_viewState.value == null) {
            _viewState.value = createNewViewState()
            _viewState.value!!
        } else {
            _viewState.value!!.copy()
        }
    }

    override fun postUpdateViewStateWithCopy(
        viewStateToCopy: HomeScreenViewState<ProjectUser>,
    ): HomeScreenViewState<ProjectUser> {
        postViewState(viewStateToCopy.copy(), "postUpdateViewStateWithCopy")
        return _viewState.value!!
    }

    private fun getContext(): Context {
        return (ApplicationProvider.getApplicationContext() as Context)
    }

    companion object {
        val searchedEntities1ListType = object : TypeToken<List<TaskAssignment>>() {}.type
        val SEARCHED_ENTITIES1_FRESHNESS_INTERVAL = 600000L
    }
}