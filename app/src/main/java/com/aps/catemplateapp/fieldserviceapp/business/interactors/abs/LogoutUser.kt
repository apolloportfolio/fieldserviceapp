package com.aps.catemplateapp.fieldserviceapp.business.interactors.abs

import com.aps.catemplateapp.common.business.domain.state.DataState
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenViewState
import kotlinx.coroutines.flow.Flow

interface LogoutUser {
    fun logout(
        user : ProjectUser,
        stateEvent: StateEvent,
        returnViewState: HomeScreenViewState<ProjectUser>,
        updateReturnViewState: (HomeScreenViewState<ProjectUser>)-> HomeScreenViewState<ProjectUser>,
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?>
}