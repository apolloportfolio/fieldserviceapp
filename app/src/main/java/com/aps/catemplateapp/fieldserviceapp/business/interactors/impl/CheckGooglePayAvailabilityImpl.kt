package com.aps.catemplateapp.fieldserviceapp.business.interactors.impl

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.network.ApiResponseHandler
import com.aps.catemplateapp.common.business.data.util.safeApiCall
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.core.util.GooglePayPaymentsUtil
import com.aps.catemplateapp.fieldserviceapp.business.interactors.abs.CheckGooglePayAvailability
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenViewState
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.wallet.IsReadyToPayRequest
import com.google.android.gms.wallet.PaymentsClient
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

private const val TAG = "CheckGooglePayAvailabilityImpl"
private const val LOG_ME = true

class CheckGooglePayAvailabilityImpl
@Inject
constructor(): CheckGooglePayAvailability {
    override fun checkGooglePayAvailability(
        context: Context,
        continueFlag: Boolean?,
        stateEvent: StateEvent,
        onErrorAction: () -> Unit,
        returnViewState: HomeScreenViewState<ProjectUser>,
        updateReturnViewState: (
            HomeScreenViewState<ProjectUser>,
            PaymentsClient?,
            Boolean?,
            Boolean?
        )-> HomeScreenViewState<ProjectUser>,
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?> = flow {
        if(LOG_ME) ALog.d(TAG, ".checkGooglePayAvailability(): Checking Google Pay availability.")
        val googlePayIsAvailableApiCall = safeApiCall(Dispatchers.IO, onErrorAction = onErrorAction){
            if(LOG_ME)ALog.d(TAG, ".checkGooglePayAvailability(): fetching")
            fetchCanUseGooglePay(context)
        }


        val response = object: ApiResponseHandler<HomeScreenViewState<ProjectUser>, Pair<PaymentsClient?, Boolean?>?>(
            response = googlePayIsAvailableApiCall,
            stateEvent = stateEvent
        ){
            override suspend fun handleSuccess(
                resultObj: Pair<PaymentsClient?, Boolean?>?
            ): DataState<HomeScreenViewState<ProjectUser>>? {
                val methodName: String = "handleSuccess"
                if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
                var messageId : Int
                var message : String
                var uiComponentType : UIComponentType
                var messageType : MessageType

                if(resultObj != null) {
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): resultObj != null")
                    messageId = R.string.successfully_checked_google_pay_availability
                    message = (ApplicationProvider.getApplicationContext() as Context).getString(messageId)
                    uiComponentType = UIComponentType.None()
                    messageType = MessageType.Success()

                    if(LOG_ME)ALog.d(TAG, ".$methodName(): running updateReturnViewState")
                    updateReturnViewState(
                        returnViewState,
                        resultObj.first,
                        resultObj.second,
                        continueFlag,
                    )
                } else {
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): resultObj == null")
                    messageId = R.string.failed_to_check_google_pay_availability
                    message = (ApplicationProvider.getApplicationContext() as Context).getString(messageId)
                    uiComponentType = UIComponentType.Toast()
                    messageType = MessageType.Error()
                }

                if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
                return DataState.data(
                    response = Response(
                        messageId, message, uiComponentType, messageType,
                    ),
                    data = returnViewState,
                    stateEvent,
                )
            }
        }.getResult()

        emit(response)
    }

    private suspend fun fetchCanUseGooglePay(context: Context): Pair<PaymentsClient?, Boolean?>? {
        val methodName: String = "fetchCanUseGooglePay"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        var result: Pair<PaymentsClient?, Boolean?>? = null
        try {
            val isReadyToPayJson = GooglePayPaymentsUtil.isReadyToPayRequest()
            var paymentsClient: PaymentsClient? =  null
            if (isReadyToPayJson == null) result = Pair(paymentsClient, false)

            val request = IsReadyToPayRequest.fromJson(isReadyToPayJson.toString())
            paymentsClient = GooglePayPaymentsUtil.createPaymentsClient(context)
            val task = paymentsClient.isReadyToPay(request)
            task.addOnCompleteListener { completedTask ->
                try {
                    val taskResult = completedTask.getResult(ApiException::class.java)
                    result = Pair(paymentsClient, taskResult)
                } catch (exception: ApiException) {
                    ALog.e(TAG, ".fetchCanUseGooglePay(): isReadyToPay failed", exception)
                    false
                }
            }.await()
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            result?.let {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): result == $result")
            }
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
        return result
    }
}
