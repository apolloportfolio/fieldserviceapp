package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.fragments

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEventTracker
import com.aps.catemplateapp.common.framework.presentation.layouts.LazyColumnWithPullToRefresh
import com.aps.catemplateapp.common.framework.presentation.views.BoxWithBackground
import com.aps.catemplateapp.common.framework.presentation.views.ListItemType04
import com.aps.catemplateapp.common.framework.presentation.views.ProfileStatusBar
import com.aps.catemplateapp.common.framework.presentation.views.SearchBarWithClearButton
import com.aps.catemplateapp.common.framework.presentation.views.TipContentIsUnavailable
import com.aps.catemplateapp.common.framework.presentation.views.TitleRowType01
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.DeviceLocation
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.TaskAssignment
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.factories.TaskAssignmentFactory
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.values.BackgroundsOfLayoutsFieldServiceApp
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.values.HomeScreenTheme

private const val TAG = "FieldServiceAppJobListScreenContent"
private const val LOG_ME = true

// All available jobs with search HomeScreenCompFragment1
@Composable
fun FieldServiceAppJobListScreenContent(
    dateUtil: DateUtil,
    showHeader: Boolean = true,

    stateEventTracker: StateEventTracker,
    deviceLocation: DeviceLocation?,
    showProfileStatusBar: Boolean,
    finishVerification: () -> Unit,
    availableJobsInitialSearchQuery: String = "",
    availableJobs: List<TaskAssignment>?,
    onAllAvailableJobsSearchQueryUpdate: (String) -> Unit,
    onListItemClick: (TaskAssignment) -> Unit,
    launchInitStateEvent: () -> Unit,

    isPreview: Boolean = false,
) {

    if(LOG_ME) ALog.d(TAG, "(): Recomposition start.")
    BoxWithBackground(
        backgroundDrawableId = R.drawable.example_background_1,
        composableBackground = BackgroundsOfLayoutsFieldServiceApp.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        ),
        modifier = Modifier
            .testTag(HomeScreenComposableFragment1TestTag)
            .wrapContentHeight(),
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(Dimens.columnPadding),
            verticalArrangement = Arrangement.Center,
        ) {
            var searchQuery by remember { mutableStateOf(availableJobsInitialSearchQuery) }
            var searchQueryUpdated by remember { mutableStateOf(false) }

            if(showHeader) {
                // Header
                val headerBackgroundColors = listOf(
                    MaterialTheme.colors.primary,
                    MaterialTheme.colors.primary,
                    MaterialTheme.colors.primaryVariant,
                    MaterialTheme.colors.surface,
                )
                TitleRowType01(
                    titleString = stringResource(id = R.string.available_jobs),
                    textAlign = TextAlign.Center,
                    backgroundDrawableId = R.drawable.example_background_1,
                    composableBackground = BackgroundsOfLayoutsFieldServiceApp.backgroundTitleType01(
                        colors = headerBackgroundColors,
                        brush = null,
                        shape = null,
                        alpha = 0.7f,
                    ),
                    titleFontSize = Dimens.titleFontSize,
                    leftImageTint = MaterialTheme.colors.secondary,
                    titleColor = MaterialTheme.colors.secondary,
                    modifier = Modifier
                        .testTag("AppHeader"),
                )
            }

            val searchBarBackgroundColors = listOf(
                MaterialTheme.colors.surface,
                MaterialTheme.colors.primaryVariant,
                MaterialTheme.colors.primary,
            )
            SearchBarWithClearButton(
                searchQuery = searchQuery,
                onSearchQueryChange = { query -> searchQuery = query },
                onSearchBackClick = {
                    searchQuery = ""
                    searchQueryUpdated = true
                },
                onSearchClick = {
                    searchQueryUpdated = true
                },
                composableBackground = BackgroundsOfLayoutsFieldServiceApp.backgroundScreen01(
                    colors = searchBarBackgroundColors,
                    brush = null,
                    shape = null,
                    alpha = 0.6f,
                ),
            )

            if(LOG_ME) ALog.d(
                TAG, "(): " +
                    "showProfileStatusBar = $showProfileStatusBar")
            if(showProfileStatusBar) {
                if(LOG_ME) ALog.d(
                    TAG, "(): " +
                        "Showing profile completion bar")

                val profileStatusBarBackgroundColors = listOf(
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.primaryVariant,
                    MaterialTheme.colors.primary,
                )
                ProfileStatusBar(
                    finishVerification,
                    composableBackground = BackgroundsOfLayoutsFieldServiceApp.backgroundScreen01(
                        colors = profileStatusBarBackgroundColors,
                        brush = null,
                        shape = null,
                        alpha = 0.6f,
                    ),
                    titleTint = MaterialTheme.colors.secondary,
                    subtitleTint = MaterialTheme.colors.error,)
            }

            if(availableJobs?.isNotEmpty() == true) {
                val availableJobsBackgroundColors = listOf(
                    MaterialTheme.colors.primary,
                    MaterialTheme.colors.surface,
                )
                LazyColumnWithPullToRefresh(
                    onRefresh = launchInitStateEvent,
                    isRefreshing = stateEventTracker.isRefreshing,
                ) {
                    itemsIndexed(availableJobs) { index, item ->
                        val scheduledTaskStartTime: String = item.scheduledTaskStartTimeStamp?.let {
                            dateUtil.convertFirebaseTimestampToStringDateForUser(
                                dateUtil.convertStringDateToFirebaseTimestamp(it)
                            )
                        } ?: stringResource(id = R.string.unscheduled_task)
                        ListItemType04(
                            index = index,
                            itemTitleString = item.taskDescription,
                            itemDetail1 = scheduledTaskStartTime,
                            itemDetail2 = item.taskAddress,
                            onListItemClick = { onListItemClick(item) },
                            tint = MaterialTheme.colors.onBackground,
                            backgroundDrawableId = R.drawable.list_item_example_background_1,
                            composableBackground = BackgroundsOfLayoutsFieldServiceApp.backgroundListItemType01(
                                colors = availableJobsBackgroundColors,
                                brush = null,
                                shape = null,
                                alpha = 0.6f,
                            ),
                        )
                    }
                }
            } else {
                TipContentIsUnavailable(stringResource(id = R.string.home_screen_fragment1_list_is_empty_tip))
            }

            var firstComposition by rememberSaveable { mutableStateOf(true) }
            LaunchedEffect(searchQueryUpdated) {
                if(firstComposition) {
                    if(LOG_ME) ALog.d(
                        TAG, "LaunchedEffect(searchFiltersUpdated): " +
                            "This is the first composition.")
                    firstComposition = false
                } else {
                    if(searchQueryUpdated) {
                        if(LOG_ME) ALog.d(TAG, "(): LaunchedEffect: searchQueryUpdated")
                        onAllAvailableJobsSearchQueryUpdate(searchQuery)
                    }
                }
                searchQueryUpdated = false
            }

            if(LOG_ME) ALog.d(TAG, "(): Recomposition start.")
        }
    }
}

@Preview
@Composable
private fun FieldServiceAppJobListScreenContentPreview1() {
    HomeScreenTheme {
        FieldServiceAppJobListScreenContent(
            dateUtil = DateUtil.getDateUtilForPreview(),

            stateEventTracker = StateEventTracker.PreviewStateEventTracker,
            deviceLocation = DeviceLocation.PreviewDeviceLocation,
            showProfileStatusBar = true,
            finishVerification = {},
            availableJobsInitialSearchQuery = "",
            availableJobs = TaskAssignmentFactory.createPreviewEntitiesList(),
            onAllAvailableJobsSearchQueryUpdate = {},
            onListItemClick = {},
            launchInitStateEvent = {},

            isPreview = true,
        )
    }
}

@Preview
@Composable
private fun FieldServiceAppJobListScreenContentPreview2() {
    HomeScreenTheme {
        FieldServiceAppJobListScreenContent(
            dateUtil = DateUtil.getDateUtilForPreview(),

            stateEventTracker = StateEventTracker.PreviewStateEventTracker,
            deviceLocation = DeviceLocation.PreviewDeviceLocation,
            showProfileStatusBar = false,
            finishVerification = {},
            availableJobsInitialSearchQuery = "",
            availableJobs = TaskAssignmentFactory.createPreviewEntitiesList(),
            onAllAvailableJobsSearchQueryUpdate = {},
            onListItemClick = {},
            launchInitStateEvent = {},

            isPreview = true,
        )
    }
}