package com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity2
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.model.Entity2CacheEntity
import javax.inject.Inject

class Entity2CacheMapper
@Inject
constructor() : EntityMapper<Entity2CacheEntity, Entity2> {
    override fun mapFromEntity(entity: Entity2CacheEntity): Entity2 {
        return Entity2(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.picture1URI,

            entity.name,
            entity.description,

            entity.ownerID,
        )
    }

    override fun mapToEntity(domainModel: Entity2): Entity2CacheEntity {
        return Entity2CacheEntity(
            domainModel.id!!,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.picture1URI,

            domainModel.name,
            domainModel.description,

            domainModel.ownerID,
        )
    }

    override fun mapFromEntityList(entities : List<Entity2CacheEntity>) : List<Entity2> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<Entity2>): List<Entity2CacheEntity> {
        return entities.map{mapToEntity(it)}
    }
}