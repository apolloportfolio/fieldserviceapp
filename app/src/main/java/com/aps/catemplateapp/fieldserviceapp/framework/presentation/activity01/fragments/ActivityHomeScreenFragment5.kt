package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.view.*
import android.widget.EditText
import androidx.activity.OnBackPressedCallback
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.common.framework.presentation.BaseMVIFragment
import com.aps.catemplateapp.common.framework.presentation.BaseViewModel
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.extensions.hideKeyboard
import com.aps.catemplateapp.common.util.extensions.showToast
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.core.util.ProjectConstants.FIRESTORE_IMAGES_COLLECTION
import com.aps.catemplateapp.core.util.ProjectConstants.FIRESTORE_PROFILE_PHOTOS_SUB_COLLECTION
import com.aps.catemplateapp.core.util.ProjectConstants.PHONE_NUMBER_VERIFICATION_ENFORCED
import com.aps.catemplateapp.databinding.FragmentHomeScreenFragment5Binding
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.ActivityHomeScreen
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.ActivityHomeScreenNavigation
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.HomeScreenViewModel
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenInteractionState
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenStateEvent
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenViewState
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*

const val HomeScreen_Fragment5_STATE_BUNDLE_KEY = "com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.fragments.fragment5"

private const val TAG = "ActivityHomeScreenFragment5"
private const val LOG_ME = true

@FlowPreview
@ExperimentalCoroutinesApi
@AndroidEntryPoint
class ActivityHomeScreenFragment5
constructor(
        private val viewModelFactory: ViewModelProvider.Factory,
        private val dateUtil: DateUtil
): BaseMVIFragment<ProjectUser, HomeScreenViewState<ProjectUser>>(layoutRes = R.layout.fragment_home_screen_fragment5),
    ActivityHomeScreenNavigation, View.OnFocusChangeListener {

    private val viewModel : HomeScreenViewModel
        get() = (activity as ActivityHomeScreen).viewModel

    private lateinit var binding: FragmentHomeScreenFragment5Binding

    constructor(parcel: Parcel) : this(
            TODO("viewModelFactory"),
            TODO("dateUtil")) {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.setupChannel()
    }

    @InternalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        restoreInstanceState(savedInstanceState)
        setupOnBackPressDispatcher()
    }

    override fun onResume() {
        super.onResume()
        activity?.apply {
            setTitle(R.string.app_bar_title_home_screen_fragment_5)
        }
        if(PHONE_NUMBER_VERIFICATION_ENFORCED) {
            if(LOG_ME) ALog.d(
                TAG, ".onResume(): " +
                                    "Phone number verification is enforced - showing it's buttons")
            try {
                binding.verifiedPhone.visibility = View.VISIBLE
                binding.verifiedPhoneButton.visibility = View.VISIBLE
            } catch (e: Exception) {
                ALog.e(TAG, "onResume()", e)
            }
        }
    }

    override fun restoreInstanceState(savedInstanceState: Bundle?){
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    private fun setupOnBackPressDispatcher() {
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                onBackPressed()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }

    override fun onBackPressed() {
        viewModel.exitEditState()
        super.onBackPressed()
        view?.hideKeyboard()
        //Save data from ui in ViewModel if necessary
        findNavController().popBackStack()
    }
    override fun setupUI(){
        view?.hideKeyboard()
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun setupBinding(view: View) {
        binding = FragmentHomeScreenFragment5Binding.bind(view)
    }

    companion object CREATOR : Parcelable.Creator<ActivityHomeScreenFragment5> {
        override fun createFromParcel(parcel: Parcel): ActivityHomeScreenFragment5 {
            return ActivityHomeScreenFragment5(parcel)
        }

        override fun newArray(size: Int): Array<ActivityHomeScreenFragment5?> {
            return arrayOfNulls(size)
        }
    }

    override fun subscribeCustomObservers() {
        val methodName: String = "subscribeCustomObservers"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {

            viewModel.usersDescriptionEdittextInteractionState.observe(
                viewLifecycleOwner, androidx.lifecycle.Observer { state ->
                    val inputField : EditText =  binding.usersDescriptionEdittext
                    val inputFieldsName = "binding.usersDescriptionEdittext"

                    when(state){
                        is HomeScreenInteractionState.EditState -> {
                            if(LOG_ME)ALog.d(
                                TAG,
                                ".$methodName(): EditState: $inputFieldsName.text == ${inputField.text}"
                            )
                            showQuickEditButtons()
                        }

                        is HomeScreenInteractionState.DefaultState -> {
                            if(LOG_ME)ALog.d(
                                TAG,
                                ".$methodName(): DefaultState: $inputFieldsName.text == ${inputField.text}"
                            )
                            updateUsersDescriptionInViewModel()
                        }

                        else -> {}
                    }
                })

        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override fun getViewModel(): BaseViewModel<ProjectUser, HomeScreenViewState<ProjectUser>> {
        return viewModel
    }

    override fun getInitStateEvent(): StateEvent {
        return HomeScreenStateEvent.GetUsersRating
    }

    @SuppressLint("SetTextI18n")
    override fun changeUiOrNavigateDependingOnViewState(viewState: HomeScreenViewState<ProjectUser>) {
        val methodName: String = "changeUiOrNavigateDependingOnViewState()"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            viewState.usersName?.let {
                binding.usersNameLbl.text = it
            }

            viewState.usersCity?.let {
                binding.usersCityLbl.text = it
            }

            viewState.usersDescription?.let {
                binding.usersDescriptionEdittext.setText(it)
            }

            viewState.usersRating?.let { usersRating ->
                usersRating.averageRating?.let { binding.userRatingBar.rating = it }
                usersRating.numberOfRatings?.let { binding.ratingsNumberLbl.text = it.toString() }
            }

            viewState.mainEntity?.let { user ->
                var nameAndSurname = ""
                user.name?.let {
                    nameAndSurname += user.name + " "
                }
                user.surname?.let {
                    nameAndSurname += user.surname
                }
                user.city?.let {
                    binding.usersCityLbl.text = it
                }
                user.description?.let{ binding.usersDescriptionEdittext.setText(it) }
                user.emailAddressVerified.let { binding.verifiedEmail.isChecked = it }
                user.basicVerification.let { binding.verifiedBasicInfo.isChecked = true }
                user.profilePhotoImageURI?.let { loadProfilePhotoIntoUI(user) }
            }

            if(viewState.navigateToStartScreen ?: false) {
                viewState.navigateToStartScreen = false
                viewModel.setNavigateToStartScreen(null)
                navigateToStartScreen(this)
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override fun onEntityUpdateSuccess() {

    }

    override fun onEntityDeleteSuccess() {
        
    }

    override fun getStateBundleKey(): String? {
        return HomeScreen_Fragment5_STATE_BUNDLE_KEY
    }

    private fun updateUsersDescriptionInViewModel() {
        val methodName: String = "updateUsersDescriptionInViewModel()"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val currentViewState = viewModel.getCurrentViewStateOrNew()
            val usersDescriptionInViewModel = currentViewState.usersDescription
            val usersDescriptionInView = binding.usersDescriptionEdittext?.text.toString()
            // Update in view model only if values in view model and view are different
            if(usersDescriptionInViewModel != usersDescriptionInView) {
                if (LOG_ME) ALog.d(
                    TAG,
                    "$methodName: Updating usersDescription in ViewModel from " +
                            "$usersDescriptionInViewModel to " +
                            "$usersDescriptionInView"
                )
                viewModel.updateInputField(usersDescriptionInView) {
                    viewModel.getCurrentViewStateOrNew().availableJobsSearchQuery = usersDescriptionInView
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override fun setAllOnClickAndOnTouchListeners() {
        // Inputs
        binding.usersDescriptionEdittext.onFocusChangeListener = this

        // Buttons
        binding.profilePicture.setOnClickListener(this)
        binding.profilePictureContainer.setOnClickListener(this)
        binding.usersNameLbl.setOnClickListener(this)
        binding.usersCityLbl.setOnClickListener(this)
        binding.userRatingBar.setOnClickListener(this)
        binding.ratingsNumberLbl.setOnClickListener(this)
        binding.verifiedEmail.setOnClickListener(this)
        binding.verifiedEmailButton.setOnClickListener(this)
        binding.verifiedPhone.setOnClickListener(this)
        binding.verifiedPhoneButton.setOnClickListener(this)
        binding.verifiedId.setOnClickListener(this)
        binding.verifiedIdButton.setOnClickListener(this)
        binding.verifiedDriversLicense.setOnClickListener(this)
        binding.verifiedDriversLicenseButton.setOnClickListener(this)
        binding.verifiedBasicInfo.setOnClickListener(this)
        binding.verifiedBasicInfoButton.setOnClickListener(this)
        binding.ratingIcon.setOnClickListener(this)
        binding.ratingLbl.setOnClickListener(this)
        binding.ratingButton.setOnClickListener(this)
        binding.recommendAppIcon.setOnClickListener(this)
        binding.recommendAppLbl.setOnClickListener(this)
        binding.recommendAppButton.setOnClickListener(this)
        binding.paymentCardsIcon.setOnClickListener(this)
        binding.paymentCardsLbl.setOnClickListener(this)
        binding.paymentCardsButton.setOnClickListener(this)
        binding.helpIcon.setOnClickListener(this)
        binding.helpLbl.setOnClickListener(this)
        binding.helpButton.setOnClickListener(this)
        binding.rateAppIcon.setOnClickListener(this)
        binding.rateAppLbl.setOnClickListener(this)
        binding.rateAppButton.setOnClickListener(this)
        binding.termsAndConditionsIcon.setOnClickListener(this)
        binding.termsAndConditionsLbl.setOnClickListener(this)
        binding.termsAndConditionsButton.setOnClickListener(this)
        binding.reportProblemIcon.setOnClickListener(this)
        binding.reportProblemLbl.setOnClickListener(this)
        binding.reportProblemButton.setOnClickListener(this)
        binding.logoutIcon.setOnClickListener(this)
        binding.logoutLbl.setOnClickListener(this)
        binding.logoutButton.setOnClickListener(this)
        binding.saveButton.setOnClickListener(this)
        binding.cancelButton.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        val methodName: String = "onClick()"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            super.onClick(v)
            when(v) {

                binding.profilePicture, binding.profilePictureContainer -> {
                    if (LOG_ME) ALog.d(TAG, "$methodName: profilePictureContainer")
                    viewModel.exitEditState()
                    hideKeyboard()
                    getViewModel().setStateEvent(HomeScreenStateEvent.Navigate {
                        navigateToUsersProfilePictureScreen(
                            this,
                            viewModel.getCurrentViewStateOrNew().mainEntity!!,
                        )})
                }

                binding.usersNameLbl, binding.usersCityLbl -> {
                    if (LOG_ME) ALog.d(TAG, "$methodName: usersCityLbl")
                    viewModel.exitEditState()
                    hideKeyboard()
                    getViewModel().setStateEvent(HomeScreenStateEvent.Navigate {
                        navigateToBasicInfoScreen(
                            this,
                            viewModel.getCurrentViewStateOrNew().mainEntity!!,
                        )})
                }


                binding.userRatingBar, binding.ratingsNumberLbl -> {
                    if (LOG_ME) ALog.d(TAG, "$methodName: userRatingBar, ratingsNumberLbl")
                    getViewModel().setStateEvent(HomeScreenStateEvent.Navigate {
                        navigateToUsersRatings(
                            this,
                            viewModel.getCurrentViewStateOrNew().mainEntity!!,
                        )})
                }

                binding.verifiedEmail, binding.verifiedEmailButton -> {
                    if (LOG_ME) ALog.d(TAG, "$methodName: verifiedEmail, verifiedEmailButton")
                    val emailVerified = viewModel.getCurrentViewStateOrNew().mainEntity!!.emailAddressVerified
                    binding.verifiedEmail.isChecked = emailVerified
                    viewModel.exitEditState()
                    hideKeyboard()
                    if(!emailVerified) {
                        getViewModel().setStateEvent(HomeScreenStateEvent.Navigate {
                            navigateToUsersEmailActivationScreen(
                                this,
                                viewModel.getCurrentViewStateOrNew().mainEntity!!,
                            )})
                    } else {
                        activity?.showToast(
                            R.string.email_already_verified
                        )
                    }
                }

                binding.verifiedPhone, binding.verifiedPhoneButton -> {
                    if (LOG_ME) ALog.d(TAG, "$methodName: verifiedPhone")
                    if(PHONE_NUMBER_VERIFICATION_ENFORCED) {
                        val phoneVerified = viewModel.getCurrentViewStateOrNew().mainEntity!!.phoneNumberVerified
                        binding.verifiedPhone.isChecked = phoneVerified
                        viewModel.exitEditState()
                        hideKeyboard()
                        if(!phoneVerified) {
                            getViewModel().setStateEvent(HomeScreenStateEvent.Navigate {
                                navigateToUsersPhoneActivationScreen(
                                    this,
                                    viewModel.getCurrentViewStateOrNew().mainEntity!!,
                                )})
                        } else {
                            activity?.showToast(
                                R.string.phone_already_verified
                            )
                        }
                    }
                }

                binding.verifiedId, binding.verifiedIdButton -> {
                    if (LOG_ME) ALog.d(TAG, "$methodName: verifiedIdButton")
                    viewModel.exitEditState()
                    hideKeyboard()

                }

                binding.verifiedDriversLicense, binding.verifiedDriversLicenseButton -> {
                    if (LOG_ME) ALog.d(TAG, "$methodName: verifiedDriversLicenseButton")
                    viewModel.exitEditState()
                    hideKeyboard()

                }

                binding.verifiedBasicInfo, binding.verifiedBasicInfoButton -> {
                    if (LOG_ME) ALog.d(TAG, "$methodName: verifiedBasicInfo")
                    val verifiedBasicInfo = viewModel.getCurrentViewStateOrNew().mainEntity!!.basicVerification
                    binding.verifiedBasicInfo.isChecked = verifiedBasicInfo
                    viewModel.exitEditState()
                    hideKeyboard()
                    getViewModel().setStateEvent(HomeScreenStateEvent.Navigate {
                        navigateToBasicInfoScreen(
                            this,
                            viewModel.getCurrentViewStateOrNew().mainEntity!!,
                        )})
                }

                binding.recommendAppIcon, binding.recommendAppLbl, binding.recommendAppButton -> {
                    if (LOG_ME) ALog.d(TAG, "$methodName: recommendAppButton")
                    viewModel.exitEditState()
                    hideKeyboard()
                    viewModel.setStateEvent(HomeScreenStateEvent.RecommendApp {
                        navigateToAppRecommendationScreen(
                            this,
                            viewModel.getCurrentViewStateOrNew().mainEntity!!
                        )
                    })
                }

                binding.paymentCardsIcon, binding.paymentCardsLbl, binding.paymentCardsButton -> {
                    if (LOG_ME) ALog.d(TAG, "$methodName: paymentCardsButton")
                    viewModel.exitEditState()
                    hideKeyboard()

                }

                binding.helpIcon, binding.helpLbl, binding.helpButton -> {
                    if (LOG_ME) ALog.d(TAG, "$methodName: helpButton")
                    viewModel.exitEditState()
                    hideKeyboard()
                    getViewModel().setStateEvent(HomeScreenStateEvent.Navigate {
                            navigateToHelpScreen(
                                this,
                                viewModel.getCurrentViewStateOrNew().mainEntity!!,
                            )
                        }
                    )
                }

                binding.rateAppIcon, binding.rateAppLbl, binding.rateAppButton -> {
                    if (LOG_ME) ALog.d(TAG, "$methodName: rate app")
                    viewModel.exitEditState()
                    hideKeyboard()
                    getViewModel().setStateEvent(HomeScreenStateEvent.RateApplication {
                            navigateToGooglePlayToRateThisApp(this)
                        }
                    )
                }

                binding.termsAndConditionsIcon, binding.termsAndConditionsLbl, binding.termsAndConditionsButton -> {
                    if (LOG_ME) ALog.d(TAG, "$methodName: termsAndConditions")
                    viewModel.exitEditState()
                    hideKeyboard()
                    getViewModel().setStateEvent(HomeScreenStateEvent.Navigate {
                        navigateToTermsAndConditionsScreen(
                            this,
                            viewModel.getCurrentViewStateOrNew().mainEntity!!,
                        )
                    })
                }

                binding.reportProblemIcon, binding.reportProblemLbl, binding.reportProblemButton -> {
                    if (LOG_ME) ALog.d(TAG, "$methodName: report problem")
                    viewModel.exitEditState()
                    hideKeyboard()
                    getViewModel().setStateEvent(HomeScreenStateEvent.ReportProblem{
                        navigateToProblemReporting(
                            this,
                            viewModel.getCurrentViewStateOrNew().mainEntity!!,
                            {getViewModel().setStateEvent(HomeScreenStateEvent.NotifyUserAboutLackOfEmailAppInstalled)}
                        )
                    })
                }

                binding.logoutIcon, binding.logoutButton, binding.logoutLbl -> {
                    if (LOG_ME) ALog.d(TAG, "$methodName: logout")
                    viewModel.exitEditState()
                    hideKeyboard()
                    getViewModel().setStateEvent(HomeScreenStateEvent.AskUserAboutLogout)
                }

                binding.saveButton -> {
                    if (LOG_ME) ALog.d(TAG, "$methodName: saveButton")
                    viewModel.exitEditState()
                    hideKeyboard()
                    hideQuickEditButtons()
                    viewModel.updateUserWithDataFromUI()?.let {
                        getViewModel().setStateEvent(HomeScreenStateEvent.UpdateMainEntityEvent(it))
                    }
                }

                binding.cancelButton -> {
                    if (LOG_ME) ALog.d(TAG, "$methodName: cancelButton")
                    viewModel.exitEditState()
                    hideKeyboard()
                    hideQuickEditButtons()
                    getViewModel().setStateEvent(HomeScreenStateEvent.ClearUIChangesInFragment5)
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun hideQuickEditButtons() {
        val methodName: String = "hideQuickEditButtons()"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            binding.saveButton.visibility = View.GONE
            binding.cancelButton.visibility = View.GONE
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun showQuickEditButtons() {
        val methodName: String = "showQuickEditButtons()"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            binding.saveButton.visibility = View.VISIBLE
            binding.cancelButton.visibility = View.VISIBLE
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override fun updateUIInViewModel() {
        if(viewModel.checkEditState()){
            view?.hideKeyboard()
            if (LOG_ME)ALog.d(TAG, "updateUIInViewModel():  Users interface is in EditState")
            viewModel.exitEditState()
        }
    }

    private fun updateMainEntityInViewModel() {
        if(viewModel.getIsUpdatePending()){
            viewModel.getCurrentViewStateOrNew().mainEntity?.let {
                HomeScreenStateEvent.UpdateMainEntityEvent(it)
            }?.let {
                if (LOG_ME) ALog.d(TAG, "updateMainEntityInViewModel(): Sending UpdateMainEntityEvent")
                viewModel.setStateEvent(it)
            }
        }
    }

    private fun loadProfilePhotoIntoUI(user : ProjectUser) {
        val methodName: String = "loadProfilePhotoIntoUI"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            user.profilePhotoImageURI?.let {
                val storageReference = Firebase.storage
                if(user.id != null) {
                    val imageRef = user.id?.let {
                        storageReference.reference
                            .child(FIRESTORE_IMAGES_COLLECTION)
                            .child(FIRESTORE_PROFILE_PHOTOS_SUB_COLLECTION)
                            .child(it.firestoreDocumentID)
                    }

                    val glideOptions: RequestOptions = RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.ic_launcher)
                        .error(R.drawable.ic_launcher)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)

                    if (LOG_ME) ALog.d(TAG, ".$methodName(): $imageRef")

//                    GlideApp.with(binding.profilePicture)
                    Glide.with(binding.profilePicture)
                        .load(imageRef)
                        .apply(glideOptions)
                        .into(binding.profilePicture)
                } else {
                    ALog.e(TAG, ".$methodName(): user.id == null")
                }
            } ?: ALog.d(TAG, ".$methodName(): No profile picture to set.")
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        val methodName: String = "onFocusChange"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            when(v) {
                binding.usersDescriptionEdittext -> {
                    if (LOG_ME) ALog.d(TAG, "$methodName: usersDescriptionEdittext")
                    if(!viewModel.isEditingUsersDescription()){
                        if (LOG_ME) ALog.d(
                            TAG,
                            "$methodName: Setting EditState for binding.usersDescriptionEdittext"
                        )
                        viewModel.setEditingUsersDescriptionInteractionState(
                            HomeScreenInteractionState.EditState()
                        )
                    }
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }
}