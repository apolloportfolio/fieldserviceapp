package com.aps.catemplateapp.fieldserviceapp.business.data.network.abs

import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity2

interface Entity2NetworkDataSource: StandardNetworkDataSource<Entity2> {
    override suspend fun insertOrUpdateEntity(entity: Entity2): Entity2?

    override suspend fun deleteEntity(primaryKey: UniqueID?)

    override suspend fun insertDeletedEntity(entity: Entity2)

    override suspend fun insertDeletedEntities(Entities: List<Entity2>)

    override suspend fun deleteDeletedEntity(entity: Entity2)

    override suspend fun getDeletedEntities(): List<Entity2>

    override suspend fun deleteAllEntities()

    override suspend fun searchEntity(entity: Entity2): Entity2?

    override suspend fun getAllEntities(): List<Entity2>

    override suspend fun insertOrUpdateEntities(Entities: List<Entity2>): List<Entity2>?

    override suspend fun getEntityById(id: UniqueID): Entity2?

    suspend fun getUsersEntities2(userId : UserUniqueID) : List<Entity2>?
}