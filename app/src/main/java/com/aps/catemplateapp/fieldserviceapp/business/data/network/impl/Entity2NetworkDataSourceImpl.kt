package com.aps.catemplateapp.fieldserviceapp.business.data.network.impl

import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fieldserviceapp.business.data.network.abs.Entity2NetworkDataSource
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity2
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.abs.Entity2FirestoreService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Entity2NetworkDataSourceImpl
@Inject
constructor(
    private val firestoreService: Entity2FirestoreService
): Entity2NetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: Entity2): Entity2? {
        return firestoreService.insertOrUpdateEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        return firestoreService.deleteEntity(primaryKey)
    }

    override suspend fun insertDeletedEntity(entity: Entity2) {
        return firestoreService.insertDeletedEntity(entity)
    }

    override suspend fun insertDeletedEntities(entities: List<Entity2>) {
        return firestoreService.insertDeletedEntities(entities)
    }

    override suspend fun deleteDeletedEntity(entity: Entity2) {
        return firestoreService.deleteDeletedEntity(entity)
    }

    override suspend fun getDeletedEntities(): List<Entity2> {
        return firestoreService.getAllDeletedEntities()
    }

    override suspend fun deleteAllEntities() {
        firestoreService.deleteAllEntities()
    }

    override suspend fun searchEntity(entity: Entity2): Entity2? {
        return firestoreService.searchEntity(entity)
    }

    override suspend fun getAllEntities(): List<Entity2> {
        return firestoreService.getAllEntities()
    }

    override suspend fun insertOrUpdateEntities(entities: List<Entity2>): List<Entity2>? {
        return firestoreService.insertOrUpdateEntities(entities)
    }

    override suspend fun getEntityById(id: UniqueID): Entity2? {
        return firestoreService.getEntityById(id)
    }

    override suspend fun getUsersEntities2(userId : UserUniqueID) : List<Entity2>? {
        return firestoreService.getUsersEntities2(userId)
    }
}