package com.aps.catemplateapp.fieldserviceapp.di

import com.aps.catemplateapp.core.business.data.cache.abstraction.*
import com.aps.catemplateapp.core.business.data.cache.implementation.*
import com.aps.catemplateapp.fieldserviceapp.business.data.cache.abstraction.TaskAssignmentCacheDataSource
import com.aps.catemplateapp.fieldserviceapp.business.data.cache.abstraction.Entity2CacheDataSource
import com.aps.catemplateapp.fieldserviceapp.business.data.cache.abstraction.Entity3CacheDataSource
import com.aps.catemplateapp.fieldserviceapp.business.data.cache.abstraction.Entity4CacheDataSource
import com.aps.catemplateapp.fieldserviceapp.business.data.cache.implementation.TaskAssignmentCacheDataSourceImpl
import com.aps.catemplateapp.fieldserviceapp.business.data.cache.implementation.Entity2CacheDataSourceImpl
import com.aps.catemplateapp.fieldserviceapp.business.data.cache.implementation.Entity3CacheDataSourceImpl
import com.aps.catemplateapp.fieldserviceapp.business.data.cache.implementation.Entity4CacheDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class CacheDataSourcesModule {

    @Binds
    abstract fun bindTaskAssignmentCacheDataSource(implementation: TaskAssignmentCacheDataSourceImpl): TaskAssignmentCacheDataSource

    @Binds
    abstract fun bindEntity2CacheDataSource(implementation: Entity2CacheDataSourceImpl): Entity2CacheDataSource

    @Binds
    abstract fun bindEntity3CacheDataSource(implementation: Entity3CacheDataSourceImpl): Entity3CacheDataSource

    @Binds
    abstract fun bindEntity4CacheDataSource(implementation: Entity4CacheDataSourceImpl): Entity4CacheDataSource
}