package com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.impl

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity3
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.abs.Entity3DaoService
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.daos.Entity3Dao
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.mappers.Entity3CacheMapper
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Entity3DaoServiceImpl
@Inject
constructor(
    private val dao: Entity3Dao,
    private val mapper: Entity3CacheMapper,
    private val dateUtil: DateUtil
): Entity3DaoService {

    override suspend fun insertOrUpdateEntity(entity: Entity3): Entity3 {
        val databaseEntity = dao.getEntityById(entity.id)
        if(databaseEntity != null) {
            dao.insert(mapper.mapToEntity(entity))
            return mapper.mapFromEntity(databaseEntity)
        } else {
            this.updateEntity(
                entity.id,
                entity.updated_at,
                entity.created_at,

                entity.picture1URI,

                entity.name,
                entity.description,

                entity.ownerID,
            )
            return mapper.mapFromEntity(dao.getEntityById(entity.id)!!)
        }
    }


    override suspend fun insertEntity(entity: Entity3): Long {
        return dao.insert(mapper.mapToEntity(entity))
    }

    override suspend fun insertEntities(entities: List<Entity3>): LongArray {
        return dao.insertEntities(
            mapper.mapToEntityList(entities)
        )
    }

    override suspend fun getEntityById(id: UniqueID?): Entity3? {
        return dao.getEntityById(id)?.let { entity ->
            mapper.mapFromEntity(entity)
        }
    }

    override suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        name: String?,
        description: String?,

        ownerID: UserUniqueID?,
    ): Int {
        return if(updated_at != null){
            dao.updateEntity(
                id,
                updated_at,
                created_at,

                picture1URI,

                name,
                description,

                ownerID,
            )
        } else {
            dao.updateEntity(
                id,
                dateUtil.getCurrentTimestamp(),
                created_at,

                picture1URI,

                name,
                description,

                ownerID,
            )
        }

    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return dao.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<Entity3>): Int {
        val ids = entities.mapIndexed {index, value -> value.id}
        return dao.deleteEntities(ids)
    }

    override suspend fun searchEntities(): List<Entity3> {
        return mapper.mapFromEntityList(
            dao.searchEntities()
        )
    }

    override suspend fun getAllEntities(): List<Entity3> {
        return mapper.mapFromEntityList(dao.getAllEntities())
    }

    override suspend fun getNumEntities(): Int {
        return dao.getNumEntities()
    }
}