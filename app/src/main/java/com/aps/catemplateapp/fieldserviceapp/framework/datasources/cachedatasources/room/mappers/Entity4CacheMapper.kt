package com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity4
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.model.Entity4CacheEntity
import javax.inject.Inject

class Entity4CacheMapper
@Inject
constructor() : EntityMapper<Entity4CacheEntity, Entity4> {
    override fun mapFromEntity(entity: Entity4CacheEntity): Entity4 {
        return Entity4(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.latitude ,
            entity.longitude,
            entity.geoLocation,
            entity.firestoreGeoLocation,

            entity.picture1URI,
            entity.name,
            entity.description,
        )
    }

    override fun mapToEntity(domainModel: Entity4): Entity4CacheEntity {
        return Entity4CacheEntity(
            domainModel.id!!,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.latitude ,
            domainModel.longitude,
            domainModel.geoLocation,
            domainModel.firestoreGeoLocation,

            domainModel.picture1URI,
            domainModel.name,
            domainModel.description,
        )
    }

    override fun mapFromEntityList(entities : List<Entity4CacheEntity>) : List<Entity4> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<Entity4>): List<Entity4CacheEntity> {
        return entities.map{mapToEntity(it)}
    }
}