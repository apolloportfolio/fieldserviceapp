package com.aps.catemplateapp.fieldserviceapp.business.data.cache.abstraction


import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.TaskAssignment

interface TaskAssignmentCacheDataSource: StandardCacheDataSource<TaskAssignment> {
    override suspend fun insertEntity(entity: TaskAssignment): Long

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int

    override suspend fun deleteEntities(entities: List<TaskAssignment>): Int

    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        taskDescription: String,
        taskAddress: String,
        clientName: String,
        phoneNumber: String,
        scheduledTaskStartTimeStamp: String,
        expectedTaskDuration: String,
        actualTaskStartTimeStamp: String?,
        actualTaskEndTimeStamp: String?,
        isTaskCompleted: Boolean,
        taskNotes: String?,
        customerFeedbackGrade: Int?,
        customerFeedbackGradeReview: String?,
        taskResultsPicturesURIs: String?,
        materialsNote: String?,
        jobPriority: Int?,

        lattitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description: String?,

        city: String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean?,
        switch2: Boolean?,
        switch3: Boolean?,
        switch4: Boolean?,
        switch5: Boolean?,
        switch6: Boolean?,
        switch7: Boolean?,
    ): Int

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<TaskAssignment>

    override suspend fun getAllEntities(): List<TaskAssignment>

    override suspend fun getEntityById(id: UniqueID?): TaskAssignment?

    override suspend fun getNumEntities(): Int

    override suspend fun insertEntities(entities: List<TaskAssignment>): LongArray
}