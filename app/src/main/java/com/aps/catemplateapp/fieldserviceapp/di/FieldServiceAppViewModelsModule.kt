package com.aps.catemplateapp.fieldserviceapp.di

import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProvider
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.fieldserviceapp.FieldServiceAppViewModelFactory
import com.aps.catemplateapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.factories.TaskAssignmentFactory
import com.aps.catemplateapp.core.business.domain.model.factories.UserFactory
import com.aps.catemplateapp.fieldserviceapp.business.interactors.HomeScreenInteractors
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenViewState
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Named
import javax.inject.Singleton

@ExperimentalCoroutinesApi
@FlowPreview
@Module
@InstallIn(SingletonComponent::class)
object FieldServiceAppViewModelsModule {
    @Singleton
    @JvmStatic
    @Provides
    @Named("FieldServiceAppViewModelFactory")
    fun provideFieldServiceAppViewModelFactory(
        homeScreenInteractors: HomeScreenInteractors<
                ProjectUser,
                UserCacheDataSourceImpl,
                UserNetworkDataSource,
                HomeScreenViewState<ProjectUser>
                >,
        dateUtil: DateUtil,
        entityFactory: UserFactory,
        taskAssignmentFactory: TaskAssignmentFactory,
        editor: SharedPreferences.Editor,
        //sharedPreferences: SharedPreferences
    ): ViewModelProvider.Factory {
        return FieldServiceAppViewModelFactory(
            homeScreenInteractors = homeScreenInteractors,
            dateUtil = dateUtil,
            userFactory = entityFactory,
            taskAssignmentFactory = taskAssignmentFactory,
            editor = editor,
        )
    }
}