package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.fragments

import android.Manifest
import android.app.Activity
import android.location.Location
import android.widget.Toast
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEventTracker
import com.aps.catemplateapp.common.framework.presentation.PermissionHandlingData
import com.aps.catemplateapp.common.framework.presentation.TAG_PERMISSION_ASKING
import com.aps.catemplateapp.common.framework.presentation.views.*
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.DeviceLocation
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.TaskAssignment
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.factories.TaskAssignmentFactory
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenCard1SearchFilters
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenStateEvent
import java.text.SimpleDateFormat
import java.util.Locale

private const val TAG = "HomeScreenCompFragment1"
private const val LOG_ME = true
const val HomeScreenComposableFragment1TestTag = TAG

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun HomeScreenCompFragment1(
    dateUtil: DateUtil,

    launchStateEvent: (HomeScreenStateEvent) -> Unit = {},

    activity: Activity? = null,
    permissionHandlingData: PermissionHandlingData = PermissionHandlingData(),

    stateEventTracker: StateEventTracker,

    deviceLocation: DeviceLocation?,
    showProfileStatusBar: Boolean,
    finishVerification: () -> Unit,
    availableJobsInitialSearchQuery: String = "",
    availableJobs: List<TaskAssignment>?,
    onAllAvailableJobsSearchQueryUpdate: (String) -> Unit,
    onListItemClick: (TaskAssignment) -> Unit,
    searchFilters: HomeScreenCard1SearchFilters,
    onSearchFiltersUpdated: (HomeScreenCard1SearchFilters) -> Unit,

    onSwipeLeft: () -> Unit = {},
    onSwipeRight: () -> Unit = {},
    onSwipeUp: () -> Unit = {},
    onSwipeDown: () -> Unit = {},
    onSwipeRightFromComposableSide: () -> Unit = {},
    onSwipeLeftFromComposableSide: () -> Unit = {},
    onSwipeDownFromComposableSide: () -> Unit = {},
    onSwipeUpFromComposableSide: () -> Unit = {},

    floatingActionButtonDrawableId: Int? = null,
    floatingActionButtonOnClick: (() -> Unit)? = null,
    floatingActionButtonContentDescription: String? = null,
    snackBarState: SnackBarState?,
    toastState: ToastState?,
    dialogState: DialogState?,
    progressIndicatorState: ProgressIndicatorState?,
    bottomSheetActions: HomeScreenComposableFragment1BottomSheetActions,

    isPreview: Boolean = false,
) {
    if(LOG_ME) ALog.d(TAG, "(): Recomposition start.")

    val scope = rememberCoroutineScope()
    val sheetState = rememberBottomSheetState(
        initialValue = BottomSheetValue.Collapsed,
//        initialValue = BottomSheetValue.Expanded,
        animationSpec = spring(dampingRatio = Spring.DampingRatioHighBouncy)
    )
    val launchInitStateEvent: () -> Unit = {
        if(activity != null) {
            if(LOG_ME)ALog.d(TAG, "launchInitStateEvent(): " +
                    "activity != null, launching state event in view model.")
            val path = activity.filesDir
            launchStateEvent(
                HomeScreenStateEvent.GetAvailableJobsAroundUserStateEvent(path)
            )
        } else {
            if(LOG_ME)ALog.w(TAG, "launchInitStateEvent(): " +
                    "activity == null, couldn't set initial state event!")
        }
    }

    val permissionsRequiredInFragment = mutableSetOf(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION,
    )

    if(LOG_ME)ALog.d(
        TAG_PERMISSION_ASKING+TAG, "(): " +
                            "permissionsRequiredInFragment = ${permissionsRequiredInFragment.joinToString()}")
    SwipeableFragmentWithBottomSheetAndFAB(
        launchInitStateEvent = launchInitStateEvent,
        activity = activity,
        permissionHandlingData = permissionHandlingData,
        permissionsRequiredInFragment = permissionsRequiredInFragment,
        backgroundDrawableId = R.drawable.default_background,
        onSwipeLeft = onSwipeLeft,
        onSwipeRight = onSwipeRight,
        onSwipeUp = onSwipeUp,
        onSwipeDown = onSwipeDown,
        onSwipeRightFromComposableSide = onSwipeRightFromComposableSide,
        onSwipeLeftFromComposableSide = onSwipeLeftFromComposableSide,
        onSwipeDownFromComposableSide = onSwipeDownFromComposableSide,
        onSwipeUpFromComposableSide = onSwipeUpFromComposableSide,
        leftSideThreshold = Dimens.fragmentsSwipingLeftSideThresholdFraction,
        floatingActionButtonDrawableId = floatingActionButtonDrawableId,
        floatingActionButtonOnClick = floatingActionButtonOnClick,
        floatingActionButtonContentDescription = floatingActionButtonContentDescription,
        snackBarState = snackBarState,
        toastState = toastState,
        dialogState = dialogState,
        progressIndicatorState = progressIndicatorState,
        sheetState = sheetState,
        sheetContent = HomeScreenCompFragment1BottomSheet(
            scope = scope,
            sheetState = sheetState,
            searchFilters = searchFilters,
            onSearchFiltersUpdated = onSearchFiltersUpdated,
            leftButtonOnClick = bottomSheetActions.leftButtonOnClick,
            rightButtonOnClick = bottomSheetActions.rightButtonOnClick,
            bottomPadding = BottomNavigationHeightPublic,
        ),
        content = {
            FieldServiceAppJobListScreenContent(
                dateUtil = dateUtil,
                stateEventTracker = stateEventTracker,
                deviceLocation = deviceLocation,
                showProfileStatusBar = showProfileStatusBar,
                finishVerification = finishVerification,
                availableJobsInitialSearchQuery = availableJobsInitialSearchQuery,
                availableJobs = availableJobs,
                onAllAvailableJobsSearchQueryUpdate = onAllAvailableJobsSearchQueryUpdate,
                onListItemClick = onListItemClick,
                launchInitStateEvent = launchInitStateEvent,
                isPreview = isPreview,
            )
        },
        isPreview = isPreview,
    )
    if(LOG_ME) ALog.d(TAG, "(): Recomposition end.")
}

//========================================================================================
@Preview
@Composable
fun HomeScreenComposableFragment1Preview(
    @PreviewParameter(
        HomeScreenComposableFragment1ParamsProvider::class
    ) params: HomeScreenComposableFragment1Params
) {
    HomeScreenTheme {
        // Start Interactive Mode to actually see SnackBar
        val snackBarState = remember{
            SnackBarState(
                show = true,
                message = "Snackbar",
                actionLabel = "action",
                iconDrawableRes = R.drawable.ic_launcher,
                onClickAction = {},
                onDismissAction = {},
                duration = SnackbarDuration.Indefinite,
                updateSnackBarInViewModel = {},
            )
        }
        snackBarState.onDismissAction = { snackBarState.show = false }
        snackBarState.onClickAction = { snackBarState.show = false }

        val toastState = ToastState(
            show = true,
            message = "This is a Toast from Preview!",
            duration = Toast.LENGTH_LONG,
            updateToastInViewModel = {},
        )

        val dialogState = DialogState(
            show = false,
            onDismissRequest = {},
            title = "Dialog's Title",
            text = "Dialog's text.",
            leftButtonText = "Dismiss",
            leftButtonOnClick = {},
            rightButtonText = "Confirm",
            rightButtonOnClick = {},
            updateDialogInViewModel = {},
        )

        val progressIndicatorState = ProgressIndicatorState(
            show = false,
            progress = 0.7f,
            type = ProgressIndicatorType.DeterminateCircularProgressIndicator,
            updateProgressIndicatorStateInViewModel = {},
        )

        val bottomSheetActions = HomeScreenComposableFragment1BottomSheetActions(
            leftButtonOnClick = {},
            rightButtonOnClick = {},
        )

        val dateFormatForFirestore = SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.ENGLISH)
        val dateFormatForUser = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
        val dateFormatJustYear = SimpleDateFormat("yyyy", Locale.ENGLISH)
        val dateUtil =
            DateUtil(dateFormatForFirestore, dateFormatForUser, dateFormatJustYear)

        HomeScreenCompFragment1(
            dateUtil = dateUtil,
            stateEventTracker = StateEventTracker(),
            deviceLocation = params.deviceLocation,
            showProfileStatusBar = params.showProfileStatusBar,
            finishVerification = params.finishVerification,
            availableJobsInitialSearchQuery = params.initialSearchQuery,
            availableJobs = params.entities,
            onAllAvailableJobsSearchQueryUpdate = params.onSearchQueryUpdate,
            onListItemClick = params.onListItemClick,
            searchFilters = params.searchFilters,
            onSearchFiltersUpdated = params.onSearchFiltersUpdated,
            onSwipeLeft = params.onSwipeLeft,
            onSwipeRight = params.onSwipeRight,
            onSwipeUp = params.onSwipeUp,
            onSwipeDown = params.onSwipeDown,
            floatingActionButtonDrawableId = params.floatingActionButtonDrawableId,
            floatingActionButtonOnClick = params.floatingActionButtonOnClick,
            floatingActionButtonContentDescription = params.floatingActionButtonContentDescription,
            snackBarState = snackBarState,
            toastState = toastState,
            dialogState = dialogState,
            bottomSheetActions = bottomSheetActions,
            progressIndicatorState = progressIndicatorState,
            isPreview = true,
        )
    }
}


class HomeScreenComposableFragment1ParamsProvider :
    PreviewParameterProvider<HomeScreenComposableFragment1Params> {

    override val values: Sequence<HomeScreenComposableFragment1Params>
        get() = sequenceOf(
            HomeScreenComposableFragment1Params(
                deviceLocation = DeviceLocation(
                    locationPermissionGranted = true,
                    location = Location("Lublin").apply {
                        latitude = 51.2465
                        longitude = 22.5684
                    }
                ),
                showProfileStatusBar = true,
                finishVerification = {},
                initialSearchQuery = "",
                entities = TaskAssignmentFactory.createPreviewEntitiesList(),
                onSearchQueryUpdate = {},
                onListItemClick = {},
                searchFilters = HomeScreenCard1SearchFilters(
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                ),
                onSearchFiltersUpdated = {},
                onSwipeLeft = {},
                onSwipeRight = {},
                onSwipeUp = {},
                onSwipeDown = {},
                floatingActionButtonDrawableId = null,
                floatingActionButtonOnClick = {},
                floatingActionButtonContentDescription = null,
            )
        )
}

data class HomeScreenComposableFragment1Params(
    val deviceLocation: DeviceLocation?,
    val showProfileStatusBar: Boolean,
    val finishVerification: () -> Unit,
    val initialSearchQuery: String = "",
    val entities: List<TaskAssignment>?,
    val onSearchQueryUpdate: (String) -> Unit,
    val onListItemClick: (TaskAssignment) -> Unit,
    val searchFilters: HomeScreenCard1SearchFilters,
    val onSearchFiltersUpdated: (HomeScreenCard1SearchFilters) -> Unit,
    val onSwipeLeft: () -> Unit = {},
    val onSwipeRight: () -> Unit = {},
    val onSwipeUp: () -> Unit = {},
    val onSwipeDown: () -> Unit = {},
    val floatingActionButtonDrawableId: Int? = null,
    val floatingActionButtonOnClick: (() -> Unit)? = null,
    val floatingActionButtonContentDescription: String? = null,
)