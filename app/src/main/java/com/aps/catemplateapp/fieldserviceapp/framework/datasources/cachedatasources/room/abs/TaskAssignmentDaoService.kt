package com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.abs

import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.TaskAssignment

interface TaskAssignmentDaoService {
    suspend fun insertOrUpdateEntity(entity: TaskAssignment): TaskAssignment

    suspend fun insertEntity(entity: TaskAssignment): Long

    suspend fun insertEntities(Entities: List<TaskAssignment>): LongArray

    suspend fun getEntityById(id: UniqueID?): TaskAssignment?

    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        taskDescription: String?,
        taskAddress: String?,
        clientName: String?,
        phoneNumber: String?,
        scheduledTaskStartTimeStamp: String?,
        expectedTaskDuration: String?,
        actualTaskStartTimeStamp: String?,
        actualTaskEndTimeStamp: String?,
        isTaskCompleted: Boolean?,
        taskNotes: String?,
        customerFeedbackGrade: Int?,
        customerFeedbackGradeReview: String?,
        taskResultsPicturesURIs: String?,
        materialsNote: String?,
        jobPriority: Int?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description : String?,

        city: String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean?,
        switch2: Boolean?,
        switch3: Boolean?,
        switch4: Boolean?,
        switch5: Boolean?,
        switch6: Boolean?,
        switch7: Boolean?,
    ): Int

    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteEntities(Entities: List<TaskAssignment>): Int

    suspend fun searchEntities(): List<TaskAssignment>

    suspend fun getAllEntities(): List<TaskAssignment>

    suspend fun getNumEntities(): Int
}