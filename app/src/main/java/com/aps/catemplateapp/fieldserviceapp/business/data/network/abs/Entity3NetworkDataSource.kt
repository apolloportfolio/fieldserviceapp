package com.aps.catemplateapp.fieldserviceapp.business.data.network.abs

import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity3

interface Entity3NetworkDataSource: StandardNetworkDataSource<Entity3> {
    override suspend fun insertOrUpdateEntity(entity: Entity3): Entity3?

    override suspend fun deleteEntity(primaryKey: UniqueID?)

    override suspend fun insertDeletedEntity(entity: Entity3)

    override suspend fun insertDeletedEntities(Entities: List<Entity3>)

    override suspend fun deleteDeletedEntity(entity: Entity3)

    override suspend fun getDeletedEntities(): List<Entity3>

    override suspend fun deleteAllEntities()

    override suspend fun searchEntity(entity: Entity3): Entity3?

    override suspend fun getAllEntities(): List<Entity3>

    override suspend fun insertOrUpdateEntities(Entities: List<Entity3>): List<Entity3>?

    override suspend fun getEntityById(id: UniqueID): Entity3?

    suspend fun getUsersEntities3(userId: UserUniqueID): List<Entity3>?
}