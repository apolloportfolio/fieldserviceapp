package com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity4
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.model.Entity4FirestoreEntity
import javax.inject.Inject

class Entity4FirestoreMapper
@Inject
constructor() : EntityMapper<Entity4FirestoreEntity, Entity4> {
    override fun mapFromEntity(entity: Entity4FirestoreEntity): Entity4 {
        val entity4 = Entity4(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.latitude,
            entity.longitude,
            null,
            entity.firestoreGeoLocation,

            entity.picture1URI,
            entity.name,
            entity.description,
        )
        if(entity.geoLocation != null){
            entity4.geoLocation = ParcelableGeoPoint(
                entity.geoLocation!!.latitude,
                entity.geoLocation!!.longitude
            )
        }
        return entity4
    }

    override fun mapToEntity(domainModel: Entity4): Entity4FirestoreEntity {
        return Entity4FirestoreEntity(
            domainModel.id,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.latitude,
            domainModel.longitude,
            domainModel.geoLocation,
            domainModel.firestoreGeoLocation,

            domainModel.picture1URI,
            domainModel.name,
            domainModel.description,
        )
    }

    override fun mapFromEntityList(entities : List<Entity4FirestoreEntity>) : List<Entity4> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<Entity4>): List<Entity4FirestoreEntity> {
        return entities.map{mapToEntity(it)}
    }
}