package com.aps.catemplateapp.fieldserviceapp.di.activity01

import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.abs.HomeScreenUIProvider
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.HomeScreenUIProviderImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Singleton

@FlowPreview
@ExperimentalCoroutinesApi
@Module
@InstallIn(SingletonComponent::class)
object HomeScreenUIProviderModule {
    @JvmStatic
    @Singleton
    @Provides
    fun provideHomeScreenUIProvider(): HomeScreenUIProvider {
        return HomeScreenUIProviderImpl()
    }
}