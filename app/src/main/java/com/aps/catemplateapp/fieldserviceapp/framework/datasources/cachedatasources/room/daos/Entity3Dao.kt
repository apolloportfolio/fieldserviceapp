package com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.model.Entity3CacheEntity
import java.util.*


@Dao
interface Entity3Dao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity : Entity3CacheEntity) : Long

    @Query("SELECT * FROM entity3")
    suspend fun get() : List<Entity3CacheEntity>
    
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEntities(entities: List<Entity3CacheEntity>): LongArray

    @Query("SELECT * FROM entity3 WHERE id = :id")
    suspend fun getEntityById(id: UniqueID?): Entity3CacheEntity?

    @Query("DELETE FROM entity3 WHERE id IN (:ids)")
    suspend fun deleteEntities(ids: List<UniqueID?>): Int

    @Query("DELETE FROM entity3")
    suspend fun deleteAllEntities()

    @Query("SELECT * FROM entity3")
    suspend fun getAllEntities(): List<Entity3CacheEntity>

    @Query("""
        UPDATE entity3 
        SET 
        created_at = :created_at,
        updated_at = :updated_at,
        picture1URI = :picture1URI,
        name = :name,
        description = :description,
        ownerId = :ownerID
        WHERE id = :id
        """)
    suspend fun updateEntity(
        id : UniqueID?,
        created_at: String?,
        updated_at: String?,

        picture1URI: String?,

        name: String?,
        description: String?,

        ownerID: UserUniqueID?,
    ): Int

    @Query("DELETE FROM entity3 WHERE id = :primaryKey")
    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    @Query("SELECT * FROM entity3")
    suspend fun searchEntities(): List<Entity3CacheEntity>
    
    @Query("SELECT COUNT(*) FROM entity3")
    suspend fun getNumEntities(): Int
}