package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.fragments

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccessTime
import androidx.compose.material.icons.filled.Build
import androidx.compose.material.icons.filled.CalendarToday
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Description
import androidx.compose.material.icons.filled.Done
import androidx.compose.material.icons.filled.Feedback
import androidx.compose.material.icons.filled.LocationCity
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material.icons.filled.Notes
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material.icons.filled.PriorityHigh
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.filled.Update
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.framework.presentation.views.BoxWithBackground
import com.aps.catemplateapp.common.framework.presentation.views.EditButton
import com.aps.catemplateapp.common.framework.presentation.views.RowWithLabelAndValueType01
import com.aps.catemplateapp.common.framework.presentation.views.TipContentIsUnavailable
import com.aps.catemplateapp.common.framework.presentation.views.TitleRowWithSubtitleAndPictures
import com.aps.catemplateapp.common.util.composableutils.fallbackPainterResource
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.TaskAssignment
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.factories.TaskAssignmentFactory
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.values.BackgroundsOfLayoutsFieldServiceApp
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.values.HomeScreenTheme

// HomeScreenCompDetailsScreen1
@Composable
fun FieldServiceAppJobDetailsScreenContent(
    taskAssignment: TaskAssignment?,
    navigateToTaskAssignmentEditScreen: () -> Unit,
    navigateToEditTaskAssignmentFeedbackScreen: () -> Unit,
    navigateToEditTaskAssignmentNotesScreen: () -> Unit,
    dateUtil: DateUtil,
) {
    BoxWithBackground(
        backgroundDrawableId = R.drawable.example_background_1,
        composableBackground = BackgroundsOfLayoutsFieldServiceApp.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        )
    ) {
        if(taskAssignment == null) {
            TipContentIsUnavailable(
                stringResource(id = R.string.error_task_assignment_is_null_in_details_screen)
            )
        } else {
            Column(
                modifier = Modifier
                    .fillMaxSize(),
                verticalArrangement = Arrangement.Center,
            ) {
                // Task title and buttons
                TaskAssignmentHeader(
                    taskAssignment = taskAssignment,
                    navigateToTaskAssignmentEditScreen = navigateToTaskAssignmentEditScreen,
                    navigateToCustomerFeedbackScreen = navigateToEditTaskAssignmentFeedbackScreen,
                    dateUtil = dateUtil,
                )
                LazyColumn(
                    modifier = Modifier,
                    verticalArrangement = Arrangement.Center,
                ) {
                    // Task details
                    item {
                        TaskAssignmentInfoSection(taskAssignment = taskAssignment)
                        Spacer(modifier = Modifier.height(Dimens.spacingSmall))
                    }

                    // Client details
                    item {
                        TaskAssignmentClientDetailsSection(taskAssignment = taskAssignment)
                        Spacer(modifier = Modifier.height(Dimens.spacingSmall))
                    }

                    // Feedback
                    item {
                        TaskAssignmentFeedbackSection(taskAssignment = taskAssignment)
                        Spacer(modifier = Modifier.height(Dimens.spacingSmall))
                    }
                }

                // Edit Button with dropdown menu
                EditButton(
                    option1String = stringResource(id = R.string.add_note),
                    option2String = stringResource(id = R.string.add_feedback),
                    onEditButtonClick = navigateToTaskAssignmentEditScreen,
                    onClickOption1 = navigateToEditTaskAssignmentFeedbackScreen,
                    onClickOption2 = navigateToEditTaskAssignmentNotesScreen,
                )
            }

        }

    }
}

@Composable
private fun TaskAssignmentHeader(
    taskAssignment: TaskAssignment,
    navigateToTaskAssignmentEditScreen: () -> Unit,
    navigateToCustomerFeedbackScreen: () -> Unit,
    dateUtil: DateUtil,
) {
    val formattedScheduledTaskTime: String = taskAssignment.scheduledTaskStartTimeStamp?.let {
        dateUtil.convertFirebaseTimestampToStringDateForUser(
            dateUtil.convertStringDateToFirebaseTimestamp(it)
        )
    } ?: stringResource(id = R.string.unscheduled_task)
    val titleString: String?
    val subtitleString: String?
    if(taskAssignment.name != null) {
        titleString = taskAssignment.taskDescription
        subtitleString = formattedScheduledTaskTime
    } else {
        titleString = formattedScheduledTaskTime
        subtitleString = null
    }

    TitleRowWithSubtitleAndPictures(
        titleString = titleString,
        subtitleString = subtitleString,
        textAlign = TextAlign.Center,
        onClick = null,
        leftPictureItemRef = null,
        rightPictureItemRef = null,
        leftPicturePainter = fallbackPainterResource(id = R.drawable.edit_icon),
        rightPicturePainter = fallbackPainterResource(id = R.drawable.baseline_thumbs_up_down_24),
        leftPictureContentDescription = stringResource(id = R.string.left_image),
        rightPictureContentDescription = stringResource(id = R.string.right_image),
        composableBackground = BackgroundsOfLayoutsFieldServiceApp.backgroundTitleType01(
            colors = listOf(
                MaterialTheme.colors.surface,
                MaterialTheme.colors.primaryVariant,
                MaterialTheme.colors.primary,
            ),
            brush = null,
            shape = null,
            alpha = 0.7f,
        ),
        backgroundDrawableId = R.drawable.field_service_app_header_background,
        cardPaddingValues = PaddingValues(
            start = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
            end = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
            top = dimensionResource(id = R.dimen.list_tile_vertical_margin),
            bottom = dimensionResource(id = R.dimen.list_tile_vertical_margin),
        ),
        internalPaddingValues = PaddingValues(4.dp, 3.dp, 4.dp, 3.dp),
        titleFontSize = 24,
        subtitleFontSize = 18,
        leftImageTint = MaterialTheme.colors.onSurface,
        rightImageTint = MaterialTheme.colors.onSurface,
        titleColor = MaterialTheme.colors.onSurface,
        subtitleColor = MaterialTheme.colors.onSurface,
        modifier = Modifier,
        leftPictureOnClick = navigateToTaskAssignmentEditScreen,
        rightPictureOnClick = navigateToCustomerFeedbackScreen,
    )
}


@Composable
private fun TaskAssignmentInfoSection(taskAssignment: TaskAssignment) {
    BoxWithBackground(
        backgroundDrawableId = R.drawable.field_service_app_task_info_background,
        composableBackground = BackgroundsOfLayoutsFieldServiceApp.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        ),
        modifier = Modifier
            .fillMaxWidth(),
        contentAlignment = Alignment.TopStart,
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(Dimens.paddingMedium)
        ) {
            TaskInfoItem(
                icon = Icons.Default.CalendarToday,
                label = stringResource(id = R.string.date_and_time),
                value = taskAssignment.scheduledTaskStartTimeStamp ?: ""
            )

            TaskInfoItem(
                icon = Icons.Default.Description,
                label = stringResource(id = R.string.task_description),
                value = taskAssignment.description ?: ""
            )

            TaskInfoItem(
                icon = Icons.Default.LocationOn,
                label = stringResource(id = R.string.address),
                value = taskAssignment.taskAddress ?: ""
            )

            TaskInfoItem(
                icon = Icons.Default.AccessTime,
                label = stringResource(id = R.string.expected_task_duration),
                value = taskAssignment.expectedTaskDuration ?: ""
            )

            TaskInfoItem(
                icon = Icons.Default.Update,
                label = stringResource(id = R.string.actual_start_time),
                value = taskAssignment.actualTaskStartTimeStamp ?: ""
            )

            TaskInfoItem(
                icon = Icons.Default.Done,
                label = stringResource(id = R.string.actual_end_time),
                value = taskAssignment.actualTaskEndTimeStamp ?: ""
            )

            TaskInfoItem(
                icon = Icons.Default.Check,
                label = stringResource(id = R.string.tasks_completion_status),
                value = taskAssignment.isTaskCompleted?.toString() ?: ""
            )

            TaskInfoItem(
                icon = Icons.Default.PriorityHigh,
                label = stringResource(id = R.string.job_priority),
                value = taskAssignment.jobPriority?.toString() ?: ""
            )

        }
    }
}

@Composable
private fun TaskAssignmentClientDetailsSection(taskAssignment: TaskAssignment) {
    BoxWithBackground(
        backgroundDrawableId = R.drawable.field_service_app_task_info_background,
        composableBackground = BackgroundsOfLayoutsFieldServiceApp.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        ),
        modifier = Modifier
            .fillMaxWidth(),
        contentAlignment = Alignment.TopStart,
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(Dimens.columnPadding)
        ) {

            TaskInfoItem(
                icon = Icons.Default.Person,
                label = stringResource(id = R.string.client_name),
                value = taskAssignment.clientName ?: ""
            )

            TaskInfoItem(
                icon = Icons.Default.Phone,
                label = stringResource(id = R.string.clients_phone),
                value = taskAssignment.phoneNumber ?: ""
            )
        }
    }
}

@Composable
private fun TaskAssignmentFeedbackSection(taskAssignment: TaskAssignment) {
    BoxWithBackground(
        backgroundDrawableId = R.drawable.field_service_app_task_info_background,
        composableBackground = BackgroundsOfLayoutsFieldServiceApp.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        ),
        modifier = Modifier
            .fillMaxWidth(),
        contentAlignment = Alignment.TopStart,
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(Dimens.columnPadding)
        ) {

            TaskInfoItem(
                icon = Icons.Default.Star,
                label = stringResource(id = R.string.feedback_grade),
                value = taskAssignment.customerFeedbackGrade?.toString() ?: ""
            )

            TaskInfoItem(
                icon = Icons.Default.Feedback,
                label = stringResource(id = R.string.feedback_review),
                value = taskAssignment.customerFeedbackGradeReview ?: ""
            )

            TaskInfoItem(
                icon = Icons.Default.Notes,
                label = stringResource(id = R.string.task_notes),
                value = taskAssignment.taskNotes ?: ""
            )

            TaskInfoItem(
                icon = Icons.Default.Build,
                label = stringResource(id = R.string.materials_note),
                value = taskAssignment.materialsNote ?: ""
            )

        }
    }
}

@Composable
private fun TaskInfoItem(icon: ImageVector, label: String, value: String) {
    RowWithLabelAndValueType01(
        icon = icon,
        label = label,
        value = value,
        tint = MaterialTheme.colors.onBackground,
        borderWidth = 2.dp,
        cornerRadius = 8.dp,
        composableBackground = BackgroundsOfLayoutsFieldServiceApp.backgroundListItemType01(
            colors = listOf(
                MaterialTheme.colors.surface,
                MaterialTheme.colors.primaryVariant,
                MaterialTheme.colors.primary,
            ),
            brush = null,
            shape = null,
            alpha = 0.7f,
        ),
    )
}

@Preview
@Composable
private fun FieldServiceAppJobDetailsScreenContentPreview() {
    HomeScreenTheme {
        FieldServiceAppJobDetailsScreenContent(
            taskAssignment = TaskAssignmentFactory.createPreviewEntitiesList()[0],
            navigateToTaskAssignmentEditScreen = {},
            navigateToEditTaskAssignmentFeedbackScreen = {},
            navigateToEditTaskAssignmentNotesScreen = {},
            dateUtil = DateUtil.getDateUtilForPreview(),
        )

    }
}