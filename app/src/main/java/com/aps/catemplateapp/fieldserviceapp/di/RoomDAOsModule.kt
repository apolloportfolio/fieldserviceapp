package com.aps.catemplateapp.fieldserviceapp.di

import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.abstraction.*
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.implementation.*
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.abs.TaskAssignmentDaoService
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.abs.Entity2DaoService
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.abs.Entity3DaoService
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.abs.Entity4DaoService
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.impl.TaskAssignmentDaoServiceImpl
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.impl.Entity2DaoServiceImpl
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.impl.Entity3DaoServiceImpl
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.impl.Entity4DaoServiceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RoomDAOsModule {

    @Binds
    abstract fun bindTaskAssignmentDaoService(implementation: TaskAssignmentDaoServiceImpl): TaskAssignmentDaoService

    @Binds
    abstract fun bindEntity2DaoService(implementation: Entity2DaoServiceImpl): Entity2DaoService

    @Binds
    abstract fun bindEntity3DaoService(implementation: Entity3DaoServiceImpl): Entity3DaoService

    @Binds
    abstract fun bindEntity4DaoService(implementation: Entity4DaoServiceImpl): Entity4DaoService

}