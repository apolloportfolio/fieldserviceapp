package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.layouts

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.createGraph

@Composable
internal fun LeftNavHost(
    leftPaneStartDestination: String,
    leftPaneNavGraphBuilder: NavGraphBuilder.() -> Unit,
    leftNavController: NavHostController,
    modifier: Modifier?,
) {
    val navGraph = remember(leftPaneStartDestination, leftPaneNavGraphBuilder) {
        leftNavController.createGraph(
            leftPaneStartDestination,
            null,
            leftPaneNavGraphBuilder,
        )
    }
    NavHost(
        navController = leftNavController,
        modifier = modifier ?: Modifier,
        graph = navGraph,
    )
}