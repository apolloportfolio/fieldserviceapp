package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.view.*
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.widget.SwitchCompat
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.common.framework.presentation.ApplicationViewModel
import com.aps.catemplateapp.common.framework.presentation.BaseMVIFragment
import com.aps.catemplateapp.common.framework.presentation.BaseViewModel
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.DeviceLocation
import com.aps.catemplateapp.common.util.extensions.*
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.databinding.FragmentHomeScreenFragment1PersistentBinding
import com.aps.catemplateapp.databinding.ItemSearchOptionsBottomSheetPersistentBinding
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.ActivityHomeScreen
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.ActivityHomeScreenNavigation
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.HomeScreenViewModel
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.adapters.AdapterTaskAssignmentToListItemType3
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenInteractionState
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenStateEvent
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenViewState
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.slider.RangeSlider
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*


const val HomeScreen_Fragment1_STATE_BUNDLE_KEY = "com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.fragments.fragment1"

//Fragment with a list of searched items
private const val TAG = "ActivityHomeScreenFragment1"
private var LOG_ME = true

@FlowPreview
@ExperimentalCoroutinesApi
@AndroidEntryPoint
class ActivityHomeScreenFragment1
constructor(
        private val viewModelFactory: ViewModelProvider.Factory,
        private val dateUtil: DateUtil
) : BaseMVIFragment<ProjectUser, HomeScreenViewState<ProjectUser>>(layoutRes = R.layout.fragment_home_screen_fragment1_persistent),
    ActivityHomeScreenNavigation,
    View.OnTouchListener {


    private val viewModel : HomeScreenViewModel
    get() = (activity as ActivityHomeScreen).viewModel

    private val applicationViewModel : ApplicationViewModel by viewModels {
        viewModelFactory
    }

    private lateinit var binding: FragmentHomeScreenFragment1PersistentBinding
    private lateinit var bottomSheetBinding : ItemSearchOptionsBottomSheetPersistentBinding
    private var inputs = ArrayList<View>()

    private lateinit var recyclerView : RecyclerView

    private val fragment = this
    private val onItemClickListener : AdapterTaskAssignmentToListItemType3.OnItemClickListener =
        object : AdapterTaskAssignmentToListItemType3.OnItemClickListener {
            override fun onItemClick(position: Int) {
                    val methodName: String = "onItemClick"
                if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
                try {
                    val clickedItem =
                        getViewModel().getCurrentViewStateOrNew().searchedAvailableJobs?.get(position)
                    if(clickedItem != null) {
                        if (LOG_ME) ALog.d(
                            TAG, "$methodName(): " +
                                "Navigating to item $position\n" +
                                "clickedItem == $clickedItem")
                        val user = getViewModel().getCurrentViewStateOrNew().mainEntity
                        if(user != null) {
                            navigateToEntity1DetailsScreen(
                                fragment,
                                clickedItem,
                                user
                            )
                        } else {
                            ALog.e(TAG, ".$methodName(): user == null")
                        }
                    } else {
                        if(LOG_ME)ALog.e(TAG, ".onItemClick(): clickedItem == null")
                    }
                } catch (e: Exception) {
                    ALog.e(TAG, methodName, e)
                    return
                } finally {
                    if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
                }
            }
        }

    constructor(parcel: Parcel) : this(
            TODO("viewModelFactory"),
            TODO("dateUtil")) {
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<androidx.coordinatorlayout.widget.CoordinatorLayout>

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        viewModel.setupChannel()
    }

    @InternalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val methodName: String = "onViewCreated"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            super.onViewCreated(view, savedInstanceState)

            recyclerView = binding.recyclerView
            recyclerView.adapter = AdapterTaskAssignmentToListItemType3(
                getViewModel().getCurrentViewStateOrNew().searchedAvailableJobs,
                onItemClickListener,
                onItemClickListener,
            )
            recyclerView.layoutManager = LinearLayoutManager(activity)
            recyclerView.setHasFixedSize(false)

            restoreInstanceState(savedInstanceState)
            setupOnBackPressDispatcher()
            populateInputs()
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override fun onResume() {
        super.onResume()
        checkLocationPermission()

        makeBottomSheetBehaveItself()
    }

    override fun onPause() {
        super.onPause()
        viewModel.setSearchedEntities1List(null)
    }

    override fun restoreInstanceState(savedInstanceState: Bundle?){
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    private fun setupOnBackPressDispatcher() {
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                onBackPressed()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }

    override fun onBackPressed() {
        viewModel.exitEditState()
        super.onBackPressed()
        view?.hideKeyboard()
        //Save data from ui in ViewModel if necessary
        findNavController().popBackStack()
    }
    override fun setupUI(){
        view?.hideKeyboard()
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun setupBinding(view: View) {
        val methodName: String = "setupBinding()"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if (LOG_ME) ALog.d(TAG, "$methodName: Initializing binding")
            binding = FragmentHomeScreenFragment1PersistentBinding.bind(view)
            if (LOG_ME) ALog.d(TAG, "$methodName: Initializing bottomSheetBinding")
            bottomSheetBinding = ItemSearchOptionsBottomSheetPersistentBinding.bind(view)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override fun subscribeCustomObservers() {
        val methodName: String = "subscribeCustomObservers"
        viewModel.searchQueryEdittextInteractionState.observe(
            viewLifecycleOwner, androidx.lifecycle.Observer { state ->
                val inputField : EditText =  binding.searchQueryEdittext
                val inputFieldsName = "binding.searchQueryEdittext"

                when(state){
                    is HomeScreenInteractionState.EditState -> {
                        if(LOG_ME)ALog.d(
                            TAG,
                            ".$methodName(): EditState: $inputFieldsName.text == ${inputField.text}"
                        )
                    }

                    is HomeScreenInteractionState.DefaultState -> {
                        if(LOG_ME)ALog.d(
                            TAG, ".$methodName(): DefaultState: $inputFieldsName.text == ${inputField.text}")
                        updateSearchStringInViewModel()
                    }

                    else -> {}
                }
            })
        viewModel.range1ValueMinInteractionState.observe(
            viewLifecycleOwner, androidx.lifecycle.Observer { state ->
                val inputField : EditText =  bottomSheetBinding.range1MinEdittext
                val inputFieldsName = "bottomSheetBinding.range1MinEdittext"

                when(state){
                    is HomeScreenInteractionState.EditState -> {
                        if(LOG_ME)ALog.d(
                            TAG,
                            ".$methodName(): EditState: $inputFieldsName.text == ${inputField.text}"
                        )
                    }

                    is HomeScreenInteractionState.DefaultState -> {
                        if(LOG_ME)ALog.d(
                            TAG, ".$methodName(): DefaultState: $inputFieldsName.text == ${inputField.text}")
                        updateRange1MinEditTextInViewModel()
                    }

                    else -> {}
                }
        })
        viewModel.range1ValueMaxInteractionState.observe(
            viewLifecycleOwner, androidx.lifecycle.Observer { state ->
                val inputField : EditText =  bottomSheetBinding.range1MaxEdittext
                val inputFieldsName = "bottomSheetBinding.range1MaxEdittext"

                when(state){
                    is HomeScreenInteractionState.EditState -> {
                        if(LOG_ME)ALog.d(
                            TAG,
                            ".$methodName(): EditState: $inputFieldsName.text == ${inputField.text}"
                        )
                        if(LOG_ME)ALog.d(
                            TAG,
                            ".$methodName(): EditState: range1MaxEdittext.text == ${bottomSheetBinding.range1MaxEdittext.text}"
                        )
                    }

                    is HomeScreenInteractionState.DefaultState -> {
                        if(LOG_ME)ALog.d(
                            TAG, ".$methodName(): DefaultState: $inputFieldsName.text == ${inputField.text}")
                        updateRange1MaxEditTextInViewModel()
                    }

                    else -> {}
                }
            })
        viewModel.range1SeekbarInteractionState.observe(
            viewLifecycleOwner, androidx.lifecycle.Observer { state ->
                val inputField : RangeSlider =  bottomSheetBinding.range1RangeSlider
                val inputFieldsName = "bottomSheetBinding.range1RangeSlider"

                when(state){
                    is HomeScreenInteractionState.EditState -> {
                        if(LOG_ME)ALog.d(
                            TAG,
                            ".$methodName(): EditState: " +
                                    "$inputFieldsName.valueLeft() == ${inputField.valueLeft()}    " +
                                    "$inputFieldsName.valueRight() == ${inputField.valueRight()}"
                        )
                    }

                    is HomeScreenInteractionState.DefaultState -> {
                        if(LOG_ME)ALog.d(
                            TAG, ".$methodName(): DefaultState: " +
                                    "$inputFieldsName.valueLeft() == ${inputField.valueLeft()}   " +
                                    "$inputFieldsName.valueRight() == ${inputField.valueRight()}"
                        )
                        val range1Min = bottomSheetBinding.range1RangeSlider.valueLeft()
                        val range1Max = bottomSheetBinding.range1RangeSlider.valueRight()
                        viewModel.updateInputField(range1Min.toString()) {
                            val currentViewState = viewModel.getCurrentViewStateOrNew()
                            currentViewState.range1min = range1Min.toDouble()
                            currentViewState.range1max = range1Max.toDouble()
                        }
                    }

                    else -> {}
                }
            })
        viewModel.searchOptionsBottomSheetPricePerKmOverLimitValueMinInteractionState.observe(
            viewLifecycleOwner, androidx.lifecycle.Observer { state ->
                val inputField : EditText =  bottomSheetBinding.range2MinEdittext
                val inputFieldsName = "bottomSheetBinding.range2MinEdittext"

                when(state){
                    is HomeScreenInteractionState.EditState -> {
                        if(LOG_ME)ALog.d(
                            TAG,
                            ".$methodName(): EditState: $inputFieldsName.text == ${inputField.text}"
                        )
                    }

                    is HomeScreenInteractionState.DefaultState -> {
                        if(LOG_ME)ALog.d(
                            TAG, ".$methodName(): DefaultState: $inputFieldsName.text == ${inputField.text}")
                        updateRange2MinEditTextInViewModel()
                    }

                    else -> {}
                }
            })
        viewModel.pricePerKmOverLimitValueMaxInteractionState.observe(
            viewLifecycleOwner, androidx.lifecycle.Observer { state ->
                val inputField : EditText =  bottomSheetBinding.range2MaxEdittext
                val inputFieldsName = "bottomSheetBinding.range2MaxEdittext"

                when(state){
                    is HomeScreenInteractionState.EditState -> {
                        if(LOG_ME)ALog.d(
                            TAG,
                            ".$methodName(): EditState: $inputFieldsName.text == ${inputField.text}"
                        )
                    }

                    is HomeScreenInteractionState.DefaultState -> {
                        if(LOG_ME)ALog.d(
                            TAG, ".$methodName(): DefaultState: $inputFieldsName.text == ${inputField.text}")
                        updateRange2MaxInViewModel()
                    }

                    else -> {}
                }
            })
        viewModel.range2SeekbarInteractionState.observe(
            viewLifecycleOwner, androidx.lifecycle.Observer { state ->
                val inputField : RangeSlider =  bottomSheetBinding.range2RangeSlider
                val inputFieldsName = "bottomSheetBinding.range2RangeSlider"

                when(state){
                    is HomeScreenInteractionState.EditState -> {
                        if(LOG_ME)ALog.d(
                            TAG,
                            ".$methodName(): EditState: " +
                                    "$inputFieldsName.valueLeft() == ${inputField.valueLeft()} " +
                                    "$inputFieldsName.valueRight() == ${inputField.valueRight()}"
                        )
                    }

                    is HomeScreenInteractionState.DefaultState -> {
                        if(LOG_ME)ALog.d(
                            TAG, ".$methodName(): DefaultState: " +
                                    "$inputFieldsName.valueLeft() == ${inputField.valueLeft()} " +
                                    "$inputFieldsName.valueRight() == ${inputField.valueRight()}"
                        )
                        val range2Min = inputField.valueLeft()
                        val mange2Max = inputField.valueRight()
                        viewModel.updateInputField(range2Min.toString()) {
                            val currentViewState = viewModel.getCurrentViewStateOrNew()
                            currentViewState.range2min = range2Min.toDouble()
                            currentViewState.range2max = mange2Max.toDouble()
                        }
                    }

                    else -> {}
                }
            })
        viewModel.switch1InteractionState.observe(
            viewLifecycleOwner, androidx.lifecycle.Observer { state ->
                val inputField : SwitchCompat =  bottomSheetBinding.switch1
                val inputFieldsName = "bottomSheetBinding.switch1"

                when(state){
                    is HomeScreenInteractionState.EditState -> {
                        if(LOG_ME)ALog.d(
                            TAG,
                            ".$methodName(): EditState: $inputFieldsName.text == ${inputField.isChecked}"
                        )
                    }

                    is HomeScreenInteractionState.DefaultState -> {
                        if(LOG_ME)ALog.d(
                            TAG, ".$methodName(): DefaultState: $inputFieldsName.text == ${inputField.isChecked}")
                        updateSwitch1InViewModel()
                    }

                    else -> {}
                }
            })
        viewModel.switch2InteractionState.observe(
            viewLifecycleOwner, androidx.lifecycle.Observer { state ->
                val inputField : SwitchCompat =  bottomSheetBinding.switch2
                val inputFieldsName = "bottomSheetBinding.switch2"

                when(state){
                    is HomeScreenInteractionState.EditState -> {
                        if(LOG_ME)ALog.d(
                            TAG,
                            ".$methodName(): EditState: $inputFieldsName.text == ${inputField.isChecked}"
                        )
                    }

                    is HomeScreenInteractionState.DefaultState -> {
                        if(LOG_ME)ALog.d(
                            TAG,
                            ".$methodName(): DefaultState: $inputFieldsName.text == ${inputField.isChecked}"
                        )
                        updateSwitch2InViewModel()
                    }

                    else -> {}
                }
            })
        viewModel.switch3InteractionState.observe(
            viewLifecycleOwner, androidx.lifecycle.Observer { state ->
                val inputField : SwitchCompat =  bottomSheetBinding.switch3
                val inputFieldsName = "bottomSheetBinding.switch3"

                when(state){
                    is HomeScreenInteractionState.EditState -> {
                        if(LOG_ME)ALog.d(
                            TAG,
                            ".$methodName(): EditState: $inputFieldsName.text == ${inputField.isChecked}"
                        )
                    }

                    is HomeScreenInteractionState.DefaultState -> {
                        if(LOG_ME)ALog.d(
                            TAG,
                            ".$methodName(): DefaultState: $inputFieldsName.text == ${inputField.isChecked}"
                        )
                        updateSwitch3InViewModel()
                    }

                    else -> {}
                }
            })
        viewModel.switch4InteractionState.observe(
            viewLifecycleOwner, androidx.lifecycle.Observer { state ->
                val inputField : SwitchCompat =  bottomSheetBinding.switch4
                val inputFieldsName = "bottomSheetBinding.switchRoofRack"

                when(state){
                    is HomeScreenInteractionState.EditState -> {
                        if(LOG_ME)ALog.d(
                            TAG,
                            ".$methodName(): EditState: $inputFieldsName.text == ${inputField.isChecked}"
                        )
                    }

                    is HomeScreenInteractionState.DefaultState -> {
                        if(LOG_ME)ALog.d(
                            TAG,
                            ".$methodName(): DefaultState: $inputFieldsName.text == ${inputField.isChecked}"
                        )
                        updateSwitch4InViewModel()
                    }

                    else -> {}
                }
            })
        viewModel.switch5InteractionState.observe(
            viewLifecycleOwner, androidx.lifecycle.Observer { state ->
                val inputField : SwitchCompat =  bottomSheetBinding.switch5
                val inputFieldsName = "bottomSheetBinding.switchTakeAbroad"

                when(state){
                    is HomeScreenInteractionState.EditState -> {
                        if(LOG_ME)ALog.d(
                            TAG,
                            ".$methodName(): EditState: $inputFieldsName.text == ${inputField.isChecked}"
                        )
                    }

                    is HomeScreenInteractionState.DefaultState -> {
                        if(LOG_ME)ALog.d(
                            TAG, ".$methodName(): DefaultState: $inputFieldsName.text == ${inputField.isChecked}"
                        )
                        updateSwitch5InViewModel()
                    }

                    else -> {}
                }
            })
        viewModel.switch6InteractionState.observe(
            viewLifecycleOwner, androidx.lifecycle.Observer { state ->
                val inputField : SwitchCompat =  bottomSheetBinding.switch6
                val inputFieldsName = "bottomSheetBinding.switchSmoking"

                when(state){
                    is HomeScreenInteractionState.EditState -> {
                        if(LOG_ME)ALog.d(
                            TAG,
                            ".$methodName(): EditState: $inputFieldsName.text == ${inputField.isChecked}"
                        )
                    }

                    is HomeScreenInteractionState.DefaultState -> {
                        if(LOG_ME)ALog.d(
                            TAG,
                            ".$methodName(): DefaultState: $inputFieldsName.text == ${inputField.isChecked}"
                        )
                        updateSwitch6InViewModel()
                    }

                    else -> {}
                }
            })
        viewModel.switch7InteractionState.observe(
            viewLifecycleOwner, androidx.lifecycle.Observer { state ->
                val inputField : SwitchCompat =  bottomSheetBinding.switch7
                val inputFieldsName = "bottomSheetBinding.switchWheelchairAccess"

                when(state){
                    is HomeScreenInteractionState.EditState -> {
                        if(LOG_ME)ALog.d(
                            TAG,
                            ".$methodName(): EditState: $inputFieldsName.text == ${inputField.isChecked}"
                        )
                    }

                    is HomeScreenInteractionState.DefaultState -> {
                        if(LOG_ME)ALog.d(
                            TAG,
                            ".$methodName(): DefaultState: $inputFieldsName.text == ${inputField.isChecked}"
                        )
                        updateSwitch7InViewModel()
                    }

                    else -> {}
                }
            })
    }

    override fun getViewModel(): BaseViewModel<ProjectUser, HomeScreenViewState<ProjectUser>> {
        return viewModel
    }

    override fun getInitStateEvent(): StateEvent {
        val path = requireContext().filesDir
        return HomeScreenStateEvent.GetAvailableJobsAroundUserStateEvent(path)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun changeUiOrNavigateDependingOnViewState(viewState: HomeScreenViewState<ProjectUser>) {
        LOG_ME = true
        val methodName: String = "changeUiOrNavigateDependingOnViewState"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(viewState.mainEntity?.fullyVerified == true) {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): User is fully verified")
                binding.profileStatusBar.visibility = View.GONE
            } else {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): User isn't fully verified")
            }


            viewState.location?.let { location ->
                try {
                    if (LOG_ME) ALog.d(TAG, "$methodName(): Refreshing distances in items list.")
                    refreshDistancesInItemsList(location)
                } catch (e: Exception) {
                    ALog.e(TAG, methodName, e)
                }
            }

            if(viewState.refreshListOfSearchedEntities1) {
                if(LOG_ME)ALog.d(
                    TAG, ".changeUiOrNavigateDependingOnViewState(): " +
                        "List of searched items needs a refresh.")
                viewState.searchedAvailableJobs?.let { searchedItemsList ->
                    try {
                        val adapter = (recyclerView.adapter as AdapterTaskAssignmentToListItemType3)
                        adapter.setData(viewState.searchedAvailableJobs)
                        adapter.notifyDataSetChanged()
                        viewModel.setRefreshListOfSearchedEntities1(false)
                    } catch (e: Exception) {
                        ALog.e(TAG, methodName, e)
                    }
                }
            } else {
                if(LOG_ME)ALog.d(
                    TAG, ".changeUiOrNavigateDependingOnViewState(): " +
                                        "List of searched items doesn't need a refresh.")
            }

            if(viewState.range1min != null) {
                if(LOG_ME) ALog.d(TAG, "$methodName(): range1min updated - setting it in UI to ${viewState.range1min}")
                bottomSheetBinding.range1MinEdittext.setText(
                    String.format(
                        "%.0f",
                        viewState.range1min!!
                    )
                )
                bottomSheetBinding.range1RangeSlider.smartSetLeftValue(viewState.range1min!!.toFloat())
            }

            if(viewState.range1max != null) {
                if(LOG_ME) ALog.d(TAG, "$methodName(): range1max updated - setting it in UI to ${viewState.range1max}")
                bottomSheetBinding.range1MaxEdittext.setText(
                    String.format(
                        "%.0f",
                        viewState.range1max!!
                    )
                )
                bottomSheetBinding.range1RangeSlider.smartSetRightValue(viewState.range1max!!.toFloat())
            }


            if(viewState.range2min != null) {
                if(LOG_ME) ALog.d(TAG, "$methodName(): range2min - setting it in UI to ${viewState.range2min}")
                bottomSheetBinding.range2MinEdittext.setText(
                    String.format(
                        "%.0f",
                        viewState.range2min!!
                    )
                )
                bottomSheetBinding.range2RangeSlider.smartSetLeftValue(viewState.range2min!!.toFloat())
            }

            if(viewState.range2max != null) {
                if(LOG_ME) ALog.d(TAG, "$methodName(): range2max updated - setting it in UI to ${viewState.range2max}")
                bottomSheetBinding.range2MaxEdittext.setText(
                    String.format(
                        "%.0f",
                        viewState.range2max!!
                    )
                )
                bottomSheetBinding.range2RangeSlider.smartSetRightValue(viewState.range2max!!.toFloat())
            }

            if(viewState.switch1 != null)
                bottomSheetBinding.switch1.isChecked = viewState.switch1!!

            if(viewState.switch2 != null)
                bottomSheetBinding.switch2.isChecked = viewState.switch2!!

            if(viewState.switch3 != null)
                bottomSheetBinding.switch3.isChecked = viewState.switch3!!

            if(viewState.switch4 != null)
                bottomSheetBinding.switch4.isChecked = viewState.switch4!!

            if(viewState.switch5 != null)
                bottomSheetBinding.switch5.isChecked = viewState.switch5!!

            if(viewState.switch6 != null)
                bottomSheetBinding.switch6.isChecked = viewState.switch6!!

            if(viewState.switch7 != null)
                bottomSheetBinding.switch7.isChecked = viewState.switch7!!

            if(viewState.navigateToStartScreen ?: false) {
                navigateToStartScreen(this)
                viewState.navigateToStartScreen = false
            }

            LOG_ME = true
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override fun onEntityUpdateSuccess() {
        // Nothing to do
    }

    override fun onEntityDeleteSuccess() {
        // Nothing to do
    }

    override fun getStateBundleKey(): String? {
        return HomeScreen_Fragment1_STATE_BUNDLE_KEY
    }

    private fun populateInputs() {
        inputs.add(binding.searchBackButton)
        inputs.add(binding.searchQueryEdittext)
        inputs.add(binding.searchButton)
        inputs.add(binding.profileStatusCompleteProfileButton)
        inputs.add(bottomSheetBinding.searchFiltersButton)
        inputs.add(bottomSheetBinding.removeFiltersButton)
        inputs.add(bottomSheetBinding.searchOptionsBottomSheetCloseButton)
        inputs.add(bottomSheetBinding.range1MinEdittext)
        inputs.add(bottomSheetBinding.range1MaxEdittext)
        inputs.add(bottomSheetBinding.range1RangeSlider)
        inputs.add(bottomSheetBinding.range2MinEdittext)
        inputs.add(bottomSheetBinding.range2MaxEdittext)
        inputs.add(bottomSheetBinding.range2RangeSlider)
        inputs.add(bottomSheetBinding.switch1)
        inputs.add(bottomSheetBinding.switch2)
        inputs.add(bottomSheetBinding.switch3)
        inputs.add(bottomSheetBinding.switch4)
        inputs.add(bottomSheetBinding.switch5)
        inputs.add(bottomSheetBinding.switch6)
        inputs.add(bottomSheetBinding.switch7)
    }

    private fun updateSearchStringInViewModel() {
        val methodName: String = "updateSearchStringInViewModel()"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val currentViewState = viewModel.getCurrentViewStateOrNew()
            val searchStringInViewModel = currentViewState.availableJobsSearchQuery
            val searchStringInView = binding.searchQueryEdittext?.text.toString()
            // Update in view model only if values in view model and view are different
            if(searchStringInViewModel != searchStringInView) {
                if (LOG_ME) ALog.d(
                    TAG,
                    "updateSearchStringInViewModel(): Updating searchString in ViewModel from " +
                            "$searchStringInViewModel to " +
                            "$searchStringInView"
                )
                viewModel.updateInputField(searchStringInView) {
                    viewModel.getCurrentViewStateOrNew().availableJobsSearchQuery = searchStringInView
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun updateRange1MinEditTextInViewModel() {
        val methodName: String = "updateRange1MinEditTextInViewModel()"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val range1MinInViewModel = viewModel.getCurrentViewStateOrNew().range1min
            var range1MinInEditText : Double? = null
            val match = "\\d+(\\.\\d+)?".toRegex().find(bottomSheetBinding.range1MinEdittext.text)
            match?.let {
                range1MinInEditText = match.value.toDouble()
            }
            if(LOG_ME)ALog.d(
                TAG, ".$methodName(): " +
                    "range1MinInViewModel == $range1MinInViewModel    " +
                    "range1MinInEditText == $range1MinInEditText    "
            )
            // Update in view model only if values in view model and view are different
            if(range1MinInEditText != range1MinInViewModel) {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): range1Min was last edited in EditText")
                viewModel.updateInputField(range1MinInEditText.toString()) {
                    viewModel.getCurrentViewStateOrNew().range1min = range1MinInEditText
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun updateRange1MaxEditTextInViewModel() {
        val methodName: String = "updateRange1MaxEditTextInViewModel()"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val range1MaxInViewModel = viewModel.getCurrentViewStateOrNew().range1max
            var range1MaxInEditText : Double? = null
            val match = "\\d+(\\.\\d+)?".toRegex().find(bottomSheetBinding.range1MaxEdittext.text)
            match?.let {
                range1MaxInEditText = match.value.toDouble()
            }
            if(LOG_ME)ALog.d(
                TAG, ".$methodName(): " +
                    "range1MaxInViewModel == $range1MaxInViewModel    " +
                    "range1MaxInEditText == $range1MaxInEditText    "
            )
            // Update in view model only if values in view model and view are different
            if(range1MaxInEditText != range1MaxInViewModel) {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): range1Max was last edited in EditText")
                viewModel.updateInputField(range1MaxInEditText.toString()) {
                    viewModel.getCurrentViewStateOrNew().range1max = range1MaxInEditText
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun updateRange2MinEditTextInViewModel() {
        val methodName: String = "updateRange2MinEditTextInViewModel()"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val range2MinInViewModel = viewModel.getCurrentViewStateOrNew().range2min
            var range2MinInEditText : Double? = null
            val match = "\\d+(\\.\\d+)?".toRegex().find(bottomSheetBinding.range2MinEdittext.text)
            match?.let {
                range2MinInEditText = match.value.toDouble()
            }
            if(LOG_ME)ALog.d(
                TAG, ".$methodName(): " +
                    "range2MinInViewModel == $range2MinInViewModel    " +
                    "range2MinInEditText == $range2MinInEditText    "
            )
            // Update in view model only if values in view model and view are different
            if(range2MinInEditText != range2MinInViewModel) {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): range2Min was last edited in EditText")
                viewModel.updateInputField(range2MinInEditText.toString()) {
                    viewModel.getCurrentViewStateOrNew().range2min = range2MinInEditText
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun updateRange2MaxInViewModel() {
        val methodName: String = "updateRange2MaxInViewModel()"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val range2MaxInViewModel = viewModel.getCurrentViewStateOrNew().range2max
            var range2MaxInEditText : Double? = null
            val match = "\\d+(\\.\\d+)?".toRegex().find(bottomSheetBinding.range2MaxEdittext.text)
            match?.let {
                range2MaxInEditText = match.value.toDouble()
            }
            if(LOG_ME)ALog.d(
                TAG, ".$methodName(): " +
                    "range2MaxInViewModel == $range2MaxInViewModel    " +
                    "range2MaxInEditText == $range2MaxInEditText    "
            )
            // Update in view model only if values in view model and view are different
            if(range2MaxInEditText != range2MaxInViewModel) {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): range2Max was last edited in EditText")
                viewModel.updateInputField(range2MaxInEditText.toString()) {
                    viewModel.getCurrentViewStateOrNew().range2max = range2MaxInEditText
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun updateSwitch1InViewModel() {
        val methodName: String = "updateSwitch1InViewModel"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val valueInViewModel = viewModel.getCurrentViewStateOrNew().switch1
            val valueInView = bottomSheetBinding.switch1.isChecked
            if(valueInViewModel != valueInView) {
                // Update in view model only if values in view model and view are different
                viewModel.updateInputField(valueInView.toString()){
                    viewModel.getCurrentViewStateOrNew().switch1 = valueInView
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun updateSwitch2InViewModel() {
        val methodName: String = "updateSwitch2InViewModel"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val valueInViewModel = viewModel.getCurrentViewStateOrNew().switch2
            val valueInView = bottomSheetBinding.switch2.isChecked
            if(valueInViewModel != valueInView) {
                // Update in view model only if values in view model and view are different
                viewModel.updateInputField(valueInView.toString()){
                    viewModel.getCurrentViewStateOrNew().switch2 = valueInView
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun updateSwitch3InViewModel() {
        val methodName: String = "updateSwitch3InViewModel"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val valueInViewModel = viewModel.getCurrentViewStateOrNew().switch3
            val valueInView = bottomSheetBinding.switch3.isChecked
            if(valueInViewModel != valueInView) {
                // Update in view model only if values in view model and view are different
                viewModel.updateInputField(valueInView.toString()){
                    viewModel.getCurrentViewStateOrNew().switch3 = valueInView
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun updateSwitch4InViewModel() {
        val methodName: String = "updateSwitch4InViewModel"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val valueInViewModel = viewModel.getCurrentViewStateOrNew().switch4
            val valueInView = bottomSheetBinding.switch4.isChecked
            if(valueInViewModel != valueInView) {
                // Update in view model only if values in view model and view are different
                viewModel.updateInputField(valueInView.toString()){
                    viewModel.getCurrentViewStateOrNew().switch4 = valueInView
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun updateSwitch5InViewModel() {
        val methodName: String = "updateSwitch5InViewModel"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val valueInViewModel = viewModel.getCurrentViewStateOrNew().switch5
            val valueInView = bottomSheetBinding.switch5.isChecked
            if(valueInViewModel != valueInView) {
                // Update in view model only if values in view model and view are different
                viewModel.updateInputField(valueInView.toString()){
                    viewModel.getCurrentViewStateOrNew().switch5 = valueInView
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun updateSwitch6InViewModel() {
        val methodName: String = "updateSwitch6InViewModel"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val valueInViewModel = viewModel.getCurrentViewStateOrNew().switch6
            val valueInView = bottomSheetBinding.switch6.isChecked
            if(valueInViewModel != valueInView) {
                // Update in view model only if values in view model and view are different
                viewModel.updateInputField(valueInView.toString()){
                    viewModel.getCurrentViewStateOrNew().switch6 = valueInView
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun updateSwitch7InViewModel() {
        val methodName: String = "updateSwitch7InViewModel"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val valueInViewModel = viewModel.getCurrentViewStateOrNew().switch7
            val valueInView = bottomSheetBinding.switch7.isChecked
            if(valueInViewModel != valueInView) {
                // Update in view model only if values in view model and view are different
                viewModel.updateInputField(valueInView.toString()){
                    viewModel.getCurrentViewStateOrNew().switch7 = valueInView
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override fun setAllOnClickAndOnTouchListeners() {
        if(inputs.isEmpty()) {
            populateInputs()
        }
        for(view : View in inputs) {
            view.setOnClickListener(this)
        }
        for(view : View in inputs) {
            view.setOnTouchListener(this)
        }
        bottomSheetBinding.searchOptionsBottomSheetRange1Lbl.setOnTouchListener(this)
        bottomSheetBinding.searchOptionsBottomSheetRange2Lbl.setOnTouchListener(this)
        bottomSheetBinding.background.setOnTouchListener(this)
        bottomSheetBinding.inputsBackground.setOnTouchListener(this)
    }

    override fun onTouch(view: View?, motionEvent: MotionEvent?): Boolean {
        val methodName: String = "onTouch()"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")

        if(motionEvent == null && LOG_ME) {
            ALog.d(TAG, "$methodName: motionEvent == null")
            ALog.d(TAG, "$methodName: Method End")
            return false
        }
        if(view == null && LOG_ME) {
            ALog.d(TAG, "$methodName: view == null")
            ALog.d(TAG, "$methodName: Method End")
            return false
        }
        try {
            if(LOG_ME){
                ALog.d(TAG, "$methodName(): State before")
                viewModel.logInteractionState()
                logUIState()
            }
            when(view) {
                // Range sliders
                bottomSheetBinding.range1RangeSlider -> {
                    if (LOG_ME) ALog.d(
                        TAG, "$methodName: " +
                            "bottomSheetBinding.range1RangeSlider: " +
                            "motionEvent.action == ${MotionEvent.actionToString(motionEvent!!.action)}")
                    when(motionEvent!!.action) {
                        MotionEvent.ACTION_DOWN -> {
                            if(!viewModel.isEditingRange1RangeSlider()){
                                if (LOG_ME) ALog.d(
                                    TAG, "$methodName: " +
                                        "Setting EditState for bottomSheetBinding.range1RangeSlider")
                                viewModel.setRange1RangeSliderInteractionState(
                                    HomeScreenInteractionState.EditState())
                            }
                        }
                        MotionEvent.ACTION_UP -> {
                            if(viewModel.isEditingRange1RangeSlider()){
                                if (LOG_ME) ALog.d(
                                    TAG, "$methodName: " +
                                        "Setting DefaultState for bottomSheetBinding.range1RangeSlider")
                                viewModel.setRange1RangeSliderInteractionState(
                                    HomeScreenInteractionState.DefaultState())
                            }
                        }
                    }
                }

                bottomSheetBinding.range2RangeSlider -> {
                    if (LOG_ME) ALog.d(
                        TAG, "$methodName: " +
                            "bottomSheetBinding.range2RangeSlider: " +
                            "motionEvent.action == ${MotionEvent.actionToString(motionEvent!!.action)}")
                    when(motionEvent!!.action) {
                        MotionEvent.ACTION_DOWN -> {
                            if(!viewModel.isEditingRange2RangeSlider()){
                                if (LOG_ME) ALog.d(
                                    TAG, "$methodName: " +
                                        "Setting EditState for bottomSheetBinding.range2RangeSlider")
                                viewModel.setRange2RangeSliderInteractionState(
                                    HomeScreenInteractionState.EditState())
                            }
                        }
                        MotionEvent.ACTION_UP -> {
                            if(viewModel.isEditingRange2RangeSlider()){
                                if (LOG_ME) ALog.d(
                                    TAG, "$methodName: " +
                                        "Setting DefaultState for bottomSheetBinding.range2RangeSlider")
                                viewModel.setRange2RangeSliderInteractionState(
                                    HomeScreenInteractionState.DefaultState())
                            }
                        }
                    }
                }


                // Edit texts
                binding.searchQueryEdittext -> {
                    if (LOG_ME) ALog.d(
                        TAG, "$methodName: " +
                            "binding.searchQueryEdittext: motionEvent.action == " +
                            "${MotionEvent.actionToString(motionEvent!!.action)}")
                    when(motionEvent!!.action) {
                        MotionEvent.ACTION_DOWN -> {
                            if(!viewModel.isEditingSearchQuery()){
                                if (LOG_ME) ALog.d(
                                    TAG, "$methodName: " +
                                        "Setting EditState for bottomSheetBinding.searchQueryEdittext"
                                )
                                viewModel.setSearchQueryInteractionState(
                                    HomeScreenInteractionState.EditState()
                                )
                            }
                        }
                    }
                }

                bottomSheetBinding.range1MinEdittext -> {
                    if (LOG_ME) ALog.d(
                        TAG, "$methodName: " +
                            "bottomSheetBinding.range1MinEdittext: " +
                            "motionEvent.action == ${MotionEvent.actionToString(motionEvent!!.action)}")
                    when(motionEvent!!.action) {
                        MotionEvent.ACTION_DOWN -> {
                            if (!viewModel.isEditingRange1Min()) {
                                if (LOG_ME) ALog.d(
                                    TAG,"$methodName: " +
                                        "Setting EditState for bottomSheetBinding.range1MinEdittext"
                                )
                                viewModel.setRange1MinInteractionState(
                                    HomeScreenInteractionState.EditState()
                                )
                            }
                        }
                    }
                }

                bottomSheetBinding.range1MaxEdittext -> {
                    if (LOG_ME) ALog.d(
                        TAG, "$methodName: " +
                            "bottomSheetBinding.range1MaxEdittext: motionEvent.action == " +
                            "${MotionEvent.actionToString(motionEvent!!.action)}")
                    when(motionEvent!!.action) {
                        MotionEvent.ACTION_DOWN -> {
                            if (!viewModel.isEditingRange1Max()) {
                                if (LOG_ME) ALog.d(
                                    TAG,"$methodName: " +
                                        "Setting EditState for bottomSheetBinding.range1MaxEdittext"
                                )
                                viewModel.setRange1MaxInteractionState(
                                    HomeScreenInteractionState.EditState()
                                )
                            }
                        }
                    }
                }

                bottomSheetBinding.range2MinEdittext -> {
                    if (LOG_ME) ALog.d(
                        TAG, "$methodName: " +
                            "bottomSheetBinding.range2MinEdittext: motionEvent.action == " +
                            "${MotionEvent.actionToString(motionEvent!!.action)}")
                    when(motionEvent!!.action) {
                        MotionEvent.ACTION_DOWN -> {
                            if (!viewModel.isEditingRange2Min()) {
                                if (LOG_ME) ALog.d(
                                    TAG,"$methodName: " +
                                        "Setting EditState for bottomSheetBinding.range2MinEdittext"
                                )
                                viewModel.setRange2MinInteractionState(
                                    HomeScreenInteractionState.EditState()
                                )
                            }
                        }
                    }
                }

                bottomSheetBinding.range2MaxEdittext -> {
                    if (LOG_ME) ALog.d(
                        TAG, "$methodName: " +
                            "bottomSheetBinding.range2MaxEdittext: motionEvent.action == " +
                            "${MotionEvent.actionToString(motionEvent!!.action)}")
                    when(motionEvent!!.action) {
                        MotionEvent.ACTION_DOWN -> {
                            if (!viewModel.isEditingRange2Max()) {
                                if (LOG_ME) ALog.d(
                                    TAG,"$methodName: " +
                                        "Setting EditState for bottomSheetBinding.range2MaxEdittext"
                                )
                                viewModel.setRange2MaxInteractionState(
                                    HomeScreenInteractionState.EditState()
                                )
                            }
                        }
                    }
                }

                // Switches
                bottomSheetBinding.switch1 -> {
                    if (LOG_ME) ALog.d(
                        TAG, "$methodName: " +
                            "bottomSheetBinding.switch1: motionEvent.action == " +
                            "${MotionEvent.actionToString(motionEvent!!.action)}")
                    when(motionEvent!!.action) {
                        MotionEvent.ACTION_DOWN -> {
                            if (!viewModel.isEditingSwitch1()) {
                                if (LOG_ME) ALog.d(
                                    TAG, "$methodName: " +
                                        "Setting EditState for bottomSheetBinding.switch1"
                                )
                                viewModel.setSwitch1InteractionState(
                                    HomeScreenInteractionState.EditState()
                                )
                            }
                        }
                    }
                }

                bottomSheetBinding.switch2 -> {
                    if (LOG_ME) ALog.d(
                        TAG, "$methodName: " +
                            "bottomSheetBinding.switch2: motionEvent.action == " +
                            "${MotionEvent.actionToString(motionEvent!!.action)}")
                    when(motionEvent!!.action) {
                        MotionEvent.ACTION_DOWN -> {
                            if (!viewModel.isEditingSwitch2()) {
                                if (LOG_ME) ALog.d(
                                    TAG, "$methodName: " +
                                        "Setting EditState for bottomSheetBinding.switch2"
                                )
                                viewModel.setSwitch2InteractionState(
                                    HomeScreenInteractionState.EditState()
                                )
                            }
                        }
                    }
                }

                bottomSheetBinding.switch3 -> {
                    if (LOG_ME) ALog.d(
                        TAG, "$methodName: " +
                            "bottomSheetBinding.switch3: motionEvent.action == " +
                            "${MotionEvent.actionToString(motionEvent!!.action)}")
                    when(motionEvent!!.action) {
                        MotionEvent.ACTION_DOWN -> {
                            if (!viewModel.isEditingSwitch3()) {
                                if (LOG_ME) ALog.d(
                                    TAG, "$methodName: " +
                                        "Setting EditState for bottomSheetBinding.switch3"
                                )
                                viewModel.setSwitch3InteractionState(
                                    HomeScreenInteractionState.EditState()
                                )
                            }
                        }
                    }
                }

                bottomSheetBinding.switch4 -> {
                    if (LOG_ME) ALog.d(
                        TAG, "$methodName: " +
                            "bottomSheetBinding.switch4: motionEvent.action == " +
                            "${MotionEvent.actionToString(motionEvent!!.action)}")
                    when(motionEvent!!.action) {
                        MotionEvent.ACTION_DOWN -> {
                            if (!viewModel.isEditingSwitch4()) {
                                if (LOG_ME) ALog.d(
                                    TAG,
                                    "$methodName: Setting EditState for bottomSheetBinding.switch4"
                                )
                                viewModel.setSwitch4InteractionState(
                                    HomeScreenInteractionState.EditState()
                                )
                            }
                        }
                    }
                }

                bottomSheetBinding.switch5 -> {
                    if (LOG_ME) ALog.d(
                        TAG, "$methodName: " +
                            "bottomSheetBinding.switch5: motionEvent.action == " +
                            "${MotionEvent.actionToString(motionEvent!!.action)}")
                    when(motionEvent!!.action) {
                        MotionEvent.ACTION_DOWN -> {
                            if (!viewModel.isEditingSwitch5()) {
                                if (LOG_ME) ALog.d(
                                    TAG,"$methodName: " +
                                        "Setting EditState for bottomSheetBinding.switch5"
                                )
                                viewModel.setSwitch5InteractionState(
                                    HomeScreenInteractionState.EditState()
                                )
                            }
                        }
                    }
                }

                bottomSheetBinding.switch6 -> {
                    if (LOG_ME) ALog.d(
                        TAG, "$methodName: " +
                            "bottomSheetBinding.switch6: motionEvent.action == " +
                            "${MotionEvent.actionToString(motionEvent!!.action)}")
                    when(motionEvent!!.action) {
                        MotionEvent.ACTION_DOWN -> {
                            if (!viewModel.isEditingSwitch6()) {
                                if (LOG_ME) ALog.d(
                                    TAG, "$methodName: " +
                                        "Setting EditState for bottomSheetBinding.switch6"
                                )
                                viewModel.setSwitch6InteractionState(
                                    HomeScreenInteractionState.EditState()
                                )
                            }
                        }
                    }
                }

                bottomSheetBinding.switch7 -> {
                    if (LOG_ME) ALog.d(
                        TAG, "$methodName: " +
                            "bottomSheetBinding.switch7: motionEvent.action == " +
                            "${MotionEvent.actionToString(motionEvent!!.action)}")
                    when(motionEvent!!.action) {
                        MotionEvent.ACTION_DOWN -> {
                            if (!viewModel.isEditingSwitch7()) {
                                if (LOG_ME) ALog.d(
                                    TAG, "$methodName: " +
                                        "Setting EditState for bottomSheetBinding.switch7"
                                )
                                viewModel.setSwitch7InteractionState(
                                    HomeScreenInteractionState.EditState()
                                )
                            }
                        }
                    }
                }

                //Buttons
                binding.searchBackButton -> {
                    if (LOG_ME) ALog.d(
                        TAG, "$methodName: " +
                            "binding.searchBackButton: motionEvent.action == " +
                            "${MotionEvent.actionToString(motionEvent!!.action)}")
                    when(motionEvent!!.action) {
                        MotionEvent.ACTION_DOWN -> {
                            if(LOG_ME)ALog.d(TAG, ".$methodName(): Exiting EditState")
                            viewModel.exitEditState()
                            if(LOG_ME)ALog.d(
                                TAG, ".$methodName(): " +
                                    "Clearing search query edit text.")
                            binding.searchQueryEdittext.text.clear()
                        }
                    }
                }
                binding.searchButton -> {
                    if (LOG_ME) ALog.d(
                        TAG, "$methodName: " +
                            "binding.searchButton: motionEvent.action == " +
                            "${MotionEvent.actionToString(motionEvent!!.action)}")
                    when(motionEvent!!.action) {
                        MotionEvent.ACTION_DOWN -> {
                            if(LOG_ME)ALog.d(TAG, ".$methodName(): Exiting EditState")
                            viewModel.exitEditState()
                            if(LOG_ME)ALog.d(
                                TAG, ".$methodName(): " +
                                    "Setting state event to search entities1.")
                            getViewModel().setStateEvent(HomeScreenStateEvent.SearchAvailableJobsStateEvent)
                            if(LOG_ME)ALog.d(TAG, ".$methodName(): Hiding keyboard.")
                            hideKeyboard()
                            if(LOG_ME)ALog.d(
                                TAG, ".$methodName(): " +
                                    "Collapsing bottom sheet view.")
                            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                        }
                    }
                }
                binding.profileStatusCompleteProfileButton -> {
                    if (LOG_ME) ALog.d(
                        TAG, "$methodName: " +
                            "binding.profileStatusCompleteProfileButton: motionEvent.action == " +
                            "${MotionEvent.actionToString(motionEvent!!.action)}")
                    when(motionEvent!!.action) {
                        MotionEvent.ACTION_UP -> {
                            if(LOG_ME)ALog.d(TAG, ".$methodName(): Exiting EditState")
                            viewModel.exitEditState()
                            if(LOG_ME)ALog.d(
                                TAG, ".$methodName(): " +
                                    "Navigating to profile screen.")
                            navigateToProfileScreen()
                        }
                    }
                }
                bottomSheetBinding.searchFiltersButton -> {
                    if (LOG_ME) ALog.d(
                        TAG, "$methodName: " +
                            "binding.searchFiltersButton: motionEvent.action == " +
                            "${MotionEvent.actionToString(motionEvent!!.action)}")
                    when(motionEvent!!.action) {
                        MotionEvent.ACTION_UP -> {
                            if(LOG_ME)ALog.d(TAG, ".$methodName(): Exiting EditState")
                            viewModel.exitEditState()
                            if(LOG_ME)ALog.d(
                                TAG, ".$methodName(): " +
                                    "Expanding or collapsing bottom sheet.")
                            this.expandOrCollapseBottomSheet()
                        }
                    }
                }
                bottomSheetBinding.removeFiltersButton -> {
                    if (LOG_ME) ALog.d(
                        TAG, "$methodName: " +
                            "binding.removeFiltersButton: motionEvent.action == " +
                            "${MotionEvent.actionToString(motionEvent!!.action)}")
                    when(motionEvent!!.action) {
                        MotionEvent.ACTION_UP -> {
                            if(LOG_ME)ALog.d(TAG, ".$methodName(): Exiting EditState")
                            viewModel.exitEditState()
                            if(LOG_ME)ALog.d(TAG, ".$methodName(): Entering InitialState")
                            viewModel.setInitialState()
                            if(LOG_ME)ALog.d(
                                TAG, ".$methodName(): " +
                                    "Setting state event to clear filters")
                            getViewModel().setStateEvent(HomeScreenStateEvent.ClearFiltersStateEvent)
                            if(LOG_ME)ALog.d(TAG, ".$methodName(): Clearing search filters in UI.")
                            clearEntities1SearchFiltersInUI()
                            if(LOG_ME)ALog.d(TAG, ".$methodName(): Collapsing bottom sheet")
                            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                        }
                    }
                }
                bottomSheetBinding.searchOptionsBottomSheetCloseButton -> {
                    if (LOG_ME) ALog.d(
                        TAG, "$methodName: " +
                            "binding.searchOptionsBottomSheetCloseButton: motionEvent.action == " +
                            "${MotionEvent.actionToString(motionEvent!!.action)}")
                    when(motionEvent!!.action) {
                        MotionEvent.ACTION_DOWN -> {
                            if(LOG_ME)ALog.d(TAG, ".$methodName(): Exiting EditState")
                            viewModel.exitEditState()
                            if(LOG_ME)ALog.d(TAG, ".$methodName(): Collapsing bottom sheet")
                            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                        }
                    }
                }

                else -> {
                    if (LOG_ME) ALog.d(
                        TAG, "$methodName: else: " +
                            "motionEvent.action == ${MotionEvent.actionToString(motionEvent!!.action)}    " +
                            "view == ${view?.javaClass.toString()}")
                    when(motionEvent!!.action) {
                        MotionEvent.ACTION_DOWN -> {
                            if(LOG_ME)ALog.d(TAG, ".$methodName(): Exiting EditState")
                            viewModel.exitEditState()
                        }
                    }
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if(LOG_ME){
                ALog.d(TAG, "$methodName(): State after")
                viewModel.logInteractionState()
                logUIState()
            }
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
        return false
    }

    override fun onClick(v: View?) {
        super.onClick(v)
    }

    private fun expandOrCollapseBottomSheet() {
        if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
            if(LOG_ME)ALog.d(
                TAG, ".expandOrCollapseBottomSheet(): " +
                    "Expanding bottom sheet dialog.")
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        } else {
            if(LOG_ME)ALog.d(
                TAG, ".expandOrCollapseBottomSheet(): " +
                    "Collapsing bottom sheet dialog.")
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        }
    }

    private fun navigateToProfileScreen() {
        findNavController().navigate(
            R.id.action_ActivityHomeScreenFragment1_to_ActivityHomeScreenFragment5
        )
    }

    override fun updateUIInViewModel() {
        if(viewModel.checkEditState()){
            if (LOG_ME) ALog.d(TAG, "updateUIInViewModel():  Users interface is in EditState")
            view?.hideKeyboard()
            viewModel.exitEditState()
        }
    }

    private fun updateMainEntityInViewModel() {
        if(viewModel.getIsUpdatePending()){
            viewModel.getCurrentViewStateOrNew().mainEntity?.let {
                HomeScreenStateEvent.UpdateMainEntityEvent(
                    it
                )
            }?.let {
                if (LOG_ME) ALog.d(TAG, "updateMainEntityInViewModel(): Sending UpdateMainEntityEvent")
                viewModel.setStateEvent(
                    it
                )
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun refreshDistancesInItemsList(location: DeviceLocation) {
        val methodName: String = "refreshDistancesInItemsList"
        try {
            (recyclerView.adapter as AdapterTaskAssignmentToListItemType3).deviceLocation = location
            recyclerView.adapter?.notifyDataSetChanged()
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return
        } finally {
        }

    }



    // ========================================== Location tracking ================================
    private fun checkLocationPermission() {
        val methodName: String = "checkLocationPermission"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if (ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                if (LOG_ME) ALog.d(TAG, "$methodName(): App does not have location permission.")
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        requireActivity(),
                        Manifest.permission.ACCESS_FINE_LOCATION
                    )
                ) {
                    if (LOG_ME) ALog.d(TAG, "$methodName(): Permission rationale is necessary.")
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                    AlertDialog.Builder(requireContext())
                        .setTitle(requireActivity().getString(R.string.location_permission_rationale_title))
                        .setMessage(requireActivity().getString(R.string.location_permission_rationale))
                        .setPositiveButton(
                            requireActivity().getString(R.string.ok)
                        ) { _, _ ->
                            //Prompt the user once explanation has been shown
                            requestLocationPermission()
                        }
                        .create()
                        .show()
                } else {
                    if (LOG_ME) ALog.d(
                        TAG, "$methodName(): " +
                            "Permission rationale is unnecessary. Requesting location permission.")
                    // No explanation needed, we can request the permission.
                    requestLocationPermission()
                }
            } else if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
            ){
                if (LOG_ME) ALog.d(
                    TAG, "$methodName(): " +
                        "App has location permission. Requesting location updates.")
                requestLocationUpdates()
            } else {
                if (LOG_ME) ALog.d(TAG, "$methodName(): ACCESS_FINE_LOCATION wasn't found")
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun requestLocationPermission() {
        val methodName: String = "requestLocationPermission"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                if (LOG_ME) ALog.d(
                    TAG, "$methodName(): " +
                        "Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q")
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_BACKGROUND_LOCATION
                    ),
                    MY_PERMISSIONS_REQUEST_LOCATION
                )
            } else {
                if (LOG_ME) ALog.d(
                    TAG, "$methodName(): " +
                        "Build.VERSION.SDK_INT < Build.VERSION_CODES.Q")
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    MY_PERMISSIONS_REQUEST_LOCATION
                )
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        val methodName: String = "onRequestPermissionsResult"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            when (requestCode) {
                MY_PERMISSIONS_REQUEST_LOCATION -> {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        if (ContextCompat.checkSelfPermission(
                                requireContext(),
                                Manifest.permission.ACCESS_FINE_LOCATION
                            ) == PackageManager.PERMISSION_GRANTED
                        ) {
                            if (LOG_ME) ALog.d(
                                TAG, "$methodName(): " +
                                    "Location permission was granted by user.")
                            requestLocationUpdates()
                        }
                    } else {
                        if (LOG_ME) ALog.d(
                            TAG, "$methodName(): " +
                                "Location permission was denied by user.")
                        Toast.makeText(
                            requireContext(),
                            requireContext().getString(R.string.location_permission_denied),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    return
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun requestLocationUpdates() {
        val methodName = "requestLocationUpdates"
        applicationViewModel.getLocationLiveData().observe(this, Observer {
            try {
                if(LOG_ME)ALog.d(
                    TAG, ".$methodName(): " +
                        "Updating location in viewModel with data from applicationViewModel: " +
                        "deviceLocation == $it")
                viewModel.setLocation(it)
            } catch (e: Exception) {
                ALog.e(TAG, methodName, e)
            }
        })
    }

    private fun makeBottomSheetBehaveItself() {
        val methodName: String = "makeBottomSheetBehaveItself()"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val bottomSheet: androidx.coordinatorlayout.widget.CoordinatorLayout =
                requireActivity().findViewById(R.id.item_search_options_bottom_sheet_persistent)
            bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)

            bottomSheetBehavior.addBottomSheetCallback(object: BottomSheetBehavior.BottomSheetCallback(){
                override fun onStateChanged(bottomSheet: View, state: Int) {
                    print(state)
                    when (state) {

                        BottomSheetBehavior.STATE_HIDDEN -> {
                            if (LOG_ME) ALog.d(TAG, "$methodName: STATE_HIDDEN")
                            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                        }
                        BottomSheetBehavior.STATE_EXPANDED -> {
                            if (LOG_ME) ALog.d(TAG, "$methodName: STATE_EXPANDED")
                        }
                        BottomSheetBehavior.STATE_COLLAPSED -> {
                            if (LOG_ME) ALog.d(TAG, "$methodName: STATE_COLLAPSED")
                        }
                        BottomSheetBehavior.STATE_DRAGGING -> {
                            if (LOG_ME) ALog.d(TAG, "$methodName: STATE_DRAGGING")
                        }
                        BottomSheetBehavior.STATE_SETTLING -> {
                            if (LOG_ME) ALog.d(TAG, "$methodName: STATE_SETTLING")
                        }
                        BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                        }

                    }
                }
                override fun onSlide(bottomSheet: View, slideOffset: Float) {
                }
            })
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun logUIState() {
        inputs.add(binding.searchQueryEdittext)
        inputs.add(binding.searchButton)
//        inputs.add(binding.searchSortingButton)
        inputs.add(binding.profileStatusCompleteProfileButton)
        inputs.add(bottomSheetBinding.searchFiltersButton)
        inputs.add(bottomSheetBinding.removeFiltersButton)
        inputs.add(bottomSheetBinding.searchOptionsBottomSheetCloseButton)
        inputs.add(bottomSheetBinding.range1MinEdittext)
        inputs.add(bottomSheetBinding.range1MaxEdittext)
        inputs.add(bottomSheetBinding.range1RangeSlider)
        inputs.add(bottomSheetBinding.range2MinEdittext)
        inputs.add(bottomSheetBinding.range2MaxEdittext)
        inputs.add(bottomSheetBinding.range2RangeSlider)
        inputs.add(bottomSheetBinding.switch1)
        inputs.add(bottomSheetBinding.switch2)
        inputs.add(bottomSheetBinding.switch3)
        inputs.add(bottomSheetBinding.switch4)
        inputs.add(bottomSheetBinding.switch5)
        inputs.add(bottomSheetBinding.switch6)
        inputs.add(bottomSheetBinding.switch7)
        
        ALog.d(
            TAG, ".logUIState(): \n " +
                "searchQueryEdittext == ${binding.searchQueryEdittext.text} \n " +
                "range1MinEdittext == ${bottomSheetBinding.range1MinEdittext.text} \n " +
                "range1MaxEdittext == ${bottomSheetBinding.range1MaxEdittext.text} \n " +
                "range1RangeSlider.values[0] == ${bottomSheetBinding.range1RangeSlider.values[0]} \n " +
                "range1RangeSlider.values[1] == ${bottomSheetBinding.range1RangeSlider.values[1]} \n " +
                "range2MinEdittext == ${bottomSheetBinding.range2MinEdittext.text} \n " +
                "range2MaxEdittext == ${bottomSheetBinding.range2MaxEdittext.text} \n " +
                "range2RangeSlider.values[0] == ${bottomSheetBinding.range2RangeSlider.values[0]} \n " +
                "range2RangeSlider.values[1] == ${bottomSheetBinding.range2RangeSlider.values[1]} \n " +
                "switch1 == ${bottomSheetBinding.switch1.isChecked} \n " +
                "switch2 == ${bottomSheetBinding.switch2.isChecked} \n " +
                "switch3 == ${bottomSheetBinding.switch3.isChecked} \n " +
                "switch4 == ${bottomSheetBinding.switch4.isChecked} \n " +
                "switch5 == ${bottomSheetBinding.switch5.isChecked} \n " +
                "switch6 == ${bottomSheetBinding.switch6.isChecked} \n " +
                "switch7 == ${bottomSheetBinding.switch7.isChecked} \n "
        )
    }

    private fun clearEntities1SearchFiltersInUI() {
        val methodName: String = "clearEntities1SearchFiltersInUI"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val valueFromRange1RangeSlider = bottomSheetBinding.range1RangeSlider.valueFrom
            bottomSheetBinding.range1MinEdittext.setText(
                String.format(
                    "%.0f",
                    valueFromRange1RangeSlider
                )
            )
            bottomSheetBinding.range1RangeSlider.smartSetLeftValue(valueFromRange1RangeSlider)

            val valueToOfRange1RangeSlider = bottomSheetBinding.range1RangeSlider.valueTo
            bottomSheetBinding.range1MaxEdittext.setText(
                String.format(
                    "%.0f",
                    valueToOfRange1RangeSlider
                )
            )
            bottomSheetBinding.range1RangeSlider.smartSetRightValue(valueToOfRange1RangeSlider)

            val valueFromOfRange2RangeSlider = bottomSheetBinding.range2RangeSlider.valueFrom
            bottomSheetBinding.range2MinEdittext.setText(
                String.format(
                    "%.0f",
                    valueFromOfRange2RangeSlider
                )
            )
            bottomSheetBinding.range2RangeSlider.smartSetLeftValue(valueFromOfRange2RangeSlider)

            val valueToOfRange2RangeSlider = bottomSheetBinding.range2RangeSlider.valueTo
            bottomSheetBinding.range2MaxEdittext.setText(
                String.format(
                    "%.0f",
                    valueToOfRange2RangeSlider
                )
            )
            bottomSheetBinding.range2RangeSlider.smartSetRightValue(valueToOfRange2RangeSlider)

            bottomSheetBinding.switch1.isChecked = false
            bottomSheetBinding.switch2.isChecked = false
            bottomSheetBinding.switch3.isChecked = false
            bottomSheetBinding.switch4.isChecked = false
            bottomSheetBinding.switch5.isChecked = false
            bottomSheetBinding.switch6.isChecked = false
            bottomSheetBinding.switch7.isChecked = false
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun showSortingOptions() {
        // TODO Implement functionality
    }

    companion object CREATOR : Parcelable.Creator<ActivityHomeScreenFragment2> {
        private const val MY_PERMISSIONS_REQUEST_LOCATION = 99

        override fun createFromParcel(parcel: Parcel): ActivityHomeScreenFragment2 {
            return ActivityHomeScreenFragment2(parcel)
        }

        override fun newArray(size: Int): Array<ActivityHomeScreenFragment2?> {
            return arrayOfNulls(size)
        }
    }
}