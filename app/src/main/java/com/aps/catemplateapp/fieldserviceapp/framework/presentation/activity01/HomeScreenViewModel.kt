package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01

import android.content.Context
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.common.framework.presentation.BaseViewModel
import com.aps.catemplateapp.common.framework.presentation.views.SnackBarState
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.DeviceLocation
import com.aps.catemplateapp.common.util.extensions.convertToString
import com.aps.catemplateapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.currencies.ExchangeRates
import com.aps.catemplateapp.core.business.domain.model.entities.*
import com.aps.catemplateapp.core.business.domain.model.factories.UserFactory
import com.aps.catemplateapp.core.util.GooglePayPaymentsUtil
import com.aps.catemplateapp.core.util.ProjectConstants
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.TaskAssignment
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity2
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity3
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity4
import com.aps.catemplateapp.fieldserviceapp.business.interactors.HomeScreenInteractors
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.HomeScreenDestination
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.*
import com.google.android.gms.tasks.Task
import com.google.android.gms.wallet.PaymentData
import com.google.android.gms.wallet.PaymentDataRequest
import com.google.android.gms.wallet.PaymentsClient
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import java.io.File
import java.util.*
import javax.inject.Inject

private const val TAG = "HomeScreenViewModel"
private const val LOG_ME = true
private const val SEARCHED_ENTITIES1_LIST_FILE_NAME = "se1l"

@ExperimentalCoroutinesApi
@FlowPreview
@HiltViewModel
class HomeScreenViewModel
@Inject
constructor(
    private val interactors: HomeScreenInteractors<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            HomeScreenViewState<ProjectUser>
            >,
    private val dateUtil: DateUtil,
    private val entityFactory: UserFactory,
): BaseViewModel<ProjectUser, HomeScreenViewState<ProjectUser>>() {
    private val interactionManager: HomeScreenInteractionManager =
        HomeScreenInteractionManager()

    // Fragment 1 interaction states
    val searchQueryEdittextInteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.searchQueryEdittextInteractionState
    val range1ValueMinInteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.searchOptionsBottomSheetRange1MinInteractionState
    val range1ValueMaxInteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.searchOptionsBottomSheetRange1MaxInteractionState
    val range1SeekbarInteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.range1SeekbarInteractionState
    val searchOptionsBottomSheetPricePerKmOverLimitValueMinInteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.searchOptionsBottomSheetRange2MinInteractionState
    val pricePerKmOverLimitValueMaxInteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.searchOptionsBottomSheetRange2MaxInteractionState
    val range2SeekbarInteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.range2SeekbarInteractionState
    val switch1InteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.switch1InteractionState
    val switch2InteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.switch2InteractionState
    val switch3InteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.switch3InteractionState
    val switch4InteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.switch4InteractionState
    val switch5InteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.switch5InteractionState
    val switch6InteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.switch6InteractionState
    val switch7InteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.switch7InteractionState

    // Fragment5 (user's profile) interaction states
    val usersNameEdittextInteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.usersNameEdittextInteractionState
    val usersCityEdittextInteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.usersCityEdittextInteractionState
    val usersDescriptionEdittextInteractionState: LiveData<HomeScreenInteractionState>
        get() = interactionManager.usersDescriptionEdittextInteractionState


    private val collapsingToolbarState: LiveData<CollapsingToolbarState>
        get() = interactionManager.collapsingToolbarState

    private var googlePaymentsClient: PaymentsClient? = null
    fun getGooglePaymentsClient(): PaymentsClient? = googlePaymentsClient


    override fun handleNewData(data: HomeScreenViewState<ProjectUser>) {
        val methodName: String = "handleNewData"
        data.let { viewState ->
            viewState.location?.let { location ->
                if(LOG_ME) ALog.d(TAG, "$methodName(): Handling viewState.location")
                setLocation(location)
            }

            viewState.mainEntity?.let { mainEntity ->
                if(LOG_ME)ALog.d(TAG, "$methodName(): Handling viewState.mainEntity")
                setMainEntity(mainEntity)
            }

            viewState.searchedAvailableJobs?.let { searchedEntitiesList ->
                if(LOG_ME)ALog.d(TAG, "$methodName(): Handling viewState.searchedEntities1List")
                setSearchedEntities1List(searchedEntitiesList)

                if(LOG_ME) {
                    searchedEntitiesList.let {
                        ALog.d(TAG, "handleNewData(): searchedEntitiesList != null    ${searchedEntitiesList!!.size}")
                        for((index, entity) in searchedEntitiesList!!) {
                            ALog.d(TAG, ".handleNewData(): Entity #$index: ${entity.toString()}")
                        }
                    }
                }
            }

            viewState.userTakenJobs?.let { list ->
                if(LOG_ME)ALog.d(TAG, "$methodName(): Handling viewState.entities2List")
                setUserTakenJobsList(list)
            }

            viewState.userDoneJobs?.let { list ->
                if(LOG_ME)ALog.d(
                    TAG, "$methodName(): " +
                        "Handling viewState.entities3List")
                setUserDoneJobsList(list)
            }

            viewState.entities4List?.let { list ->
                if(LOG_ME)ALog.d(TAG, "$methodName(): Handling viewState.entities4List")
                setEntities4List(list)
            }

            viewState.listOfEntities?.let { listOfEntities ->
                if(LOG_ME)ALog.d(TAG, "$methodName(): Handling viewState.listOfEntities")
                setListOfEntities(listOfEntities)
            }

            viewState.noCachedEntity?.let { noCachedEntity ->
                if(LOG_ME)ALog.d(TAG, "$methodName(): Handling viewState.noCachedEntity")
                setNoCachedEntity(noCachedEntity)
            }

        }
    }

    private fun searchedEntities1ListIsFresh(internalDirectory: File): Boolean {
        if (LOG_ME) ALog.d(TAG, "Method start: searchedEntities1ListIsFresh()")
        try {
            internalDirectory.mkdirs()
            val searchedEntities1ListFile = File(internalDirectory, SEARCHED_ENTITIES1_LIST_FILE_NAME)
            if(LOG_ME)ALog.d(
                TAG, ".searchedEntities1ListIsFresh(): " +
                        "searchedEntities1ListFile.path == ${searchedEntities1ListFile.path}")
            if(searchedEntities1ListFile == null) {
                if(LOG_ME)ALog.w(
                    TAG, ".searchedEntities1ListIsFresh(): " +
                            "searchedEntities1ListFile == null")
                return false
            }
            if(!searchedEntities1ListFile.exists()) {
                if(LOG_ME)ALog.d(
                    TAG, ".searchedEntities1ListIsFresh(): " +
                            "searchedEntities1ListFile does not exist.")
                return false
            }
            val currentViewState = getCurrentViewStateOrNew()
            if(currentViewState.searchedEntities1ListRefreshDate == null) {
                if(LOG_ME)ALog.d(
                    TAG, ".searchedEntities1ListIsFresh(): " +
                            "searchedEntities1ListRefreshDate is null")
                return false
            }
            val freshDate = Date()
            freshDate.time -= SEARCHED_ENTITIES1_FRESHNESS_INTERVAL
            if(currentViewState.searchedEntities1ListRefreshDate!!.before(freshDate)) {
                if(LOG_ME)ALog.d(
                    TAG, ".searchedEntities1ListIsFresh(): " +
                            "searchedEntities1ListRefreshDate is before freshDate")
                return false
            }
            return true
        } catch (e: Exception) {
            ALog.e(TAG, "searchedEntities1ListIsFresh()", e)
            return false
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: searchedEntities1ListIsFresh()")
        }
    }

    override fun setStateEvent(stateEvent: StateEvent) {
        val methodName: String = "setStateEvent()"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        var launchJob = true
        val job: Flow<DataState<HomeScreenViewState<ProjectUser>>?> = when(stateEvent){

            is HomeScreenStateEvent.ActOnEntity1 -> {
                val currentViewState = getCurrentViewStateOrNew()
                if(currentViewState.mainEntity?.fullyVerified == true) {
                    // TODO: Unavailable in demo project.
                    if(LOG_ME)ALog.w(TAG, ".$methodName(): " +
                                            "Implement ActOnEntity1 functionality to do sth with stateEvent.entity1.")
                    emitDoNothingStateMessageEvent(stateEvent)
                } else {
                    emitOkDialogStateMessageEvent(
                        stateEvent,
                        R.string.profile_incomplete,
                        R.string.only_fully_verified_users_can_act_on_entity1,
                        object: OkButtonCallback {
                            override fun proceed() {
                                stateEvent.callbackOnProfileIncomplete()
                            }
                        }
                    )
                }
            }

            is HomeScreenStateEvent.GetAvailableJobsAroundUserStateEvent -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: GetEntities1AroundUserStateEvent")
                if(searchedEntities1ListIsFresh(stateEvent.internalDirectory)) {
                    if(LOG_ME)ALog.d(
                        TAG, ".setStateEvent(): " +
                                            "searchedEntities1List is fresh, restoring it from file")
                    restoreSearchedEntities1ListFromFile(stateEvent.internalDirectory)
                    setRefreshListOfSearchedEntities1(true)
                    emitDoNothingStateMessageEvent(stateEvent)
                } else {
                    interactors.taskAssignmentsAroundUser.getTaskAssignmentsAroundUser(
                        location = getCurrentViewStateOrNew().location,
                        stateEvent = stateEvent,
                        returnViewState = getCurrentViewStateOrNew(),
                        updateReturnViewState = {
                                viewState, foundEntities1 -> viewState.apply {
                            searchedAvailableJobs = ArrayList(foundEntities1)
                                    searchedEntities1ListRefreshDate = Date()
                                    saveSearchedEntities1ListInFile(stateEvent.internalDirectory)
                                    if(LOG_ME)ALog.d(
                                        TAG, ".setStateEvent(): " +
                                            "Refreshing searched entities1 list after GetEntities1AroundUserStateEvent")
                                    postRefreshListOfSearchedEntites1(true)

                                    if(LOG_ME) {
                                        if(searchedAvailableJobs != null) {
                                            ALog.d(
                                                TAG, "setStateEvent(): searchedEntities1List != null    " +
                                                    "${searchedAvailableJobs!!.size}")
                                            for((index, entity) in searchedAvailableJobs!!) {
                                                ALog.d(TAG, ".setStateEvent(): Entity #$index: $entity")
                                            }
                                        } else {
                                            if(LOG_ME)ALog.w(
                                                TAG, ".setStateEvent(): " +
                                                                    "searchedEntities1List == null")
                                        }
                                    }
                                }
                        }
                    )
                }
            }

            is HomeScreenStateEvent.SaveInFileAndClearSearchedEntities1ListStateEvent -> {
                if (LOG_ME) {
                    ALog.d(TAG, "setStateEvent: SaveInFileAndClearSearchedEntities1ListStateEvent")
                }
                saveSearchedEntities1ListInFile(stateEvent.internalDirectory)
                emitDoNothingStateMessageEvent(stateEvent)
            }

            is HomeScreenStateEvent.SearchAvailableJobsStateEvent -> {
                val currentViewState = getCurrentViewStateOrNew()
                if (LOG_ME) {
                    ALog.d(
                        TAG, ".$methodName(): SearchEntities1StateEvent\n" +
                            "currentViewState:\n${currentViewState}\n")
                }
                interactors.searchTaskAssignments.searchTaskAssignments(
                    location = getCurrentViewStateOrNew().location,
                    searchParameters = getCurrentViewStateOrNew(),
                    stateEvent = stateEvent,
                    returnViewState = currentViewState,
                    updateReturnViewState = {
                            viewState, foundEntites1 -> viewState.apply {
                                searchedAvailableJobs = ArrayList(foundEntites1)
                                if(LOG_ME)ALog.d(
                                    TAG, ".setStateEvent(): " +
                                            "Refreshing searched entities1 list after SearchEntities1StateEvent\n" +
                                            "searchedEntities1List == ${searchedAvailableJobs?.convertToString()}")
                                postRefreshListOfSearchedEntites1(true)
                        }
                    }
                )
            }

            is HomeScreenStateEvent.ClearFiltersStateEvent -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: ClearFiltersStateEvent")
                val currentViewState = getCurrentViewStateOrNew()
                currentViewState.clearEntity1SearchFilters()
                setViewState(currentViewState)
                emitDoNothingStateMessageEvent(stateEvent)
            }

            is HomeScreenStateEvent.GetUsersRating -> {
                if(LOG_ME)ALog.d(
                    TAG, ".setStateEvent(): " +
                                        "GetUsersRating")
                val currentViewState = getCurrentViewStateOrNew()
                if(currentViewState.mainEntity != null) {
                    if(currentViewState.mainEntity!!.id != null) {
                        interactors.getUsersRating.getUsersRating(
                            stateEvent = stateEvent,
                            returnViewState = currentViewState,
                            updateReturnViewState = {
                                viewState, obtainedUsersRating -> viewState.apply {
                                        usersRating = obtainedUsersRating
                                }
                            }
                        )
                    } else {
                        emitToastStateMessageEvent(stateEvent, R.string.mainEntity_is_null_when_trying_to_get_users_rating)
                    }
                } else {
                    emitToastStateMessageEvent(stateEvent, R.string.mainEntity_is_null_when_trying_to_get_users_rating)
                }
            }

            is HomeScreenStateEvent.ClearUIChangesInFragment5 -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: ClearUIChangesInFragment5")
                val update = getCurrentViewStateOrNew()
                update.mainEntity?.let {
                    update.usersName = it.name
                    update.usersCity = it.city
                    update.usersDescription = it.description
                }
                setViewState(update)
                emitToastStateMessageEvent(stateEvent, R.string.changes_reverted)
            }

            is HomeScreenStateEvent.AskUserAboutLogout -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: AskUserAboutLogout")
                emitStateMessageEvent(
                    stateMessage = StateMessage(
                        response = Response(
                            messageId = R.string.are_you_sure_you_want_to_logout,
                            message = (getApplicationContext() as Context).getString(R.string.are_you_sure_you_want_to_logout),
                            uiComponentType = UIComponentType.AreYouSureDialog(
                                callback = object : AreYouSureCallback {
                                    override fun proceed() {
                                        if(LOG_ME)ALog.d(TAG, ".$methodName(): Logging user out.")
                                        setStateEvent(HomeScreenStateEvent.LogoutUser)
                                    }
                                    override fun cancel() {
                                        if(LOG_ME)ALog.d(TAG, ".$methodName(): User cancelled logout.")
                                    }
                                }
                            ),
                            messageType = MessageType.Info()
                        )
                    ),
                    stateEvent = stateEvent
                )
            }

            is HomeScreenStateEvent.DownloadExchangeRates -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: DownloadExchangeRates")
                val currentViewState = getCurrentViewStateOrNew()
                interactors.downloadExchangeRates.downloadExchangeRates(
                    baseCurrency = ProjectConstants.COMPANYS_ACCEPTED_CURRENCY,
                    stateEvent = stateEvent,
                    returnViewState = currentViewState,
                    updateReturnViewState = {
                        viewState, exchangeRatesP -> viewState.apply {
                            exchangeRates = exchangeRatesP
                        }
                    }
                )
            }

            is HomeScreenStateEvent.LogoutUser -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: AskUserAboutLogout")
                val currentViewState = getCurrentViewStateOrNew()
                if(currentViewState.mainEntity != null) {
                    interactors.logout.logout(
                        user = currentViewState.mainEntity!!,
                        stateEvent = stateEvent,
                        returnViewState = getCurrentViewStateOrNew(),
                        updateReturnViewState = {
                            viewState -> viewState.apply {
                                mainEntity = null
                                usersName = null
                                usersSurname = null
                                usersCity = null
                                usersDescription = null
                                navigateToStartScreen = true
                            }
                        }
                    )
                } else {
                    emitToastStateMessageEvent(stateEvent, R.string.error_user_is_null_during_logout)
                }
            }

            is HomeScreenStateEvent.ReportProblem -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: ReportProblem")
                val currentViewState = getCurrentViewStateOrNew()
                if(currentViewState.mainEntity != null) {
                    stateEvent.reportProblemFunction.invoke()
                    emitDoNothingStateMessageEvent(stateEvent)
                } else {
                    emitToastStateMessageEvent(stateEvent, R.string.error_user_is_null_during_error_reporting)
                }
            }

            is HomeScreenStateEvent.RateApplication -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: RateApplication")
                val currentViewState = getCurrentViewStateOrNew()
                if(currentViewState.mainEntity != null) {
                    stateEvent.rateAppFunction.invoke()
                    emitDoNothingStateMessageEvent(stateEvent)
                } else {
                    emitToastStateMessageEvent(stateEvent, R.string.error_user_is_null_during_app_rating)
                }
            }

            is HomeScreenStateEvent.RecommendApp -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: RecommendApp")
                val currentViewState = getCurrentViewStateOrNew()
                if(currentViewState.mainEntity != null) {
                    stateEvent.recommendationFunction.invoke()
                    emitDoNothingStateMessageEvent(stateEvent)
                } else {
                    emitToastStateMessageEvent(stateEvent, R.string.error_user_is_null_during_app_recommending)
                }
            }

            is HomeScreenStateEvent.GetMerchantName -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: GetMerchantName")
                val currentViewState = getCurrentViewStateOrNew()
                if(currentViewState.mainEntity != null) {
                    interactors.getMerchantName.getMerchantName(
                        continueFlag = stateEvent.continueFlag,
                        stateEvent = stateEvent,
                        returnViewState = getCurrentViewStateOrNew(),
                        updateReturnViewState = {
                            viewState,
                            merchantNameP,
                            continueFlag -> viewState.apply {
                                merchantName = merchantNameP
                            }
                        }
                    )
                } else {
                    emitToastStateMessageEvent(stateEvent, R.string.mainEntity_is_null_when_trying_to_get_merchant_name)
                }
            }

            is HomeScreenStateEvent.GetGatewayNameAndMerchantID -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: GetGatewayNameAndMerchantID")
                val currentViewState = getCurrentViewStateOrNew()
                if(currentViewState.mainEntity != null) {
                    interactors.getGatewayNameAndMerchantID.getGatewayNameAndMerchantID(
                        continueFlag = stateEvent.continueFlag,
                        stateEvent = stateEvent,
                        returnViewState = getCurrentViewStateOrNew(),
                        updateReturnViewState = {
                            viewState,
                            gatewayNameP,
                            gatewayMerchantIDP,
                            continueFlag -> viewState.apply {
                                gatewayName = gatewayNameP
                                gatewayMerchantID = gatewayMerchantIDP
                            }
                        }
                    )
                } else {
                    emitToastStateMessageEvent(stateEvent, R.string.mainEntity_is_null_when_trying_to_get_merchant_name)
                }
            }

            is HomeScreenStateEvent.CheckGooglePayAvailability -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: CheckGooglePayAvailability")
                val currentViewState = getCurrentViewStateOrNew()
                if(currentViewState.mainEntity != null) {
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): Checking Google Pay availability.")
                    try {
                        interactors.checkGooglePayAvailability.checkGooglePayAvailability(
                            context = stateEvent.context,
                            continueFlag = stateEvent.continueFlag,
                            stateEvent = stateEvent,
                            returnViewState = getCurrentViewStateOrNew(),
                            updateReturnViewState = {
                                    viewState,
                                    gPaymentsClient,
                                    gPayIsAvailable,
                                    continueFlag -> viewState.apply {
                                        googlePaymentsClient = gPaymentsClient
                                        googlePayIsAvailable = gPayIsAvailable
                                    }
                            }
                        )
                    } catch (e: Exception) {
                        ALog.e(TAG, methodName, e)
                        emitToastStateMessageEvent(stateEvent, R.string.error_user_is_null_while_checking_gpay_availability)
                    }
                } else {
                    emitToastStateMessageEvent(stateEvent, R.string.error_user_is_null_while_checking_gpay_availability)
                }
            }

            is HomeScreenStateEvent.ShowDialogSayingThatGooglePayIsUnavailable -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: ShowDialogSayingThatGooglePayIsUnavailable")
                val currentViewState = getCurrentViewStateOrNew()
                emitOkDialogStateMessageEvent(
                    stateEvent,
                    R.string.google_pay_is_unavailable,
                    R.string.please_make_google_pay_available_on_this_device,
                    object: OkButtonCallback {
                        override fun proceed() {
                            //navigateToPlayStoreToInstallGooglePay(stateEvent.context)
                        }
                    },
                )
            }

            is HomeScreenStateEvent.NotifyUserAboutLackOfEmailAppInstalled -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: NotifyUserAboutLackOfEmailAppInstalled")
                val currentViewState = getCurrentViewStateOrNew()
                emitOkDialogStateMessageEvent(
                    stateEvent,
                    R.string.no_email_application_installed,
                    R.string.please_install_an_email_application,
                    object: OkButtonCallback {
                        override fun proceed() {
                            //navigateToPlayStoreToInstallEmailApp(stateEvent.context)
                        }
                    },
                )
            }

            is HomeScreenStateEvent.Navigate -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: Navigate")
                val currentViewState = getCurrentViewStateOrNew()
                if(currentViewState.mainEntity != null) {
                    stateEvent.navigationFunction.invoke()
                    emitDoNothingStateMessageEvent(stateEvent)
                } else {
                    emitToastStateMessageEvent(stateEvent, R.string.error_user_is_null_while_navigating)
                }
            }

            is HomeScreenStateEvent.CreateStateMessageEvent -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: CreateStateMessageEvent")
                emitStateMessageEvent(
                    stateMessage = stateEvent.stateMessage,
                    stateEvent = stateEvent
                )
            }

            is HomeScreenStateEvent.None -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: None")
                launchJob = false
                interactors.doNothingAtAll.doNothing()
            }

            else -> {
                if (LOG_ME) Log.d(TAG, "setStateEvent: InvalidStateEvent")
                emitInvalidStateEvent(stateEvent)
            }
        }
        if (LOG_ME) ALog.d(TAG, "launching stateEvent in job: ${stateEvent.eventName()}")
        launchJob(stateEvent, job, launchJob)
        if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
    }

    override fun initNewViewState(): HomeScreenViewState<ProjectUser> {
        return HomeScreenViewState()
    }

    override fun setMainEntity(projectUser: ProjectUser?){
        if (LOG_ME) ALog.d(
            TAG,
            ".setMainEntity(): viewState == ${viewState.javaClass.simpleName} $viewState    " +
                    "this == ${this.javaClass.simpleName} ${this.hashCode()}")
        val update = getCurrentViewStateOrNew()
        update.mainEntity = projectUser
        projectUser?.let {
            update.usersName = projectUser.name
            update.usersCity = projectUser.city
            update.usersDescription = projectUser.description
            update.usersProfilePhotoImageURI = projectUser.profilePhotoImageURI
        }
        setViewState(update)
    }

    fun updateUserWithDataFromUI(): ProjectUser? {
        val methodName: String = "updateUser"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val update = getCurrentViewStateOrNew()
            return if(update.mainEntity != null) {
                update.mainEntity!!.apply {
                    description = update.usersDescription
                    profilePhotoImageURI = update.usersProfilePhotoImageURI
                }
            } else {
                ALog.w(TAG, ".$methodName(): user == null")
                null
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
        return null
    }

    fun setCurrentComposableDestination(destination: HomeScreenDestination){
        if(LOG_ME)ALog.d(TAG, ".setCurrentComposableDestination(): " +
                "Setting currentComposableDestination to $destination")
        val update = getCurrentViewStateOrNew()
        update.currentComposableDestination = destination
        setViewState(update)
    }

    fun setLocation(location: DeviceLocation?){
        if(LOG_ME)ALog.d(TAG, ".setLocation(): Setting location to ${location.toString()}")
        val update = getCurrentViewStateOrNew()
        update.location = location
        setViewState(update)
    }
    fun setShowSnackbar(showSnackbar: SnackBarState?){
        if(LOG_ME)ALog.d(TAG, ".setShowSnackbar(): " +
                "Setting showSnackbar to ${showSnackbar?.message}")
        val update = getCurrentViewStateOrNew()
        update.snackBarState = showSnackbar
        setViewState(update)
    }

    fun setCard1CurrentlyShownEntity(entity: TaskAssignment) {
        if(LOG_ME)ALog.d(TAG,
            ".setCard1CurrentlyShownEntity(): Setting card1CurrentlyShownEntity")
        val update = getCurrentViewStateOrNew()
        update.card1CurrentlyShownEntity = entity
        setViewState(update)
    }

    fun setCard2CurrentlyShownEntity(entity: TaskAssignment) {
        if(LOG_ME)ALog.d(TAG,
            ".setCard2CurrentlyShownEntity(): Setting card2CurrentlyShownEntity")
        val update = getCurrentViewStateOrNew()
        update.card2CurrentlyShownEntity = entity
        setViewState(update)
    }

    fun setCard3CurrentlyShownEntity(entity: TaskAssignment) {
        if(LOG_ME)ALog.d(TAG,
            ".setCard3CurrentlyShownEntity(): Setting card3CurrentlyShownEntity")
        val update = getCurrentViewStateOrNew()
        update.card3CurrentlyShownEntity = entity
        setViewState(update)
    }

    fun setCard4CurrentlyShownEntity(entity: Entity4) {
        if(LOG_ME)ALog.d(TAG,
            ".setCard4CurrentlyShownEntity(): Setting card4CurrentlyShownEntity")
        val update = getCurrentViewStateOrNew()
        update.card4CurrentlyShownEntity = entity
        setViewState(update)
    }

    fun setEntity1SearchQuery(entity1SearchQuery: String?){
        if(LOG_ME)ALog.d(TAG,
            ".setEntity1SearchQuery(): Setting entity1SearchQuery to $entity1SearchQuery")
        val update = getCurrentViewStateOrNew()
        update.availableJobsSearchQuery = entity1SearchQuery
        setViewState(update)
    }

    fun setEntity1SearchFilters(filters: HomeScreenCard1SearchFilters){
        if(LOG_ME)ALog.d(TAG,
            ".setEntity1SearchQuery(): Setting setEntity1SearchFilters")
        val update = getCurrentViewStateOrNew()
        update.setEntity1SearchFilters(filters)
        setViewState(update)
    }

    fun setNavigateToStartScreen(navigateToStartScreen: Boolean?){
        if(LOG_ME)ALog.d(TAG, ".setNavigateToStartScreen(): Setting navigateToStartScreen to ${navigateToStartScreen}")
        val update = getCurrentViewStateOrNew()
        update.navigateToStartScreen = navigateToStartScreen
        setViewState(update)
    }

    fun setSearchedEntities1List(searchedEntities1List : ArrayList<TaskAssignment>?) {
        val update = getCurrentViewStateOrNew()
        update.searchedAvailableJobs = searchedEntities1List
        setViewState(update)
    }

    fun postSearchedEntities1List(searchedEntities1List : ArrayList<TaskAssignment>?) {
        val update = getCurrentViewStateOrNew()
        update.searchedAvailableJobs = searchedEntities1List
        postViewState(update)
    }

    private fun postRefreshListOfSearchedEntites1(refreshListOfSearchedEntites1: Boolean) {
        if(LOG_ME)ALog.d(TAG, ".postRefreshListOfSearchedEntites1(): Setting refreshListOfSearchedEntities1")
        val update = getCurrentViewStateOrNew()
        update.refreshListOfSearchedEntities1 = refreshListOfSearchedEntites1
        postViewState(update)
    }


    fun setRefreshListOfSearchedEntities1(refreshListOfSearchedEntities1: Boolean) {
        if(LOG_ME)ALog.d(TAG, ".setRefreshListOfSearchedEntities1(): Setting refreshListOfSearchedEntities1")
        val update = getCurrentViewStateOrNew()
        update.refreshListOfSearchedEntities1 = refreshListOfSearchedEntities1
        setViewState(update)
    }


    fun setRefreshListOfEntites3(refreshListOfEntities3: Boolean) {
        if(LOG_ME)ALog.d(TAG, ".setRefreshListOfEntites3(): Setting refreshListOfEntities3")
        val update = getCurrentViewStateOrNew()
        update.refreshListOfEntities3 = refreshListOfEntities3
        setViewState(update)
    }

    private fun setUserTakenJobsList(entities2List : ArrayList<TaskAssignment>) {
        if(LOG_ME)ALog.d(TAG, ".setUserTakenJobsList(): Setting userTakenJobs")
        val update = getCurrentViewStateOrNew()
        update.userTakenJobs = entities2List
        setViewState(update)
    }

    private fun setUserDoneJobsList(entities3List : ArrayList<TaskAssignment>) {
        if(LOG_ME)ALog.d(TAG, ".setUserDoneJobsList(): Setting userDoneJobs")
        val update = getCurrentViewStateOrNew()
        update.userDoneJobs = entities3List
        setViewState(update)
    }

    fun setExchangeRates(exchangeRatesP: ExchangeRates?) {
        if(LOG_ME)ALog.d(
            TAG, ".setExchangeRates(): " +
                "Setting exchangeRates to $exchangeRatesP")
        val update = getCurrentViewStateOrNew()
        update.exchangeRates = exchangeRatesP
        setViewState(update)
    }

    private fun setEntities4List(entities : ArrayList<Entity4>) {
        if(LOG_ME)ALog.d(TAG, ".setEntities4List(): Setting entities4List")
        val update = getCurrentViewStateOrNew()
        update.entities4List = entities
        setViewState(update)
    }

    private fun setNoCachedEntity(noCachedEntity: Boolean?) {
        if(LOG_ME)ALog.d(TAG, ".setNoCachedEntity(): Setting noCachedEntity to $noCachedEntity")
        val update = getCurrentViewStateOrNew()
        update.noCachedEntity = noCachedEntity
        setViewState(update)
    }

    fun setCollapsingToolbarState(
        state: CollapsingToolbarState
    ) = interactionManager.setCollapsingToolbarState(state)

    fun validateAndUpdateInputField(inputFieldContent: String?, updateFun : ()->Unit){
        val methodName = "validateAndUpdateInputField"
        if(stringIsNeitherNullNorEmpty(inputFieldContent)) {
            if(LOG_ME)ALog.d(TAG, ".$methodName(): Input fields content $inputFieldContent is valid. Updating it in ViewModel")
            val update = getCurrentViewStateOrNew()
            updateFun.invoke()

            setViewState(update)
        } else {
            if(LOG_ME)ALog.d(TAG, ".$methodName(): Input fields content $inputFieldContent is invalid.")
            setStateEvent(
                HomeScreenStateEvent.CreateStateMessageEvent(
                    stateMessage = StateMessage(
                        response = Response(
                            messageId = R.string.text_ok,
                            message = (getApplicationContext() as Context).getString(R.string.text_ok),
                            uiComponentType = UIComponentType.SnackBar(
                                null,
                                R.string.invalid_input,
                                (getApplicationContext() as Context).getString(R.string.invalid_input),
                                View.OnClickListener {  }
                            ),
                            messageType = MessageType.Error()
                        )
                    )
                )
            )
        }
    }


    /**
     * Creates a [Task] that starts the payment process with the transaction details included.
     *
     * @param priceCents the price to show on the payment sheet.
     * @return a [Task] with the payment information.
     * @see [](https://developers.google.com/android/reference/com/google/android/gms/wallet/PaymentsClient#loadPaymentData(com.google.android.gms.wallet.PaymentDataRequest)
    ) */
    fun getLoadPaymentDataTask(
        priceCents: Long,
    ): Task<PaymentData> {
        val currentViewState = getCurrentViewStateOrNew()
        val gatewayName = currentViewState.gatewayName
        val gatewayMerchantID = currentViewState.gatewayMerchantID
        if(gatewayName == null || gatewayMerchantID == null) {
            ALog.e(
                TAG, ".getLoadPaymentDataTask(): " +
                    "gatewayName == $gatewayName and " +
                    "gatewayMerchantID == $gatewayMerchantID")
            throw NullPointerException("gateway name and merchant id should be not null at this point.")
        }

        val paymentDataRequestJson = GooglePayPaymentsUtil.getPaymentDataRequest(
            priceCents,
            GooglePayPaymentsUtil.generateMerchantInfo(getCurrentViewStateOrNew().merchantName!!),
            gatewayName!!,
            gatewayMerchantID!!,
        )
        val request = PaymentDataRequest.fromJson(paymentDataRequestJson.toString())

        return googlePaymentsClient!!.loadPaymentData(request)
    }

    fun getLoadPaymentDataTask(
        priceCents: Long,
        feesCurrency: String,
    ): Task<PaymentData> {
        val currentViewState = getCurrentViewStateOrNew()
        val gatewayName = currentViewState.gatewayName
        val gatewayMerchantID = currentViewState.gatewayMerchantID
        if(gatewayName == null || gatewayMerchantID == null) {
            ALog.e(
                TAG, ".getLoadPaymentDataTask(): " +
                    "gatewayName == $gatewayName and " +
                    "gatewayMerchantID == $gatewayMerchantID")
            throw NullPointerException("gateway name and merchant id should be not null at this point.")
        }

        val paymentDataRequestJson = GooglePayPaymentsUtil.getPaymentDataRequest(
            priceCents,
            GooglePayPaymentsUtil.generateMerchantInfo(getCurrentViewStateOrNew().merchantName!!),
            gatewayName!!,
            gatewayMerchantID!!,
        )
        val request = PaymentDataRequest.fromJson(paymentDataRequestJson.toString())

        return googlePaymentsClient!!.loadPaymentData(request)
    }

    fun updateInputField(inputFieldContent: String?, updateFun : ()->Unit){
        val methodName = "updateInputField"
        if(LOG_ME)ALog.d(TAG, ".$methodName(): Input fields content $inputFieldContent is valid. Updating it in ViewModel")
        val update = getCurrentViewStateOrNew()
        updateFun.invoke()

        setViewState(update)
    }

    private fun inputField1IsValid(inputFieldContent : String?): Boolean {
        return stringIsNeitherNullNorEmpty(inputFieldContent)
    }

    private fun inputField2IsValid(inputFieldContent : String?): Boolean {
        return stringIsNeitherNullNorEmpty(inputFieldContent)
    }

    private fun inputField3IsValid(inputFieldContent : String?): Boolean {
        return stringIsNeitherNullNorEmpty(inputFieldContent)
    }

    private fun stringIsNeitherNullNorEmpty(string : String?): Boolean {
        return string?.isNotEmpty() ?: false
    }

    fun setSearchQueryInteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField1InteractionState(state)
    }

    fun setRange1MinInteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField2InteractionState(state)
    }

    fun setRange1MaxInteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField3InteractionState(state)
    }

    fun setRange1RangeSliderInteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField4InteractionState(state)
    }

    fun setRange2MinInteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField5InteractionState(state)
    }

    fun setRange2MaxInteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField6InteractionState(state)
    }

    fun setRange2RangeSliderInteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField7InteractionState(state)
    }

    fun setSwitch1InteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField8InteractionState(state)
    }

    fun setSwitch2InteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField9InteractionState(state)
    }

    fun setSwitch3InteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField10InteractionState(state)
    }

    fun setSwitch4InteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField11InteractionState(state)
    }

    fun setSwitch5InteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField12InteractionState(state)
    }

    fun setSwitch6InteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField13InteractionState(state)
    }

    fun setSwitch7InteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField14InteractionState(state)
    }



    fun setEditingUsersNameInteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField15InteractionState(state)
    }
    fun setEditingUsersDescriptionInteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField16InteractionState(state)
    }
    fun setEditingUsersCityInteractionState(state: HomeScreenInteractionState){
        interactionManager.setNewInputField17InteractionState(state)
    }


    fun isToolbarCollapsed() = collapsingToolbarState.toString()
        .equals(CollapsingToolbarState.Collapsed().toString())

    fun isToolbarExpanded() = collapsingToolbarState.toString()
        .equals(CollapsingToolbarState.Expanded().toString())

    // return true if in EditState
    fun checkEditState() = interactionManager.checkEditState()

    fun exitEditState() = interactionManager.exitEditState()

    fun setInitialState() = interactionManager.setInitialState()

    fun isEditingSearchQuery() = interactionManager.isEditingInputField1()

    fun isEditingRange1Min() = interactionManager.isEditingInputField2()

    fun isEditingRange1Max() = interactionManager.isEditingInputField3()

    fun isEditingRange1RangeSlider() = interactionManager.isEditingInputField4()

    fun isEditingRange2Min() = interactionManager.isEditingInputField5()

    fun isEditingRange2Max() = interactionManager.isEditingInputField6()

    fun isEditingRange2RangeSlider() = interactionManager.isEditingInputField7()

    fun isEditingSwitch1() = interactionManager.isEditingInputField8()

    fun isEditingSwitch2() = interactionManager.isEditingInputField9()

    fun isEditingSwitch3() = interactionManager.isEditingInputField10()

    fun isEditingSwitch4() = interactionManager.isEditingInputField11()

    fun isEditingSwitch5() = interactionManager.isEditingInputField12()

    fun isEditingSwitch6() = interactionManager.isEditingInputField13()

    fun isEditingSwitch7() = interactionManager.isEditingInputField14()


    fun isEditingUsersName() = interactionManager.isEditingInputField15()

    fun isEditingUsersDescription() = interactionManager.isEditingInputField16()

    fun isEditingUsersCity() = interactionManager.isEditingInputField17()



    // force observers to refresh
    fun triggerEntityObservers(){
        getCurrentViewStateOrNew().mainEntity?.let { mainEntity ->
            setMainEntity(mainEntity)
        }
        getCurrentViewStateOrNew().listOfEntities?.let { listOfEntities ->
            setListOfEntities(listOfEntities)
        }
    }

    fun logInteractionState() {
        ALog.d(TAG, ".logInteractionState(): interactionManager == \n $interactionManager")
    }

    // Method saves searchedEntities1List from ViewState and clears it so that savedInstanceState was small enough.
    // Reference: https://stackoverflow.com/questions/45193941/how-to-read-and-write-txt-files-in-android-in-kotlin
    fun saveSearchedEntities1ListInFile(internalDirectory: File) {
        if (LOG_ME) ALog.d(TAG, "Method start: saveSearchedEntities1ListInFile()")
        try {
            val currentViewState = getCurrentViewStateOrNew()
            if(currentViewState.searchedAvailableJobs == null) {
                if(LOG_ME)ALog.d(
                    TAG, ".saveSearchedEntities1ListInFile(): " +
                                        "There is nothing to save: searchedEntities1List == null")
                return
            }
            if(currentViewState.searchedAvailableJobs!!.isEmpty()) {
                if(LOG_ME)ALog.d(
                    TAG, ".saveSearchedEntities1ListInFile(): " +
                        "There is nothing to save: searchedEntities1List is empty")
                return
            }

            internalDirectory.mkdirs()
            val searchedEntities1ListFile = File(internalDirectory, SEARCHED_ENTITIES1_LIST_FILE_NAME)

            val gson = Gson()
            val searchedEntities1ListJson: String = gson.toJson(currentViewState.searchedAvailableJobs!!)
            if(LOG_ME)ALog.d(
                TAG, ".saveSearchedEntities1ListInFile(): " +
                                    "searchedEntities1ListFile.path == ${searchedEntities1ListFile.path}")
            searchedEntities1ListFile.writeText(searchedEntities1ListJson)
            if(LOG_ME)ALog.d(
                TAG, ".saveSearchedEntities1ListInFile(): " +
                    "Saved searchedEntities1List as string in file $SEARCHED_ENTITIES1_LIST_FILE_NAME\n" +
                    "Content:\n" + searchedEntities1ListJson
            )

        } catch (e: Exception) {
            ALog.e(TAG, "saveSearchedEntities1ListInFile()", e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: saveSearchedEntities1ListInFile()")
        }
    }

    // Reference: https://stackoverflow.com/questions/12384064/gson-convert-from-json-to-a-typed-arraylistt
    private fun restoreSearchedEntities1ListFromFile(internalDirectory: File) {
        if (LOG_ME) ALog.d(TAG, "Method start: restoreSearchedEntities1ListFromFile()")
        try {

            val searchedEntities1ListFile = File(internalDirectory, SEARCHED_ENTITIES1_LIST_FILE_NAME)
            if(!searchedEntities1ListFile.exists()) {
                if(LOG_ME)ALog.d(
                    TAG, ".restoreSearchedEntities1ListFromFile(): " +
                                        "searchedEntities1ListFile does not exist.")
                return
            }

            val gson = Gson()
            val fileContent = searchedEntities1ListFile.readText()
            val searchedEntities1List = gson.fromJson<ArrayList<TaskAssignment>>(fileContent, searchedEntities1ListType)
            if(searchedEntities1List != null) {
                setSearchedEntities1List(searchedEntities1List)
            } else {
                if(LOG_ME)ALog.d(
                    TAG, ".restoreSearchedEntities1ListFromFile(): " +
                                        "searchedEntities1List == null\n" +
                        "fileContent == $fileContent")
            }
        } catch (e: Exception) {
            ALog.e(TAG, "restoreSearchedEntities1ListFromFile()", e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: restoreSearchedEntities1ListFromFile()")
        }
    }

    companion object {
        val searchedEntities1ListType = object : TypeToken<List<TaskAssignment>>() {}.type
        val SEARCHED_ENTITIES1_FRESHNESS_INTERVAL = 600000L
    }
}
