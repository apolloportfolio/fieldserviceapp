package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state

import com.aps.catemplateapp.common.util.ALog

private const val TAG = "HomeScreenInteractionState"
private const val LOG_ME = false

sealed class HomeScreenInteractionState {
    class EditState: HomeScreenInteractionState() {

        override fun toString(): String {
            return "EditState"
        }
    }

    class DefaultState: HomeScreenInteractionState(){

        override fun toString(): String {
            return "DefaultState"
        }
    }

    class InitialState: HomeScreenInteractionState(){

        override fun toString(): String {
            return "InitialState"
        }
    }

    override fun equals(other: Any?): Boolean {
        val methodName: String = "equals"
        try {
            if(other is HomeScreenInteractionState) {
                val result = other.toString() == this.toString()
                if(LOG_ME) ALog.w(
                    TAG, ".equals(): result == $result    " +
                        "other.toString() == ${other.toString()}    " +
                        "this.toString() == ${this.toString()}")
                return result
            } else {
                if(LOG_ME) ALog.w(
                    TAG, ".equals(): other is not of the same class! " +
                        "other class name: ${other!!::class.java.canonicalName}    " +
                        "this class name: ${this::class.java.canonicalName}")
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
        }
        return false
    }
}