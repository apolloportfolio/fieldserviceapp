package com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities

import android.os.Parcelable
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.core.util.ProjectConstants
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import kotlinx.android.parcel.Parcelize
import kotlinx.parcelize.IgnoredOnParcel
import java.io.Serializable


private const val TAG = "Entity4"
private const val LOG_ME = true

// Entity4
@Parcelize
data class Entity4(
    var id: UniqueID?,
    var created_at: String?,
    var updated_at: String?,

    var latitude: Double?,
    var longitude: Double?,
    var geoLocation: ParcelableGeoPoint?,
    var firestoreGeoLocation: Double?,

    var picture1URI: String?,
    var name: String?,
    var description: String?,
): Parcelable, Serializable {
    var hasPictures:Boolean =
        picture1URI != null
    @IgnoredOnParcel
    val picture1FirebaseImageRef: StorageReference?
        get() {
            return if(id != null && picture1URI != null) {
                Firebase.storage.reference
                    .child(ProjectConstants.FIRESTORE_IMAGES_COLLECTION)
                    .child(ProjectConstants.FIRESTORE_ENTITY_4_IMAGES_SUB_COLLECTION)
                    .child(this.id!!.firestoreDocumentID)
                    .child(this.picture1URI!!)
            } else {
                null
            }
        }

    object ItemsConstants {
        const val LOCATION_FIELD_NAME = "firestoreGeoLocation"
    }

    override fun toString(): String {
        return "${id.toString()}: geoLocation == $geoLocation \n"
    }

    fun toIdString() : String {
        return "${this.javaClass.simpleName}($id)"
    }
}