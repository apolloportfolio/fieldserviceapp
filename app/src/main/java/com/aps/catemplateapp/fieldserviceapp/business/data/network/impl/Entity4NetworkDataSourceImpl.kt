package com.aps.catemplateapp.fieldserviceapp.business.data.network.impl

import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.fieldserviceapp.business.data.network.abs.Entity4NetworkDataSource
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity4
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.abs.Entity4FirestoreService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Entity4NetworkDataSourceImpl
@Inject
constructor(
    private val firestoreService: Entity4FirestoreService
): Entity4NetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: Entity4): Entity4? {
        return firestoreService.insertOrUpdateEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        return firestoreService.deleteEntity(primaryKey)
    }

    override suspend fun insertDeletedEntity(entity: Entity4) {
        return firestoreService.insertDeletedEntity(entity)
    }

    override suspend fun insertDeletedEntities(entities: List<Entity4>) {
        return firestoreService.insertDeletedEntities(entities)
    }

    override suspend fun deleteDeletedEntity(entity: Entity4) {
        return firestoreService.deleteDeletedEntity(entity)
    }

    override suspend fun getDeletedEntities(): List<Entity4> {
        return firestoreService.getAllDeletedEntities()
    }

    override suspend fun deleteAllEntities() {
        firestoreService.deleteAllEntities()
    }

    override suspend fun searchEntity(entity: Entity4): Entity4? {
        return firestoreService.searchEntity(entity)
    }

    override suspend fun getAllEntities(): List<Entity4> {
        return firestoreService.getAllEntities()
    }

    override suspend fun insertOrUpdateEntities(entities: List<Entity4>): List<Entity4>? {
        return firestoreService.insertOrUpdateEntities(entities)
    }

    override suspend fun getEntityById(id: UniqueID): Entity4? {
        return firestoreService.getEntityById(id)
    }
}