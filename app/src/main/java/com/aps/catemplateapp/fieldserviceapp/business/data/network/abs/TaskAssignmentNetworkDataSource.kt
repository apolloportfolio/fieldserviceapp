package com.aps.catemplateapp.fieldserviceapp.business.data.network.abs

import android.net.Uri
import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.TaskAssignment
import com.aps.catemplateapp.fieldserviceapp.business.interactors.impl.FirestoreEntity1SearchParameters

interface TaskAssignmentNetworkDataSource: StandardNetworkDataSource<TaskAssignment> {
    override suspend fun insertOrUpdateEntity(entity: TaskAssignment): TaskAssignment?

    override suspend fun deleteEntity(primaryKey: UniqueID?)

    override suspend fun insertDeletedEntity(entity: TaskAssignment)

    override suspend fun insertDeletedEntities(Entities: List<TaskAssignment>)

    override suspend fun deleteDeletedEntity(entity: TaskAssignment)

    override suspend fun getDeletedEntities(): List<TaskAssignment>

    override suspend fun deleteAllEntities()

    override suspend fun searchEntity(entity: TaskAssignment): TaskAssignment?

    override suspend fun getAllEntities(): List<TaskAssignment>

    override suspend fun insertOrUpdateEntities(Entities: List<TaskAssignment>): List<TaskAssignment>?

    override suspend fun getEntityById(id: UniqueID): TaskAssignment?

    suspend fun searchEntities(
        searchParameters : FirestoreEntity1SearchParameters
    ) : List<TaskAssignment>?

    suspend fun getUserTakenJobs(userID: UserUniqueID) : List<TaskAssignment>?

    suspend fun getUserDoneJobs(userID: UserUniqueID) : List<TaskAssignment>?

    suspend fun uploadEntity1PhotoToFirestore(
        entity : TaskAssignment,
        entitysPhotoUri : Uri,
        entitysPhotoNumber: Int,
    ) : String?
}