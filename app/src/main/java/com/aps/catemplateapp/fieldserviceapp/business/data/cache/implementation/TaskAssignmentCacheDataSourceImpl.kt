package com.aps.catemplateapp.fieldserviceapp.business.data.cache.implementation

import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fieldserviceapp.business.data.cache.abstraction.TaskAssignmentCacheDataSource
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.TaskAssignment
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.abs.TaskAssignmentDaoService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TaskAssignmentCacheDataSourceImpl
@Inject
constructor(
    private val entityDaoService: TaskAssignmentDaoService
): TaskAssignmentCacheDataSource {

    override suspend fun insertOrUpdateEntity(entity: TaskAssignment): TaskAssignment? {
        return entityDaoService.insertOrUpdateEntity(entity)
    }

    override suspend fun insertEntity(entity: TaskAssignment): Long {
        return entityDaoService.insertEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return entityDaoService.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<TaskAssignment>): Int {
        return entityDaoService.deleteEntities(entities)
    }

    override suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        taskDescription: String,
        taskAddress: String,
        clientName: String,
        phoneNumber: String,
        scheduledTaskStartTimeStamp: String,
        expectedTaskDuration: String,
        actualTaskStartTimeStamp: String?,
        actualTaskEndTimeStamp: String?,
        isTaskCompleted: Boolean,
        taskNotes: String?,
        customerFeedbackGrade: Int?,
        customerFeedbackGradeReview: String?,
        taskResultsPicturesURIs: String?,
        materialsNote: String?,
        jobPriority: Int?,

        lattitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description: String?,

        city: String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean?,
        switch2: Boolean?,
        switch3: Boolean?,
        switch4: Boolean?,
        switch5: Boolean?,
        switch6: Boolean?,
        switch7: Boolean?,
    ): Int {
        return entityDaoService.updateEntity(
            id,
            created_at,
            updated_at,

            taskDescription,
            taskAddress,
            clientName,
            phoneNumber,
            scheduledTaskStartTimeStamp,
            expectedTaskDuration,
            actualTaskStartTimeStamp,
            actualTaskEndTimeStamp,
            isTaskCompleted,
            taskNotes,
            customerFeedbackGrade,
            customerFeedbackGradeReview,
            taskResultsPicturesURIs,
            materialsNote,
            jobPriority,

            lattitude,
            longitude,
            geoLocation,
            firestoreGeoLocation,

            picture1URI,
            description,
            city,
            ownerID,
            name,

            switch1,
            switch2,
            switch3,
            switch4,
            switch5,
            switch6,
            switch7,
        )
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<TaskAssignment> {
        return entityDaoService.searchEntities()
    }

    override suspend fun getAllEntities(): List<TaskAssignment> {
        return entityDaoService.getAllEntities()
    }

    override suspend fun getEntityById(id: UniqueID?): TaskAssignment? {
        return entityDaoService.getEntityById(id)
    }

    override suspend fun getNumEntities(): Int {
        return entityDaoService.getNumEntities()
    }

    override suspend fun insertEntities(entities: List<TaskAssignment>): LongArray{
        return entityDaoService.insertEntities(entities)
    }
}