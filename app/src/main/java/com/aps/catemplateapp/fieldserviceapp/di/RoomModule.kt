package com.aps.catemplateapp.fieldserviceapp.di

import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.ProjectRoomDatabase
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.daos.*
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.daos.TaskAssignmentDao
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.daos.Entity2Dao
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.daos.Entity3Dao
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.daos.Entity4Dao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class RoomModule {

    @Singleton
    @Provides
    fun provideEntity1Dao(database : ProjectRoomDatabase) : TaskAssignmentDao {
        return database.taskAssignmentDao()
    }
}