package com.aps.catemplateapp.fieldserviceapp.di.activity01

import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.ActivityHomeScreenFragmentFactory
import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@EntryPoint
@InstallIn(ActivityComponent::class)
interface ActivityHomeScreenFragmentFactoryEntryPoint {
    @OptIn(ExperimentalCoroutinesApi::class, FlowPreview::class)
    fun getFieldServiceAppHomeScreenFragmentFactory(): ActivityHomeScreenFragmentFactory
}