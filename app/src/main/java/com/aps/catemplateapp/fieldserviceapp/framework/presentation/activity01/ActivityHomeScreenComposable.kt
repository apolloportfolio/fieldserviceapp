package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.lifecycle.viewmodel.compose.viewModel
import com.aps.catemplateapp.common.framework.presentation.IntentExtras
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.TaskAssignment
import com.aps.catemplateapp.core.framework.presentation.ProjectComposableActivity
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.HomeScreenDestination
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.abs.HomeScreenUIProvider
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject

private const val TAG = "ActivityHomeScreenComposable"
private const val LOG_ME = true

@OptIn(ExperimentalCoroutinesApi::class, FlowPreview::class)
@AndroidEntryPoint
class ActivityHomeScreenComposable : ProjectComposableActivity() {
    lateinit var viewModel : HomeScreenViewModelCompose
    @Inject lateinit var homeScreenUIProvider : HomeScreenUIProvider

    @ExperimentalMaterialApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val methodName = "onCreate"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")


        setContent {
            ActivityContent()
        }
    }

    @Composable
    fun ActivityContent(
        composableViewModel: HomeScreenViewModelCompose = viewModel()
    ) {

        homeScreenUIProvider.getComposable(
            viewModel = composableViewModel,
            activity = this,
        )()

        LaunchedEffect(Unit) {
            viewModel = composableViewModel
            setMainEntityInViewModelFromBundle(composableViewModel, IntentExtras.APP_USER)
            setSearchedEntities1ListInViewModelFromBundle(
                composableViewModel,
                IntentExtras.SEARCHED_ITEMS_LIST,
            )
            navigateToDestinationFromBundle()
        }
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    private fun setSearchedEntities1ListInViewModelFromBundle(
        viewModel : HomeScreenViewModelCompose,
        searchedEntities1ListTag : String,
    ) {
        val methodName = "setSearchedEntities1ListInViewModelFromBundle"
        if (LOG_ME) ALog.d(
            TAG, "Method start: $methodName")
        try {
            val bundle : Bundle? = intent.extras
            bundle?.let {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): bundle != null")
                val searchedEntities1List : ArrayList<TaskAssignment>? =
                    it.get(searchedEntities1ListTag) as ArrayList<TaskAssignment>?
                searchedEntities1List?.let { entities ->
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): item != null")
                    viewModel.setSearchedEntities1List(entities)
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun navigateToDestinationFromBundle() {
        val methodName = "navigateToDestinationFromBundle"
        if (LOG_ME) ALog.d(
            TAG, "Method start: $methodName")
        try {
            val bundle : Bundle? = intent.extras
            if(bundle != null) {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): bundle != null")
                val destination : Int = bundle.getInt(IntentExtras.DESTINATION)
                if(destination != null) {
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): destination == $destination")
                    this.viewModel.setCurrentComposableDestination(
                        HomeScreenDestination.fromNumber(destination)
                    )
                } else {
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): destination == 0")
                }
            } else {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): bundle == null")
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    // Legacy code for Views
//    override fun displayInputCaptureDialog(
//        title: String,
//        callback: DialogInputCaptureCallback
//    ) {
//        dialogInView = MaterialDialog(this).show {
//            title(text = title)
//            positiveButton(R.string.text_ok)
//            onDismiss {
//                dialogInView = null
//            }
//            cancelable(true)
//        }
//    }
}