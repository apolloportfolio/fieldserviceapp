package com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.abs


import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity3

interface Entity3DaoService {
    suspend fun insertOrUpdateEntity(entity: Entity3): Entity3

    suspend fun insertEntity(entity: Entity3): Long

    suspend fun insertEntities(Entities: List<Entity3>): LongArray

    suspend fun getEntityById(id: UniqueID?): Entity3?

    suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        name: String?,
        description: String?,

        ownerID: UserUniqueID?,
    ): Int

    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteEntities(Entities: List<Entity3>): Int

    suspend fun searchEntities(): List<Entity3>

    suspend fun getAllEntities(): List<Entity3>

    suspend fun getNumEntities(): Int
}