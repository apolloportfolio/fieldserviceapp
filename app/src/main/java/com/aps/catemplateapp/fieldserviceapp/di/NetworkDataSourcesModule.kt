package com.aps.catemplateapp.fieldserviceapp.di

import com.aps.catemplateapp.fieldserviceapp.business.data.network.abs.TaskAssignmentNetworkDataSource
import com.aps.catemplateapp.fieldserviceapp.business.data.network.abs.Entity2NetworkDataSource
import com.aps.catemplateapp.fieldserviceapp.business.data.network.abs.Entity3NetworkDataSource
import com.aps.catemplateapp.fieldserviceapp.business.data.network.abs.Entity4NetworkDataSource
import com.aps.catemplateapp.fieldserviceapp.business.data.network.impl.TaskAssignmentNetworkDataSourceImpl
import com.aps.catemplateapp.fieldserviceapp.business.data.network.impl.Entity2NetworkDataSourceImpl
import com.aps.catemplateapp.fieldserviceapp.business.data.network.impl.Entity3NetworkDataSourceImpl
import com.aps.catemplateapp.fieldserviceapp.business.data.network.impl.Entity4NetworkDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class NetworkDataSourcesModule {

    @Binds
    abstract fun bindTaskAssignmentNetworkDataSource(implementation: TaskAssignmentNetworkDataSourceImpl): TaskAssignmentNetworkDataSource

    @Binds
    abstract fun bindEntity2NetworkDataSource(implementation: Entity2NetworkDataSourceImpl): Entity2NetworkDataSource

    @Binds
    abstract fun bindEntity3NetworkDataSource(implementation: Entity3NetworkDataSourceImpl): Entity3NetworkDataSource

    @Binds
    abstract fun bindEntity4NetworkDataSource(implementation: Entity4NetworkDataSourceImpl): Entity4NetworkDataSource

}