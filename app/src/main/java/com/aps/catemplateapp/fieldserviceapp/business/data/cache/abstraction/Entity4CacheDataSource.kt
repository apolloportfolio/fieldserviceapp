package com.aps.catemplateapp.fieldserviceapp.business.data.cache.abstraction


import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity4

interface Entity4CacheDataSource: StandardCacheDataSource<Entity4> {
    override suspend fun insertEntity(entity: Entity4): Long

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int

    override suspend fun deleteEntities(entities: List<Entity4>): Int

    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        lattitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        name: String?,
        description: String?,
    ): Int

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<Entity4>

    override suspend fun getAllEntities(): List<Entity4>

    override suspend fun getEntityById(id: UniqueID?): Entity4?

    override suspend fun getNumEntities(): Int

    override suspend fun insertEntities(entities: List<Entity4>): LongArray
}