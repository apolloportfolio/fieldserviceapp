package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.values

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

/*
Material Design 2 color system defines a set of colors that are used to create a consistent visual
hierarchy in user interfaces. Here's an explanation of each color and its typical usage:

1. Primary: This is the main color used for branding or to represent the primary actions
in an interface.
It's often the dominant color throughout an app or website.

2. Primary Variant: This color is a variant of the primary color.
It can be used for highlighting certain elements or to provide visual contrast to the primary color.

3. Secondary: Secondary color is used to complement the primary color.
It can be used for secondary actions, accents, or to differentiate certain elements from the primary
ones.

4. Secondary Variant: Similar to the primary variant, this is a variant of the secondary color.
It's used for additional accents or to provide more variation in the interface.

5. Background: Background color is used for the overall background of the interface,
such as the background of cards, screens, or panels.

6. Surface: Surface color is used for the surfaces of components such as cards, dialogs, and sheets.
It provides a visual distinction between the background and the surfaces.

7. Error: Error color is used to indicate errors or alerts in the interface.
It's typically used for error messages, validation indicators, or other critical feedback.

8. On Primary: This color is used for text and icons that are placed on top of the primary color.
It ensures readability and contrast against the primary color.

9. On Secondary: Similar to "On Primary," this color is used for text and icons placed
on top of the secondary color, ensuring readability and contrast.

10. On Background: This color is used for text and icons that are placed on top of the background
color. It ensures readability and contrast against the background.

11. On Surface: Similar to "On Background," this color is used for text and icons that are placed
on top of the surface color, ensuring readability and contrast.

12. On Error: This color is used for text and icons that are placed on top of the error color.
It ensures readability and contrast against the error color, typically used for error messages
or alerts.

By defining these colors and their usage guidelines, Material Design 2 provides a consistent
and visually pleasing experience across different platforms and devices.
*/

private val DarkColorPalette = darkColors(
    primary = Color(0xFF3F51B5),
    primaryVariant = Color(0xFF03A9F4),
    secondary = Color(0xFFFFCC01),
    secondaryVariant = Color(0xFFFF9800),
    background = Color(0xFF121212),
    surface = Color(0xFF1E1E1E),
    error = Color(0xFFC53C55),
    onPrimary = Color(0xFFFFFFFF),
    onSecondary = Color(0xFF000000),
    onBackground = Color(0xFFFFFFFF),
    onSurface = Color(0xFFFFFFFF),
    onError = Color(0xFF000000),
)


private val LightColorPalette = lightColors(
    primary = Color(0xFF3F51B5),
    primaryVariant = Color(0xFF03A9F4),
    secondary = Color(0xFFFFCC01),
    secondaryVariant = Color(0xFFFF9800),
    background = Color(0xFFFFFFFF),
    surface = Color(0xFFFFFFFF),
    error = Color(0xFFC53C55),
    onPrimary = Color(0xFFFFFFFF),
    onSecondary = Color(0xFF000000),
    onBackground = Color(0xFF000000),
    onSurface = Color(0xFF000000),
    onError = Color(0xFF000000),
)


@Composable
fun HomeScreenTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit,
) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}