package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.aps.catemplateapp.common.util.extensions.updateValue

private const val TAG = "HomeScreenInteractionManager"
private const val LOG_ME = true

class HomeScreenInteractionManager {

    // Fragment 1 interaction states
    private val _searchQueryEdittextInteractionState: MutableLiveData<HomeScreenInteractionState>
            = MutableLiveData(HomeScreenInteractionState.InitialState())

    private val _searchOptionsBottomSheetRange1MinInteractionState: MutableLiveData<HomeScreenInteractionState>
            = MutableLiveData(HomeScreenInteractionState.InitialState())

    private val _searchOptionsBottomSheetRange1MaxInteractionState: MutableLiveData<HomeScreenInteractionState>
            = MutableLiveData(HomeScreenInteractionState.InitialState())

    private val _range1SeekbarInteractionState: MutableLiveData<HomeScreenInteractionState>
            = MutableLiveData(HomeScreenInteractionState.InitialState())

    private val _searchOptionsBottomSheetRange2MinInteractionState: MutableLiveData<HomeScreenInteractionState>
            = MutableLiveData(HomeScreenInteractionState.InitialState())

    private val _searchOptionsBottomSheetRange2MaxInteractionState: MutableLiveData<HomeScreenInteractionState>
            = MutableLiveData(HomeScreenInteractionState.InitialState())

    private val _range2SeekbarInteractionState: MutableLiveData<HomeScreenInteractionState>
            = MutableLiveData(HomeScreenInteractionState.InitialState())

    private val _switch1InteractionState: MutableLiveData<HomeScreenInteractionState>
            = MutableLiveData(HomeScreenInteractionState.InitialState())

    private val _switch2InteractionState: MutableLiveData<HomeScreenInteractionState>
            = MutableLiveData(HomeScreenInteractionState.InitialState())

    private val _switch3InteractionState: MutableLiveData<HomeScreenInteractionState>
            = MutableLiveData(HomeScreenInteractionState.InitialState())

    private val _switch4InteractionState: MutableLiveData<HomeScreenInteractionState>
            = MutableLiveData(HomeScreenInteractionState.InitialState())

    private val _switch5InteractionState: MutableLiveData<HomeScreenInteractionState>
            = MutableLiveData(HomeScreenInteractionState.InitialState())

    private val _switch6InteractionState: MutableLiveData<HomeScreenInteractionState>
            = MutableLiveData(HomeScreenInteractionState.InitialState())

    private val _switch7InteractionState: MutableLiveData<HomeScreenInteractionState>
            = MutableLiveData(HomeScreenInteractionState.InitialState())

    private val _collapsingToolbarState: MutableLiveData<CollapsingToolbarState>
            = MutableLiveData(CollapsingToolbarState.Expanded())


    // Fragment5 interaction states
    private val _usersNameEdittextInteractionState: MutableLiveData<HomeScreenInteractionState>
            = MutableLiveData(HomeScreenInteractionState.InitialState())

    private val _usersCityEdittextInteractionState: MutableLiveData<HomeScreenInteractionState>
            = MutableLiveData(HomeScreenInteractionState.InitialState())

    private val _usersDescriptionEdittextInteractionState: MutableLiveData<HomeScreenInteractionState>
            = MutableLiveData(HomeScreenInteractionState.InitialState())



    // Fragment 1 interaction states
    val searchQueryEdittextInteractionState: LiveData<HomeScreenInteractionState>
        get() = _searchQueryEdittextInteractionState

    val searchOptionsBottomSheetRange1MinInteractionState: LiveData<HomeScreenInteractionState>
        get() = _searchOptionsBottomSheetRange1MinInteractionState

    val searchOptionsBottomSheetRange1MaxInteractionState: LiveData<HomeScreenInteractionState>
        get() = _searchOptionsBottomSheetRange1MaxInteractionState

    val range1SeekbarInteractionState: LiveData<HomeScreenInteractionState>
        get() = _range1SeekbarInteractionState

    val searchOptionsBottomSheetRange2MinInteractionState: LiveData<HomeScreenInteractionState>
        get() = _searchOptionsBottomSheetRange2MinInteractionState

    val searchOptionsBottomSheetRange2MaxInteractionState: LiveData<HomeScreenInteractionState>
        get() = _searchOptionsBottomSheetRange2MaxInteractionState

    val range2SeekbarInteractionState: LiveData<HomeScreenInteractionState>
        get() = _range2SeekbarInteractionState

    val switch1InteractionState: LiveData<HomeScreenInteractionState>
        get() = _switch1InteractionState

    val switch2InteractionState: LiveData<HomeScreenInteractionState>
        get() = _switch2InteractionState

    val switch3InteractionState: LiveData<HomeScreenInteractionState>
        get() = _switch3InteractionState

    val switch4InteractionState: LiveData<HomeScreenInteractionState>
        get() = _switch4InteractionState

    val switch5InteractionState: LiveData<HomeScreenInteractionState>
        get() = _switch5InteractionState

    val switch6InteractionState: LiveData<HomeScreenInteractionState>
        get() = _switch6InteractionState

    val switch7InteractionState: LiveData<HomeScreenInteractionState>
        get() = _switch7InteractionState


    // Fragment5 interaction states
    val usersNameEdittextInteractionState: LiveData<HomeScreenInteractionState>
        get() = _usersNameEdittextInteractionState

    val usersCityEdittextInteractionState: LiveData<HomeScreenInteractionState>
        get() = _usersCityEdittextInteractionState

    val usersDescriptionEdittextInteractionState: LiveData<HomeScreenInteractionState>
        get() = _usersDescriptionEdittextInteractionState

    val collapsingToolbarState: LiveData<CollapsingToolbarState>
        get() = _collapsingToolbarState


    fun setCollapsingToolbarState(state: CollapsingToolbarState){
        if(!state.toString().equals(_collapsingToolbarState.value.toString())){
            _collapsingToolbarState.value = state
        }
    }
    
    private fun isNotInInitialState(interactionState : MutableLiveData<HomeScreenInteractionState>) =
        interactionState.value.toString() != HomeScreenInteractionState.InitialState().toString()
    
    private fun isInEditState(interactionState : MutableLiveData<HomeScreenInteractionState>) =
        interactionState.value.toString() == HomeScreenInteractionState.EditState().toString()
    
    private fun changeEditStateToDefaultState(interactionState : MutableLiveData<HomeScreenInteractionState>) {
        if(isInEditState(interactionState))interactionState.value =
            HomeScreenInteractionState.DefaultState()
    }

    fun setNewInputField1InteractionState(state: HomeScreenInteractionState){
        if(searchQueryEdittextInteractionState.toString() != state.toString()){
            _searchQueryEdittextInteractionState.value = state
            val defaultState = HomeScreenInteractionState.DefaultState()
            when(state){
                is HomeScreenInteractionState.EditState -> {
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MinInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MaxInteractionState)
                    changeEditStateToDefaultState( _range1SeekbarInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange2MinInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange2MaxInteractionState)
                    changeEditStateToDefaultState(_range2SeekbarInteractionState)
                    changeEditStateToDefaultState(_switch1InteractionState)
                    changeEditStateToDefaultState(_switch2InteractionState)
                    changeEditStateToDefaultState( _switch3InteractionState)
                    changeEditStateToDefaultState(_switch4InteractionState)
                    changeEditStateToDefaultState(_switch5InteractionState)
                    changeEditStateToDefaultState(_switch6InteractionState)
                    changeEditStateToDefaultState(_switch7InteractionState)
                    changeEditStateToDefaultState(_usersNameEdittextInteractionState)
                    changeEditStateToDefaultState(_usersDescriptionEdittextInteractionState)
                    changeEditStateToDefaultState(_usersCityEdittextInteractionState)
                } else -> {}
            }
        }
    }

    fun setNewInputField2InteractionState(state: HomeScreenInteractionState){
        val methodName: String = "setNewInputField2InteractionState(${state.toString()})"
        // If state has changed
        if(!searchOptionsBottomSheetRange1MinInteractionState.toString().equals(state.toString())){
            _searchOptionsBottomSheetRange1MinInteractionState.value = state
            val defaultState = HomeScreenInteractionState.DefaultState()
            when(state){
                is HomeScreenInteractionState.EditState -> {
                    changeEditStateToDefaultState(_searchQueryEdittextInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MaxInteractionState)
                    changeEditStateToDefaultState( _range1SeekbarInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange2MinInteractionState)
                    changeEditStateToDefaultState( _searchOptionsBottomSheetRange2MaxInteractionState)
                    changeEditStateToDefaultState(_range2SeekbarInteractionState)
                    changeEditStateToDefaultState(_switch1InteractionState)
                    changeEditStateToDefaultState(_switch2InteractionState)
                    changeEditStateToDefaultState( _switch3InteractionState)
                    changeEditStateToDefaultState(_switch4InteractionState)
                    changeEditStateToDefaultState(_switch5InteractionState)
                    changeEditStateToDefaultState(_switch6InteractionState)
                    changeEditStateToDefaultState(_switch7InteractionState)
                    changeEditStateToDefaultState(_usersNameEdittextInteractionState)
                    changeEditStateToDefaultState(_usersDescriptionEdittextInteractionState)
                    changeEditStateToDefaultState(_usersCityEdittextInteractionState)
                } else -> {}
            }
        }
    }

    fun setNewInputField3InteractionState(state: HomeScreenInteractionState){
        if(!searchOptionsBottomSheetRange1MaxInteractionState.toString().equals(state.toString())){
            _searchOptionsBottomSheetRange1MaxInteractionState.value = state
            val defaultState = HomeScreenInteractionState.DefaultState()
            when(state){
                is HomeScreenInteractionState.EditState -> {
                    changeEditStateToDefaultState(_searchQueryEdittextInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MinInteractionState)
                    changeEditStateToDefaultState( _range1SeekbarInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange2MinInteractionState)
                    changeEditStateToDefaultState( _searchOptionsBottomSheetRange2MaxInteractionState)
                    changeEditStateToDefaultState(_range2SeekbarInteractionState)
                    changeEditStateToDefaultState(_switch1InteractionState)
                    changeEditStateToDefaultState(_switch2InteractionState)
                    changeEditStateToDefaultState( _switch3InteractionState)
                    changeEditStateToDefaultState(_switch4InteractionState)
                    changeEditStateToDefaultState(_switch5InteractionState)
                    changeEditStateToDefaultState(_switch6InteractionState)
                    changeEditStateToDefaultState(_switch7InteractionState)
                    changeEditStateToDefaultState(_usersNameEdittextInteractionState)
                    changeEditStateToDefaultState(_usersDescriptionEdittextInteractionState)
                    changeEditStateToDefaultState(_usersCityEdittextInteractionState)
                } else -> {}
            }
        }
    }

    fun setNewInputField4InteractionState(state: HomeScreenInteractionState){
        if(!range1SeekbarInteractionState.toString().equals(state.toString())){
            _range1SeekbarInteractionState.value = state
            val defaultState = HomeScreenInteractionState.DefaultState()
            when(state){
                is HomeScreenInteractionState.EditState -> {
                    changeEditStateToDefaultState(_searchQueryEdittextInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MinInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MaxInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange2MinInteractionState)
                    changeEditStateToDefaultState( _searchOptionsBottomSheetRange2MaxInteractionState)
                    changeEditStateToDefaultState(_range2SeekbarInteractionState)
                    changeEditStateToDefaultState(_switch1InteractionState)
                    changeEditStateToDefaultState(_switch2InteractionState)
                    changeEditStateToDefaultState( _switch3InteractionState)
                    changeEditStateToDefaultState(_switch4InteractionState)
                    changeEditStateToDefaultState(_switch5InteractionState)
                    changeEditStateToDefaultState(_switch6InteractionState)
                    changeEditStateToDefaultState(_switch7InteractionState)
                    changeEditStateToDefaultState(_usersNameEdittextInteractionState)
                    changeEditStateToDefaultState(_usersDescriptionEdittextInteractionState)
                    changeEditStateToDefaultState(_usersCityEdittextInteractionState)
                } else -> {}
            }
        }
    }

    fun setNewInputField5InteractionState(state: HomeScreenInteractionState){
        if(!searchOptionsBottomSheetRange2MinInteractionState.toString().equals(state.toString())){
            _searchOptionsBottomSheetRange2MinInteractionState.value = state
            val defaultState = HomeScreenInteractionState.DefaultState()
            when(state){
                is HomeScreenInteractionState.EditState -> {
                    changeEditStateToDefaultState(_searchQueryEdittextInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MinInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MaxInteractionState)
                    changeEditStateToDefaultState( _range1SeekbarInteractionState)
                    changeEditStateToDefaultState( _searchOptionsBottomSheetRange2MaxInteractionState)
                    changeEditStateToDefaultState(_range2SeekbarInteractionState)
                    changeEditStateToDefaultState(_switch1InteractionState)
                    changeEditStateToDefaultState(_switch2InteractionState)
                    changeEditStateToDefaultState( _switch3InteractionState)
                    changeEditStateToDefaultState(_switch4InteractionState)
                    changeEditStateToDefaultState(_switch5InteractionState)
                    changeEditStateToDefaultState(_switch6InteractionState)
                    changeEditStateToDefaultState(_switch7InteractionState)
                    changeEditStateToDefaultState(_usersNameEdittextInteractionState)
                    changeEditStateToDefaultState(_usersDescriptionEdittextInteractionState)
                    changeEditStateToDefaultState(_usersCityEdittextInteractionState)
                } else -> {}
            }
        }
    }

    fun setNewInputField6InteractionState(state: HomeScreenInteractionState){
        if(!searchOptionsBottomSheetRange2MaxInteractionState.toString().equals(state.toString())){
            _searchOptionsBottomSheetRange2MaxInteractionState.value = state
            val defaultState = HomeScreenInteractionState.DefaultState()
            when(state){
                is HomeScreenInteractionState.EditState -> {
                    changeEditStateToDefaultState(_searchQueryEdittextInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MinInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MaxInteractionState)
                    changeEditStateToDefaultState( _range1SeekbarInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange2MinInteractionState)
                    changeEditStateToDefaultState(_range2SeekbarInteractionState)
                    changeEditStateToDefaultState(_switch1InteractionState)
                    changeEditStateToDefaultState(_switch2InteractionState)
                    changeEditStateToDefaultState( _switch3InteractionState)
                    changeEditStateToDefaultState(_switch4InteractionState)
                    changeEditStateToDefaultState(_switch5InteractionState)
                    changeEditStateToDefaultState(_switch6InteractionState)
                    changeEditStateToDefaultState(_switch7InteractionState)
                    changeEditStateToDefaultState(_usersNameEdittextInteractionState)
                    changeEditStateToDefaultState(_usersDescriptionEdittextInteractionState)
                    changeEditStateToDefaultState(_usersCityEdittextInteractionState)
                } else -> {}
            }
        }
    }

    fun setNewInputField7InteractionState(state: HomeScreenInteractionState){
        if(!range2SeekbarInteractionState.toString().equals(state.toString())){
            _range2SeekbarInteractionState.value = state
            val defaultState = HomeScreenInteractionState.DefaultState()
            when(state){
                is HomeScreenInteractionState.EditState -> {
                    changeEditStateToDefaultState(_searchQueryEdittextInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MinInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MaxInteractionState)
                    changeEditStateToDefaultState( _range1SeekbarInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange2MinInteractionState)
                    changeEditStateToDefaultState( _searchOptionsBottomSheetRange2MaxInteractionState)
                    changeEditStateToDefaultState(_switch1InteractionState)
                    changeEditStateToDefaultState(_switch2InteractionState)
                    changeEditStateToDefaultState( _switch3InteractionState)
                    changeEditStateToDefaultState(_switch4InteractionState)
                    changeEditStateToDefaultState(_switch5InteractionState)
                    changeEditStateToDefaultState(_switch6InteractionState)
                    changeEditStateToDefaultState(_switch7InteractionState)
                    changeEditStateToDefaultState(_usersNameEdittextInteractionState)
                    changeEditStateToDefaultState(_usersDescriptionEdittextInteractionState)
                    changeEditStateToDefaultState(_usersCityEdittextInteractionState)
                } else -> {}
            }
        }
    }

    fun setNewInputField8InteractionState(state: HomeScreenInteractionState){
        if(!switch1InteractionState.toString().equals(state.toString())){
            _switch1InteractionState.value = state
            val defaultState = HomeScreenInteractionState.DefaultState()
            when(state){
                is HomeScreenInteractionState.EditState -> {
                    changeEditStateToDefaultState(_searchQueryEdittextInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MinInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MaxInteractionState)
                    changeEditStateToDefaultState( _range1SeekbarInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange2MinInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange2MaxInteractionState)
                    changeEditStateToDefaultState(_range2SeekbarInteractionState)
                    changeEditStateToDefaultState(_switch2InteractionState)
                    changeEditStateToDefaultState( _switch3InteractionState)
                    changeEditStateToDefaultState(_switch4InteractionState)
                    changeEditStateToDefaultState(_switch5InteractionState)
                    changeEditStateToDefaultState(_switch6InteractionState)
                    changeEditStateToDefaultState(_switch7InteractionState)
                    changeEditStateToDefaultState(_usersNameEdittextInteractionState)
                    changeEditStateToDefaultState(_usersDescriptionEdittextInteractionState)
                    changeEditStateToDefaultState(_usersCityEdittextInteractionState)
                } else -> {}
            }
        }
    }

    fun setNewInputField9InteractionState(state: HomeScreenInteractionState){
        if(!switch2InteractionState.toString().equals(state.toString())){
            _switch2InteractionState.value = state
            val defaultState = HomeScreenInteractionState.DefaultState()
            when(state){
                is HomeScreenInteractionState.EditState -> {
                    changeEditStateToDefaultState(_searchQueryEdittextInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MinInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MaxInteractionState)
                    changeEditStateToDefaultState( _range1SeekbarInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange2MinInteractionState)
                    changeEditStateToDefaultState( _searchOptionsBottomSheetRange2MaxInteractionState)
                    changeEditStateToDefaultState(_range2SeekbarInteractionState)
                    changeEditStateToDefaultState(_switch1InteractionState)
                    changeEditStateToDefaultState( _switch3InteractionState)
                    changeEditStateToDefaultState(_switch4InteractionState)
                    changeEditStateToDefaultState(_switch5InteractionState)
                    changeEditStateToDefaultState(_switch6InteractionState)
                    changeEditStateToDefaultState(_switch7InteractionState)
                    changeEditStateToDefaultState(_usersNameEdittextInteractionState)
                    changeEditStateToDefaultState(_usersDescriptionEdittextInteractionState)
                    changeEditStateToDefaultState(_usersCityEdittextInteractionState)
                } else -> {}
            }
        }
    }

    fun setNewInputField10InteractionState(state: HomeScreenInteractionState){
        if(!switch3InteractionState.toString().equals(state.toString())){
            _switch3InteractionState.value = state
            val defaultState = HomeScreenInteractionState.DefaultState()
            when(state){
                is HomeScreenInteractionState.EditState -> {
                    changeEditStateToDefaultState(_searchQueryEdittextInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MinInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MaxInteractionState)
                    changeEditStateToDefaultState( _range1SeekbarInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange2MinInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange2MaxInteractionState)
                    changeEditStateToDefaultState(_range2SeekbarInteractionState)
                    changeEditStateToDefaultState(_switch1InteractionState)
                    changeEditStateToDefaultState(_switch2InteractionState)
                    changeEditStateToDefaultState(_switch4InteractionState)
                    changeEditStateToDefaultState(_switch5InteractionState)
                    changeEditStateToDefaultState(_switch6InteractionState)
                    changeEditStateToDefaultState(_switch7InteractionState)
                    changeEditStateToDefaultState(_usersNameEdittextInteractionState)
                    changeEditStateToDefaultState(_usersDescriptionEdittextInteractionState)
                    changeEditStateToDefaultState(_usersCityEdittextInteractionState)
                } else -> {}
            }
        }
    }

    fun setNewInputField11InteractionState(state: HomeScreenInteractionState){
        if(!switch4InteractionState.toString().equals(state.toString())){
            _switch4InteractionState.value = state
            val defaultState = HomeScreenInteractionState.DefaultState()
            when(state){
                is HomeScreenInteractionState.EditState -> {
                    changeEditStateToDefaultState(_searchQueryEdittextInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MinInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MaxInteractionState)
                    changeEditStateToDefaultState( _range1SeekbarInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange2MinInteractionState)
                    changeEditStateToDefaultState( _searchOptionsBottomSheetRange2MaxInteractionState)
                    changeEditStateToDefaultState(_range2SeekbarInteractionState)
                    changeEditStateToDefaultState(_switch1InteractionState)
                    changeEditStateToDefaultState(_switch2InteractionState)
                    changeEditStateToDefaultState( _switch3InteractionState)
                    changeEditStateToDefaultState(_switch5InteractionState)
                    changeEditStateToDefaultState(_switch6InteractionState)
                    changeEditStateToDefaultState(_switch7InteractionState)
                    changeEditStateToDefaultState(_usersNameEdittextInteractionState)
                    changeEditStateToDefaultState(_usersDescriptionEdittextInteractionState)
                    changeEditStateToDefaultState(_usersCityEdittextInteractionState)
                } else -> {}
            }
        }
    }

    fun setNewInputField12InteractionState(state: HomeScreenInteractionState){
        if(!switch5InteractionState.toString().equals(state.toString())){
            _switch5InteractionState.value = state
            val defaultState = HomeScreenInteractionState.DefaultState()
            when(state){
                is HomeScreenInteractionState.EditState -> {
                    changeEditStateToDefaultState(_searchQueryEdittextInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MinInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MaxInteractionState)
                    changeEditStateToDefaultState( _range1SeekbarInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange2MinInteractionState)
                    changeEditStateToDefaultState( _searchOptionsBottomSheetRange2MaxInteractionState)
                    changeEditStateToDefaultState(_range2SeekbarInteractionState)
                    changeEditStateToDefaultState(_switch1InteractionState)
                    changeEditStateToDefaultState(_switch2InteractionState)
                    changeEditStateToDefaultState( _switch3InteractionState)
                    changeEditStateToDefaultState(_switch4InteractionState)
                    changeEditStateToDefaultState(_switch6InteractionState)
                    changeEditStateToDefaultState(_switch7InteractionState)
                    changeEditStateToDefaultState(_usersNameEdittextInteractionState)
                    changeEditStateToDefaultState(_usersDescriptionEdittextInteractionState)
                    changeEditStateToDefaultState(_usersCityEdittextInteractionState)
                } else -> {}
            }
        }
    }

    fun setNewInputField13InteractionState(state: HomeScreenInteractionState){
        if(switch6InteractionState.toString() != state.toString()){
            _switch6InteractionState.value = state
            val defaultState = HomeScreenInteractionState.DefaultState()
            when(state){
                is HomeScreenInteractionState.EditState -> {
                    changeEditStateToDefaultState(_searchQueryEdittextInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MinInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MaxInteractionState)
                    changeEditStateToDefaultState( _range1SeekbarInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange2MinInteractionState)
                    changeEditStateToDefaultState( _searchOptionsBottomSheetRange2MaxInteractionState)
                    changeEditStateToDefaultState(_range2SeekbarInteractionState)
                    changeEditStateToDefaultState(_switch1InteractionState)
                    changeEditStateToDefaultState(_switch2InteractionState)
                    changeEditStateToDefaultState( _switch3InteractionState)
                    changeEditStateToDefaultState(_switch4InteractionState)
                    changeEditStateToDefaultState(_switch5InteractionState)
                    changeEditStateToDefaultState(_switch7InteractionState)
                    changeEditStateToDefaultState(_usersNameEdittextInteractionState)
                    changeEditStateToDefaultState(_usersDescriptionEdittextInteractionState)
                    changeEditStateToDefaultState(_usersCityEdittextInteractionState)
                } else -> {}
            }
        }
    }

    fun setNewInputField14InteractionState(state: HomeScreenInteractionState){
        if(switch7InteractionState.toString() != state.toString()){
            _switch7InteractionState.value = state
            val defaultState = HomeScreenInteractionState.DefaultState()
            when(state){
                is HomeScreenInteractionState.EditState -> {
                    changeEditStateToDefaultState(_searchQueryEdittextInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MinInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MaxInteractionState)
                    changeEditStateToDefaultState( _range1SeekbarInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange2MinInteractionState)
                    changeEditStateToDefaultState( _searchOptionsBottomSheetRange2MaxInteractionState)
                    changeEditStateToDefaultState(_range2SeekbarInteractionState)
                    changeEditStateToDefaultState(_switch1InteractionState)
                    changeEditStateToDefaultState(_switch2InteractionState)
                    changeEditStateToDefaultState( _switch3InteractionState)
                    changeEditStateToDefaultState(_switch4InteractionState)
                    changeEditStateToDefaultState(_switch5InteractionState)
                    changeEditStateToDefaultState(_switch6InteractionState)
                    changeEditStateToDefaultState(_usersNameEdittextInteractionState)
                    changeEditStateToDefaultState(_usersDescriptionEdittextInteractionState)
                    changeEditStateToDefaultState(_usersCityEdittextInteractionState)
                } else -> {}
            }
        }
    }

    fun setNewInputField15InteractionState(state: HomeScreenInteractionState){
        if(usersNameEdittextInteractionState.toString() != state.toString()){
            _usersNameEdittextInteractionState.value = state
            val defaultState = HomeScreenInteractionState.DefaultState()
            when(state){
                is HomeScreenInteractionState.EditState -> {
                    changeEditStateToDefaultState(_searchQueryEdittextInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MinInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MaxInteractionState)
                    changeEditStateToDefaultState( _range1SeekbarInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange2MinInteractionState)
                    changeEditStateToDefaultState( _searchOptionsBottomSheetRange2MaxInteractionState)
                    changeEditStateToDefaultState(_range2SeekbarInteractionState)
                    changeEditStateToDefaultState(_switch1InteractionState)
                    changeEditStateToDefaultState(_switch2InteractionState)
                    changeEditStateToDefaultState( _switch3InteractionState)
                    changeEditStateToDefaultState(_switch4InteractionState)
                    changeEditStateToDefaultState(_switch5InteractionState)
                    changeEditStateToDefaultState(_switch6InteractionState)
                    changeEditStateToDefaultState(_switch7InteractionState)
                    changeEditStateToDefaultState(_usersDescriptionEdittextInteractionState)
                    changeEditStateToDefaultState(_usersCityEdittextInteractionState)
                } else -> {}
            }
        }
    }

    fun setNewInputField16InteractionState(state: HomeScreenInteractionState){
        if(usersDescriptionEdittextInteractionState.toString() != state.toString()){
            _usersDescriptionEdittextInteractionState.value = state
            val defaultState = HomeScreenInteractionState.DefaultState()
            when(state){
                is HomeScreenInteractionState.EditState -> {
                    changeEditStateToDefaultState(_searchQueryEdittextInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MinInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MaxInteractionState)
                    changeEditStateToDefaultState( _range1SeekbarInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange2MinInteractionState)
                    changeEditStateToDefaultState( _searchOptionsBottomSheetRange2MaxInteractionState)
                    changeEditStateToDefaultState(_range2SeekbarInteractionState)
                    changeEditStateToDefaultState(_switch1InteractionState)
                    changeEditStateToDefaultState(_switch2InteractionState)
                    changeEditStateToDefaultState( _switch3InteractionState)
                    changeEditStateToDefaultState(_switch4InteractionState)
                    changeEditStateToDefaultState(_switch5InteractionState)
                    changeEditStateToDefaultState(_switch6InteractionState)
                    changeEditStateToDefaultState(_switch7InteractionState)
                    changeEditStateToDefaultState(_usersNameEdittextInteractionState)
                    changeEditStateToDefaultState(_usersCityEdittextInteractionState)
                } else -> {}
            }
        }
    }

    fun setNewInputField17InteractionState(state: HomeScreenInteractionState){
        if(usersCityEdittextInteractionState.toString() != state.toString()){
            _usersCityEdittextInteractionState.value = state
            val defaultState = HomeScreenInteractionState.DefaultState()
            when(state){
                is HomeScreenInteractionState.EditState -> {
                    changeEditStateToDefaultState(_searchQueryEdittextInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MinInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MaxInteractionState)
                    changeEditStateToDefaultState( _range1SeekbarInteractionState)
                    changeEditStateToDefaultState(_searchOptionsBottomSheetRange2MinInteractionState)
                    changeEditStateToDefaultState( _searchOptionsBottomSheetRange2MaxInteractionState)
                    changeEditStateToDefaultState(_range2SeekbarInteractionState)
                    changeEditStateToDefaultState(_switch1InteractionState)
                    changeEditStateToDefaultState(_switch2InteractionState)
                    changeEditStateToDefaultState( _switch3InteractionState)
                    changeEditStateToDefaultState(_switch4InteractionState)
                    changeEditStateToDefaultState(_switch5InteractionState)
                    changeEditStateToDefaultState(_switch6InteractionState)
                    changeEditStateToDefaultState(_switch7InteractionState)
                    changeEditStateToDefaultState(_usersNameEdittextInteractionState)
                    changeEditStateToDefaultState(_usersDescriptionEdittextInteractionState)
                } else -> {}
            }
        }
    }

    fun isEditingInputField1() = searchQueryEdittextInteractionState.value.toString().equals(
        HomeScreenInteractionState.EditState().toString())

    fun isEditingInputField2() : Boolean {
        return searchOptionsBottomSheetRange1MinInteractionState.value.toString().equals(
            HomeScreenInteractionState.EditState().toString())
    }

    fun isEditingInputField3() = searchOptionsBottomSheetRange1MaxInteractionState.value.toString().equals(
        HomeScreenInteractionState.EditState().toString())

    fun isEditingInputField4() = range1SeekbarInteractionState.value.toString().equals(
        HomeScreenInteractionState.EditState().toString())

    fun isEditingInputField5() = searchOptionsBottomSheetRange2MinInteractionState.value.toString().equals(
        HomeScreenInteractionState.EditState().toString())

    fun isEditingInputField6() = searchOptionsBottomSheetRange2MaxInteractionState.value.toString().equals(
        HomeScreenInteractionState.EditState().toString())

    fun isEditingInputField7() = range2SeekbarInteractionState.value.toString().equals(
        HomeScreenInteractionState.EditState().toString())

    fun isEditingInputField8() = switch1InteractionState.value.toString().equals(
        HomeScreenInteractionState.EditState().toString())

    fun isEditingInputField9() = switch2InteractionState.value.toString().equals(
        HomeScreenInteractionState.EditState().toString())

    fun isEditingInputField10() = switch3InteractionState.value.toString().equals(
        HomeScreenInteractionState.EditState().toString())

    fun isEditingInputField11() = switch4InteractionState.value.toString().equals(
        HomeScreenInteractionState.EditState().toString())

    fun isEditingInputField12() = switch5InteractionState.value.toString().equals(
        HomeScreenInteractionState.EditState().toString())

    fun isEditingInputField13() = switch6InteractionState.value.toString().equals(
        HomeScreenInteractionState.EditState().toString())

    fun isEditingInputField14() = switch7InteractionState.value.toString().equals(
        HomeScreenInteractionState.EditState().toString())

    fun isEditingInputField15() = usersNameEdittextInteractionState.value.toString().equals(
        HomeScreenInteractionState.EditState().toString())
    fun isEditingInputField16() = usersDescriptionEdittextInteractionState.value.toString().equals(
        HomeScreenInteractionState.EditState().toString())
    fun isEditingInputField17() = usersCityEdittextInteractionState.value.toString().equals(
        HomeScreenInteractionState.EditState().toString())

    fun exitEditState(){
        changeEditStateToDefaultState(_searchQueryEdittextInteractionState)
        changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MinInteractionState)
        changeEditStateToDefaultState(_searchOptionsBottomSheetRange1MaxInteractionState)
        changeEditStateToDefaultState( _range1SeekbarInteractionState)
        changeEditStateToDefaultState(_searchOptionsBottomSheetRange2MinInteractionState)
        changeEditStateToDefaultState( _searchOptionsBottomSheetRange2MaxInteractionState)
        changeEditStateToDefaultState(_range2SeekbarInteractionState)
        changeEditStateToDefaultState(_switch1InteractionState)
        changeEditStateToDefaultState(_switch2InteractionState)
        changeEditStateToDefaultState( _switch3InteractionState)
        changeEditStateToDefaultState(_switch4InteractionState)
        changeEditStateToDefaultState(_switch5InteractionState)
        changeEditStateToDefaultState(_switch6InteractionState)
        changeEditStateToDefaultState(_switch7InteractionState)
        changeEditStateToDefaultState(_usersNameEdittextInteractionState)
        changeEditStateToDefaultState(_usersDescriptionEdittextInteractionState)
        changeEditStateToDefaultState(_usersCityEdittextInteractionState)
    }
    
    fun setInitialState() {
        val initialState = HomeScreenInteractionState.InitialState()
        _searchQueryEdittextInteractionState.updateValue(initialState)
        _searchOptionsBottomSheetRange1MinInteractionState.updateValue(initialState)
        _searchOptionsBottomSheetRange1MaxInteractionState.updateValue(initialState)
        _range1SeekbarInteractionState.updateValue(initialState)
        _searchOptionsBottomSheetRange2MinInteractionState.updateValue(initialState)
        _searchOptionsBottomSheetRange2MaxInteractionState.updateValue(initialState)
        _range2SeekbarInteractionState.updateValue(initialState)
        _switch1InteractionState.updateValue(initialState)
        _switch2InteractionState.updateValue(initialState)
        _switch3InteractionState.updateValue(initialState)
        _switch4InteractionState.updateValue(initialState)
        _switch5InteractionState.updateValue(initialState)
        _switch6InteractionState.updateValue(initialState)
        _switch7InteractionState.updateValue(initialState)
        _usersNameEdittextInteractionState.updateValue(initialState)
        _usersDescriptionEdittextInteractionState.updateValue(initialState)
        _usersCityEdittextInteractionState.updateValue(initialState)
    }

    // return true if either of input fields are in EditState
    fun checkEditState() =
                searchQueryEdittextInteractionState.value.toString().equals(
                    HomeScreenInteractionState.EditState().toString())
                || searchOptionsBottomSheetRange1MinInteractionState.value.toString().equals(
                    HomeScreenInteractionState.EditState().toString())
                || searchOptionsBottomSheetRange1MaxInteractionState.value.toString().equals(
                    HomeScreenInteractionState.EditState().toString())
                || range1SeekbarInteractionState.value.toString().equals(
                    HomeScreenInteractionState.EditState().toString())
                || searchOptionsBottomSheetRange2MinInteractionState.value.toString().equals(
                    HomeScreenInteractionState.EditState().toString())
                || searchOptionsBottomSheetRange2MaxInteractionState.value.toString().equals(
                    HomeScreenInteractionState.EditState().toString())
                || range2SeekbarInteractionState.value.toString().equals(
                    HomeScreenInteractionState.EditState().toString())
                || switch1InteractionState.value.toString().equals(
                    HomeScreenInteractionState.EditState().toString())
                || switch2InteractionState.value.toString().equals(
                    HomeScreenInteractionState.EditState().toString())
                || switch3InteractionState.value.toString().equals(
                    HomeScreenInteractionState.EditState().toString())
                || switch4InteractionState.value.toString().equals(
                    HomeScreenInteractionState.EditState().toString())
                || switch5InteractionState.value.toString().equals(
                    HomeScreenInteractionState.EditState().toString())
                || switch6InteractionState.value.toString().equals(
                    HomeScreenInteractionState.EditState().toString())
                || switch7InteractionState.value.toString().equals(
                    HomeScreenInteractionState.EditState().toString())
                || usersNameEdittextInteractionState.value.toString().equals(
                    HomeScreenInteractionState.EditState().toString())
                || usersDescriptionEdittextInteractionState.value.toString().equals(
                    HomeScreenInteractionState.EditState().toString())
                || usersCityEdittextInteractionState.value.toString().equals(
                    HomeScreenInteractionState.EditState().toString())

    override fun toString(): String {
        return "searchQueryEdittextInteractionState == ${searchQueryEdittextInteractionState.value} \n" +
                "searchOptionsBottomSheetRange1MinInteractionState == ${searchOptionsBottomSheetRange1MinInteractionState.value} \n" +
                "searchOptionsBottomSheetRange1MaxInteractionState == ${searchOptionsBottomSheetRange1MaxInteractionState.value} \n" +
                "searchOptionsBottomSheetRange2MinInteractionState == ${searchOptionsBottomSheetRange2MinInteractionState.value} \n" +
                "searchOptionsBottomSheetRange2MaxInteractionState == ${searchOptionsBottomSheetRange2MaxInteractionState.value} \n" +
                "range2SeekbarInteractionState == ${range2SeekbarInteractionState.value} \n" +
                "switch1InteractionState == ${switch1InteractionState.value} \n" +
                "switch2InteractionState == ${switch2InteractionState.value} \n" +
                "switch3InteractionState == ${switch3InteractionState.value} \n" +
                "switch4InteractionState == ${switch4InteractionState.value} \n" +
                "switch5InteractionState == ${switch5InteractionState.value} \n" +
                "switch6InteractionState == ${switch6InteractionState.value} \n" +
                "switch7InteractionState == ${switch7InteractionState.value} \n" +
                "usersNameEdittextInteractionState == ${usersNameEdittextInteractionState.value} \n" +
                "usersDescriptionEdittextInteractionState == ${usersDescriptionEdittextInteractionState.value} \n" +
                "usersCityEdittextInteractionState == ${usersCityEdittextInteractionState.value} \n"
    }
}