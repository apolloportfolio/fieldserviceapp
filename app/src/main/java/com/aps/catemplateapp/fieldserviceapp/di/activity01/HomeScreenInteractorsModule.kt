package com.aps.catemplateapp.fieldserviceapp.di.activity01

import com.aps.catemplateapp.common.business.interactors.abstraction.DoNothingAtAll
import com.aps.catemplateapp.common.business.interactors.implementation.DoNothingAtAllImpl
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.fieldserviceapp.business.data.cache.abstraction.TaskAssignmentCacheDataSource
import com.aps.catemplateapp.fieldserviceapp.business.data.cache.abstraction.Entity2CacheDataSource
import com.aps.catemplateapp.fieldserviceapp.business.data.cache.abstraction.Entity3CacheDataSource
import com.aps.catemplateapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.catemplateapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.catemplateapp.fieldserviceapp.business.data.network.abs.TaskAssignmentNetworkDataSource
import com.aps.catemplateapp.fieldserviceapp.business.data.network.abs.Entity2NetworkDataSource
import com.aps.catemplateapp.fieldserviceapp.business.data.network.abs.Entity3NetworkDataSource
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.factories.TaskAssignmentFactory
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.factories.Entity2Factory
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.factories.Entity3Factory
import com.aps.catemplateapp.core.util.SecureKeyStorage
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.TaskAssignment
import com.aps.catemplateapp.fieldserviceapp.business.interactors.abs.*
import com.aps.catemplateapp.fieldserviceapp.business.interactors.impl.*
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenViewState
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@ExperimentalCoroutinesApi
@FlowPreview
@Module
@InstallIn(SingletonComponent::class)
object HomeScreenInteractorsModule {

    @Provides
    fun provideGetUsersRating(
        cacheDataSource: UserCacheDataSource,
        networkDataSource: UserNetworkDataSource,
    ): GetUsersRating {
        return GetUsersRatingImpl(cacheDataSource, networkDataSource)
    }

    @Provides
    fun InitiateGooglePayPaymentProcess(
        secureKeyStorage: SecureKeyStorage,
        dateUtil: DateUtil,
    ): InitiateGooglePayPaymentProcess {
        return InitiateGooglePayPaymentProcessImpl(secureKeyStorage, dateUtil)
    }

    @Provides
    fun provideGetMerchantName(
        secureKeyStorage: SecureKeyStorage,
        dateUtil: DateUtil,
    ): GetMerchantName {
        return GetMerchantNameImpl(secureKeyStorage, dateUtil)
    }

    @Provides
    fun provideGetGatewayNameAndMerchantID(
        secureKeyStorage: SecureKeyStorage,
        dateUtil: DateUtil,
    ): GetGatewayNameAndMerchantID {
        return GetGatewayNameAndMerchantIDImpl(secureKeyStorage, dateUtil)
    }

    @Provides
    fun provideDownloadExchangeRates(): DownloadExchangeRates {
        return DownloadExchangeRatesImpl()
    }

    @Provides
    fun provideCheckGooglePayAvailability(): CheckGooglePayAvailability {
        return CheckGooglePayAvailabilityImpl()
    }

    @Provides
    fun provideLogoutUser(
        userCacheDataSource: UserCacheDataSource,
        userNetworkDataSource: UserNetworkDataSource,
    ): LogoutUser {
        return LogoutUserImpl(
            userCacheDataSource,
            userNetworkDataSource,
        )
    }

    @Provides
    fun provideGetEntities1AroundUser(
        cacheDataSource: TaskAssignmentCacheDataSource,
        networkDataSource: TaskAssignmentNetworkDataSource,
        entityFactory: TaskAssignmentFactory
    ): TaskAssignmentsAroundUser {
        return TaskAssignmentsAroundUserImpl(
            cacheDataSource,
            networkDataSource,
            entityFactory,
        )
    }


    @Provides
    fun provideGetEntities2(
        cacheDataSource: TaskAssignmentCacheDataSource,
        networkDataSource: TaskAssignmentNetworkDataSource,
        entityFactory: TaskAssignmentFactory,
    ): GetUserTakenJobs {
        return GetUserTakenJobsImpl(
            cacheDataSource,
            networkDataSource,
            entityFactory,
        )
    }

    @Provides
    fun provideGetEntities3(
        cacheDataSource: TaskAssignmentCacheDataSource,
        networkDataSource: TaskAssignmentNetworkDataSource,
        entityFactory: TaskAssignmentFactory,
    ): GetUserDoneJobs {
        return GetUserDoneJobsImpl(
            cacheDataSource,
            networkDataSource,
            entityFactory,
        )
    }

    @Provides
    fun provideSearchEntities1(
        cacheDataSource: TaskAssignmentCacheDataSource,
        networkDataSource: TaskAssignmentNetworkDataSource,
        entityFactory: TaskAssignmentFactory
    ): SearchTaskAssignments {
        return SearchTaskAssignmentsImpl(
            cacheDataSource,
            networkDataSource,
        )
    }

    @Provides
    fun provideDoNothingAtAll(
        cacheDataSource: UserCacheDataSourceImpl,
        networkDataSource: UserNetworkDataSource
    ): DoNothingAtAll<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            HomeScreenViewState<ProjectUser>> {
        return DoNothingAtAllImpl<
                ProjectUser,
                UserCacheDataSourceImpl,
                UserNetworkDataSource,
                HomeScreenViewState<ProjectUser>>()
    }
}