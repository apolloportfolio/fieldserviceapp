package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.core.business.domain.model.currencies.Currency
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity2
import com.aps.catemplateapp.core.util.ProjectConstants
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import java.text.DecimalFormat

private const val TAG = "AdapterEntity2ToListItemType2"
private const val LOG_ME = true

class AdapterEntity2ToListItemType2(
    private var objectsList: ArrayList<Pair<Entity2, Long>>?,
    private var paymentCurrency: Currency,
    private val onItemClickListener: OnItemClickListener?,
    private val onThumbnailClickListener: OnItemClickListener?,
    private val dateUtil: DateUtil,
    )
    : RecyclerView.Adapter<AdapterEntity2ToListItemType2.ListItemType2ViewHolder>() {

    lateinit var context : Context

    fun getData() = objectsList
    fun setData(data : ArrayList<Pair<Entity2, Long>>?) {
        objectsList = data
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListItemType2ViewHolder {
        context = parent.context
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.list_item_type2,
            parent,
            false
        )
        return ListItemType2ViewHolder(itemView)
    }

    @SuppressLint("LongLogTag")
    override fun onBindViewHolder(holder: ListItemType2ViewHolder, position: Int) {
        val methodName: String = "onBindViewHolder"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(objectsList != null) {
                val entity2PairedWithSomeMonetaryValue = objectsList!![position]
                val entity2 = entity2PairedWithSomeMonetaryValue.first
                val someMonetaryValue = entity2PairedWithSomeMonetaryValue.second


                val dec = DecimalFormat("#,##0.00")
                val totalFee: Float = someMonetaryValue.toFloat()/100
                val valueToShow =
                    "${dec.format(totalFee)}$paymentCurrency"
                holder.number.text = valueToShow
                if(LOG_ME)ALog.d(TAG, ".$methodName(): feeToShow == $valueToShow")

                setImage(entity2, holder)
            } else {
                Log.w(TAG, "onBindViewHolder: objectsList == null", )
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override fun getItemCount(): Int {
        return objectsList?.size ?: 0
    }

    inner class ListItemType2ViewHolder(itemView : View)
        : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val imgThumbnail : ImageView = itemView.findViewById(R.id.item_thumbnail)
        val lblTitle : TextView = itemView.findViewById(R.id.lblInfo)
        val number : TextView = itemView.findViewById(R.id.lblNumber)
        init {
            if(onItemClickListener != null)itemView.setOnClickListener(this)
            if(onThumbnailClickListener != null)imgThumbnail.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                when(v) {
                    imgThumbnail -> {
                        onThumbnailClickListener?.onItemClick(position)
                        return
                    }
                }
                onItemClickListener?.onItemClick(position)
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

    private fun setImage(
        entity2 : Entity2,
        holder : ListItemType2ViewHolder
    ) {
        val methodName: String = "setImage"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(entity2.picture1URI == null) {
                if(LOG_ME)ALog.w(
                    TAG, ".$methodName(): " +
                        "rentalOffer.picture1URI == null")
                return
            }
            if(entity2.id == null) {
                if(LOG_ME)ALog.w(
                    TAG, ".$methodName(): " +
                        "rentalOffer.id == null")
                return
            }

            val glideOptions: RequestOptions = RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.ic_launcher)
                .error(R.drawable.ic_launcher)
                .diskCacheStrategy(DiskCacheStrategy.ALL)

            val storageReference = Firebase.storage
            val imageRef = storageReference.reference
                .child(ProjectConstants.FIRESTORE_IMAGES_COLLECTION)
                .child(ProjectConstants.FIRESTORE_ENTITY_2_IMAGES_SUB_COLLECTION)
                .child(entity2.id!!.firestoreDocumentID)
                .child(entity2.picture1URI!!)

            if (LOG_ME) ALog.d(TAG, ".$methodName(): $imageRef")

            Glide.with(holder.imgThumbnail)
                .load(imageRef)
                .apply(glideOptions)
                .into(holder.imgThumbnail)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }
}