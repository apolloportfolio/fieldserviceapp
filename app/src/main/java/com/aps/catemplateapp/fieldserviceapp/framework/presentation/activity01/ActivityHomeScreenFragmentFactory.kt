package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01

import androidx.fragment.app.FragmentFactory
import androidx.lifecycle.ViewModelProvider
import com.aps.catemplateapp.common.framework.presentation.UIController
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.fragments.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject

private const val TAG = "ActivityHomeScreenFragmentFactory"
private const val LOG_ME = true

@ExperimentalCoroutinesApi
@FlowPreview
class ActivityHomeScreenFragmentFactory
@Inject
constructor(
        private val viewModelFactory: ViewModelProvider.Factory,
        private val dateUtil: DateUtil
): FragmentFactory() {

    lateinit var uiController: UIController
    override fun instantiate(classLoader: ClassLoader, className: String) =
        when(className){
            ActivityHomeScreenFragment1::class.java.name -> {
                val fragment = ActivityHomeScreenFragment1(viewModelFactory, dateUtil)
                if(::uiController.isInitialized){
                    fragment.setUIController(uiController)
                    if(LOG_ME) ALog.d(
                        TAG, "instantiate(): " +
                            "ActivityHomeScreenFragment1.uiController initialized")
                }
                fragment
            }
			
            ActivityHomeScreenFragment2::class.java.name -> {
                val fragment = ActivityHomeScreenFragment2(viewModelFactory, dateUtil)
                if(::uiController.isInitialized){
                    fragment.setUIController(uiController)
                    if(LOG_ME) ALog.d(
                        TAG, "instantiate(): " +
                            "ActivityHomeScreenFragment2.uiController initialized")
                }
                fragment
            }
			
            ActivityHomeScreenFragment3::class.java.name -> {
                val fragment = ActivityHomeScreenFragment3(viewModelFactory, dateUtil)
                if(::uiController.isInitialized){
                    fragment.setUIController(uiController)
                    if(LOG_ME) ALog.d(
                        TAG, "instantiate(): " +
                            "ActivityHomeScreenFragment3.uiController initialized")
                }
                fragment
            }
			
            ActivityHomeScreenFragment4::class.java.name -> {
                val fragment = ActivityHomeScreenFragment4(viewModelFactory, dateUtil)
                if(::uiController.isInitialized){
                    fragment.setUIController(uiController)
                    if(LOG_ME) ALog.d(
                        TAG, "instantiate(): " +
                            "ActivityHomeScreenFragment4.uiController initialized")
                }
                fragment
            }

            ActivityHomeScreenFragment5::class.java.name -> {
                val fragment = ActivityHomeScreenFragment5(viewModelFactory, dateUtil)
                if(::uiController.isInitialized){
                    fragment.setUIController(uiController)
                    if(LOG_ME) ALog.d(
                        TAG, "instantiate(): " +
                            "ActivityHomeScreenFragment5.uiController initialized")
                }
                fragment
            }
			
            else -> {
                if(LOG_ME) ALog.w(
                    TAG, "instantiate(): " +
                        "Unknown class $className, uiController uninitialized")
                super.instantiate(classLoader, className)
            }
        }
}