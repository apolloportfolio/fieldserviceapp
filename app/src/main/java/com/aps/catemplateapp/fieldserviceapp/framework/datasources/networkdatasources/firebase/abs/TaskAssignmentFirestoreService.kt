package com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.abs

import android.net.Uri
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.BasicFirestoreService
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.TaskAssignment
import com.aps.catemplateapp.fieldserviceapp.business.interactors.impl.FirestoreEntity1SearchParameters
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.mappers.TaskAssignmentFirestoreMapper
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.model.TaskAssignmentFirestoreEntity

interface TaskAssignmentFirestoreService: BasicFirestoreService<
        TaskAssignment,
        TaskAssignmentFirestoreEntity,
        TaskAssignmentFirestoreMapper
        > {
    suspend fun searchEntities(
        searchParameters : FirestoreEntity1SearchParameters
    ) : List<TaskAssignment>?

    suspend fun getUsersEntities1(userID: UserUniqueID): List<TaskAssignment>?

    suspend fun getUserDoneJobs(userID: UserUniqueID): List<TaskAssignment>?

    suspend fun uploadEntity1PhotoToFirestore(
        entity : TaskAssignment,
        entitysPhotoUri : Uri,
        entitysPhotoNumber: Int,
    ) : String?
}