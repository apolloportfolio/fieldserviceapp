package com.aps.catemplateapp.fieldserviceapp.business.interactors.impl

import android.location.Location
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.core.util.ProjectConstants
import com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.state.HomeScreenViewState
import com.ckdroid.geofirequery.utils.BoundingBoxUtils

private const val TAG = "FirestoreEntity1SearchParameters"
private const val LOG_ME = true
class FirestoreEntity1SearchParameters(
    searchParameters : HomeScreenViewState<ProjectUser>?,
    val location : Location?,
    val searchRadius : Double = ProjectConstants.DEFAULT_SEARCH_RADIUS_KM,
    val searchRadiusUnit : BoundingBoxUtils.DistanceUnit = BoundingBoxUtils.DistanceUnit.KILOMETERS
) {
    var searchQuery : ArrayList<String>? = null
    var range1Min: Double? = null
    var range1Max: Double? = null
    var range2Min: Double? = null
    var range2Max: Double? = null
    var switch1: Boolean? = null
    var switch2: Boolean? = null
    var switch3: Boolean? = null
    var switch4: Boolean? = null
    var switch5: Boolean? = null
    var switch6: Boolean? = null
    var switch7: Boolean? = null

    init {
        if(searchParameters?.availableJobsSearchQuery != null && searchParameters?.availableJobsSearchQuery != "")
            searchQuery = ArrayList(searchParameters.availableJobsSearchQuery!!.split(" "))
        if(searchParameters != null) {
            this.range1Min = searchParameters.range1min
            this.range1Max = searchParameters.range1max
            this.range2Min = searchParameters.range2min
            this.range2Max = searchParameters.range2max
            this.switch1 = searchParameters.switch1
            this.switch2 = searchParameters.switch2
            this.switch3 = searchParameters.switch3
            this.switch4 = searchParameters.switch4
            this.switch5 = searchParameters.switch5
            this.switch6 = searchParameters.switch6
            this.switch7 = searchParameters.switch7
        } else {
            ALog.w(TAG, ".init(): searchParameters == null")
        }
    }

    override fun toString() : String {
        return "location == ${location.toString()}; \n" +
                "searchRadius == $searchRadius $searchRadiusUnit; \n" +
                "searchQuery == $searchQuery; \n" +
                "range1Min == $range1Min; \n" +
                "range1Max == $range1Max; \n" +
                "range2Min == $range2Min; \n" +
                "range2Max == $range2Max; \n" +
                "switch1 == $switch1; \n" +
                "switch2 == $switch2; \n" +
                "switch3 == $switch3; \n" +
                "switch4 == $switch4; \n" +
                "switch5 == $switch5; \n" +
                "switch6 == $switch6; \n" +
                "switch7 == $switch7;\n"

    }
}