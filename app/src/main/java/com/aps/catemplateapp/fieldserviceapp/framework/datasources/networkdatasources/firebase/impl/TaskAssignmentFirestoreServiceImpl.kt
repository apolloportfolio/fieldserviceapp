package com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.impl

import android.net.Uri
import android.util.Log
import com.aps.catemplateapp.common.business.data.util.cLog
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.common.util.extensions.getSizeString
import com.aps.catemplateapp.common.util.extensions.whereNearToLocation
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.implementation.BasicFirestoreServiceImpl
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.TaskAssignment
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.abs.TaskAssignmentFirestoreService
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.mappers.TaskAssignmentFirestoreMapper
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.model.TaskAssignmentFirestoreEntity
import com.aps.catemplateapp.core.util.ProjectConstants
import com.aps.catemplateapp.fieldserviceapp.business.interactors.impl.FirestoreEntity1SearchParameters
import com.ckdroid.geofirequery.model.Distance
import com.ckdroid.geofirequery.model.QueryLocation
import com.ckdroid.geofirequery.setLocation
import com.google.firebase.Timestamp
import com.google.firebase.firestore.Query
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import kotlinx.coroutines.tasks.await
import java.util.Locale
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class TaskAssignmentFirestoreServiceImpl
@Inject
constructor(
//    private val firebaseAuth: FirebaseAuth,
//    private val firestore: FirebaseFirestore,
    private val entity1NetworkMapper: TaskAssignmentFirestoreMapper,
    private val dateUtil: DateUtil,
): TaskAssignmentFirestoreService,
    BasicFirestoreServiceImpl<TaskAssignment, TaskAssignmentFirestoreEntity, TaskAssignmentFirestoreMapper>(
//        firebaseAuth,
//        firestore,
        entity1NetworkMapper,
        dateUtil,
    )
        {
    override fun getCollectionName(): String {
        return COLLECTION_NAME
    }

    override fun getDeletesCollectionName(): String {
        return DELETES_COLLECTION_NAME
    }

    override fun setEntityID(entity: TaskAssignment, id: UniqueID) {
        entity.id = id
    }

    override fun getEntityID(entity: TaskAssignment): UniqueID? {
        return entity.id
    }

    override fun setFirestoreEntityID(entity: TaskAssignmentFirestoreEntity, id: UniqueID) {
        entity.id = id
    }

    override fun updateEntityTimestamp(entity: TaskAssignment, timestamp: String){
        entity.updated_at = timestamp
    }

    override fun updateFirestoreEntityTimestamp(entity: TaskAssignmentFirestoreEntity, timestamp: String){
        entity.updated_at = timestamp
    }

    override fun getFirestoreEntityType(): Class<TaskAssignmentFirestoreEntity> {
        return TaskAssignmentFirestoreEntity::class.java
    }

    override fun getFirestoreEntityID(entity: TaskAssignmentFirestoreEntity): UniqueID? {
        return entity.id
    }

    override suspend fun searchEntities(
        searchParameters : FirestoreEntity1SearchParameters
    ) : List<TaskAssignment>? {
        val methodName = "searchEntities"
        val result: List<TaskAssignment>?
        if(LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(LOG_ME)Log.d(TAG, "$methodName(): searchParameters == $searchParameters")
            val entity1Ref = firestore.collection(getCollectionName())
            var entityQuery : Query = entity1Ref

            if(LOG_ME)ALog.d(
                TAG, ".$methodName(): " +
                                    "entity1Ref == $entity1Ref")

            if(LOG_ME)ALog.d(
                TAG, ".$methodName(): " +
                    "firestore.firestoreSettings.host == ${firestore.firestoreSettings.host}")

            // Search entities around location from searchParameters
            // https://medium.com/android-kotlin/geo-firestore-query-with-native-query-bfb0ac1adf0a
            // https://firebase.google.com/docs/firestore/solutions/geoqueries
            // https://firebaseopensource.com/projects/firebase/geofire-android/
            if(searchParameters.location != null) {
                if(LOG_ME)ALog.d(
                    TAG, "searchParameters.searchRadius == ${searchParameters.searchRadius}\n" +
                        "latitude == ${searchParameters.location.latitude}\n" +
                        "longitude == ${searchParameters.location.longitude}\n" +
                        "LOCATION_FIELD_NAME == ${TaskAssignment.Entity1Constants.LOCATION_FIELD_NAME}")
                val distanceForRadius = Distance(
                    searchParameters.searchRadius,
                    searchParameters.searchRadiusUnit,
                )
                val queryAtLocation = QueryLocation.fromDegrees(
                    searchParameters.location.latitude,
                    searchParameters.location.longitude
                )
                entityQuery = entityQuery.whereNearToLocation(
                    queryAtLocation,
                    distanceForRadius,
                    TaskAssignment.Entity1Constants.LOCATION_FIELD_NAME
                )
            }

            entityQuery = entityQuery.whereNotEqualTo(
                    "firestoreGeoLocation",
                    null,
                )

            if(searchParameters.switch1 != null){
                entityQuery = entityQuery.whereEqualTo(
                    "switch1",
                    searchParameters.switch1!!
                )
            }
            if(searchParameters.switch2 != null){
                entityQuery = entityQuery.whereEqualTo(
                    "switch2",
                    searchParameters.switch2!!
                )
            }
            if(searchParameters.switch3 != null){
                entityQuery = entityQuery.whereEqualTo(
                    "switch3",
                    searchParameters.switch3!!
                )
            }
            if(searchParameters.switch4 != null){
                entityQuery = entityQuery.whereEqualTo(
                    "switch4",
                    searchParameters.switch4!!
                )
            }
            if(searchParameters.switch5 != null){
                entityQuery = entityQuery.whereEqualTo(
                    "switch5",
                    searchParameters.switch5!!
                )
            }
            if(searchParameters.switch6 != null){
                entityQuery = entityQuery.whereEqualTo(
                    "switch6",
                    searchParameters.switch6!!
                )
            }
            if(searchParameters.switch7 != null){
                entityQuery = entityQuery.whereEqualTo(
                    "switch7",
                    searchParameters.switch7!!
                )
            }


            // Searching by keywords
            // Reference: https://firebase.google.com/docs/firestore/query-data/queries
            if(searchParameters.searchQuery != null) {
                var searchQuery = ""
                for(word in searchParameters.searchQuery!!)searchQuery += "$word "
                searchQuery = searchQuery.lowercase(Locale.ROOT)
                if(LOG_ME)ALog.d(
                    TAG, ".$methodName(): " +
                        "whereArrayContains: searchQuery == $searchQuery")
                entityQuery = entityQuery.whereArrayContains("keywords", searchQuery)
            }



            result = entity1NetworkMapper.mapFromEntityList(
                entityQuery
                    .get()
                    .addOnFailureListener {
                        // send error reports to Firebase Crashlytics
                        if (it != null) {
                            cLog(it.message)
                        }
                        ALog.w(TAG, "$methodName(): Failed to search entities1.")
                    }
                    .addOnSuccessListener {
                        if(LOG_ME)ALog.d(TAG, "$methodName(): Successfully searched for entities1.")
                    }
                    .await()
                    .toObjects(TaskAssignmentFirestoreEntity::class.java)
            )
            if(LOG_ME) ALog.d(TAG, ".$methodName(): Before filtering: result.size == ${result.size}")
//            result = filterSearchResults(result, searchParameters)
            if(LOG_ME) {
                ALog.d(TAG, ".$methodName(): After filtering: result.size == ${result.size}")
            }
            return result
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            e.printStackTrace()
            return null
        } finally {
            ALog.d(TAG, "Method end: $methodName")
        }
    }

//    private fun filterSearchResults(
//        entities : List<Entity1>,
//        searchParameters : SearchEntities1Impl.FirestoreEntity1SearchParameters
//    ) : List<Entity1> {
//        val methodName: String = "filterSearchResults()"
//        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
//        var result : ArrayList<Entity1> = ArrayList<Entity1>()
//        try {
//            if(entities != null) {
//                for(entity1 in entities) {
//
//                    if(searchParameters.range1Min != null)
//                        if(entity1.range1Min!! < searchParameters.range1Min!!)continue
//                    if(searchParameters.range1Max != null)
//                        if(entity1.range1Max!! > searchParameters.range1Max!!)continue
//                    if(searchParameters.range2Min != null)
//                        if(entity1.range2Min!! < searchParameters.range2Min!!)continue
//                    if(searchParameters.range2Max != null)
//                        if(entity1.range2Max!! > searchParameters.range2Max!!)continue
//
//                    if(searchParameters.switch1 != null)
//                        if(entity1.switch1 != searchParameters.switch1)continue
//                    if(searchParameters.switch2 != null)
//                        if(entity1.switch2 != searchParameters.switch2)continue
//                    if(searchParameters.switch3 != null)
//                        if(entity1.switch3 != searchParameters.switch3)continue
//                    if(searchParameters.switch4 != null)
//                        if(entity1.switch4 != searchParameters.switch4)continue
//                    if(searchParameters.switch5 != null)
//                        if(entity1.switch5 != searchParameters.switch5)continue
//                    if(searchParameters.switch6 != null)
//                        if(entity1.switch6 != searchParameters.switch6)continue
//                    if(searchParameters.switch7 != null)
//                        if(entity1.switch7 != searchParameters.switch7)continue
//
//                    result.add(entity1)
//                }
//                return result
//            } else {
//                ALog.d(TAG, ".$methodName(): entities == null")
//            }
//        } catch (e: Exception) {
//            ALog.e(TAG, methodName, e)
//        } finally {
//            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
//        }
//        return entities
//    }

    // Method adds new entity1 or updates existing one.
    override suspend fun insertOrUpdateEntity(entity: TaskAssignment): TaskAssignment? {
        val methodName = "insertOrUpdateEntity"
        if(LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val entity1NetworkEntity = entity1NetworkMapper.mapToEntity(entity)
            entity1NetworkEntity.generateKeywords()
            val timestampNow = dateUtil.convertFirebaseTimestampToStringData(Timestamp.now())
            updateEntityTimestamp(entity, timestampNow)
            updateFirestoreEntityTimestamp(entity1NetworkEntity, timestampNow)


            if(getEntityID(entity) == null) {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): We are adding new entity")
                val newlyGeneratedEntity1ID = firestore.collection(getCollectionName()).document().id
                val entity1ID = UniqueID(newlyGeneratedEntity1ID)
                setEntityID(entity, entity1ID)
                setFirestoreEntityID(entity1NetworkEntity,UniqueID(newlyGeneratedEntity1ID))

                val entity1Document = firestore
                    .collection(getCollectionName())
                    .document(getEntityID(entity)!!.getFirestoreId())

                // Running batch to add the entity.
                firestore.runBatch { batch ->
                    batch.set(entity1Document, entity1NetworkEntity)
                }
                .addOnFailureListener {
                    // send error reports to Firebase Crashlytics
                    cLog(it.message)
                    ALog.w(TAG, "$methodName(): Failed to run batch inserting new entity1.")
                }
                .addOnSuccessListener {
                    if(LOG_ME) ALog.d(
                        TAG,
                        "$methodName(): " +
                                "Successfully inserted new entity1. Now setting it's location."
                    )
                    // https://medium.com/android-kotlin/geo-firestore-query-with-native-query-bfb0ac1adf0a
                    if(entity.latitude != null && entity.longitude != null){
                        entity1Document.setLocation(
                            entity.latitude!!,
                            entity.longitude!!,
                            TaskAssignment.Entity1Constants.LOCATION_FIELD_NAME,
                        )
                    } else {
                        if(LOG_ME)ALog.d(
                            TAG, ".$methodName(): " +
                                "entity1.latitude == ${entity.latitude}    " +
                                "entity1.longitude == ${entity.longitude}")
                    }
                }
                .await()
                return entity
            } else {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): We are updating existing entity")
                val entity1Document = firestore
                    .collection(getCollectionName())
                    .document(getEntityID(entity)!!.getFirestoreId())
                entity1Document.set(entity1NetworkEntity)
                    .addOnFailureListener {
                        // send error reports to Firebase Crashlytics
                        cLog(it.message)
                        ALog.w(TAG, "$methodName(): Failed to set entity.")
                    }
                    .addOnSuccessListener {
                        if(LOG_ME) ALog.d(
                            TAG,
                            "$methodName(): Successfully set entity. Now setting it's location."
                        )
                        // https://medium.com/android-kotlin/geo-firestore-query-with-native-query-bfb0ac1adf0a
                        if(entity.latitude != null && entity.longitude != null){
                            entity1Document.setLocation(
                                entity.latitude!!,
                                entity.longitude!!,
                                TaskAssignment.Entity1Constants.LOCATION_FIELD_NAME,
                            )
                        }
                    }
                    .await()
                return entity
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return null
        } finally {
            if(LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    // https://firebase.google.com/docs/storage/android/upload-files#kotlin+ktx
    override suspend fun uploadEntity1PhotoToFirestore(
        entity : TaskAssignment,
        entitysPhotoUri : Uri,
        entitysPhotoNumber: Int,
    ) : String? {
        val methodName = "uploadEntity1PhotoToFirestore"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        var result : String? = null
        var entities1PictureRef: StorageReference? = null
        try {
            val storage = Firebase.storage
            val storageRef = storage.reference
            if(entitysPhotoUri.path != null) {
                entities1PictureRef = storageRef.child(
                    "${ProjectConstants.FIRESTORE_IMAGES_COLLECTION}/" +
                            "${ProjectConstants.FIRESTORE_ENTITY_1_IMAGES_SUB_COLLECTION}/" +
                            "${entity.id!!.firestoreDocumentID}/$entitysPhotoNumber"
                )
                val uploadTask = entities1PictureRef.putFile(entitysPhotoUri)

                uploadTask.continueWithTask { task ->
                    if (!task.isSuccessful) {
                        task.exception?.let {
                            throw it
                        }
                    }
                    entities1PictureRef.downloadUrl
                }.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        if(LOG_ME)ALog.d(
                            TAG, ".$methodName(): Upload successful." +
                                "task.result.path == ${task.result.path}")
//                        result = task.result.path
                    } else {
                        ALog.e(TAG, ".$methodName(): Failed to upload user's profile picture.")
                    }
                }.await()

                result = "$entitysPhotoNumber"
            } else {
                ALog.e(TAG, ".$methodName(): usersProfilePhotoUri.path == null")
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if(LOG_ME)ALog.d(
                TAG, "Method end: $methodName: " +
                    "\n entities1PhotoUri == $entitysPhotoUri" +
                    "\n entities1PictureRef == $entities1PictureRef" +
                    "\n result == $result")
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName result == $result")
        }
        return result
    }

    override suspend fun getUsersEntities1(userID: UserUniqueID): List<TaskAssignment> {
        val methodName = "getUsersEntities1"
        val result: List<TaskAssignment>
        ALog.d(TAG, "Method start: $methodName")
        try {
            val collectionName = getCollectionName()
            result = entity1NetworkMapper.mapFromEntityList(
                firestore
                    .collection(collectionName)
                    .whereEqualTo(TaskAssignment.ownerIDFieldName, userID)
                    .get()
                    .addOnFailureListener {
                        // send error reports to Firebase Crashlytics
                        cLog(it.message)
                        ALog.w(TAG, "$methodName(): Failed to get user's entities1.")
                    }
                    .addOnSuccessListener {
                        if(LOG_ME)ALog.d(TAG, "$methodName(): Successfully got all user's entities1.")
                    }
                    .await()
                    .toObjects(getFirestoreEntityType())
            )
            if(LOG_ME)ALog.d(
                TAG, ".$methodName(): " +
                    "Got ${result.getSizeString()} entities1 " +
                    "belonging to user: $userID stored in $collectionName")
            return result
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return emptyList()
        } finally {
            ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun getUserDoneJobs(userID: UserUniqueID): List<TaskAssignment> {
        val methodName = "getUserDoneJobs"
        val result: List<TaskAssignment>
        ALog.d(TAG, "Method start: $methodName")
        try {
            val collectionName = getCollectionName()
            result = entity1NetworkMapper.mapFromEntityList(
                firestore
                    .collection(collectionName)
                    .whereEqualTo(TaskAssignment.ownerIDFieldName, userID)
                    .whereEqualTo(TaskAssignment.isDoneFieldName, true)
                    .get()
                    .addOnFailureListener {
                        // send error reports to Firebase Crashlytics
                        cLog(it.message)
                        ALog.w(TAG, "$methodName(): Failed to get user's entities1.")
                    }
                    .addOnSuccessListener {
                        if(LOG_ME)ALog.d(TAG, "$methodName(): Successfully got all user's entities1.")
                    }
                    .await()
                    .toObjects(getFirestoreEntityType())
            )
            if(LOG_ME)ALog.d(
                TAG, ".$methodName(): " +
                        "Got ${result.getSizeString()} entities1 " +
                        "belonging to user: $userID stored in $collectionName")
            return result
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return emptyList()
        } finally {
            ALog.d(TAG, "Method end: $methodName")
        }
    }

    companion object {
        const val TAG = "Entity1FirestoreServiceImpl"
        const val LOG_ME = true
        const val COLLECTION_NAME = "entity1"
        const val DELETES_COLLECTION_NAME = "entity1_d"
    }
}
