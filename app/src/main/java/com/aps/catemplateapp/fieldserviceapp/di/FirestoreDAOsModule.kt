package com.aps.catemplateapp.fieldserviceapp.di

import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.*
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.implementation.*
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.abs.TaskAssignmentFirestoreService
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.abs.Entity2FirestoreService
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.abs.Entity3FirestoreService
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.abs.Entity4FirestoreService
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.impl.TaskAssignmentFirestoreServiceImpl
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.impl.Entity2FirestoreServiceImpl
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.impl.Entity3FirestoreServiceImpl
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.impl.Entity4FirestoreServiceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class FirestoreDAOsModule {

    @Binds
    abstract fun bindTaskAssignmentFirestoreService(implementation: TaskAssignmentFirestoreServiceImpl): TaskAssignmentFirestoreService

    @Binds
    abstract fun bindEntity2FirestoreService(implementation: Entity2FirestoreServiceImpl): Entity2FirestoreService

    @Binds
    abstract fun bindEntity3FirestoreService(implementation: Entity3FirestoreServiceImpl): Entity3FirestoreService

    @Binds
    abstract fun bindEntity4FirestoreService(implementation: Entity4FirestoreServiceImpl): Entity4FirestoreService


}