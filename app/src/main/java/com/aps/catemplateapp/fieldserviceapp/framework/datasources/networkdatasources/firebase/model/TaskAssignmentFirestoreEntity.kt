package com.aps.catemplateapp.fieldserviceapp.framework.datasources.networkdatasources.firebase.model

import androidx.room.Entity
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.google.firebase.firestore.GeoPoint
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

private const val TAG = "TaskAssignmentFirestoreEntity"
private const val LOG_ME = true

@Entity(tableName = "taskassignment")
data class TaskAssignmentFirestoreEntity(
    @SerializedName("id")
    @Expose
    var id : UniqueID?,

    @SerializedName("created_at")
    @Expose
    val created_at: String?,

    @SerializedName("updated_at")
    @Expose
    var updated_at: String?,

    @SerializedName("taskDescription")
    @Expose
    var taskDescription : String?,

    @SerializedName("taskAddress")
    @Expose
    var taskAddress : String?,

    @SerializedName("clientName")
    @Expose
    var clientName : String?,

    @SerializedName("phoneNumber")
    @Expose
    var phoneNumber : String?,

    @SerializedName("scheduledTaskStartTimeStamp")
    @Expose
    var scheduledTaskStartTimeStamp : String?,

    @SerializedName("expectedTaskDuration")
    @Expose
    var expectedTaskDuration : String?,

    @SerializedName("actualTaskStartTimeStamp")
    @Expose
    var actualTaskStartTimeStamp : String?,

    @SerializedName("actualTaskEndTimeStamp")
    @Expose
    var actualTaskEndTimeStamp : String?,

    @SerializedName("isTaskCompleted")
    @Expose
    var isTaskCompleted : Boolean?,

    @SerializedName("taskNotes")
    @Expose
    var taskNotes : String?,

    @SerializedName("customerFeedbackGrade")
    @Expose
    var customerFeedbackGrade : Int?,

    @SerializedName("customerFeedbackGradeReview")
    @Expose
    var customerFeedbackGradeReview : String?,

    @SerializedName("taskResultsPicturesURIs")
    @Expose
    var taskResultsPicturesURIs : String?,

    @SerializedName("materialsNote")
    @Expose
    var materialsNote : String?,

    @SerializedName("jobPriority")
    @Expose
    var jobPriority : Int?,

    @SerializedName("lattitude")
    @Expose
    var lattitude : Double?,

    @SerializedName("longitude")
    @Expose
    var longitude: Double?,

    @SerializedName("geoLocation")
    @Expose
    var geoLocation: GeoPoint?,

    @SerializedName("firestoreGeoLocation")
    @Expose
    var firestoreGeoLocation: Double?,

    @SerializedName("picture1URI")
    @Expose
    var picture1URI: String?,

    @SerializedName("description")
    @Expose
    var description : String?,

    @SerializedName("city")
    @Expose
    var city : String?,

    @SerializedName("keywords")
    @Expose
    var keywords : List<String>? = null,

    @SerializedName("ownerID")
    @Expose
    var ownerID: UserUniqueID?,

    @SerializedName("name")
    @Expose
    var name : String?,

    @SerializedName("switch1")
    @Expose
    var switch1 : Boolean?,

    @SerializedName("switch2")
    @Expose
    var switch2 : Boolean?,

    @SerializedName("switch3")
    @Expose
    var switch3 : Boolean?,

    @SerializedName("switch4")
    @Expose
    var switch4 : Boolean?,

    @SerializedName("switch5")
    @Expose
    var switch5 : Boolean?,

    @SerializedName("switch6")
    @Expose
    var switch6 : Boolean?,

    @SerializedName("switch7")
    @Expose
    var switch7 : Boolean?,

    ) {

    // Method generates keywords for searching entities in Firestore
    // https://medium.com/flobiz-blog/full-text-search-with-firestore-on-android-622af6ca5410
    fun generateKeywords(): TaskAssignmentFirestoreEntity {
        val methodName: String = "generateKeywords"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val generatedKeywords = mutableListOf<String>()

            this.city?.let {
                for (i in 0 until city!!.length) {
                    for (j in (i+1)..city!!.length) {
                        generatedKeywords.add(city!!.slice(i until j).toLowerCase() + " ")
                    }
                }
            }

            keywords = generatedKeywords
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
        return this
    }

    constructor() : this(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
    )
}