package com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity3
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.model.Entity3CacheEntity
import javax.inject.Inject

class Entity3CacheMapper
@Inject
constructor() : EntityMapper<Entity3CacheEntity, Entity3> {
    override fun mapFromEntity(entity: Entity3CacheEntity): Entity3 {
        return Entity3(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.picture1URI,

            entity.name,
            entity.description,

            entity.ownerID,
        )
    }

    override fun mapToEntity(domainModel: Entity3): Entity3CacheEntity {
        return Entity3CacheEntity(
            domainModel.id!!,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.picture1URI,

            domainModel.name,
            domainModel.description,

            domainModel.ownerID,
        )
    }

    override fun mapFromEntityList(entities : List<Entity3CacheEntity>) : List<Entity3> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<Entity3>): List<Entity3CacheEntity> {
        return entities.map{mapToEntity(it)}
    }
}