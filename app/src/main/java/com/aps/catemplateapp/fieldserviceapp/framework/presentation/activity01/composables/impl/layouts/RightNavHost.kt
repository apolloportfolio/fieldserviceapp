package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01.composables.impl.layouts

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.createGraph

@Composable
internal fun RightNavHost(
    rightPaneStartDestination: String,
    rightPaneNavGraphBuilder: NavGraphBuilder.() -> Unit,
    rightNavController: NavHostController,
    modifier: Modifier?,
) {
    val navGraph = remember(rightPaneStartDestination, rightPaneNavGraphBuilder) {
        rightNavController.createGraph(
            rightPaneStartDestination,
            null,
            rightPaneNavGraphBuilder,
        )
    }
    NavHost(
        navController = rightNavController,
        modifier = modifier ?: Modifier,
        graph = navGraph,
    )
}