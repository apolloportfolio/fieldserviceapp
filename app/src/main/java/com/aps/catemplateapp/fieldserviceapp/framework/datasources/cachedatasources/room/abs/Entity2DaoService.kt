package com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.abs


import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity2

interface Entity2DaoService {
    suspend fun insertOrUpdateEntity(entity: Entity2): Entity2

    suspend fun insertEntity(entity: Entity2): Long

    suspend fun insertEntities(Entities: List<Entity2>): LongArray

    suspend fun getEntityById(id: UniqueID?): Entity2?

    suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        name: String?,
        description: String?,

        ownerID: UserUniqueID?,
    ): Int

    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteEntities(Entities: List<Entity2>): Int

    suspend fun searchEntities(): List<Entity2>

    suspend fun getAllEntities(): List<Entity2>

    suspend fun getNumEntities(): Int
}