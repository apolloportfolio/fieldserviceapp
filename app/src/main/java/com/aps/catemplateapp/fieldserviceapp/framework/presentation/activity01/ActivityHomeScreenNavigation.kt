package com.aps.catemplateapp.fieldserviceapp.framework.presentation.activity01

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Parcelable
import androidx.fragment.app.Fragment
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.framework.presentation.IntentExtras
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.extensions.getApplicationName
import com.aps.catemplateapp.core.business.domain.model.entities.*
import com.aps.catemplateapp.feature01.framework.presentation.activity01.RegisterLoginActivity
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.TaskAssignment
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity2
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity3
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity4
import com.aps.catemplateapp.feature03.framework.presentation.activity01.ActivityEntity1Details
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

private const val TAG = "ActivityHomeScreenNavigation"
private const val LOG_ME = true

interface ActivityHomeScreenNavigation {

    @FlowPreview
    @ExperimentalCoroutinesApi
    fun navigateToEntity1DetailsScreen(
        fragment: Fragment,
        taskAssignment: TaskAssignment,
        projectUser : ProjectUser,
    ) {
        val intent = Intent(fragment.activity, ActivityEntity1Details::class.java)
        if(LOG_ME)ALog.d(
            TAG, ".navigateToEntity1DetailsScreen(): " +
                                "entity1 == $taskAssignment\n" +
                "projectUser == $projectUser")
        intent.putExtra(IntentExtras.APP_USER, projectUser as Parcelable)
        intent.putExtra(IntentExtras.ENTITY1, taskAssignment as Parcelable)
        fragment.activity?.startActivity(intent)
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    fun navigateToEntity4DetailsScreen(
        fragment: Fragment,
        entity4: Entity4,
        projectUser : ProjectUser,
    ) {
        val methodName: String = "navigateToEntity4DetailsScreen"
        if(LOG_ME)ALog.w(
            TAG, ".$methodName(): " +
                "Unimplemented functionality.")
//        val intent = Intent(fragment.activity, ActivityEntity4Details::class.java)
//        intent.putExtra(IntentExtras.APP_USER, projectUser as Parcelable)
//        intent.putExtra(IntentExtras.ITEM, entity4 as Parcelable)
//        fragment.activity?.startActivity(intent)
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    fun navigateToAddEntity4Screen(
        fragment: Fragment,
        entity4: Entity4?,
        projectUser : ProjectUser,
    ) {
        val methodName: String = "navigateToAddEntity4Screen"
        if(LOG_ME)ALog.w(
            TAG, ".$methodName(): " +
                "Unimplemented functionality.")
//        val intent = Intent(fragment.activity, ActivityAddEntity4::class.java)
//        intent.putExtra(IntentExtras.APP_USER, projectUser as Parcelable)
//        entity4?.let { intent.putExtra(IntentExtras.ITEM, it as Parcelable) }
//        fragment.activity?.startActivity(intent)
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    fun navigateToEntity2DetailsScreen(
        fragment: Fragment,
        entity2 : TaskAssignment,
        entityOwner : ProjectUser,
    ) {
        val methodName: String = "navigateToEntity2DetailsScreen"
        if(LOG_ME)ALog.w(
            TAG, ".$methodName(): " +
                "Unimplemented functionality.")
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    fun navigateToEntity3DetailsScreen(
        fragment: Fragment,
        entity3 : TaskAssignment,
        entityOwner : ProjectUser,
    ) {
        val methodName: String = "navigateToEntity3DetailsScreen"
        if(LOG_ME)ALog.w(
            TAG, ".$methodName(): " +
                "Unimplemented functionality.")
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    fun navigateToEntity1SearchActivity(
        fragment: Fragment,
        projectUser : ProjectUser,
    ) {
        val methodName: String = "navigateToEntity1SearchActivity"
        if(LOG_ME)ALog.w(
            TAG, ".$methodName(): " +
                "Unimplemented functionality.")
//        val intent = Intent(fragment.activity, ActivityEntity1Search::class.java)
//        intent.putExtra(IntentExtras.APP_USER, projectUser as Parcelable)
//        fragment.activity?.startActivity(intent)
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    fun navigateToTermsAndConditionsScreen(
        fragment: Fragment,
        projectUser : ProjectUser,
    ) {
        val methodName: String = "navigateToTermsAndConditionsScreen"
        if(LOG_ME)ALog.w(
            TAG, ".$methodName(): " +
                "Unimplemented functionality.")
//        val intent = Intent(fragment.activity, ActivityTermsAndConditions::class.java)
//        intent.putExtra(IntentExtras.APP_USER, projectUser as Parcelable)
//        fragment.activity?.startActivity(intent)
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    fun navigateToGooglePlayToRateThisApp(fragment: Fragment,) {
        val methodName: String = "navigateToGooglePlayToRateThisApp"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            fragment.activity?.let {
                val uri: Uri = Uri.parse("market://details?id=${it.packageName}")
                val goToMarket = Intent(Intent.ACTION_VIEW, uri)
                // To count with Play market backstack, After pressing back button,
                // to taken back to our application, we need to add following flags to intent.
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
                try {
                    it.startActivity(goToMarket)
                } catch (e: ActivityNotFoundException) {
                    it.startActivity(Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id=${it.packageName}")))
                }
            } ?: ALog.w(TAG, ".$methodName(): activity == null")
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    fun navigateToHelpScreen(
        fragment: Fragment,
        projectUser : ProjectUser,
    ) {
        val methodName: String = "navigateToHelpScreen"
        if(LOG_ME)ALog.w(
            TAG, ".$methodName(): " +
                "Unimplemented functionality.")
//        val intent = Intent(fragment.activity, ActivityHelp::class.java)
//        intent.putExtra(IntentExtras.APP_USER, projectUser as Parcelable)
//        fragment.activity?.startActivity(intent)
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    fun navigateToBasicInfoScreen(
        fragment: Fragment,
        projectUser : ProjectUser,
    ) {
        val methodName: String = "navigateToBasicInfoScreen"
        if(LOG_ME)ALog.w(
            TAG, ".$methodName(): " +
                                "Unimplemented functionality.")
//        val intent = Intent(fragment.activity, ActivityBasicInfo::class.java)
//        intent.putExtra(IntentExtras.APP_USER, projectUser as Parcelable)
//        fragment.activity?.startActivity(intent)
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    fun navigateToUsersRatings(
        fragment: Fragment,
        projectUser : ProjectUser,
    ) {
        val methodName: String = "navigateToUsersRatings"
        if(LOG_ME)ALog.w(
            TAG, ".$methodName(): " +
                "Unimplemented functionality.")
//        if(ProjectConstants.USERS_RATINGS_SCREEN_IMPLEMENTED) {
//            val intent = Intent(fragment.activity, ActivityUsersRatings::class.java)
//            intent.putExtra(IntentExtras.APP_USER, projectUser as Parcelable)
//            fragment.activity?.startActivity(intent)
//        } else {
//            ALog.w(TAG, ".navigateToUsersRatings(): " +
//                    "Users rating screen isn't implemented yet.")
//        }
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    fun navigateToUsersPhoneActivationScreen(
        fragment: Fragment,
        projectUser : ProjectUser,
    ) {
        val methodName: String = "navigateToUsersPhoneActivationScreen"
        if(LOG_ME)ALog.w(
            TAG, ".$methodName(): " +
                "Unimplemented functionality.")
//        val intent = Intent(fragment.activity, ActivityPhoneActivationNumber::class.java)
//        if(LOG_ME)ALog.d(TAG, "navigateToUsersPhoneActivationScreen.(): $projectUser")
//        intent.putExtra(IntentExtras.APP_USER, projectUser as Parcelable)
//        fragment.activity?.startActivity(intent)
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    fun navigateToUsersEmailActivationScreen(
        fragment: Fragment,
        projectUser : ProjectUser,
    ) {
        val methodName: String = "navigateToUsersEmailActivationScreen"
        if(LOG_ME)ALog.w(
            TAG, ".$methodName(): " +
                "Unimplemented functionality.")
//        val intent = Intent(fragment.activity, ActivityEmailActivation::class.java)
//        intent.putExtra(IntentExtras.APP_USER, projectUser as Parcelable)
//        fragment.activity?.startActivity(intent)
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    fun navigateToUsersProfilePictureScreen(
        fragment: Fragment,
        projectUser : ProjectUser,
    ) {
        val methodName: String = "navigateToUsersProfilePictureScreen"
        if(LOG_ME)ALog.w(
            TAG, ".$methodName(): " +
                "Unimplemented functionality.")
//        val intent = Intent(fragment.activity, ActivityProfilePhoto::class.java)
//        intent.putExtra(IntentExtras.APP_USER, projectUser as Parcelable)
//        fragment.activity?.startActivity(intent)
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    fun navigateToStartScreen(
        fragment: Fragment,
    ) {
        val intent = Intent(fragment.activity, RegisterLoginActivity::class.java)
        fragment.activity?.startActivity(intent)
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    fun navigateToAppRecommendationScreen(
        fragment: Fragment,
        projectUser : ProjectUser,
    ) {
        val methodName: String = "navigateToAppRecommendationScreen"
        if(LOG_ME)ALog.w(
            TAG, ".$methodName(): " +
                "Unimplemented functionality.")
//        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
//        try {
//            val intent = Intent(fragment.activity, ActivityInvitation::class.java)
//            intent.putExtra(IntentExtras.APP_USER, projectUser as Parcelable)
//            fragment.activity?.startActivity(intent)
//        } catch (e: Exception) {
//            ALog.e(TAG, methodName, e)
//        } finally {
//            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
//        }
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    fun navigateToProblemReporting(
        fragment: Fragment,
        projectUser : ProjectUser,
        thingTodoIfNoEmailClientIsInstalledOnUsersDevice: () -> (Unit),
    ) {
        val methodName: String = "navigateToProblemReporting"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(fragment.activity != null) {
                fragment.activity?.let { activity ->
                    val customerSupportEmailAddress = fragment.getString(R.string.app_customer_support_email_address)
                    val emailSubject = getEmailSubjectForErrorReporting(activity)
                    val emailAddresses = arrayOf<String>(customerSupportEmailAddress)

                    val intent = Intent(Intent.ACTION_SENDTO).apply {
                        data = Uri.parse("mailto:") // only email apps should handle this
                        putExtra(Intent.EXTRA_EMAIL, emailAddresses)
                        putExtra(Intent.EXTRA_SUBJECT, emailSubject)
                    }
                    if (intent.resolveActivity(activity.packageManager) != null) {
                        if(LOG_ME){
                            var emailAddressesString = ""
                            for(emailAddress in emailAddresses)
                                emailAddressesString += "$emailAddress;"
                            ALog.d(
                                TAG, "$methodName(): " +
                                    "Starting email activity with " +
                                    "emailSubject == $emailSubject and " +
                                    "emailAddresses$ == emailAddressesString")
                        }
                        activity.startActivity(intent)
                    } else {
                        ALog.e(
                            TAG, "$methodName(): " +
                                "intent.resolveActivity(activity.packageManager) == null, " +
                                "invoking thingTodoIfNoEmailClientIsInstalledOnUsersDevice")
                        thingTodoIfNoEmailClientIsInstalledOnUsersDevice.invoke()
                    }
                }
            } else {
                ALog.e(TAG, ".$methodName(): ")
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    companion object {
        fun getEmailSubjectForErrorReporting(context : Context) : String {
            return context.getApplicationName() + " " +
                    context.getString(R.string.report_problem_email_subject)
        }
    }

}