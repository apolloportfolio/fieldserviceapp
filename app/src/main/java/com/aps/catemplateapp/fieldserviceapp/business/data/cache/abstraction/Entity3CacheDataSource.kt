package com.aps.catemplateapp.fieldserviceapp.business.data.cache.abstraction

import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.Entity3

interface Entity3CacheDataSource: StandardCacheDataSource<Entity3> {
    override suspend fun insertOrUpdateEntity(entity: Entity3): Entity3?

    override suspend fun insertEntity(entity: Entity3): Long

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int

    override suspend fun deleteEntities(entities: List<Entity3>): Int

    suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        name: String?,
        description: String?,

        ownerID: UserUniqueID?,
    ): Int

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<Entity3>

    override suspend fun getAllEntities(): List<Entity3>

    override suspend fun getEntityById(id: UniqueID?): Entity3?

    override suspend fun getNumEntities(): Int

    override suspend fun insertEntities(entities: List<Entity3>): LongArray
}