package com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.impl

import com.aps.catemplateapp.common.util.*
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.TaskAssignment
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.abs.TaskAssignmentDaoService
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.daos.TaskAssignmentDao
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.mappers.TaskAssignmentCacheMapper
import javax.inject.Inject
import javax.inject.Singleton

private const val TAG = "Entity1DaoServiceImpl"
private const val LOG_ME = true

@Singleton
class TaskAssignmentDaoServiceImpl
@Inject
constructor(
    private val dao: TaskAssignmentDao,
    private val mapper: TaskAssignmentCacheMapper,
    private val dateUtil: DateUtil
): TaskAssignmentDaoService {

    override suspend fun insertOrUpdateEntity(entity: TaskAssignment): TaskAssignment {
        val databaseEntity = dao.getEntityById(entity.id)
        if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): databaseEntity == $databaseEntity")
        if(databaseEntity == null) {
            if(LOG_ME)ALog.d(TAG, ".insertOrUpdateEntity(): entity before inserting:\n$entity")
            dao.insert(mapper.mapToEntity(entity))
            val entityFromDao = dao.getEntityById(entity.id)
            if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): entity after inserting == ${entityFromDao}\n")
            return mapper.mapFromEntity(entityFromDao!!)
        } else {
            this.updateEntity(
                entity.id,
                entity.created_at,
                entity.updated_at,

                entity.taskDescription,
                entity.taskAddress,
                entity.clientName,
                entity.phoneNumber,
                entity.scheduledTaskStartTimeStamp,
                entity.expectedTaskDuration,
                entity.actualTaskStartTimeStamp,
                entity.actualTaskEndTimeStamp,
                entity.isTaskCompleted,
                entity.taskNotes,
                entity.customerFeedbackGrade,
                entity.customerFeedbackGradeReview,
                entity.taskResultsPicturesURIs,
                entity.materialsNote,
                entity.jobPriority,

                entity.latitude ,
                entity.longitude,
                entity.geoLocation,
                entity.firestoreGeoLocation,
                entity.picture1URI,
                entity.description,
                entity.city,
                entity.ownerID,
                entity.name,

                entity.switch1,
                entity.switch2,
                entity.switch3,
                entity.switch4,
                entity.switch5,
                entity.switch6,
                entity.switch7,
            )
            val entityFromDao = dao.getEntityById(entity.id)
            if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): entityFromDao == $entityFromDao")
            return mapper.mapFromEntity(entityFromDao!!)
        }
    }

    override suspend fun insertEntity(entity: TaskAssignment): Long {
        return dao.insert(mapper.mapToEntity(entity))
    }

    override suspend fun insertEntities(entities: List<TaskAssignment>): LongArray {
        return dao.insertEntities(
            mapper.mapToEntityList(entities)
        )
    }

    override suspend fun getEntityById(id: UniqueID?): TaskAssignment? {
        return dao.getEntityById(id)?.let { entity ->
            mapper.mapFromEntity(entity)
        }
    }

    override suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        taskDescription: String?,
        taskAddress: String?,
        clientName: String?,
        phoneNumber: String?,
        scheduledTaskStartTimeStamp: String?,
        expectedTaskDuration: String?,
        actualTaskStartTimeStamp: String?,
        actualTaskEndTimeStamp: String?,
        isTaskCompleted: Boolean?,
        taskNotes: String?,
        customerFeedbackGrade: Int?,
        customerFeedbackGradeReview: String?,
        taskResultsPicturesURIs: String?,
        materialsNote: String?,
        jobPriority: Int?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description : String?,

        city : String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean?,
        switch2: Boolean?,
        switch3: Boolean?,
        switch4: Boolean?,
        switch5: Boolean?,
        switch6: Boolean?,
        switch7: Boolean?,
    ): Int {
        return if(updated_at != null){
            dao.updateEntity(
                id,
                created_at,
                updated_at,

                taskDescription,
                taskAddress,
                clientName,
                phoneNumber,
                scheduledTaskStartTimeStamp,
                expectedTaskDuration,
                actualTaskStartTimeStamp,
                actualTaskEndTimeStamp,
                isTaskCompleted,
                taskNotes,
                customerFeedbackGrade,
                customerFeedbackGradeReview,
                taskResultsPicturesURIs,
                materialsNote,
                jobPriority,

                latitude,
                longitude,
                geoLocation,
                firestoreGeoLocation,

                picture1URI,
                description,

                city,

                ownerID,

                name,

                switch1,
                switch2,
                switch3,
                switch4,
                switch5,
                switch6,
                switch7,
            )
        }else{
            dao.updateEntity(
                id,
                created_at,
                dateUtil.getCurrentTimestamp(),

                taskDescription,
                taskAddress,
                clientName,
                phoneNumber,
                scheduledTaskStartTimeStamp,
                expectedTaskDuration,
                actualTaskStartTimeStamp,
                actualTaskEndTimeStamp,
                isTaskCompleted,
                taskNotes,
                customerFeedbackGrade,
                customerFeedbackGradeReview,
                taskResultsPicturesURIs,
                materialsNote,
                jobPriority,

                latitude,
                longitude,
                geoLocation,
                firestoreGeoLocation,

                picture1URI,

                description,

                city,

                ownerID,

                name,

                switch1,
                switch2,
                switch3,
                switch4,
                switch5,
                switch6,
                switch7,
            )
        }

    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return dao.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<TaskAssignment>): Int {
        val ids = entities.mapIndexed {index, value -> value.id}
        return dao.deleteEntities(ids)
    }

    override suspend fun searchEntities(): List<TaskAssignment> {
        return mapper.mapFromEntityList(
            dao.searchEntities()
        )
    }

    override suspend fun getAllEntities(): List<TaskAssignment> {
        return mapper.mapFromEntityList(dao.getAllEntities())
    }

    override suspend fun getNumEntities(): Int {
        return dao.getNumEntities()
    }
}