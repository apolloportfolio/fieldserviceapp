package com.aps.catemplateapp.feature03.framework.presentation.activity01

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.common.framework.presentation.BaseViewModel
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.feature03.business.interactors.ActivityEntity1DetailsInteractors
import com.aps.catemplateapp.feature03.framework.presentation.activity01.state.ActivityEntity1DetailsInteractionManager
import com.aps.catemplateapp.feature03.framework.presentation.activity01.state.ActivityEntity1DetailsStateEvent
import com.aps.catemplateapp.feature03.framework.presentation.activity01.state.ActivityEntity1DetailsViewState
import com.aps.catemplateapp.feature03.framework.presentation.activity01.state.CollapsingToolbarState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import com.aps.catemplateapp.core.util.Entity1
import com.aps.catemplateapp.core.util.Entity1CacheDataSourceImpl
import com.aps.catemplateapp.core.util.Entity1Factory
import com.aps.catemplateapp.core.util.Entity1NetworkDataSource

private const val TAG = "ActivityEntity1DetailsViewModel"
private const val LOG_ME = true

@ExperimentalCoroutinesApi
@FlowPreview
@HiltViewModel
class ActivityEntity1DetailsViewModel
@Inject
constructor(
    private val interactors: ActivityEntity1DetailsInteractors<
            Entity1,
            Entity1CacheDataSourceImpl,
            Entity1NetworkDataSource,
            ActivityEntity1DetailsViewState<Entity1>
            >,
    private val dateUtil: DateUtil,
    private val entityFactory: Entity1Factory,
//    private val savedStatehandle: SavedStateHandle,
): BaseViewModel<Entity1, ActivityEntity1DetailsViewState<Entity1>>() {
    private val interactionManager: ActivityEntity1DetailsInteractionManager =
        ActivityEntity1DetailsInteractionManager()
    private val collapsingToolbarState: LiveData<CollapsingToolbarState>
        get() = interactionManager.collapsingToolbarState


    override fun handleNewData(data: ActivityEntity1DetailsViewState<Entity1>) {
        val methodName: String = "handleNewData"
        data.let { viewState ->
            viewState.entity1?.let {
                if(LOG_ME) ALog.d(TAG, "$methodName(): Handling viewState.entity1")
                setMainEntity(it)
            }

            viewState.mainEntity?.let { mainEntity ->
                if(LOG_ME)ALog.d(TAG, "$methodName(): Handling viewState.mainEntity")
                setMainEntity(mainEntity)
            }

            viewState.listOfEntities?.let { listOfEntities ->
                if(LOG_ME)ALog.d(TAG, "$methodName(): Handling viewState.listOfEntities")
                setListOfEntities(listOfEntities)
            }

            viewState.noCachedEntity?.let { noCachedEntity ->
                if(LOG_ME)ALog.d(TAG, "$methodName(): Handling viewState.noCachedEntity")
                setNoCachedEntity(noCachedEntity)
            }

        }
    }

    @SuppressLint("LongLogTag")
    override fun setStateEvent(stateEvent: StateEvent) {
        val methodName: String = "setStateEvent()"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        var launchJob = true
        val job: Flow<DataState<ActivityEntity1DetailsViewState<Entity1>>?> = when(stateEvent){

            is ActivityEntity1DetailsStateEvent.Entity1ActionEvent -> {
                val currentViewState = getCurrentViewStateOrNew()
                if(currentViewState.user != null) {
                    if(currentViewState.entity1 != null) {
                        if(currentViewState.user!!.fullyVerified) {
                            ALog.e(TAG, ".$methodName(): " +
                                    "User is verified. Setting userWantsToActOnEntity1")
                            setUserWantsToActOnEntity1(true)
                            emitToastStateMessageEvent(stateEvent, R.string.act_on_entity1)
                        } else {
                            ALog.e(TAG, ".$methodName(): currentViewState.entity1 != null")
                            emitStateMessageEvent(
                                stateMessage = StateMessage(
                                    response = Response(
                                        messageId = R.string.finish_verification,
                                        message = (getApplicationContext() as Context).getString(R.string.only_fully_verified_users_can_act_on_entity1),
                                        uiComponentType = UIComponentType.Dialog(),
                                        messageType = MessageType.Error()
                                    )
                                ),
                                stateEvent = stateEvent
                            )
                        }
                    } else {
                        ALog.e(TAG, ".$methodName(): currentViewState.entity1 != null")
                        emitStateMessageEvent(
                            stateMessage = StateMessage(
                                response = Response(
                                    messageId = R.string.error,
                                    message = (getApplicationContext() as Context).getString(R.string.error_entity1_is_null),
                                    uiComponentType = UIComponentType.Dialog(),
                                    messageType = MessageType.Error()
                                )
                            ),
                            stateEvent = stateEvent
                        )
                    }
                } else {
                    ALog.e(TAG, ".$methodName(): currentViewState.user != null")
                    emitStateMessageEvent(
                        stateMessage = StateMessage(
                            response = Response(
                                messageId = R.string.error,
                                message = (getApplicationContext() as Context).getString(R.string.error_user_trying_to_act_on_entity1_is_null),
                                uiComponentType = UIComponentType.Dialog(),
                                messageType = MessageType.Error()
                            )
                        ),
                        stateEvent = stateEvent
                    )
                }
            }

            is ActivityEntity1DetailsStateEvent.CreateStateMessageEvent -> {
                emitStateMessageEvent(
                    stateMessage = stateEvent.stateMessage,
                    stateEvent = stateEvent
                )
            }

            is ActivityEntity1DetailsStateEvent.None -> {
                launchJob = false
                interactors.doNothingAtAll.doNothing()
            }

            else -> {
                emitInvalidStateEvent(stateEvent)
            }
        }
        if (LOG_ME) ALog.d(TAG, "launching stateEvent in job: ${stateEvent.eventName()}")
        launchJob(stateEvent, job, launchJob)
        if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
    }

    override fun initNewViewState(): ActivityEntity1DetailsViewState<Entity1> {
        return ActivityEntity1DetailsViewState()
    }

    override fun setMainEntity(sportsGame: Entity1?){
        val update = getCurrentViewStateOrNew()
        update.mainEntity = sportsGame
        update.entity1 = sportsGame
        setViewState(update)
    }

    private fun setNoCachedEntity(noCachedEntity: Boolean?) {
        val update = getCurrentViewStateOrNew()
        update.noCachedEntity = noCachedEntity
        setViewState(update)
    }

    fun setUserWantsToActOnEntity1(userWantsToActOnEntity1 : Boolean?) {
        if(LOG_ME)ALog.d(TAG, ".setUserWantsToActOnEntity1(): Setting userWantsToActOnEntity1 to $userWantsToActOnEntity1")
        val update = getCurrentViewStateOrNew()
        update.userWantsToActOnEntity1 = userWantsToActOnEntity1
        setViewState(update)
    }

    fun setCollapsingToolbarState(
        state: CollapsingToolbarState
    ) = interactionManager.setCollapsingToolbarState(state)

    fun isToolbarCollapsed() = collapsingToolbarState.toString()
        .equals(CollapsingToolbarState.Collapsed().toString())

    fun isToolbarExpanded() = collapsingToolbarState.toString()
        .equals(CollapsingToolbarState.Expanded().toString())

    // return true if in EditState
    fun checkEditState() = interactionManager.checkEditState()

    fun exitEditState() = interactionManager.exitEditState()


    // force observers to refresh
    fun triggerEntityObservers(){
        getCurrentViewStateOrNew().mainEntity?.let { mainEntity ->
            setMainEntity(mainEntity)
        }
        getCurrentViewStateOrNew().listOfEntities?.let { listOfEntities ->
            setListOfEntities(listOfEntities)
        }
        getCurrentViewStateOrNew().user?.let { user ->
            setUser(user)
        }
    }

    fun setUser(user: ProjectUser) {
        val update = getCurrentViewStateOrNew()
        update.user = user
        setViewState(update)
    }
}