package com.aps.catemplateapp.feature03.framework.presentation.activity01

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.callbacks.onDismiss
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.DialogInputCaptureCallback
import com.aps.catemplateapp.common.framework.presentation.IntentExtras
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.core.framework.presentation.ProjectActivity
import com.aps.catemplateapp.feature03.di.activity01.ActivityEntity1DetailsFragmentFactoryEntryPoint
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.EntryPointAccessors
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject

private const val TAG = "ActivityEntity1Details"
private const val LOG_ME = true

@ExperimentalCoroutinesApi
@AndroidEntryPoint
@FlowPreview
class ActivityEntity1Details : ProjectActivity() {
    @Inject
    lateinit var fragmentFactory: ActivityEntity1DetailsFragmentFactory
    override val getContentView: Int
        get() = R.layout.activity_entity1_details
    override val mainFragmentId: Int
        get() = R.id.nav_host_fragment

    lateinit var viewModel : ActivityEntity1DetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ActivityEntity1DetailsViewModel::class.java)
        setFragmentFactory()
        setContentView(getContentView)

        setMainEntityInViewModelFromBundle(
            viewModel,
            IntentExtras.ENTITY1,
        )
        setUserInViewModelFromBundle(
            viewModel,
            IntentExtras.APP_USER,
        )
    }

    private fun setUserInViewModelFromBundle(
        viewModel : ActivityEntity1DetailsViewModel,
        userTag : String,
    ) {
        val methodName: String = "setUserInViewModelFromBundle"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val bundle : Bundle? = intent.extras
            bundle?.let {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): bundle != null")
                val user : ProjectUser? = it.get(userTag) as ProjectUser?
                user?.let {
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): user != null")
                    viewModel.setUser(it)
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun setFragmentFactory(){
        val entryPoint = EntryPointAccessors.fromActivity(
            this,
            ActivityEntity1DetailsFragmentFactoryEntryPoint::class.java
        )

        fragmentFactory = entryPoint.getActivityEntity1DetailsFragmentFactory()
        supportFragmentManager.fragmentFactory = fragmentFactory
    }

    override fun onSupportNavigateUp(): Boolean {
        return findNavController(R.id.nav_host_fragment)
                .navigateUp(appBarConfiguration as AppBarConfiguration)
                || super.onSupportNavigateUp()
    }

    override fun displayInputCaptureDialog(
            title: String,
            callback: DialogInputCaptureCallback
    ) {
        dialogInView = MaterialDialog(this).show {
            title(text = title)
            positiveButton(R.string.text_ok)
            onDismiss {
                dialogInView = null
            }
            cancelable(true)
        }
    }
}