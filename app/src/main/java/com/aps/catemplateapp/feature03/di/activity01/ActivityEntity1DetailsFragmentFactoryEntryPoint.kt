package com.aps.catemplateapp.feature03.di.activity01

import com.aps.catemplateapp.feature03.framework.presentation.activity01.ActivityEntity1DetailsFragmentFactory
import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@EntryPoint
@InstallIn(ActivityComponent::class)
interface ActivityEntity1DetailsFragmentFactoryEntryPoint {
    fun getActivityEntity1DetailsFragmentFactory(): ActivityEntity1DetailsFragmentFactory
}