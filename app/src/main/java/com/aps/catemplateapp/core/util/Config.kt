package com.aps.catemplateapp.core.util

class Config {

    companion object {
        const val APP_NAME = "Field Service App"
        const val LOGGING = true
        const val LOGGING_TAG = "ALog"
        const val DEBUGGING_MODE = false
    }
}