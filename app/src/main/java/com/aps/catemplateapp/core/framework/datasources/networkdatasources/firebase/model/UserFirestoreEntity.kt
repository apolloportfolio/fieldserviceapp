package com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.model

import com.aps.catemplateapp.common.util.UserUniqueID
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserFirestoreEntity(
    @SerializedName("id")
    @Expose
    var id: UserUniqueID?,

    @SerializedName("updated_at")
    @Expose
    var updated_at: String?,

    @SerializedName("created_at")
    @Expose
    val created_at: String?,

    @SerializedName("emailAddress")
    @Expose
    var emailAddress: String,

    @SerializedName("password")
    @Expose
    var password: String,

    @SerializedName("profilePhotoImageURI")
    @Expose
    var profilePhotoImageURI: String?,

    @SerializedName("name")
    @Expose
    var name: String?,

    @SerializedName("surname")
    @Expose
    var surname: String?,

    @SerializedName("description")
    @Expose
    var description: String?,

    @SerializedName("city")
    @Expose
    var city: String?,

    @SerializedName("emailAddressVerified")
    @Expose
    var emailAddressVerified: Boolean,

    @SerializedName("phoneNumberVerified")
    @Expose
    var phoneNumberVerified: Boolean,
    ) {
    constructor() : this(
        null,
        null,
        null,
        "null",
        "null",
        null,
        null,
        null,
        null,
        null,
        false,
        false,
    )
}