package com.aps.catemplateapp.core.util

import com.aps.catemplateapp.fieldserviceapp.business.data.cache.implementation.TaskAssignmentCacheDataSourceImpl
import com.aps.catemplateapp.fieldserviceapp.business.data.network.abs.TaskAssignmentNetworkDataSource
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.entities.TaskAssignment
import com.aps.catemplateapp.fieldserviceapp.business.domain.model.factories.TaskAssignmentFactory


// fiedlserviceapp
typealias Entity1 = TaskAssignment
typealias Entity1NetworkDataSource = TaskAssignmentNetworkDataSource
typealias Entity1CacheDataSourceImpl = TaskAssignmentCacheDataSourceImpl
typealias Entity1Factory = TaskAssignmentFactory