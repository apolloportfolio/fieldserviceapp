package com.aps.catemplateapp.core.business.domain.model.entities

import android.os.Parcelable
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.util.ProjectConstants
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

private const val TAG = "ProjectUser"
private const val LOG_ME = true

@Parcelize
data class ProjectUser(
    var id: UserUniqueID?,
    var updated_at: String?,
    var created_at: String?,
    var emailAddress: String,
    var password: String,
    var profilePhotoImageURI: String?,
    var name: String?,
    var surname: String?,
    var description: String?,
    var city: String?,
    var emailAddressVerified: Boolean = false,
    var phoneNumberVerified: Boolean = false,
    ): Parcelable {
    val fullyVerified : Boolean
        get() {
            return emailAddressVerified && basicVerification
        }
    val basicVerification : Boolean
        get() {
            return name != null && surname != null
        }

    @IgnoredOnParcel
    val profilePictureFirebaseImageRef: StorageReference?
        get() {
            return if(id != null && profilePhotoImageURI != null) {
                if(LOG_ME) ALog.d(TAG,
                    ".profilePhotoImageURI(): " +
                            "this.id!!.firestoreDocumentID == ${this.id!!.firestoreDocumentID}"
                )
                Firebase.storage.reference
                    .child(ProjectConstants.FIRESTORE_IMAGES_COLLECTION)
                    .child(ProjectConstants.FIRESTORE_PROFILE_PHOTOS_SUB_COLLECTION)
                    .child(this.id!!.firestoreDocumentID)
                    .child(this.profilePhotoImageURI!!)
            } else {
                null
            }
        }

    fun toIdString() : String {
        return "${this.javaClass.simpleName}( $id)"
    }
}