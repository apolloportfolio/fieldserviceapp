package com.aps.catemplateapp.core.business.domain.model.currencies

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize
import java.time.LocalDate

@Parcelize
@JsonClass(generateAdapter = true)
data class ExchangeRates(
    @field:Json(name = "success") val success: Boolean?,
    @field:Json(name = "error") val error: String?,

    @field:Json(name = "base") val base: Currency?,
    @field:Json(name = "date") val date: LocalDate?,
    @field:Json(name = "rates") val rates: List<Rate>?,

    val provider: ApiProvider?

) : Parcelable
