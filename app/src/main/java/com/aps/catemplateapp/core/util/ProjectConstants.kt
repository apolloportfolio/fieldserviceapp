package com.aps.catemplateapp.core.util

import com.aps.catemplateapp.core.business.domain.model.currencies.Currency

object ProjectConstants {
    const val GRAPHQL_APOLLO_CLIENT_SERVER_URL = ""
    const val PHONE_NUMBER_VERIFICATION_ENFORCED = false
    const val FIRESTORE_IMAGES_COLLECTION : String = "images"
    const val FIRESTORE_ENTITY_1_IMAGES_SUB_COLLECTION : String = "en1images"
    const val FIRESTORE_ENTITY_2_IMAGES_SUB_COLLECTION : String = "en2images"
    const val FIRESTORE_ENTITY_3_IMAGES_SUB_COLLECTION : String = "en3images"
    const val FIRESTORE_ENTITY_4_IMAGES_SUB_COLLECTION : String = "en4images"
    const val FIRESTORE_PROFILE_PHOTOS_SUB_COLLECTION : String = "profilePhotos"
    const val DEFAULT_SEARCH_RADIUS_KM : Double = 20.0
    val COMPANYS_ACCEPTED_CURRENCY = Currency.PLN
    const val EMAIL_VERIFICATION_CODE_LENGTH : Int = 4
    const val PHONE_ACTIVATION_CODE_LENGTH : Int = 4
    val PHONE_ACTIVATION_CODE_CHARSET : List<Char> = ('A'..'Z') + ('0'..'9')
    private const val GUIDE_1_URL: String = ""
    private const val GUIDE_2_URL: String = ""

    fun getGuide1Url() : String {
        var url = GUIDE_1_URL
        if (!url.startsWith("http://") && !url.startsWith("https://"))url = "http://$url"
        return url
    }

    fun getGuide2Url() : String {
        var url = GUIDE_2_URL
        if (!url.startsWith("http://") && !url.startsWith("https://"))url = "http://$url"
        return url
    }
}
