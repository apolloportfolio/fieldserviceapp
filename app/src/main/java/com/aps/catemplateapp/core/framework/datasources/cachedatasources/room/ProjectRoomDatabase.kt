package com.aps.catemplateapp.core.framework.datasources.cachedatasources.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.daos.*
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.model.*
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.daos.TaskAssignmentDao
import com.aps.catemplateapp.fieldserviceapp.framework.datasources.cachedatasources.room.model.TaskAssignmentCacheEntity

@Database(entities = [
    UserCacheEntity::class,

    TaskAssignmentCacheEntity::class,
], version = 1, exportSchema = false)
@TypeConverters(RoomTypeConverters::class)
abstract class ProjectRoomDatabase : RoomDatabase() {
    abstract fun userDao() : UserDao

    // Field Service App
    abstract fun taskAssignmentDao() : TaskAssignmentDao


    companion object {
        const val DATABASE_NAME: String = "APSDB"
    }
}