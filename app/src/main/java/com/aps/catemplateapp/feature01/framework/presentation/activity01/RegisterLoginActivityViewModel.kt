package com.aps.catemplateapp.feature01.framework.presentation.activity01

import android.content.Context
import android.content.Context.NFC_SERVICE
import android.nfc.NfcAdapter
import android.nfc.NfcManager
import android.view.View
import androidx.lifecycle.LiveData
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.common.framework.presentation.BaseViewModel
import com.aps.catemplateapp.common.util.*
import com.aps.catemplateapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.core.business.domain.model.factories.UserFactory
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.model.UserCacheEntity
import com.aps.catemplateapp.feature01.business.interactors.registerlogin.RegisterLoginInteractors
import com.aps.catemplateapp.feature01.framework.presentation.activity01.state.*
import com.google.firebase.auth.FirebaseUser
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

private const val TAG = "RegisterLoginActivityViewModel"
private const val LOG_ME = true
@ExperimentalCoroutinesApi
@FlowPreview
@HiltViewModel
class RegisterLoginActivityViewModel
@Inject
constructor(
    private val interactors: RegisterLoginInteractors<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            RegisterLoginActivityViewState<ProjectUser>
            >,
    private val dateUtil: DateUtil,
    private val entityFactory: UserFactory
): BaseViewModel<ProjectUser, RegisterLoginActivityViewState<ProjectUser>>() {
    private val interactionManager: RegisterLoginActivityInteractionManager =
        RegisterLoginActivityInteractionManager()
    val emailInteractionState: LiveData<RegisterLoginActivityInteractionState>
        get() = interactionManager.emailState
    val passwordInteractionState: LiveData<RegisterLoginActivityInteractionState>
        get() = interactionManager.passwordState
    val passwordConfirmationInteractionState: LiveData<RegisterLoginActivityInteractionState>
        get() = interactionManager.passwordConfirmationState
    private val collapsingToolbarState: LiveData<CollapsingToolbarState>
        get() = interactionManager.collapsingToolbarState

    // Method handles incoming data calling methods that set _viewState.value
    // _viewState is observed by fragments to decide what to do with user interface
    // observers of _viewState are subscribed in BaseMVIFragment.subscribeObservers()
    // Whenever _viewState changes BaseMVIFragment.observeViewState() is called which in turn calls
    // an abstract method BaseMVIFragment.changeUiOrNavigateDependingOnViewState()
    // That method is overridden in every fragment to change it's UI or navigate to another fragment.
    override fun handleNewData(data: RegisterLoginActivityViewState<ProjectUser>) {
        val methodName: String = "handleNewData"
        data.let { viewState ->
            // Set values of all LiveData instances that come from ViewState
            viewState.mainEntity?.let { mainEntity ->
                if(LOG_ME) ALog.d(TAG, "$methodName(): Handling viewState.mainEntity")
                setMainEntity(mainEntity)

                if(this.getCurrentViewStateOrNew().firebaseUserIsLoggedIn == null) {
                    if(LOG_ME)ALog.d(TAG, "$methodName(): Setting StartStateEvent.LoginUserEvent state")
                    this.setStateEvent(RegisterLoginActivityStateEvent.LoginUserEvent)
                } else {
                    if(LOG_ME)ALog.d(TAG, "$methodName(): Setting StartStateEvent.LoginUserEvent state")
                    if(!this.getCurrentViewStateOrNew().firebaseUserIsLoggedIn!!)this.setStateEvent(
                        RegisterLoginActivityStateEvent.LoginUserEvent)
                }
            }

            viewState.listOfEntities?.let { listOfEntities ->
                if(LOG_ME)ALog.d(TAG, "$methodName(): Handling viewState.listOfEntities")
                setListOfEntities(listOfEntities)
            }

            viewState.appProjectUser?.let { appUser ->
                if(LOG_ME)ALog.d(TAG, "$methodName(): Handling viewState.appUser")
                setAppUser(appUser)
                if(this.getCurrentViewStateOrNew().firebaseUserIsLoggedIn == null) {
                    if(LOG_ME)ALog.d(TAG, "$methodName(): Setting StartStateEvent.LoginUserEvent state")
                    this.setStateEvent(RegisterLoginActivityStateEvent.LoginUserEvent)
                } else {
                    if(LOG_ME)ALog.d(TAG, "$methodName(): Setting StartStateEvent.LoginUserEvent state")
                    if(!this.getCurrentViewStateOrNew().firebaseUserIsLoggedIn!!)this.setStateEvent(
                        RegisterLoginActivityStateEvent.LoginUserEvent)
                }
            }

            viewState.firebaseUser?.let { firebaseUser ->
                if(LOG_ME)ALog.d(TAG, "$methodName(): Handling viewState.firebaseUser")
                setUsersIdIfNecessary(firebaseUser)
                setFirebaseUser(firebaseUser)
            }

            viewState.emailIsTaken?.let { emailIsTaken ->
                if(LOG_ME)ALog.d(TAG, "$methodName(): Handling viewState.emailIsTaken")
                setEmailIsTaken(emailIsTaken)
                if(!emailIsTaken){
                    if(this.getIsUpdatePending()){
                        this.setStateEvent(RegisterLoginActivityStateEvent.RegisterUserEvent)
                    }
                }
            }

            viewState.firebaseUserIsRegistered?.let { firebaseUserIsRegistered ->
                if(LOG_ME)ALog.d(TAG, "$methodName(): Handling viewState.firebaseUserIsRegistered")
                if(LOG_ME)ALog.d(TAG, ".handleNewData(): " +
                        "firebaseUserIsRegistered == $firebaseUserIsRegistered")
                setFirebaseUserIsRegistered(firebaseUserIsRegistered)
                if(firebaseUserIsRegistered){
                    val isUpdatePending = this.getIsUpdatePending()
                    if(LOG_ME)ALog.d(TAG, ".handleNewData(): " +
                                            "isUpdatePending == $isUpdatePending")
                    if(isUpdatePending){
                        this.setStateEvent(RegisterLoginActivityStateEvent.AddNewAppUserEvent)
                    }
                }
            }

            viewState.firebaseUserIsLoggedIn?.let { firebaseUserIsLoggedIn ->
                if(LOG_ME)ALog.d(TAG, "$methodName(): Handling viewState.firebaseUserIsLoggedIn")
                setFirebaseUserIsLoggedIn(firebaseUserIsLoggedIn)
            }

            viewState.passwordMatchesEmail?.let { passwordMatchesEmail ->
                if(LOG_ME)ALog.d(TAG, "$methodName(): Handling viewState.passwordMatchesEmail")
                setPasswordMatchesEmail(passwordMatchesEmail)
                if(this.getIsUpdatePending()){
                    this.setStateEvent(RegisterLoginActivityStateEvent.LoginUserEvent)
                }
            }

            viewState.noCachedEntity?.let { noCachedEntity ->
                if(LOG_ME)ALog.d(TAG, "$methodName(): Handling viewState.noCachedEntity")
                setNoCachedEntity(noCachedEntity)
            }

        }
    }

    override fun setStateEvent(stateEvent: StateEvent) {
        val methodName: String = "setStateEvent"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        var launchJob = true
        val job: Flow<DataState<RegisterLoginActivityViewState<ProjectUser>>?> = when(stateEvent){

            is RegisterLoginActivityStateEvent.CheckDeviceCompatibility -> {
                if (LOG_ME) ALog.d(TAG, "$methodName: CheckDeviceCompatibility")
                val deviceIsCompatible = this.isDeviceCompatible()
                if(deviceIsCompatible) {
                    setStateEvent(RegisterLoginActivityStateEvent.GetAppUserEvent)
                    emitDoNothingStateMessageEvent(stateEvent)
                } else {
                    emitOkDialogStateMessageEvent(
                        stateEvent,
                        R.string.device_is_incompatible,
                        R.string.your_device_doesnt_have_nfc,
                        object : OkButtonCallback {
                            override fun proceed() {
                                setDeviceIsCompatible(false)
                            }
                        }
                    )
                }
            }

            is RegisterLoginActivityStateEvent.GetAppUserEvent -> {
                //interactors.getAllCachedUsers.getAllCachedEntities(stateEvent)
                if(LOG_ME)ALog.d(TAG, ".$methodName(): GetAppUserEvent")
                if (LOG_ME) ALog.d(TAG, "$methodName: getting all cached users")
                interactors.getAllCachedEntities.getAllCachedEntities(stateEvent, getCurrentViewStateOrNew())
            }

            is RegisterLoginActivityStateEvent.CheckIfEmailIsTakenUserEvent -> {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): CheckIfEmailIsTakenUserEvent")
                val passwordsMatch = this.passwordsMatch()
                val emailIsValid = this.emailIsValid(getCurrentViewStateOrNew().email)
                if(emailIsValid) {
                    if(passwordsMatch) {
                        interactors.checkIfEmailIsTaken.checkIfEmailIsTaken(getCurrentViewStateOrNew().email, stateEvent)
                    } else {
                        emitStateMessageEvent(
                            stateMessage = StateMessage(
                                response = Response(
                                    messageId = R.string.error,
                                    message = (getApplicationContext() as Context).getString(R.string.passwords_do_not_match),
                                    uiComponentType = UIComponentType.Dialog(),
                                    messageType = MessageType.Error()
                                )
                            ),
                            stateEvent = stateEvent
                        )
                    }
                } else {
                    emitStateMessageEvent(
                        stateMessage = StateMessage(
                            response = Response(
                                messageId = R.string.error,
                                message = (getApplicationContext() as Context).getString(R.string.email_is_registered),
                                uiComponentType = UIComponentType.Dialog(),
                                messageType = MessageType.Error()
                            )
                        ),
                        stateEvent = stateEvent
                    )
                }
            }

            is RegisterLoginActivityStateEvent.CheckIfPasswordMatchesEmailUserEvent -> {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): CheckIfPasswordMatchesEmailUserEvent")
                interactors.checkIfPasswordMatchesEmail.checkIfPasswordMatchesEmail(
                    getCurrentViewStateOrNew().email,
                    getCurrentViewStateOrNew().password,
                    stateEvent)
            }

            is RegisterLoginActivityStateEvent.LoginAppUserStateEvent -> {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): LoginAppUserStateEvent")
                //interactors.getUser.getEntity(getCurrentViewStateOrNew().idOfAppUserToLogin, stateEvent)
                val returnViewState: RegisterLoginActivityViewState<ProjectUser> = getCurrentViewStateOrNew()
                val updateReturnViewState: (viewState: RegisterLoginActivityViewState<ProjectUser>, projectUser: ProjectUser?)-> RegisterLoginActivityViewState<ProjectUser> = {
                        viewState, user -> viewState.apply {
                            appProjectUser = user
                            mainEntity = user
                        }
                }
                val getUpdateDate: (ProjectUser) -> String? = {
                    it.updated_at
                }
                val setUpdateDate: (projectUser:ProjectUser, newUpdateDate:String)->ProjectUser = {
                    user, newUpdateDate -> user.apply { updated_at = newUpdateDate }
                }
                interactors.getEntity.getEntity(
                    getCurrentViewStateOrNew().idOfAppUserToLogin,
                    stateEvent,
                    returnViewState,
                    updateReturnViewState,
                    getUpdateDate,
                    setUpdateDate,
                )
            }

            is RegisterLoginActivityStateEvent.RegisterUserEvent -> {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): RegisterUserEvent")
                interactors.registerUser.registerUser(getAppUser(), stateEvent)
            }

            is RegisterLoginActivityStateEvent.LoginUserEvent -> {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): LoginUserEvent")
                if(LOG_ME)ALog.d(
                    TAG, ".$methodName(): " +
                        "login user: " + getAppUser().toString())
                val okButtonCallbackWhenNoInternetConnection = object: OkButtonCallback {
                    override fun proceed() {
                        setKillAppAndOpenNetworkSettings(true)
                    }
                }
                interactors.loginUser.loginUser(
                    getAppUser(),
                    stateEvent,
                    okButtonCallbackWhenNoInternetConnection,
                )
            }

            is RegisterLoginActivityStateEvent.AddNewAppUserEvent -> {
                if (LOG_ME) ALog.d(TAG, "$methodName: AddNewAppUserEvent")
                //interactors.insertUser.insertNewEntity(getAppUser(), stateEvent)
                val updateReturnViewState: (
                    viewState: RegisterLoginActivityViewState<ProjectUser>,
                    projectUser: ProjectUser?,
                )-> RegisterLoginActivityViewState<ProjectUser> = {
                    viewState, user -> viewState.apply {
                        appProjectUser = user
                        mainEntity = user
                    }
                }
                val user = getAppUser()
                if(LOG_ME){
                    val currentViewState = getCurrentViewStateOrNew()
                    ALog.d(TAG, ".$methodName(): Adding new user.")
                    ALog.d(TAG, ".$methodName(): user == $user")
                    ALog.d(TAG, ".$methodName(): firebaseUser.uid == ${currentViewState.firebaseUser!!.uid}")
                }
                interactors.insertEntity.insertNewEntity(
                    user,
                    stateEvent,
                    getCurrentViewStateOrNew(),
                    updateReturnViewState
                )
            }

            is RegisterLoginActivityStateEvent.UpdateAppUserEvent -> {
                if (LOG_ME) ALog.d(TAG, "$methodName: UpdateAppUserEvent")
                //interactors.updateUser.updateEntity(entity = stateEvent.appUser, stateEvent = stateEvent)
                val updateReturnViewState: (viewState: RegisterLoginActivityViewState<ProjectUser>, projectUser: ProjectUser?)-> RegisterLoginActivityViewState<ProjectUser> = {
                        viewState, user -> viewState.apply {
                        appProjectUser = user
                        mainEntity = user
                    }
                }
                interactors.updateEntity.updateEntity(
                    entity = stateEvent.appProjectUser,
                    stateEvent = stateEvent,
                    getCurrentViewStateOrNew(),
                    updateReturnViewState
                )
            }

            is RegisterLoginActivityStateEvent.DeleteAppUserEvent -> {
                if (LOG_ME) ALog.d(TAG, "$methodName: DeleteAppUserEvent")
                //interactors.deleteUser.deleteEntity(stateEvent.appUser, stateEvent = stateEvent)
                val getId: (ProjectUser) -> UniqueID? = {
                    it.id
                }
                interactors.deleteEntity.deleteEntity(stateEvent.appProjectUser, stateEvent = stateEvent, getId)
            }

            is RegisterLoginActivityStateEvent.CreateStateMessageEvent -> {
                if (LOG_ME) ALog.d(TAG, "$methodName: CreateStateMessageEvent")
                emitStateMessageEvent(
                    stateMessage = stateEvent.stateMessage,
                    stateEvent = stateEvent
                )
            }

            is RegisterLoginActivityStateEvent.None -> {
                if (LOG_ME) ALog.d(TAG, "$methodName: None")
                launchJob = false
                interactors.doNothingAtAll.doNothing()
            }

            else -> {
                emitInvalidStateEvent(stateEvent)
            }
        }
        if (LOG_ME) ALog.d(TAG, "launching stateEvent in job: ${stateEvent.eventName()}")
        launchJob(stateEvent, job, launchJob)
        if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
    }

    fun emailIsValid(email : String? = getCurrentViewStateOrNew().email): Boolean {
        return RegexEmailValidator.validate(email)
    }

    open fun passwordsMatch(): Boolean {
        return StringComparator.stringsAreEqual(
            getCurrentViewStateOrNew()?.password,
            getCurrentViewStateOrNew()?.passwordConfirmation)
    }

    fun beginPendingDelete(entity: ProjectUser){
        setStateEvent(
            RegisterLoginActivityStateEvent.DeleteAppUserEvent(
                appProjectUser = entity
            )
        )
    }

    private fun getAppUser(): ProjectUser {
        val currentViewState = getCurrentViewStateOrNew()
        val firebaseUser = currentViewState.firebaseUser
        val appUser = currentViewState.appProjectUser
        return if(appUser != null) {
            if(appUser.id == null) {
                if(LOG_ME)ALog.d(
                    TAG, ".getAppUser(): " +
                        "appUser != null but appUser.id == null Getting id from firebaseUser")
                if(firebaseUser != null) {
                    appUser.id = UserUniqueID(firebaseUser.uid, firebaseUser.uid)
                } else {
                    ALog.e(TAG, ".getAppUser(): firebaseUser == null")
//                    throw NullPointerException("firebaseUser == null")
                }
            }
            appUser
        } else {
            if(LOG_ME)ALog.d(
                TAG, ".getAppUser(): " +
                    "appUser == null generating new entity.")
            val user = entityFactory.generateEmpty()
            if(firebaseUser != null) {
                user.id = UserUniqueID(firebaseUser.uid, firebaseUser.uid)
            } else {
                ALog.e(TAG, ".getAppUser(): firebaseUser == null")
//                throw NullPointerException("firebaseUser == null")
            }
            user.emailAddress = currentViewState.email.toString()
            user.password = currentViewState.password.toString()
            this.setAppUser(user)
            user
        }
    }

    override fun initNewViewState(): RegisterLoginActivityViewState<ProjectUser> {
        return RegisterLoginActivityViewState()
    }


    override fun setMainEntity(mainEntity: ProjectUser?){
        if(LOG_ME)ALog.d(TAG, ".setMainEntity(): Setting main entity")
        val update = getCurrentViewStateOrNew()
        update.appProjectUser = mainEntity
        update.mainEntity = mainEntity
        setViewState(update)
    }

    fun setNavigatedAway(navigatedAway: Boolean){
        if(LOG_ME)ALog.d(TAG, ".setNavigatedAway(): Setting navigatedAway")
        val update = getCurrentViewStateOrNew()
        update.navigatedAway = navigatedAway
        setViewState(update)
    }

    fun setDeviceIsCompatible(deviceIsCompatible: Boolean?){
        if(LOG_ME)ALog.d(TAG, ".setDeviceIsCompatible(): Setting deviceIsCompatible to $deviceIsCompatible")
        val update = getCurrentViewStateOrNew()
        update.deviceIsCompatible = deviceIsCompatible
        setViewState(update)
    }

    private fun setAppUser(appProjectUser: ProjectUser?){
        val update = getCurrentViewStateOrNew()
        update.appProjectUser = appProjectUser
        update.mainEntity = appProjectUser
        setViewState(update)
    }

    fun setKillAppAndOpenNetworkSettings(killAppAndOpenNetworkSettings: Boolean?){
        val update = getCurrentViewStateOrNew()
        update.killAppAndOpenNetworkSettings = killAppAndOpenNetworkSettings
        setViewState(update)
    }

    private fun setFirebaseUser(firebaseUser: FirebaseUser?){
        val update = getCurrentViewStateOrNew()
        update.firebaseUser = firebaseUser
        setViewState(update)
    }

    private fun setUsersIdIfNecessary(firebaseUser: FirebaseUser?){
        val update = getCurrentViewStateOrNew()
        if(firebaseUser != null) {
            val userUniqueId = UserUniqueID(firebaseUser.uid, firebaseUser.uid)
            if(update.appProjectUser != null) {
                update.appProjectUser!!.id = userUniqueId
            } else {
                ALog.e(TAG, ".setUsersIdIfNecessary(): appUser != null")
            }
            if(update.mainEntity != null) {
                update.mainEntity!!.id = userUniqueId
            } else {
                ALog.e(TAG, ".setUsersIdIfNecessary(): mainEntity != null")
            }
        } else {
            ALog.e(TAG, ".setUsersIdIfNecessary(): firebaseUser == null")
        }
        setViewState(update)
    }

    private fun setEmailIsTaken(emailIsTaken: Boolean?) {
        val update = getCurrentViewStateOrNew()
        update.emailIsTaken = emailIsTaken ?: true
        setViewState(update)
    }

    private fun setFirebaseUserIsRegistered(firebaseUserIsRegistered: Boolean?) {
        val update = getCurrentViewStateOrNew()
        update.firebaseUserIsRegistered = firebaseUserIsRegistered ?: false
        setViewState(update)
    }

    private fun setPasswordMatchesEmail(passwordMatchesEmail: Boolean?) {
        val update = getCurrentViewStateOrNew()
        update.passwordMatchesEmail = passwordMatchesEmail ?: false
        setViewState(update)
    }

    private fun setNoCachedEntity(noCachedEntity: Boolean?) {
        val update = getCurrentViewStateOrNew()
        update.noCachedEntity = noCachedEntity
        setViewState(update)
    }

    private fun setFirebaseUserIsLoggedIn(firebaseUserIsLoggedIn: Boolean?) {
        val update = getCurrentViewStateOrNew()
        update.firebaseUserIsLoggedIn = firebaseUserIsLoggedIn ?: false
        setViewState(update)
    }


    fun setCollapsingToolbarState(
        state: CollapsingToolbarState
    ) = interactionManager.setCollapsingToolbarState(state)

    fun updateEmail(email: String?){
        val methodName = "updateEmail"
        if(emailIsValid(email)) {
            if(LOG_ME)ALog.d(TAG, ".$methodName(): Email $email is valid. Updating it in ViewModel")
            val update = getCurrentViewStateOrNew()
            update.email = email
            if (email != null) {
                update.appProjectUser?.emailAddress = email
            }
            setViewState(update)
        } else {
            if(LOG_ME)ALog.d(TAG, ".$methodName(): Email $email is invalid.")
            setStateEvent(
                RegisterLoginActivityStateEvent.CreateStateMessageEvent(
                    stateMessage = StateMessage(
                        response = Response(
                            messageId = UserCacheEntity.nullEmailError(),
                            message = (getApplicationContext() as Context).getString(UserCacheEntity.nullEmailError()),
//                            uiComponentType = UIComponentType.Dialog(),
                            uiComponentType = UIComponentType.SnackBar(
                                null,
                                R.string.text_ok,
                                (getApplicationContext() as Context).getString(R.string.text_ok),
                                View.OnClickListener {  }
                            ),
                            messageType = MessageType.Error()
                        )
                    )
                )
            )
        }
    }

    fun clearEmail() {
        val update = getCurrentViewStateOrNew()
        update.email = null
        setViewState(update)
    }

    fun clearPassword() {
        val update = getCurrentViewStateOrNew()
        update.password = null
        setViewState(update)
    }

    fun clearPasswordConfirmation() {
        val update = getCurrentViewStateOrNew()
        update.passwordConfirmation = null
        setViewState(update)
    }

    fun clearAppUser() {
        val update = getCurrentViewStateOrNew()
        update.mainEntity = null
        update.appProjectUser = null
        setViewState(update)
    }

    fun updatePassword(password: String?){
        val methodName = "updatePassword"
        if(passwordIsValid(password)) {
            if(LOG_ME)ALog.d(TAG, ".$methodName(): Password $password is valid. Updating it in ViewModel")
            val update = getCurrentViewStateOrNew()
            update.password = password
            if (password != null) {
                update.appProjectUser?.password = password
            }
            setViewState(update)
        } else {
            setStateEvent(
                RegisterLoginActivityStateEvent.CreateStateMessageEvent(
                    stateMessage = StateMessage(
                        response = Response(
                            messageId = R.string.text_ok,
                            message = (getApplicationContext() as Context).getString(R.string.text_ok),
//                            uiComponentType = UIComponentType.Dialog(),
                            uiComponentType = UIComponentType.SnackBar(
                                null,
                                UserCacheEntity.nullPasswordError(),
                                (getApplicationContext() as Context).getString(UserCacheEntity.nullPasswordError()),
                                View.OnClickListener {  }
                            ),
                            messageType = MessageType.Error()
                        )
                    )
                )
            )
        }
    }

    fun updatePasswordConfirmation(passwordConfirmation: String?){
        if(passwordIsValid(passwordConfirmation)) {
            val update = getCurrentViewStateOrNew()
            update.passwordConfirmation = passwordConfirmation
            setViewState(update)
        } else {
            setStateEvent(
                RegisterLoginActivityStateEvent.CreateStateMessageEvent(
                    stateMessage = StateMessage(
                        response = Response(
                            messageId = R.string.text_ok,
                            message = (getApplicationContext() as Context).getString(R.string.text_ok),
//                            uiComponentType = UIComponentType.Dialog(),
                            uiComponentType = UIComponentType.SnackBar(
                                null,
                                UserCacheEntity.nullPasswordError(),
                                (getApplicationContext() as Context).getString(UserCacheEntity.nullPasswordError()),
                                View.OnClickListener {  }
                            ),
                            messageType = MessageType.Error()
                        )
                    )
                )
            )
        }
    }

    private fun passwordIsValid(password : String?): Boolean {
        return password?.isNotEmpty() ?: false
    }

    fun setEmailInteractionState(state: RegisterLoginActivityInteractionState){
        interactionManager.setNewEmailState(state)
    }

    fun setPasswordInteractionState(state: RegisterLoginActivityInteractionState){
        interactionManager.setNewPasswordState(state)
    }

    fun setPasswordConfirmationInteractionState(state: RegisterLoginActivityInteractionState){
        interactionManager.setNewPasswordConfirmationState(state)
    }

    fun isToolbarCollapsed() = collapsingToolbarState.toString()
        .equals(CollapsingToolbarState.Collapsed().toString())

    fun isToolbarExpanded() = collapsingToolbarState.toString()
        .equals(CollapsingToolbarState.Expanded().toString())

    // return true if in EditState
    fun checkEditState() = interactionManager.checkEditState()

    fun exitEditState() = interactionManager.exitEditState()

    fun isEditingEmail() = interactionManager.isEditingEmail()

    fun isEditingPassword() = interactionManager.isEditingPassword()

    fun isEditingPasswordConfirmation() = interactionManager.isEditingPasswordConfirmation()


    // force observers to refresh
    fun triggerEntityObservers(){
        getCurrentViewStateOrNew().appProjectUser?.let { appUser ->
            setAppUser(appUser)
        }
        getCurrentViewStateOrNew().mainEntity?.let { mainEntity ->
            setMainEntity(mainEntity)
        }
        getCurrentViewStateOrNew().listOfEntities?.let { listOfEntities ->
            setListOfEntities(listOfEntities)
        }
    }

    private fun isDeviceCompatible(): Boolean {
        val methodName: String = "isDeviceCompatible"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        var result = false
        try {
//            result = deviceSupportsNfc()
            result = true
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
        return result
    }

    // https://www.youtube.com/watch?v=xgjaC4jQkZk
    private fun deviceSupportsNfc(): Boolean {
        val methodName: String = "deviceSupportsNfc"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        var result = false
        try {
            val nfcManager: NfcManager = (getApplicationContext() as Context)
                .getSystemService(NFC_SERVICE) as NfcManager
            val nfcAdapter: NfcAdapter = nfcManager.defaultAdapter
            nfcAdapter?.let { result = nfcAdapter.isEnabled }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
        return result
    }
}
