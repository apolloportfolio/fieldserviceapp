package com.aps.catemplateapp.feature01.framework.presentation.activity01.state

import android.os.Parcelable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.aps.catemplateapp.common.business.domain.state.ViewState
import com.aps.catemplateapp.common.business.domain.state.ViewStateCompose
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.parcel.Parcelize


@Parcelize
data class RegisterLoginActivityViewState<T>(
    var appProjectUser: ProjectUser? = null,
    var firebaseUser: FirebaseUser? = null,
    var firebaseUserIsRegistered: Boolean? = null,
    var firebaseUserIsLoggedIn: Boolean? = null,
    var email: String? = null,
    var password: String? = null,
    var passwordConfirmation: String? = null,
    var emailIsTaken: Boolean = true,
    var passwordMatchesEmail: Boolean = false,
    var idOfAppUserToLogin: UniqueID = UniqueID.INVALID_ID,
    var deviceIsCompatible: Boolean? = null,
    var killAppAndOpenNetworkSettings: Boolean? = null,
    var navigatedAway: Boolean = false,
) : Parcelable, ViewState<T>() {
    override fun copy(): RegisterLoginActivityViewState<T> {
        return RegisterLoginActivityViewState<T>().apply {
            // Copy properties from the ViewState superclass
            mainEntity = this@RegisterLoginActivityViewState.mainEntity
            listOfEntities = ArrayList(this@RegisterLoginActivityViewState.listOfEntities.orEmpty())
            isUpdatePending = this@RegisterLoginActivityViewState.isUpdatePending
            isInternetAvailable = this@RegisterLoginActivityViewState.isInternetAvailable
            isNetworkRepositoryAvailable = this@RegisterLoginActivityViewState.isNetworkRepositoryAvailable
            noCachedEntity = this@RegisterLoginActivityViewState.noCachedEntity
            snackBarState = this@RegisterLoginActivityViewState.snackBarState?.copy()
            toastState = this@RegisterLoginActivityViewState.toastState?.shallowCopy()
            dialogState = this@RegisterLoginActivityViewState.dialogState?.copy()

            // Copy properties specific to RegisterLoginActivityViewState
            appProjectUser = this@RegisterLoginActivityViewState.appProjectUser
            firebaseUser = this@RegisterLoginActivityViewState.firebaseUser
            firebaseUserIsRegistered = this@RegisterLoginActivityViewState.firebaseUserIsRegistered
            firebaseUserIsLoggedIn = this@RegisterLoginActivityViewState.firebaseUserIsLoggedIn
            email = this@RegisterLoginActivityViewState.email
            password = this@RegisterLoginActivityViewState.password
            passwordConfirmation = this@RegisterLoginActivityViewState.passwordConfirmation
            emailIsTaken = this@RegisterLoginActivityViewState.emailIsTaken
            passwordMatchesEmail = this@RegisterLoginActivityViewState.passwordMatchesEmail
            idOfAppUserToLogin = this@RegisterLoginActivityViewState.idOfAppUserToLogin
            deviceIsCompatible = this@RegisterLoginActivityViewState.deviceIsCompatible
            killAppAndOpenNetworkSettings = this@RegisterLoginActivityViewState.killAppAndOpenNetworkSettings
            navigatedAway = this@RegisterLoginActivityViewState.navigatedAway
        }
    }
}

// Version with LiveData
//data class RegisterLoginActivityViewState<T>(
//    // User related properties
//    private val _appProjectUser: MutableLiveData<ProjectUser?> = MutableLiveData(),
//    private val _firebaseUser: MutableLiveData<FirebaseUser?> = MutableLiveData(),
//    private val _firebaseUserIsRegistered: MutableLiveData<Boolean?> = MutableLiveData(),
//    private val _firebaseUserIsLoggedIn: MutableLiveData<Boolean?> = MutableLiveData(),
//    private val _email: MutableLiveData<String?> = MutableLiveData(),
//    private val _password: MutableLiveData<String?> = MutableLiveData(),
//    private val _passwordConfirmation: MutableLiveData<String?> = MutableLiveData(),
//    private val _emailIsTaken: MutableLiveData<Boolean> = MutableLiveData(true),
//    private val _passwordMatchesEmail: MutableLiveData<Boolean> = MutableLiveData(false),
//    private val _idOfAppUserToLogin: MutableLiveData<UniqueID> = MutableLiveData(UniqueID.INVALID_ID),
//
//    // Device compatibility properties
//    private val _deviceIsCompatible: MutableLiveData<Boolean?> = MutableLiveData(),
//    private val _killAppAndOpenNetworkSettings: MutableLiveData<Boolean?> = MutableLiveData(),
//
//    // Navigation related properties
//    private val _navigatedAway: MutableLiveData<Boolean> = MutableLiveData(false),
//) : ViewStateCompose<T>() {
//
//    // User related properties
//    val appProjectUser: LiveData<ProjectUser?> get() = _appProjectUser
//    val firebaseUser: LiveData<FirebaseUser?> get() = _firebaseUser
//    val firebaseUserIsRegistered: LiveData<Boolean?> get() = _firebaseUserIsRegistered
//    val firebaseUserIsLoggedIn: LiveData<Boolean?> get() = _firebaseUserIsLoggedIn
//    val email: LiveData<String?> get() = _email
//    val password: LiveData<String?> get() = _password
//    val passwordConfirmation: LiveData<String?> get() = _passwordConfirmation
//    val emailIsTaken: LiveData<Boolean> get() = _emailIsTaken
//    val passwordMatchesEmail: LiveData<Boolean> get() = _passwordMatchesEmail
//    val idOfAppUserToLogin: LiveData<UniqueID> get() = _idOfAppUserToLogin
//
//    // Device compatibility properties
//    val deviceIsCompatible: LiveData<Boolean?> get() = _deviceIsCompatible
//    val killAppAndOpenNetworkSettings: LiveData<Boolean?> get() = _killAppAndOpenNetworkSettings
//
//    // Navigation related properties
//    val navigatedAway: LiveData<Boolean> get() = _navigatedAway
//
//    // Setter methods for user related properties
//    fun setAppProjectUser(newValue: ProjectUser?) { _appProjectUser.value = newValue }
//    fun setFirebaseUser(newValue: FirebaseUser?) { _firebaseUser.value = newValue }
//    fun setFirebaseUserIsRegistered(newValue: Boolean?) { _firebaseUserIsRegistered.value = newValue }
//    fun setFirebaseUserIsLoggedIn(newValue: Boolean?) { _firebaseUserIsLoggedIn.value = newValue }
//    fun setEmail(newValue: String?) { _email.value = newValue }
//    fun setPassword(newValue: String?) { _password.value = newValue }
//    fun setPasswordConfirmation(newValue: String?) { _passwordConfirmation.value = newValue }
//    fun setEmailIsTaken(newValue: Boolean) { _emailIsTaken.value = newValue }
//    fun setPasswordMatchesEmail(newValue: Boolean) { _passwordMatchesEmail.value = newValue }
//    fun setIdOfAppUserToLogin(newValue: UniqueID) { _idOfAppUserToLogin.value = newValue }
//
//    // Setter methods for device compatibility properties
//    fun setDeviceIsCompatible(newValue: Boolean?) { _deviceIsCompatible.value = newValue }
//    fun setKillAppAndOpenNetworkSettings(newValue: Boolean?) { _killAppAndOpenNetworkSettings.value = newValue }
//
//    // Setter method for navigation related properties
//    fun setNavigatedAway(newValue: Boolean) { _navigatedAway.value = newValue }
//}