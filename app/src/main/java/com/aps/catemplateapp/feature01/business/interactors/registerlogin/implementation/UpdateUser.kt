package com.aps.catemplateapp.feature01.business.interactors.registerlogin.implementation

import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.cache.CacheResponseHandler
import com.aps.catemplateapp.common.business.data.util.safeApiCall
import com.aps.catemplateapp.common.business.data.util.safeCacheCall
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.feature01.business.interactors.registerlogin.abstraction.UpdateUser
import com.aps.catemplateapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class UpdateUserImpl @Inject constructor(
    private val cacheDataSource: UserCacheDataSource,
    private val networkDataSource: UserNetworkDataSource
): UpdateUser {

    override fun updateEntity(
        entity: ProjectUser,
        stateEvent: StateEvent
    ): Flow<DataState<RegisterLoginActivityViewState<ProjectUser>>?> = flow {
        val cacheResult = safeCacheCall(Dispatchers.IO){
            cacheDataSource.updateEntity(
                entity.id,
                entity.updated_at,
                entity.created_at,
                entity.emailAddress,
                entity.password,
                entity.profilePhotoImageURI,
                entity.name,
                entity.surname,
                entity.description,
                entity.city,
                entity.emailAddressVerified,
                entity.phoneNumberVerified,
            )
        }

        val response = object: CacheResponseHandler<RegisterLoginActivityViewState<ProjectUser>, Int>(
            response = cacheResult,
            stateEvent = stateEvent
        ){
            override suspend fun handleSuccess(resultObj: Int): DataState<RegisterLoginActivityViewState<ProjectUser>>? {
                return if(resultObj > 0){
                    DataState.data(
                        response = Response(
                            messageId = R.string.error,
                            message = UPDATE_ENTITY_SUCCESS,
                            uiComponentType = UIComponentType.Toast(),
                            messageType = MessageType.Success()
                        ),
                        data = null,
                        stateEvent = stateEvent
                    )
                }
                else{
                    DataState.data(
                        response = Response(
                            messageId = R.string.error,
                            message = UPDATE_ENTITY_FAILED,
                            uiComponentType = UIComponentType.Toast(),
                            messageType = MessageType.Error()
                        ),
                        data = null,
                        stateEvent = stateEvent
                    )
                }
            }
        }.getResult()

        emit(response)

        updateNetwork(response?.stateMessage?.response?.message, entity)
    }

    private suspend fun updateNetwork(response: String?, note: ProjectUser) {
        if(response.equals(UPDATE_ENTITY_SUCCESS)){

            safeApiCall(Dispatchers.IO){
                networkDataSource.insertOrUpdateEntity(note)
            }
        }
    }

    companion object{
        val UPDATE_ENTITY_SUCCESS = "Successfully updated entity."
        val UPDATE_ENTITY_FAILED = "Failed to update entity."
        val UPDATE_ENTITY_FAILED_PK = "Update failed. Entity is missing primary key."

    }
}