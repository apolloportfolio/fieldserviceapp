package com.aps.catemplateapp.feature01.business.interactors.registerlogin

import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.business.interactors.abstraction.*
import com.aps.catemplateapp.feature01.business.interactors.registerlogin.abstraction.*
import javax.inject.Inject

// Use cases
class RegisterLoginInteractors<
        Entity,
        CacheDataSource: StandardCacheDataSource<Entity>,
        NetworkDataSource: StandardNetworkDataSource<Entity>,
        ViewState: com.aps.catemplateapp.common.business.domain.state.ViewState<Entity>>
@Inject
constructor(
    val registerUser: RegisterUser,
    val loginUser: LoginUser,
    val checkIfEmailIsTaken: CheckIfEmailIsTaken,
    val checkIfPasswordMatchesEmail: CheckIfPasswordMatchesEmail,
    val updateUser: UpdateUser,

    val getEntity: GetEntity<
            Entity,
            CacheDataSource,
            NetworkDataSource,
            ViewState
            >,
    val getAllCachedEntities: GetAllCachedEntities<
            Entity,
            CacheDataSource,
            NetworkDataSource,
            ViewState
            >,
    val insertEntity: InsertEntity<
            Entity,
            CacheDataSource,
            NetworkDataSource,
            ViewState
            >,
    val updateEntity: UpdateEntity<
            Entity,
            CacheDataSource,
            NetworkDataSource,
            ViewState
            >,
    val deleteEntity: DeleteEntity<
            Entity,
            CacheDataSource,
            NetworkDataSource,
            ViewState
            >,
    val doNothingAtAll: DoNothingAtAll<
            Entity,
            CacheDataSource,
            NetworkDataSource,
            ViewState
            >,
)