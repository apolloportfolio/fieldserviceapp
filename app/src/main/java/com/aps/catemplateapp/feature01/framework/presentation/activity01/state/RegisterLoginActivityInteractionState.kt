package com.aps.catemplateapp.feature01.framework.presentation.activity01.state

sealed class RegisterLoginActivityInteractionState {
    class EditState: RegisterLoginActivityInteractionState() {

        override fun toString(): String {
            return "EditState"
        }
    }

    class DefaultState: RegisterLoginActivityInteractionState(){

        override fun toString(): String {
            return "DefaultState"
        }
    }
}