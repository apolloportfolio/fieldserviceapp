package com.aps.catemplateapp.core.business.data.network.implementation

import android.net.Uri
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.data.util.FakeFirestoreIDGenerator
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUsersRating
import com.google.firebase.auth.FirebaseUser
import io.mockk.every
import io.mockk.mockk

private const val TAG = "FakeUserNetworkDataSourceImpl"
private const val LOG_ME = true

class FakeUserNetworkDataSourceImpl constructor(
    private val data: HashMap<String, ProjectUser>,
    private val deletedData: HashMap<String, ProjectUser>,
    private val ratings: HashMap<String, ProjectUsersRating>,
    private val fakeFirestoreIDGenerator: FakeFirestoreIDGenerator,
    private val dateUtil: DateUtil,
) : UserNetworkDataSource {
    private var user: FirebaseUser = mockk<FirebaseUser>(relaxed = true)
    private var userIsRegistered: Boolean = false
    private var userIsLoggedIn: Boolean = false
    private var usersEmail: String? = null
    private var usersPassword: String? = null

    init {
        every { user.email } returns this.usersEmail
    }

    override suspend fun registerNewUser(entity: ProjectUser): FirebaseUser? {
        this.usersPassword = entity.password
        this.usersEmail = entity.emailAddress
        this.userIsRegistered = true
        return this.user
    }

    override suspend fun updateUser(entity: ProjectUser): ProjectUser? {
        this.usersPassword = entity.password
        this.usersEmail = entity.emailAddress
        return entity
    }

    override suspend fun loginUser(entity: ProjectUser): FirebaseUser? {
        return if(this.usersPassword == entity.password && this.usersEmail == entity.emailAddress) {
            this.userIsLoggedIn = true
            this.user
        } else {
            ALog.w(TAG, "loginUser(): email or password dont match")
            null
        }
    }

    override suspend fun logoutUser(entity: ProjectUser) {
        this.userIsLoggedIn = false
    }

    override suspend fun insertOrUpdateEntity(entity: ProjectUser): ProjectUser? {
        val methodName: String = "insertOrUpdateEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewUserFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewUserFirestoreId()
            val newEntity = ProjectUser(
                entity.id,
                entity.created_at,
                dateUtil.getCurrentTimestamp(),
                entity.emailAddress,
                entity.password,
                entity.profilePhotoImageURI,
                entity.name,
                entity.surname,
                entity.description,
                entity.city,
                entity.emailAddressVerified,
                entity.phoneNumberVerified,
            )
            this.data[newEntity.id!!.getFirestoreId()] = newEntity
            return newEntity
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return null
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun getEntityByCredentials(email: String?, password: String?): ProjectUser? {
        val methodName: String = "getEntityByCredentials"
        var result: ProjectUser?
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            for(user: ProjectUser in this.data.values) {
                if(user.emailAddress == email && user.password == password)return user
            }
            if (LOG_ME) ALog.d(TAG, "$methodName(): User not found by credentials.")
            return null
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return null
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        val methodName: String = "deleteEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if (primaryKey != null) {
                this.data.remove(primaryKey.firestoreDocumentID)
            } else {
                ALog.w(TAG, "$methodName(): Failed to delete entity.")
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun insertDeletedEntity(entity: ProjectUser) {
        val methodName: String = "insertDeletedEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewUserFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewUserFirestoreId()
            this.deletedData[entity.id!!.getFirestoreId()] = entity
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun insertDeletedEntities(entities: List<ProjectUser>) {
        for(entity in entities){
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewUserFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewUserFirestoreId()
            this.deletedData[entity.id!!.getFirestoreId()] = entity
        }
    }

    override suspend fun deleteDeletedEntity(entity: ProjectUser) {
        entity.id?.let { this.deletedData.remove(it.firestoreDocumentID) }
    }

    override suspend fun getDeletedEntities(): List<ProjectUser> {
        return ArrayList(this.deletedData.values)
    }

    override suspend fun deleteAllEntities() {
        this.deletedData.clear()
    }

    override suspend fun searchEntity(entity: ProjectUser): ProjectUser? {
        return this.data[entity.id?.firestoreDocumentID]
    }

    override suspend fun getAllEntities(): List<ProjectUser> {
        return ArrayList(this.data.values)
    }

    override suspend fun insertOrUpdateEntities(entities: List<ProjectUser>): List<ProjectUser>? {
        for(entity in entities) {
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewUserFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewUserFirestoreId()
            this.data[entity.id!!.firestoreDocumentID] = entity
        }
        return entities
    }

    override suspend fun checkIfEmailExistsInRepository(email: String?): Boolean {
        val methodName: String = "checkIfEmailExistsInRepository"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            for(user: ProjectUser in this.data.values) {
                if(user.emailAddress == email)return true
            }
            return false
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return false
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun getIdOfUserWithTheseCredentials(
        email: String?,
        password: String?
    ): UserUniqueID? {
        val methodName: String = "getIdOfUserWithTheseCredentials"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            for(user: ProjectUser in this.data.values) {
                if(user.emailAddress == email && user.password == password)return user.id
            }
            if (LOG_ME) ALog.d(TAG, "$methodName(): User not found by credentials.")
            return null
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return null
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun getEntityById(id: UniqueID): ProjectUser? {
        return this.data[id.firestoreDocumentID]
    }

    override suspend fun deleteAccount(user: ProjectUser) {
        user.id?.let { this.data.remove(it.firestoreDocumentID) }
    }

    override suspend fun uploadUsersProfilePhotoToFirestore(
        projectUser: ProjectUser,
        usersProfilePhotoUri: Uri
    ): String? {
        return "Requires instrumentation."
    }

    override suspend fun uploadUsersProfilePhotoToFirestore(
        userID: UserUniqueID,
        usersProfilePhotoUri: Uri
    ): String? {
        return "Requires instrumentation."
    }

    override suspend fun getUsersRating(userID: UserUniqueID): ProjectUsersRating? {
        return ratings[userID.firestoreDocumentID]
    }

    override suspend fun insertUsersRating(usersUsersRating: ProjectUsersRating) {
        ratings[usersUsersRating.usersID.firestoreDocumentID] = usersUsersRating
    }

}