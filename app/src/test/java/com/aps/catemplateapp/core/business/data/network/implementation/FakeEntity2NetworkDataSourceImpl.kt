package com.aps.catemplateapp.core.business.data.network.implementation

import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.feature02.business.data.network.abs.Entity2NetworkDataSource
import com.aps.catemplateapp.core.business.data.util.FakeFirestoreIDGenerator
import com.aps.catemplateapp.feature02.business.domain.model.entities.Entity2

private const val TAG = "FakeEntity2NetworkDataSourceImpl"
private const val LOG_ME = true

class FakeEntity2NetworkDataSourceImpl constructor(
    private val data: HashMap<UniqueID, Entity2>,
    private val deletedData: HashMap<UniqueID, Entity2>,
    private val fakeFirestoreIDGenerator: FakeFirestoreIDGenerator,
    private val dateUtil: DateUtil
) : Entity2NetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: Entity2): Entity2? {
        val methodName: String = "insertOrUpdateEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            val newEntity = Entity2(
                entity.id,
                entity.created_at,
                dateUtil.getCurrentTimestamp(),

                entity.picture1URI,

                entity.description,

                entity.ownerID,
            )
            this.data[newEntity.id!!] = newEntity
            return newEntity
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return null
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        val methodName: String = "deleteEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if (primaryKey != null) {
                this.data.remove(primaryKey)
            } else {
                ALog.w(TAG, "$methodName(): Failed to delete entity.")
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun insertDeletedEntity(entity: Entity2) {
        val methodName: String = "insertDeletedEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            this.deletedData[entity.id!!] = entity
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun insertDeletedEntities(entities: List<Entity2>) {
        for(entity in entities){
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            this.deletedData[entity.id!!] = entity
        }
    }

    override suspend fun deleteDeletedEntity(entity: Entity2) {
        entity.id?.let { this.deletedData.remove(it) }
    }

    override suspend fun getDeletedEntities(): List<Entity2> {
        return ArrayList(this.deletedData.values)
    }

    override suspend fun deleteAllEntities() {
        this.deletedData.clear()
    }

    override suspend fun searchEntity(entity: Entity2): Entity2? {
        return this.data[entity.id]
    }

    override suspend fun getAllEntities(): List<Entity2> {
        return ArrayList(this.data.values)
    }

    override suspend fun insertOrUpdateEntities(entities: List<Entity2>): List<Entity2>? {
        for(entity in entities) {
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            this.data[entity.id!!] = entity
        }
        return entities
    }

    override suspend fun getEntityById(id: UniqueID): Entity2? {
        return this.data[id]
    }

    override suspend fun getUsersEntities2(userId: UserUniqueID): List<Entity2>? {
        val result = ArrayList<Entity2>()
        for(entity in data.values) {
            if(entity.ownerID!!.equalByValueTo(userId)) result.add(entity)
        }
        return result
    }
}