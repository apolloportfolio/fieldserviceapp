package com.aps.catemplateapp.core.business.data

import com.aps.catemplateapp.feature02.business.domain.model.entities.Entity1
import com.google.common.reflect.TypeToken
import com.google.gson.Gson

class EntityDataFactoryEntity1(
    override val testClassLoader: ClassLoader,
    override val fileNameWithTestData: String): EntityDataFactory<Entity1>(testClassLoader, fileNameWithTestData) {


    override fun produceListOfEntities(): List<Entity1> {
        return Gson()
            .fromJson(
                getEntitiesFromFile(fileNameWithTestData),
                object : TypeToken<List<Entity1>>() {}.type
            )
    }
}