package com.aps.catemplateapp.core.business.data.cache.implementation

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.feature02.business.data.cache.abstraction.Entity2CacheDataSource
import com.aps.catemplateapp.feature02.business.domain.model.entities.Entity2
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.daos.PAGINATION_PAGE_SIZE
import javax.inject.Inject

class FakeEntity2CacheDataSourceImpl
@Inject
constructor(
    private val entitiesData: HashMap<UniqueID, Entity2>,
    private val dateUtil: DateUtil
): Entity2CacheDataSource {
    override suspend fun insertOrUpdateEntity(entity: Entity2): Entity2? {
        this.insertEntity(entity)
        return this.getEntityById(entity.id)
    }

    override suspend fun insertEntity(entity: Entity2): Long {
        if(entity.id == null){
            throw NullPointerException("entity.id == null when inserting the entity.")
        }
        if(entity.id!!.firestoreDocumentID == null){
            throw NullPointerException("entity.id.firestoreDocumentID == null when inserting the entity.")
        }
        if(entity.id!!.firestoreDocumentID == FORCE_NEW_EXCEPTION){
            throw Exception("Something went wrong inserting the entity.")
        }
        if(entity.id!!.firestoreDocumentID == FORCE_GENERAL_FAILURE){
            return -1 // fail
        }
        entitiesData[entity.id!!] = entity
        return 1 // success
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        if(primaryKey == null){
            throw NullPointerException("entity.id == null when deleting the entity.")
        }
        if(primaryKey!!.firestoreDocumentID == null){
            throw NullPointerException("entity.id.firestoreDocumentID == null when deleting the entity.")
        }
        if(primaryKey.firestoreDocumentID.equals(FakeEntity1CacheDataSourceImpl.FORCE_DELETE_EXCEPTION)){
            throw Exception("Something went wrong deleting the entity.")
        }
        else if(primaryKey.firestoreDocumentID.equals(FakeEntity1CacheDataSourceImpl.FORCE_DELETES_EXCEPTION)){
            throw Exception("Something went wrong deleting the entity.")
        }
        return entitiesData.remove(primaryKey)?.let {
            1 // return 1 for success
        }?: - 1 // -1 for failure
    }

    override suspend fun deleteEntities(entities: List<Entity2>): Int {
        var failOrSuccess = 1
        for(entity in entities){
            if(entitiesData.remove(entity.id) == null){
                failOrSuccess = -1 // mark for failure
            }
        }
        return failOrSuccess
    }

    override suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        description: String?,

        ownerID: UserUniqueID?,
    ): Int {
        if(id == null){
            throw NullPointerException("entity.id == null when inserting the entity.")
        }
        if(id!!.firestoreDocumentID == null){
            throw NullPointerException("entity.id.firestoreDocumentID == null when inserting the entity.")
        }
        if(id!!.firestoreDocumentID == FORCE_NEW_EXCEPTION){
            throw Exception("Something went wrong inserting the entity.")
        }
        if(id!!.firestoreDocumentID == FORCE_GENERAL_FAILURE){
            return -1 // fail
        }
        val updatedEntity = Entity2(
            id,
            updated_at,
            created_at,

            picture1URI,

            description,

            ownerID,
        )
        return entitiesData[id]?.let {
            entitiesData[id] = updatedEntity
            1 // success
        }?: -1 // nothing to update
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<Entity2> {
        if(query == FORCE_SEARCH_EXCEPTION){
            throw Exception("Something went searching the cache for entities.")
        }
        val results: ArrayList<Entity2> = ArrayList()
        for(entity in entitiesData.values){
            if(entity.description?.contains(query) == true){
                results.add(entity)
            }
            else if(entity.description?.contains(query) == true){
                results.add(entity)
            }
            if(results.size > (page * PAGINATION_PAGE_SIZE)){
                break
            }
        }
        return results
    }

    override suspend fun getAllEntities(): List<Entity2> {
        return ArrayList(entitiesData.values)
    }

    override suspend fun getEntityById(id: UniqueID?): Entity2? {
        return entitiesData.get(id)
    }

    override suspend fun getNumEntities(): Int {
        return entitiesData.size
    }

    override suspend fun insertEntities(entities: List<Entity2>): LongArray {
        val results = LongArray(entities.size)
        for((index, entity) in entities.withIndex()){
            results[index] = 1
            entity.id?.let { entitiesData.put(it, entity) }
        }
        return results
    }
    
    companion object {
        const val FORCE_DELETE_EXCEPTION = "FORCE_DELETE_EXCEPTION"
        const val FORCE_DELETES_EXCEPTION = "FORCE_DELETES_EXCEPTION"
        const val FORCE_UPDATE_EXCEPTION = "FORCE_UPDATE_EXCEPTION"
        const val FORCE_NEW_EXCEPTION = "FORCE_NEW_EXCEPTION"
        const val FORCE_SEARCH_EXCEPTION = "FORCE_SEARCHES_EXCEPTION"
        const val FORCE_GENERAL_FAILURE = "FORCE_GENERAL_FAILURE"
    }
}