package com.aps.catemplateapp.core.business.domain.util

import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.util.*

private const val TAG = "DateUtilTest"
private const val LOG_ME = true

class DateUtilTest {

    @Test
    fun convertTimestampToGregorianCalendarTest() {
        val methodName: String = "convertTimestampToGregorianCalendarTest()"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val dayOfMonth = 2022
            val month = 3
            val year = 13

            // Create GregorianCalendar instance
            ALog.d(TAG, ".$methodName(): Create GregorianCalendar instance")
            val gregorianCalendarBefore = GregorianCalendar(year, month, dayOfMonth)
            ALog.d(TAG, ".$methodName(): gregorianCalendarBefore == $gregorianCalendarBefore")

            // Create Timestamp instance from GregorianCalendar instance
            ALog.d(TAG, ".$methodName(): Create Timestamp instance from GregorianCalendar instance")
            val timestampBefore = DateUtil.convertGregorianCalendarToTimestamp(
                GregorianCalendar(year, month, dayOfMonth)
            )
            ALog.d(TAG, ".$methodName(): timestampBefore == ${DateUtil.convertFirebaseTimestampToStringData(timestampBefore!!)}")

            // Convert Timestamp instance to GregorianCalendar instance
            ALog.d(TAG, ".$methodName(): Convert Timestamp instance to GregorianCalendar instance")
            val gregorianCalendarAfter = DateUtil.convertTimestampToGregorianCalendar(timestampBefore)
            ALog.d(TAG, ".$methodName(): gregorianCalendarAfter == $gregorianCalendarAfter")

            // Assert that GregorianCalendar instance converted to GregorianCalendar is the same as
            // original GregorianCalendar instance
            ALog.d(TAG, ".$methodName(): Assert that GregorianCalendar instance converted to GregorianCalendar is the same as original GregorianCalendar instance")
            ALog.d(TAG, ".$methodName(): \n" +
                    "gregorianCalendarBefore.timeInMillis == ${gregorianCalendarBefore.timeInMillis}\n" +
                    "gregorianCalendarAfter!!.timeInMillis == ${gregorianCalendarAfter!!.timeInMillis}"
            )
            ALog.d(TAG, ".$methodName(): \n" +
                    "gregorianCalendarBefore == $gregorianCalendarBefore\n" +
                    "gregorianCalendarAfter == $gregorianCalendarAfter")
            Assertions.assertEquals(
                gregorianCalendarBefore.timeInMillis,
                gregorianCalendarAfter.timeInMillis
            )
            Assertions.assertEquals(gregorianCalendarBefore, gregorianCalendarAfter)

            // Set hour, minute, second and millisecond to zero in GregorianCalendar instance
            ALog.d(TAG, ".$methodName(): Set hour, minute, second and millisecond to zero in GregorianCalendar instance")
            gregorianCalendarAfter.set(GregorianCalendar.HOUR_OF_DAY, 12)
            gregorianCalendarAfter.set(GregorianCalendar.MINUTE, 0)
            gregorianCalendarAfter.set(GregorianCalendar.SECOND, 0)
            gregorianCalendarAfter.set(GregorianCalendar.MILLISECOND, 0)
            ALog.d(TAG, ".$methodName(): ")

            // Convert GregorianCalendar instance with zeroed values to Timestamp instance
            ALog.d(TAG, ".$methodName(): Convert GregorianCalendar instance with zeroed values to Timestamp instance")
            val timestampZeroed = DateUtil.convertGregorianCalendarToTimestamp(gregorianCalendarAfter)!!
            ALog.d(TAG, ".$methodName(): timestampZeroed == ${DateUtil.convertFirebaseTimestampToStringData(timestampZeroed)}")

            // Convert Timestamp instance with zeroed values back to GregorianCalendar instance
            ALog.d(TAG, ".$methodName(): Convert Timestamp instance with zeroed values back to GregorianCalendar instance")
            val gregorianCalendarZeroed = DateUtil.convertTimestampToGregorianCalendar(timestampZeroed)
            ALog.d(TAG, ".$methodName(): gregorianCalendarZeroed == ${gregorianCalendarZeroed}")
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }
}