package com.aps.catemplateapp.core.business.data.network.implementation

import android.net.Uri
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.feature02.business.data.network.abs.Entity1NetworkDataSource
import com.aps.catemplateapp.core.business.data.util.FakeFirestoreIDGenerator
import com.aps.catemplateapp.feature02.business.domain.model.entities.Entity1
import com.aps.catemplateapp.feature02.business.interactors.impl.FirestoreEntity1SearchParameters
import com.aps.catemplateapp.feature02.business.interactors.impl.SearchEntities1Impl

private const val TAG = "FakeEntity1NetworkDataSourceImpl"
private const val LOG_ME = true

class FakeEntity1NetworkDataSourceImpl constructor(
    private val entitiesData: HashMap<UniqueID, Entity1>,
    private val deletedEntitiesData: HashMap<UniqueID, Entity1>,
    private val fakeFirestoreIDGenerator: FakeFirestoreIDGenerator,
    private val dateUtil: DateUtil
) : Entity1NetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: Entity1): Entity1? {
        val methodName: String = "insertOrUpdateEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            val newEntity = Entity1(
                entity.id,
                entity.created_at,
                dateUtil.getCurrentTimestamp(),

                entity.latitude,
                entity.longitude,
                entity.geoLocation,
                entity.firestoreGeoLocation,

                entity.picture1URI,
                entity.description,

                entity.city,
                entity.ownerID,

                entity.name
            )
            this.entitiesData[newEntity.id!!] = newEntity
            return newEntity
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return null
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        val methodName: String = "deleteEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if (primaryKey != null) {
                this.entitiesData.remove(primaryKey)
            } else {
                ALog.w(TAG, "$methodName(): Failed to delete entity.")
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun insertDeletedEntity(entity: Entity1) {
        val methodName: String = "insertDeletedEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            this.deletedEntitiesData[entity.id!!] = entity
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun insertDeletedEntities(entities: List<Entity1>) {
        for(entity in entities){
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            this.deletedEntitiesData[entity.id!!] = entity
        }
    }

    override suspend fun deleteDeletedEntity(entity: Entity1) {
        entity.id?.let { this.deletedEntitiesData.remove(it) }
    }

    override suspend fun getDeletedEntities(): List<Entity1> {
        return ArrayList(this.deletedEntitiesData.values)
    }

    override suspend fun deleteAllEntities() {
        this.deletedEntitiesData.clear()
    }

    override suspend fun searchEntity(entity: Entity1): Entity1? {
        return this.entitiesData[entity.id]
    }

    override suspend fun getAllEntities(): List<Entity1> {
        return ArrayList(this.entitiesData.values)
    }

    override suspend fun insertOrUpdateEntities(entities: List<Entity1>): List<Entity1>? {
        for(entity in entities) {
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            this.entitiesData[entity.id!!] = entity
        }
        return entities
    }

    override suspend fun getEntityById(id: UniqueID): Entity1? {
        return this.entitiesData[id]
    }

    override suspend fun searchEntities(
        searchParameters: FirestoreEntity1SearchParameters
    ): List<Entity1>? {
        TODO("Unavailable in demo project.")
    }

    override suspend fun getUsersEntites1(userID: UserUniqueID): List<Entity1>? {
        val result = ArrayList<Entity1>()
        for(entity in entitiesData.values) {
            if(entity.ownerID!!.equalByValueTo(userID))result.add(entity)
        }
        return result
    }

    override suspend fun uploadEntity1PhotoToFirestore(
        entity: Entity1,
        entitysPhotoUri: Uri,
        entitysPhotoNumber: Int
    ): String? {
        return "Test requires instrumentation."
    }
}