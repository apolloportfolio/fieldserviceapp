package com.aps.catemplateapp.core.business.data.network.implementation

import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.feature02.business.data.network.abs.Entity3NetworkDataSource
import com.aps.catemplateapp.core.business.data.util.FakeFirestoreIDGenerator
import com.aps.catemplateapp.feature02.business.domain.model.entities.Entity3

private const val TAG = "FakeEntity3NetworkDataSourceImpl"
private const val LOG_ME = true

class FakeEntity3NetworkDataSourceImpl constructor(
    private val data: HashMap<UniqueID, Entity3>,
    private val deletedData: HashMap<UniqueID, Entity3>,
    private val fakeFirestoreIDGenerator: FakeFirestoreIDGenerator,
    private val dateUtil: DateUtil
) : Entity3NetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: Entity3): Entity3? {
        val methodName: String = "insertOrUpdateEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            val newEntity = Entity3(
                entity.id,
                entity.created_at,
                dateUtil.getCurrentTimestamp(),

                entity.picture1URI,

                entity.description,

                entity.ownerID,
            )
            this.data[newEntity.id!!] = newEntity
            return newEntity
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return null
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        val methodName: String = "deleteEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if (primaryKey != null) {
                this.data.remove(primaryKey)
            } else {
                ALog.w(TAG, "$methodName(): Failed to delete entity.")
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun insertDeletedEntity(entity: Entity3) {
        val methodName: String = "insertDeletedEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            this.deletedData[entity.id!!] = entity
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun insertDeletedEntities(entities: List<Entity3>) {
        for(entity in entities){
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            this.deletedData[entity.id!!] = entity
        }
    }

    override suspend fun deleteDeletedEntity(entity: Entity3) {
        entity.id?.let { this.deletedData.remove(it) }
    }

    override suspend fun getDeletedEntities(): List<Entity3> {
        return ArrayList(this.deletedData.values)
    }

    override suspend fun deleteAllEntities() {
        this.deletedData.clear()
    }

    override suspend fun searchEntity(entity: Entity3): Entity3? {
        return this.data[entity.id]
    }

    override suspend fun getAllEntities(): List<Entity3> {
        return ArrayList(this.data.values)
    }

    override suspend fun insertOrUpdateEntities(entities: List<Entity3>): List<Entity3>? {
        for(entity in entities) {
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            this.data[entity.id!!] = entity
        }
        return entities
    }

    override suspend fun getEntityById(id: UniqueID): Entity3? {
        return this.data[id]
    }

    override suspend fun getUsersEntities3(userId: UserUniqueID): List<Entity3>? {
        val result = ArrayList<Entity3>()
        for(entity in data.values) {
            if(entity.ownerID!!.equalByValueTo(userId)) result.add(entity)
        }
        return result
    }
}