package com.aps.catemplateapp.core.business.data.cache.implementation

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.daos.PAGINATION_PAGE_SIZE
import javax.inject.Inject

class FakeUserCacheDataSourceImpl
@Inject
constructor(
    private val entitiesData: HashMap<String, ProjectUser>,
    private val dateUtil: DateUtil
): UserCacheDataSource {
    override suspend fun insertOrUpdateEntity(entity: ProjectUser): ProjectUser? {
        this.insertEntity(entity)
        return this.getEntityById(entity.id)
    }

    override suspend fun insertEntity(entity: ProjectUser): Long {
        if(entity.id == null){
            throw NullPointerException("entity.id == null when inserting the entity.")
        }
        if(entity.id!!.firestoreDocumentID == null){
            throw NullPointerException("entity.id.firestoreDocumentID == null when inserting the entity.")
        }
        if(entity.id!!.firestoreDocumentID == FORCE_NEW_EXCEPTION){
            throw Exception("Something went wrong inserting the entity.")
        }
        if(entity.id!!.firestoreDocumentID == FORCE_GENERAL_FAILURE){
            return -1 // fail
        }
        entitiesData[entity.id!!.firestoreDocumentID] = entity
        return 1 // success
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        if(primaryKey == null){
            throw NullPointerException("entity.id == null when deleting the entity.")
        }
        if(primaryKey!!.firestoreDocumentID == null){
            throw NullPointerException("entity.id.firestoreDocumentID == null when deleting the entity.")
        }
        if(primaryKey.firestoreDocumentID.equals(FORCE_DELETE_EXCEPTION)){
            throw Exception("Something went wrong deleting the entity.")
        }
        else if(primaryKey.firestoreDocumentID.equals(FORCE_DELETES_EXCEPTION)){
            throw Exception("Something went wrong deleting the entity.")
        }
        return entitiesData.remove(primaryKey.firestoreDocumentID)?.let {
            1 // return 1 for success
        }?: - 1 // -1 for failure
    }

    override suspend fun deleteAllEntities() {
        entitiesData.clear()
    }

    override suspend fun deleteEntities(entities: List<ProjectUser>): Int {
        var failOrSuccess = 1
        for(entity in entities){
            if(entitiesData.remove(entity.id!!.firestoreDocumentID) == null){
                failOrSuccess = -1 // mark for failure
            }
        }
        return failOrSuccess
    }

    override suspend fun updateEntity(
        id: UserUniqueID?,
        updated_at: String?,
        created_at: String?,
        emailAddress: String,
        password: String,
        profilePhotoImageURI: String?,
        name: String?,
        surname: String?,
        description: String?,
        city: String?,
        emailAddressVerified: Boolean,
        phoneNumberVerified: Boolean,
    ): Int {
        if(id == null){
            throw NullPointerException("entity.id == null when inserting the entity.")
        }
        if(id!!.firestoreDocumentID == null){
            throw NullPointerException("entity.id.firestoreDocumentID == null when inserting the entity.")
        }
        if(id!!.firestoreDocumentID == FORCE_NEW_EXCEPTION){
            throw Exception("Something went wrong inserting the entity.")
        }
        if(id!!.firestoreDocumentID == FORCE_GENERAL_FAILURE){
            return -1 // fail
        }
        val updatedEntity = ProjectUser(
            id,
            updated_at,
            created_at,
            emailAddress,
            password,
            profilePhotoImageURI,
            name,
            surname,
            description,
            city,
            emailAddressVerified,
            phoneNumberVerified,
        )
        return entitiesData[id.firestoreDocumentID]?.let {
            entitiesData[id.firestoreDocumentID] = updatedEntity
            1 // success
        }?: -1 // nothing to update
    }

    override suspend fun updateUsersEmail(
        id: UniqueID?,
        updated_at: String?,
        emailAddress: String?
    ): Int {
        return if(emailAddress != null) {
            entitiesData[id!!.firestoreDocumentID]?.emailAddress = emailAddress
            1
        } else {
            throw NullPointerException("emailAddress == null")
            -1
        }
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<ProjectUser> {
        if(query == FORCE_SEARCH_EXCEPTION){
            throw Exception("Something went searching the cache for entitys.")
        }
        val results: ArrayList<ProjectUser> = ArrayList()
        for(entity in entitiesData.values){
            if(entity.surname?.contains(query) == true){
                results.add(entity)
            }
            else if(entity.surname?.contains(query) == true){
                results.add(entity)
            }
            if(results.size > (page * PAGINATION_PAGE_SIZE)){
                break
            }
        }
        return results
    }

    override suspend fun getAllEntities(): List<ProjectUser> {
        return ArrayList(entitiesData.values)
    }

    override suspend fun getEntityById(id: UniqueID?): ProjectUser? {
        return entitiesData.get(id!!.firestoreDocumentID)
    }

    override suspend fun getNumEntities(): Int {
        return entitiesData.size
    }

    override suspend fun insertEntities(entities: List<ProjectUser>): LongArray {
        val results = LongArray(entities.size)
        for((index, entity) in entities.withIndex()){
            results[index] = 1
            entity.id?.let { entitiesData.put(it.firestoreDocumentID, entity) }
        }
        return results
    }
    
    companion object {
        const val FORCE_DELETE_EXCEPTION = "FORCE_DELETE_EXCEPTION"
        const val FORCE_DELETES_EXCEPTION = "FORCE_DELETES_EXCEPTION"
        const val FORCE_UPDATE_EXCEPTION = "FORCE_UPDATE_EXCEPTION"
        const val FORCE_NEW_EXCEPTION = "FORCE_NEW_EXCEPTION"
        const val FORCE_SEARCH_EXCEPTION = "FORCE_SEARCHES_EXCEPTION"
        const val FORCE_GENERAL_FAILURE = "FORCE_GENERAL_FAILURE"
    }
}