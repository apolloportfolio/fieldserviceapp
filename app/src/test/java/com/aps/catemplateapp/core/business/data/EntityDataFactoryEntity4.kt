package com.aps.catemplateapp.core.business.data

import com.aps.catemplateapp.feature02.business.domain.model.entities.Entity4
import com.google.common.reflect.TypeToken
import com.google.gson.Gson

class EntityDataFactoryEntity4(
    override val testClassLoader: ClassLoader,
    override val fileNameWithTestData: String): EntityDataFactory<Entity4>(testClassLoader, fileNameWithTestData) {


    override fun produceListOfEntities(): List<Entity4>{
        val entities: List<Entity4> = Gson()
            .fromJson(
                getEntitiesFromFile(fileNameWithTestData),
                object: TypeToken<List<Entity4>>() {}.type
            )
        return entities
    }
}