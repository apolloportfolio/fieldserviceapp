package com.aps.catemplateapp.core.business.data

import com.aps.catemplateapp.feature02.business.domain.model.entities.Entity2
import com.google.common.reflect.TypeToken
import com.google.gson.Gson

class EntityDataFactoryEntity2(
    override val testClassLoader: ClassLoader,
    override val fileNameWithTestData: String): EntityDataFactory<Entity2>(testClassLoader, fileNameWithTestData) {


    override fun produceListOfEntities(): List<Entity2>{
        val entities: List<Entity2> = Gson()
            .fromJson(
                getEntitiesFromFile(fileNameWithTestData),
                object: TypeToken<List<Entity2>>() {}.type
            )
        return entities
    }
}