plugins {
    id("com.google.devtools.ksp")
    id("com.android.application")
    id("kotlin-android")
    id("com.google.dagger.hilt.android")
    id("kotlin-kapt")
    id("com.apollographql.apollo3").version("3.7.3")
    id("com.google.gms.google-services")
    id("com.google.firebase.crashlytics")
    id("kotlin-parcelize")
}

apollo {
    service("service") {
        packageNamesFromFilePaths()
    }
}


android {
    namespace = "com.aps.catemplateapp"

    defaultConfig {
        compileSdk = 34
        applicationId = "com.aps.catemplateapp"
        minSdkVersion(26)
        targetSdkVersion(34)
        versionCode = 3
        versionName = "1.03"

        testInstrumentationRunner = "com.aps.catemplateapp.core.framework.MockTestRunner"
        testInstrumentationRunnerArguments(mapOf("clearPackageData" to "true"))
    }

    buildTypes {
        getByName("debug") {
            isMinifyEnabled = false
            isShrinkResources = false // Enable resource shrinking
            isDebuggable = true
        }
        getByName("release") {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            isShrinkResources = true // Enable resource shrinking
            isDebuggable = false
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }

    buildFeatures {
        viewBinding = true
        compose = true
    }

    sourceSets {
        getByName("test").resources.srcDirs("src/test/res")
        getByName("main") {
            // Exclude preview_assets folder from the production build
            // if (!project.hasProperty('android.debug') && !project.hasProperty('android.preview')) {
            //     assets.exclude '**/preview_assets/**'
            // }
        }
    }

    testOptions {
        unitTests {
            isReturnDefaultValues = true // Whether unmocked methods in the test should return their default values
            isIncludeAndroidResources = true // Whether Android resources should be compiled and merged with test resources
        }
    }

    android.sourceSets {
        getByName("androidTest") {
            resources.srcDir("src/androidTest/res")
        }
    }

    packagingOptions {
        exclude("META-INF/DEPENDENCIES")
        exclude("META-INF/LICENSE")
        exclude("META-INF/LICENSE.txt")
        exclude("META-INF/license.txt")
        exclude("META-INF/NOTICE")
        exclude("META-INF/NOTICE.txt")
        exclude("META-INF/notice.txt")
        exclude("META-INF/ASL2.0")
        exclude("META-INF/*.kotlin_module")
        exclude("META-INF/AL2.0")
        exclude("META-INF/LICENSE.md")
        exclude("META-INF/LICENSE-notice.md")
        exclude("META-INF/LGPL2.1")
    }

    composeOptions {
        kotlinCompilerExtensionVersion = "1.5.2"
    }

    tasks.register("revokeLocationPermissions", Exec::class) {
        val revokeFineLocationPermission = listOf(
            "adb",
            "shell",
            "pm",
            "revoke",
            "com.aps.catemplateapp",
            "android.permission.ACCESS_FINE_LOCATION"
        )
        val revokeCoarseLocationPermission = listOf(
            "adb",
            "shell",
            "pm",
            "revoke",
            "com.aps.catemplateapp",
            "android.permission.ACCESS_COARSE_LOCATION"
        )
        commandLine(revokeFineLocationPermission)
        commandLine(revokeCoarseLocationPermission)
        outputs.upToDateWhen { false }
    }

    tasks.register("revokeAllPermissions", Exec::class) {
        val revokeAllPermissions = listOf(
            "E:/AndroidSDK/sdk/platform-tools/adb",
            "shell",
            "pm",
            "reset-permissions"
        )
        commandLine(revokeAllPermissions)
        outputs.upToDateWhen { false }
    }
}

dependencies {
//    implementation(fileTree(org.gradle.internal.impldep.bsh.commands.dir: "libs", include: ["*.jar"]))
    implementation(fileTree(mapOf("dir" to "libs", "include" to "*.jar")))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk7:1.9.0")
    //implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk7:1.4.31")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("androidx.core:core-ktx:1.12.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.6.2")
    implementation("androidx.recyclerview:recyclerview:1.3.1")
    implementation("androidx.fragment:fragment-ktx:1.7.0-alpha05")
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:2.6.2")
    implementation("androidx.lifecycle:lifecycle-viewmodel-compose:2.7.0")


    // Kotlin Coroutines ===========================================================================
    //def coroutines = "1.4.3"
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.7.1")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.7.1")
    // Kotlin Coroutines ===========================================================================


    // Hilt ========================================================================================
    // Hilt still uses kapt instead of ksp so no performance gain from using ksp.
    //def hilt_version = "2.28-alpha"
//    def hilt_version = "2.33-beta"
    val hilt_version = "2.48"
//    def hilt_version = "2.44"
    implementation("com.google.dagger:hilt-android:$hilt_version")
    kapt("com.google.dagger:hilt-compiler:$hilt_version")
    kapt("com.google.dagger:hilt-android-compiler:$hilt_version")
    kapt("androidx.hilt:hilt-compiler:1.2.0")
    implementation("androidx.hilt:hilt-navigation-compose:1.2.0")
    // For instrumentation tests
    androidTestImplementation("com.google.dagger:hilt-android-testing:$hilt_version")
    kaptAndroidTest("com.google.dagger:hilt-compiler:$hilt_version")
    // For local unit tests
    testImplementation("com.google.dagger:hilt-android-testing:$hilt_version")
    kaptTest("com.google.dagger:hilt-compiler:$hilt_version")
    // Hilt ========================================================================================

    // Apollo GraphQL ==============================================================================
    implementation("com.apollographql.apollo3:apollo-runtime:3.7.3")
    // Apollo GraphQL ==============================================================================


    // Retrofit ====================================================================================
    val retrofit = "2.9.0"
    implementation("com.squareup.retrofit2:retrofit:2.9.0")
    implementation("com.squareup.retrofit2:converter-gson:$retrofit")
    implementation("com.squareup.okhttp3:okhttp:4.9.3")
    implementation("com.squareup.okhttp3:logging-interceptor:4.5.0")
    // Retrofit ====================================================================================


    // Room ========================================================================================
//    def room = "2.2.6"
//    def room = "2.4.3"
//    def room = "2.5.2"      // error: Not sure how to convert a Cursor to this method"s return type (java.lang.Object).
//    def room = "2.4.2"
//    def room = "2.6.0-alpha02"      // > A failure occurred while executing org.jetbrains.kotlin.gradle.internal.KaptWithoutKotlincTask$KaptExecutionWorkAction
    val room = "2.5.0-alpha02"
    implementation("androidx.room:room-runtime:2.6.0-rc01")
    implementation("androidx.room:room-ktx:2.6.0-rc01")
//    kapt "androidx.room:room-compiler:$room"
    ksp("androidx.room:room-compiler:$room")
//    kapt "org.jetbrains.kotlinx:kotlinx-metadata-jvm:0.5.0")
    // Room ========================================================================================

    // Material ====================================================================================
    implementation("com.google.android.material:material:1.11.0-alpha03")
    implementation("androidx.compose.material3:material3:1.1.2")
    //implementation("com.google.android.material:$material_components")
    //implementation group: "com.google.android.material", name: "material", version: "1.1.0-alpha05"
    implementation("com.afollestad.material-dialogs:core:3.2.1")
    // Material ====================================================================================


    // Navigation ==================================================================================
    implementation("androidx.navigation:navigation-fragment-ktx:2.7.3")
    implementation("androidx.navigation:navigation-ui-ktx:2.7.3")
    // Navigation ==================================================================================


    // Firebase and Firestore ======================================================================
//    implementation platform("com.google.firebase:firebase-bom:26.5.0")
//    implementation platform("com.google.firebase:firebase-bom:28.4.0")
//    implementation("com.google.firebase:firebase-analytics-ktx"
    // Firebase Authentication
    implementation("com.google.firebase:firebase-auth:22.1.2")
    // Firestore for online database
//    implementation("com.google.firebase:firebase-firestore:24.8.1"
    implementation("com.google.firebase:firebase-firestore-ktx")
    // Coroutines support for firebase operations
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-play-services:1.7.1")
    implementation("com.google.firebase:firebase-auth:22.1.2")
    implementation("com.google.firebase:firebase-database:20.2.2")
    // Import the BoM for the Firebase platform
    // https://firebase.google.com/docs/crashlytics/get-started?platform=android
    implementation(platform("com.google.firebase:firebase-bom:32.3.1"))
//    implementation platform("com.google.firebase:firebase-bom:28.4.0")
    implementation("com.google.firebase:firebase-crashlytics-ktx")
    implementation("com.google.firebase:firebase-analytics-ktx")
    implementation("com.google.firebase:firebase-storage-ktx")
    // Firebase functions
    implementation(platform("com.google.firebase:firebase-bom:30.3.1"))
    implementation("com.google.firebase:firebase-functions-ktx")
    // Geo-Firestore Query with Native Query
    // https://medium.com/android-kotlin/geo-firestore-query-with-native-query-bfb0ac1adf0a
    implementation("com.github.chintan369:Geo-FireStore-Query:1.1.0")
    // FirebaseUI Storage only
    implementation("com.firebaseui:firebase-ui-storage:7.2.0")
    // Firebase and Firestore ======================================================================


    // Google location =============================================================================
    implementation("com.google.android.gms:play-services-base:18.2.0")
    implementation("com.google.android.gms:play-services-maps:18.1.0")
    implementation("com.google.android.gms:play-services-location:21.0.1")
    // Google location =============================================================================


    // Highlighting date ranges in calendar view ===================================================
    // https://github.com/Thanvandh/Date-Range-Highlight
    implementation("com.github.prolificinteractive:material-calendarview:2.0.1")
    implementation("com.jakewharton.threetenabp:threetenabp:1.1.1")
    // Highlighting date ranges in calendar view ===================================================


    // Sending email without intent ================================================================
//    implementation("com.github.1902shubh:SendMail:1.0.0"
    // Sending email without intent ================================================================


    // JSON with Jackson ===========================================================================
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.12.1")
    // JSON with Jackson ===========================================================================


    // GSON ========================================================================================
    //testImplementation "com.google.code.gson:gson:2.8.6"
    //implementation("com.google.code.gson:gson:2.8.6"
    // GSON ========================================================================================


    // Floating window with an image ===============================================================
    // https://github.com/chathuralakmal/AndroidImagePopup
    implementation("com.github.chathuralakmal:AndroidImagePopup:1.2.2")
    // Floating window with an image ===============================================================


    // Tests =======================================================================================
    val testsVersion = "1.4.0"
    implementation("androidx.test:core:$testsVersion")
    implementation( "androidx.test.espresso:espresso-idling-resource:3.5.1@aar")

    debugImplementation("androidx.fragment:fragment-testing:$testsVersion")

    androidTestUtil("androidx.test:orchestrator:1.4.2")

    testImplementation("androidx.test:core:$testsVersion")
    testImplementation("androidx.test:rules:$testsVersion")
    testImplementation("androidx.test.ext:truth:$testsVersion")
    testImplementation("androidx.test:core-ktx:$testsVersion")
    testImplementation("org.junit.jupiter:junit-jupiter")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.7.1")
    testImplementation("org.junit.jupiter:junit-jupiter-params:5.7.1")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.7.1")
    testImplementation("junit:junit:4.13.2")
    testImplementation("org.hamcrest:hamcrest-all:1.3")
    testImplementation("androidx.arch.core:core-testing:2.2.0")
    testImplementation("org.robolectric:robolectric:4.5.1")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.7.1")
    testImplementation("com.google.truth:truth:1.1.3")
    testImplementation("org.mockito:mockito-core:2.28.2")
    testImplementation("io.mockk:mockk:1.10.6")
    testImplementation("junit:junit:4.13.2")

    androidTestImplementation("androidx.test:core:$testsVersion")
    androidTestImplementation("androidx.test:rules:$testsVersion")
    androidTestImplementation("androidx.test.ext:truth:$testsVersion")
    androidTestImplementation("androidx.test:core-ktx:$testsVersion")
    androidTestImplementation("androidx.test:runner:1.5.2")
    androidTestImplementation("junit:junit:4.13.2")
    androidTestImplementation("com.linkedin.dexmaker:dexmaker-mockito:2.28.1")
    androidTestImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.7.1")
    androidTestImplementation("androidx.arch.core:core-testing:2.2.0")
    androidTestImplementation("com.google.truth:truth:1.1.3")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1") // Espresso version 3.4.0 causes duplicate class error
    androidTestImplementation("androidx.test.espresso:espresso-contrib:3.5.1") {
        // Without this we get java.lang.NoSuchMethodError from Firestore:
        // https://stackoverflow.com/questions/66154727/java-lang-nosuchmethoderror-no-static-method-registerdefaultinstance-with-fireb
        exclude(module = "protobuf-lite")
    }
    androidTestImplementation("org.mockito:mockito-core:2.28.2")
//    androidTestImplementation(group: "org.jetbrains.kotlin", name: "kotlin-test", version: "1.8.22")
    androidTestImplementation("org.jetbrains.kotlin:kotlin-test:1.8.22")
    androidTestImplementation("com.android.support.test.uiautomator:uiautomator-v18:2.1.3")
    androidTestImplementation("androidx.navigation:navigation-testing:2.7.3")
    // https://mvnrepository.com/artifact/com.linkedin.dexmaker/dexmaker-mockito
//    androidTestImplementation(group: "com.linkedin.dexmaker", name: "dexmaker-mockito", version: "2.28.1")
    androidTestImplementation("com.linkedin.dexmaker:dexmaker-mockito:2.28.1")
    androidTestImplementation("org.junit.jupiter:junit-jupiter-api:5.7.1")
    androidTestImplementation("org.junit.jupiter:junit-jupiter-params:5.7.1")
    androidTestImplementation("org.junit.jupiter:junit-jupiter-engine:5.7.1")
    // https://mvnrepository.com/artifact/androidx.test.uiautomator/uiautomator
//    androidTestImplementation(group: "androidx.test.uiautomator", name: "uiautomator", version: "2.2.0")
    androidTestImplementation("androidx.test.uiautomator:uiautomator:2.2.0")
    // Testing Intents https://developer.android.com/training/testing/espresso/intents
    androidTestImplementation("androidx.test.espresso:espresso-intents:3.5.1")
    // Tests =======================================================================================


    // QR code scanner: ============================================================================
    // https://github.com/yuriy-budiyev/code-scanner
    implementation("com.github.yuriy-budiyev:code-scanner:2.3.2")
    // QR code scanner =============================================================================


    // QR code generator ===========================================================================
    // https://stackoverflow.com/questions/64443791/android-qr-generator-api
    implementation("com.google.zxing:core:3.4.0")
    // QR code generator ===========================================================================


    // Google Pay ==================================================================================
    implementation("com.google.android.gms:play-services-wallet:19.2.1")
    implementation("com.google.android.gms:play-services-pay:16.1.0")
    // Google Pay ==================================================================================


    // Exchange Rates Downloader ===================================================================
    val fuelVersion = "2.3.1"
    implementation("com.github.kittinunf.fuel:fuel:$fuelVersion")
    implementation("com.github.kittinunf.fuel:fuel-android:$fuelVersion")
    implementation("com.github.kittinunf.fuel:fuel-moshi:$fuelVersion")
    implementation("com.github.kittinunf.fuel:fuel-coroutines:$fuelVersion")
    val moshiVersion = "1.13.0"
    implementation("com.squareup.moshi:moshi-kotlin:$moshiVersion")
    ksp("com.squareup.moshi:moshi-kotlin-codegen:$moshiVersion")
    // Exchange Rates Downloader ===================================================================


    // Tool for debugging TransactionTooLargeException =============================================
//    implementation("com.gu.android:toolargetool:0.3.0"
    // Tool for debugging TransactionTooLargeException =============================================


    // Jetpack Compose =============================================================================
    val compose_version = "1.5.1"
    implementation("androidx.compose.ui:ui:$compose_version")
    implementation("androidx.compose.material:material:$compose_version")
    implementation("androidx.compose.foundation:foundation:$compose_version")
    implementation("androidx.compose.runtime:runtime:$compose_version")
    implementation("androidx.compose.ui:ui-tooling:$compose_version")
    implementation("androidx.compose.ui:ui-graphics:$compose_version")
    implementation("androidx.compose.ui:ui:$compose_version")
    implementation("androidx.compose.runtime:runtime:$compose_version")
    implementation("androidx.compose.ui:ui-tooling:$compose_version")
    implementation("androidx.compose.ui:ui-graphics:$compose_version")
    implementation("androidx.navigation:navigation-compose:2.7.3")
    implementation("androidx.constraintlayout:constraintlayout-compose:1.0.1")
    androidTestImplementation("androidx.compose.ui:ui-test-junit4:$compose_version")
    testImplementation("androidx.compose.ui:ui-test-junit4:$compose_version")
    implementation("androidx.compose.material:material-icons-extended:1.5.1")
//    implementation("androidx.compose.ui:ui-layout:$compose_version")
//    implementation("androidx.activity:activity-compose:1.4.0")
    implementation("androidx.compose.runtime:runtime-livedata:$compose_version")
    // Jetpack Compose custom RatingBar
    implementation("com.github.a914-gowtham:compose-ratingbar:1.3.4")
    // Jetpack Compose =============================================================================


    // Glide =======================================================================================
    implementation("com.github.bumptech.glide:glide:4.14.2")
    ksp("com.github.bumptech.glide:compiler:4.12.0")
    // Glide =======================================================================================


    // Glide for Jetpack Compose ===================================================================
    implementation("com.github.bumptech.glide:compose:1.0.0-alpha.1")
    // Glide for Jetpack Compose ===================================================================


    // Coil for Jetpack Compose ====================================================================
    implementation("io.coil-kt:coil-compose:1.3.2")
    // Coil for Jetpack Compose ====================================================================

}